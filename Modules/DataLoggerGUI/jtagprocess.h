#ifndef JTAGPROCESS_H
#define JTAGPROCESS_H

#include "abstractprocess.h"

class JTAGProcess : public AbstractProcess{
    Q_OBJECT
public:
    JTAGProcess(QString firmware, QWidget *parent=nullptr);
    void start();

private:
    QString device;
    QString firmwarefile;
    void win_start_jtag();
    void lin_start_jtag();
    void mac_start_jtag();
protected:
    void customReadOutput(const QString &line);
};

#endif // JTAGPROCESS_H
