#include "writeconfigprocess.h"

WriteConfigProcess::WriteConfigProcess(QString device, QString cfg, QWidget *parent)
    : AbstractProcess("WriteConfig", parent), device(device), cfgfile(cfg)
{
}

void WriteConfigProcess::start(){
    if(device == "Docking Station"){
        docking_writeconfig();
    }
    else{
    #if defined(Q_OS_WIN)
        win_start_writeconfig();
    #elif defined(Q_OS_MAC)
        mac_start_writeconfig();
    #elif defined(Q_OS_LINUX)
        lin_start_writeconfig();
    #else
    #error "OS not supported!"
    #endif
    }
}

void WriteConfigProcess::win_start_writeconfig(){

    QRegularExpression rex("(.*)([0-9]{1,3})"); //Takes the last digits of device string (PhysicalDrive1) -> 1
    QRegularExpressionMatch m = rex.match(device);
    if (m.hasMatch() && m.lastCapturedIndex() > 1) {
        QStringList args;
        args << m.captured(2);
        args << cfgfile;

        process->start(".\\windows_sd_util\\writeConfig.exe", args);
    }

}

void WriteConfigProcess::lin_start_writeconfig(){
    process->start("./linux_sd_util/writeConfig", {device, cfgfile});
}

void WriteConfigProcess::mac_start_writeconfig()
{
    process->start("./macos_sd_util/writeConfig", {device, cfgfile});
}

void WriteConfigProcess::customReadOutput(const QString &line){
    if(line.contains("continue?", Qt::CaseInsensitive)){
//        QMessageBox::StandardButton reply;
//        reply = QMessageBox::question(nullptr, "Overwriting SD config",
//                                      "Overwriting card configuration data with data from file " +
//                                      cfgfile + "\nContinue?"
//                                      ,QMessageBox::Yes|QMessageBox::No);
//        if(reply == QMessageBox::Yes){
////            console->raise();
            console->append("> yes\n");
            process->write("yes\n");
//        }
//        else{
////            console->raise();
//            console->insertPlainText("> No\n");
//            process->write("no\n");
//        }
    }
}

void WriteConfigProcess::docking_writeconfig(){
    process->start(DOCKINGPATH, {"-w", cfgfile});
}
