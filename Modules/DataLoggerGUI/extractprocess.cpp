#include "extractprocess.h"
#include <QtDebug>
ExtractProcess::ExtractProcess(QString device, QString outputpath, QWidget *parent)
    : AbstractProcess("Extract", parent), device(device), outputpath(outputpath)
{
}

void ExtractProcess::start(){
    if(device == "Docking Station"){
        docking_extract();
    }
    else{
    #if defined(Q_OS_WIN)
        win_start_extract();
    #elif defined(Q_OS_MAC)
        mac_start_extract();
    #elif defined(Q_OS_LINUX)
        lin_start_extract();
    #else
    #error "OS not supported!"
    #endif
    }
}

void ExtractProcess::customReadOutput(const QString &line){
    if(line.contains("continue?", Qt::CaseInsensitive)){
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Continue extract?",
                                      "WARNING: This does not look like a disk used for recordings.\n"
                                      "Are you sure this is the correct disk?"
                                      ,QMessageBox::Yes|QMessageBox::No);
        if(reply == QMessageBox::Yes){
//            console->raise();
            console->insertPlainText("> Yes\n");
            process->write("yes\n");
        }
        else{
//            console->raise();
            console->insertPlainText("> No\n");
            process->write("no\n");
        }
    }
}


void ExtractProcess::win_start_extract(){
    QRegularExpression rex("(.*)([0-9]{1,3})"); //Takes the last digits of device string (PhysicalDrive1) -> 1
    QRegularExpressionMatch m = rex.match(device);
    if (m.hasMatch() && m.lastCapturedIndex() > 1) {
        QStringList args;
        args << m.captured(2) << outputpath;

        emit newOutputLine("windows_sd_util/extract.exe " + args.join(" "));
        process->start(".\\windows_sd_util\\extract.exe", args);
    }

}


void ExtractProcess::lin_start_extract(){
    QStringList args;
    args << device << outputpath;

    emit newOutputLine("./linux_sd_util/extract " +args.join(" "));
    process->start("./linux_sd_util/extract", {device, outputpath});
}

void ExtractProcess::mac_start_extract()
{
    QStringList args;
    args << device << outputpath;

    emit newOutputLine("./macos_sd_util/extract " +args.join(" "));
    process->start("./macos_sd_util/extract", {device, outputpath});
}

void ExtractProcess::docking_extract(){
    emit newOutputLine(QString(DOCKINGPATH) + " -e " + outputpath);
    process->start(DOCKINGPATH, {"-e", outputpath});
}

