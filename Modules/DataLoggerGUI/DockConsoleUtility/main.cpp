#include <QCoreApplication>
#include "commandengine.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    CommandEngine engine;
    int c;
    int detect=0, readconfig=0, extract=0, cardenable=0, writeconfig=0, pcheck=0, trodesparse=0,
        writeserial=0, readserial=0, startrf=0, stoprf=0, setrfchan = 0, lastpl = 0, getrfchan=0,
        erasesd=0, getdocksettings=0, setdocksettings=0; //setsamplerate=0;
    uint16_t model=0, serial=0;
    uint8_t rfchan = 0;
    char *extractfile=NULL;
    char *configfile=NULL;
    while (1){
        static struct option long_options[] = {
            //command line options
            //must also add the letter to the string in the function getopt_long() below
            {"startrf",       no_argument,        0, 'a'},
            {"stoprf",        no_argument,        0, 'b'},
            {"cardenable",    no_argument,        0, 'c'},
            {"detect",        no_argument,        0, 'd'},
            {"extract",       required_argument,  0, 'e'},
            {"setrfchan",     required_argument,  0, 'f'},
            {"getserial",     no_argument,        0, 'g'},
            {"help",          no_argument,        0, 'h'},
            {"getrfchan",     no_argument,        0, 'i'},
            {"erasesd",       no_argument,        0, 'j'},
            // {"setsamplerate", required_argument,  0, 'k'},
            {"lastpl",        no_argument,        0, 'l'},
            {"getdocksettings",no_argument,       0, 'm'},
            {"setdocksettings",no_argument,       0, 'n'},
            {"packetcheck",   no_argument,        0, 'p'},
            {"readconfig",    no_argument,        0, 'r'},
            {"setserial",     required_argument,  0, 's'},
            {"trodesparse",   no_argument,        0, 't'},
            {"version",       no_argument,        0, 'v'},
            {"writeconfig",   required_argument,  0, 'w'},
            {0, 0, 0, 0}
        };
        /* getopt_long stores the option index here. */
        int option_index = 0;

        //just the letter if no_argument, letter + ':' for required arguments
        c = getopt_long (argc, argv, "abhdrpctglijmnvk:s:e:w:f:", long_options, &option_index);

        /* Detect the end of the options. */
        if (c == -1)
            break;

        switch (c){
            case 'h':
                engine.printHelpmenu(argv[0]);
                exit(0);
                break;
            case 'a':
                startrf=1;
                break;
            case 'b':
                stoprf=1;
                break;
            case 'l':
                lastpl = 1;
                break;
            case 'f':
                setrfchan=1;
                rfchan = strtoul(optarg,0,0);
                break;
            case 'i':
                getrfchan=1;
                break;
            case 'j':
                erasesd=1;
                break;
            // case 'k':
            //     setsamplerate=1;
            //     samplerate = strtoul(optarg, 0, 0);
            //     break;
            case 'm':
                getdocksettings=1;
                break;
            case 'n':
                setdocksettings=1;
                break;
            case 'd':
                detect = 1;
                break;
            case 'r':
                readconfig = 1;
                break;
            case 'p':
                pcheck = 1;
                break;
            case 'e':
                extract = 1;
                extractfile = (char*)malloc(strlen(optarg)+1);
                strcpy(extractfile, optarg);
                break;
            case 'c':
                cardenable = 1;
                break;
            case 'w':
                writeconfig = 1;
                configfile = (char*)malloc(strlen(optarg)+1);
                strcpy(configfile, optarg);
                break;
            case 't':
                trodesparse = 1;
                break;
            case 'g':
                readserial = 1;
                // dockReadSerial();
                // return 0;
                break;
            case 's':
                model = strtoul(optarg, 0, 0);
                if (optind >= 0 && optind < argc && *argv[optind] != '-'){
                    serial = strtoul(argv[optind], 0, 0);
                    optind++;
                } else {
                    fprintf(stderr, "\n-s option require TWO arguments <model> <serial>\n\n");
                    engine.printHelpmenu(argv[0]);
                    return 2;
                }
                writeserial = 1;
                break;
            case 'v':{
                uint8_t major, minor, patch;
                int ret = engine.getDockFirmwareVersion(&major, &minor, &patch);
                printf("Firmware version: %u.%u.%u\n", major, minor, patch);
                return ret;
            }
            default:
                printf("Problem with provided arguments. \n");
                exit(2);
        }
    }

    if(detect || readconfig || pcheck || extract || trodesparse){
        CommandEngine::DockingStation_t d;
        int ret = engine.readDockingstation(&d, detect, readconfig, pcheck, extract, extractfile, trodesparse);


        if(detect){
            if(ret==0){
                printf("Docking station detected.\n");
                if(d.hs_detected)
                    printf("Device: Headstage\n");
                else if(d.sd_detected)
                    printf("Device: SD Card\n");
                else
                    printf("Device: None mounted\n");
                printf("Size: %d MB\n\n", d.disk_size_MB);
            }
            else{
                printf("Docking station not detected. Please check your USB connection\n");
                return ret;
            }
        }

        if (ret != 0) {
            //something went wrong
            //TODO: Error output here
            return ret;

        }

        if(!d.hs_detected && !d.sd_detected){
            printf("No device detected on the docking station! \nExiting\n");
            exit(0);
        }
        else if(trodesparse){
            engine.printconfigfortrodes(d);
            return 0;
        }

        if(readconfig){
            if(d.sd_detected || d.hs_detected)
                engine.printconfig(d);
            else
                printf("No device detected! \n");
        }

        if(pcheck){
            if(d.packetsvalid){
                printf( "Recording information: \n"
                    "Packet size:           %d bytes\n"
                    "Aux size:              %d bytes\n"
                    //!!!!!!!Don't change %llu for now, windows needs it to be llu for some reason ...
#if defined(_WIN32) || defined(__APPLE_CC__)
                    "Max packets:           %llu\n"
                    "Recorded packets:      %llu\n"
                    "Dropped packets:       %llu\n"
#else
                    "Max packets:           %lu\n"
                    "Recorded packets:      %lu\n"
                    "Dropped packets:       %lu\n"
#endif
                    "%s%s\n",
                    d.packet_size_bytes, d.aux_size_bytes, d.max_packets, d.recorded_packets, d.dropped_packets,
                    (strlen(d.recording_datetime) ? "Recording datetime:    " : ""),
                    (strlen(d.recording_datetime) ? d.recording_datetime : "")
                );
            }
            else{
                printf( "No recording or packet information available!\n");
                exit(1);
            }
        }
    }
    else if(cardenable){
        CommandEngine::DockingStation_t d;
        int ret = engine.readDockingstation(&d, 1, 0, 0, 0, 0, 0);
        ret = engine.dockCardEnable();
        if(ret == -2){
            printf("Error running card-enable: No device mounted on the docking station!\n");
            exit(2);
        }
// #ifndef _WIN32
// #ifndef __APPLE_CC__
//         if(d.sd_detected){
//             Sleep(1000);
//             ret = dockCardEnable();
//         }
// #endif
// #endif
        if(ret){
            printf("Error running card-enable: Communication error with USB. %d \n", ret);
            exit(1);
        }
        else{
            printf("Card-enable successful. You may now use this device for recordings.\n");
        }

        return 0;
    }
    else if(writeconfig){
        int ret = engine.dockWriteConfig(configfile);
        if(ret == -2){
            printf("Error running write-config: No device mounted on the docking station!\n");
            exit(2);
        }
        else if(ret){
            printf("Error running write-config: Communication error with USB. \n");
            exit(1);
        }
        else{
            printf("Write-config successful.\n");
        }

        return 0;
    }
    else if(writeserial){
        int ret = engine.dockWriteSerial(model, serial);
        if(ret==0) printf("Model %d and serial %d written\n", model, serial);
        else printf("Error writing model/serial\n");
        return ret;
    }
    else if(readserial){
        uint16_t model, serial;
        int ret = engine.dockReadSerial(&model, &serial);
        if(ret == 0){
            printf("Model:  %d\n", model);
            printf("Serial: %d\n", serial);
        }
        else{
            printf("Error reading model/serial\n");
        }
        return ret;
    }
    else if(startrf || stoprf){
        return engine.dockRFStartStop(startrf, stoprf);
    }
    else if(lastpl){
        return engine.dockLastPL();
    }
    else if(setrfchan){
        return engine.dockSetRFChan(rfchan);
    }
    else if(getrfchan){
        return engine.dockGetRFChan();
    }
    else if(erasesd){
        return engine.dockEraseSD();
    }
    // else if(setsamplerate){
    //   return dockSetSampleRate(samplerate);
    // }
    else if(getdocksettings){
        uint8_t major, minor, patch;
        engine.getDockFirmwareVersion(&major, &minor, &patch);
        if(major==0){
            printf("Not supported");
            return 0;
        }
        return engine.getDockSettings();
    }
    else if(setdocksettings){
        uint8_t major, minor, patch;
        engine.getDockFirmwareVersion(&major, &minor, &patch);
        if(major==0){
            printf("Not supported");
            return 0;
        }
        return engine.setDockSettings(argc-2, argv+2);
    }
    else{
        engine.printHelpmenu(argv[0]);
        return 0;
    }

    return 0;

}
