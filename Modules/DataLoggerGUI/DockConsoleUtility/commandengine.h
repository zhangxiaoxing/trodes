#ifndef COMMANDENGINE_H
#define COMMANDENGINE_H
#include <stdio.h>
#include <stdbool.h>
#include <inttypes.h>
//#include <unistd.h>
#include <time.h>



#include <errno.h>

#ifdef _WIN32
    #include <Windows.h>
    #include "ftd2xx.h"
   // #include "XGetopt.h"
    #include <getopt.h>

#else
    #include "WinTypes.h"
    #include <sys/time.h>
    #include <getopt.h>
    #include <unistd.h>
    #include <string.h>
    #include <stdlib.h>
    #define Sleep(ms) usleep(ms*1000)
    #ifdef __APPLE_CC__
      #include "ftd2xx.h"
    #else
      //#include "Linux/ftdi_wrapper.h"
      #include "WinTypes.h"
      #include "ftd2xx.h"
    #endif
#endif

/*#ifdef _WIN32
    #include <windows.h>
#else
    #include "WinTypes.h"
    #include <unistd.h>
    #include <string.h>
    #include <stdlib.h>
#endif
#include "ftd2xx.h"*/


/*#define VENDOR 0x0403
#define DEVICE 0x6010
extern FT_STATUS res;
extern FT_HANDLE ftdi;*/

// dock stations commands
#define HS_GET_STATUS_CMD       0x30
#define HS_GET_SIZE_CMD         0x31
#define HS_RESET_CMD            0x32
#define HS_READ_SECTORS_CMD     0x33
#define HS_WRITE_SECTORS_CMD    0x34
#define DOCK_SETSERIAL_CMD      0X35
#define DOCK_GETSERIAL_CMD      0x36
#define SD_GET_STATUS_CMD       0x37
#define SD_GET_SIZE_CMD         0x38
#define SD_RESET_CMD            0x39
#define SD_READ_SECTORS_CMD     0x3a
#define SD_WRITE_SECTORS_CMD    0x3b
#define DOCK_LATEST_PAYLOAD     0x3c
#define DOCK_SET_RF_CHANNEL     0x3d
#define DOCK_GET_RF_CHANNEL     0x3e
#define HS_CARD_ERASE_CMD       0x3f
#define SD_CARD_ERASE_CMD       0x40
#define GET_ADC                 0x41
#define DOCK_USERDATA_LEN       0x42
#define DOCK_GET_SETTINGS       0x43
#define DOCK_SET_SETTINGS       0x44
#define DOCK_SET_SAMPLERATE     0x45
#define DOCK_HAS_ACK            0x46
#define DOCK_FIRMWARE_VERSION   0x47

#define DOCK_PROGRAMMINGMODE    0x55

#define DOCK_START_CMD          0x61
#define DOCK_STOP_CMD           0x62
#define DOCK_START_ECU_CMD      0x64
#define HS_FLASH_SETTINGS       0x67
#define HS_GET_SETTINGS         0x81
#define HS_SET_SETTINGS         0x82
#define HS_GET_BSIZE            0x98
#define HS_SET_SERIAL           0xf0

// dock station STATUS register bit definitions
#define STATUS_TX_FIFO_EMPTY    0x0001
#define STATUS_TX_FIFO_FULL     0x0002
#define STATUS_RX_FIFO_EMPTY    0x0004
#define STATUS_RX_FIFO_FULL     0x0008
#define STATUS_SD_DETECT        0x0010
#define STATUS_MOUNTED          0x0040
//#define STATUS_MOUNTED          0x0010
#define STATUS_ACTIVE           0x0080
#define STATUS_READING          0x0100
#define STATUS_WRITING          0x0200
#define STATUS_ERASING          0x0400
#define STATUS_READ_PENDING     0x0800
#define STATUS_WRITE_PENDING    0x1000
#define STATUS_ERASE_PENDING    0x2000
#define STATUS_ERROR            0x4000
#define STATUS_READ_CRC_ERROR   0x8000




//For WriteConfig
#define NUM_CHANNELS_PER_MODULE 32
#define NUM_MODULES 8


class CommandEngine
{
public:
    //CommandEngine(int argc, char *argv[]);
    CommandEngine();
    //DockingStation_t
    typedef struct{
        int sd_detected;
        int hs_detected;
        int dock_USB_communication_ok;

        int hs_serial_number;
        int hs_model_number;
        int hs_major_version;
        int hs_minor_version;
        int hs_patch_version;

        int channels;
        int waitforstart_override;
        int cardenablecheck_override;
        int sample_size_bits;
        int sample_rate_khz;
        int mag_enabled;
        int acc_enabled;
        int gyr_enabled;
        int smartref_enabled;
        int rf_channel;
        int autofs_channels;
        int autofs_timeout_samples;
        int autofs_threshold;
        int sd_card_enabled_for_record;
        float autofs_threshold_mV;

        int blocks;
        int disk_size_MB;
        int packet_size_bytes;
        int trodes_packet_size;
        int aux_size_bytes;
        uint64_t max_packets;
        uint64_t recorded_packets;
        uint64_t dropped_packets;

        char recording_datetime[80];

        uint16_t batterysize_mAh;
        uint64_t chipid;
        uint8_t sensor_version;
        uint32_t serial_sensor;
        uint16_t intan_channels;

        //For internal use
        int start;
        int configvalid;
        int packetsvalid;
    } DockingStation_t;



    int readDockingstation(DockingStation_t *d, int detect, int readconfig, int pcheck, int extract, const char* filename, int trodesparse);
    int dockCardEnable();
    int dockWriteConfig(const char *cfgfile);
    int dockWriteSerial(uint16_t model, uint16_t serial);
    int dockReadSerial(uint16_t* model, uint16_t* serial);
    int dockRFStartStop(int start, int stop);
    int dockLastPL();
    int dockSetRFChan(uint8_t chan);
    int dockGetRFChan();
    int dockEraseSD();
    int getDockSettings();
    int setDockSettings(int argc, char **argv);
    int getDockFirmwareVersion(uint8_t *major, uint8_t *minor, uint8_t *patch);
    void printHelpmenu(const char* p);
    void printconfig(DockingStation_t d);
    void printconfigfortrodes(DockingStation_t d);

private:

    char const *desc = "DockingStation A";
    FT_STATUS ftStatus;
    FT_HANDLE ftHandle;
    DWORD BytesWritten, BytesReceived;
    uint8_t TxBuffer[1024], RxBuffer[131072];

    int strendswith(const char *str, const char *suffix);
    int timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y);
    uint8_t packet_read (
            uint8_t *buff,			/* Pointer to the data buffer to store read data */
            DWORD start,    /* First data sector */
            ULONGLONG packet,
            DWORD psize,
            WORD count,
            uint8_t readcmd);

};

#endif // COMMANDENGINE_H
