QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

include(../../module_defaults.pri)

#QT       += core gui xml network charts

TARGET = dockconsoleutility


SOURCES += \
        commandengine.cpp \
        main.cpp

HEADERS += \
    commandengine.h


win32{
    INCLUDEPATH += $$PWD/../../../Trodes/Libraries/Windows
    HEADERS += ../../../Trodes/Libraries/Windows/ftd2xx.h
   # LIBS += -L$${PWD}/../../../Trodes/Libraries/Windows -lftd2xx
    LIBS   += $$quote($$PWD/../../../Trodes/Libraries/Windows/FTDI/amd64/ftd2xx64.lib)


    INCLUDEPATH += $$PWD/Windows
    SOURCES += \
        Windows/getopt.c
    HEADERS += \
        Windows/getopt.h

}

unix:!macx{
    INCLUDEPATH += ../../../Trodes/Libraries/Linux
    LIBS += -L../../../Trodes/Libraries/Linux
    HEADERS    += ../../../Trodes/Libraries/Linux/WinTypes.h \
                  ../../../Trodes/Libraries/Linux/ftd2xx.h

   # HEADERS     += Linux/ftdi_wrapper.h
    LIBS       += -lftd2xx

}
macx{

    INCLUDEPATH += ../../../Trodes/Libraries/Mac
    LIBS += -L../../../Trodes/Libraries/Mac
    HEADERS    += ../../../Trodes/Libraries/Mac/WinTypes.h \
                  ../../../Trodes/Libraries/Mac/ftd2xx.h
    #LIBS       += -lftd2xx
    LIBS       += $$quote($$PWD/../../../Trodes/Libraries/Mac/libftd2xx.a) -framework Cocoa


}
