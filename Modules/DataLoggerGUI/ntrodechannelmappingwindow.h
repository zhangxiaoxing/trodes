#ifndef NTRODECHANNELMAPPINGWINDOW_H
#define NTRODECHANNELMAPPINGWINDOW_H

#include "workspaceEditor.h"
#include <QtWidgets>

class NTrodeChannelMappingWindow : public QDialog
{
public:
    NTrodeChannelMappingWindow(int numchans, QWidget *parent = nullptr);
    QList<int> getChannelMappings();

signals:
    void editingFinished();
private:
    void okpressed();
    NTrodeChannelMappingTable *table;
};

#endif // NTRODECHANNELMAPPINGWINDOW_H
