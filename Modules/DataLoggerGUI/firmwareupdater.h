#ifndef FIRMWAREUPDATER_H
#define FIRMWAREUPDATER_H

#include <QtWidgets>
#include "hardwaresettings.h"
#include "disklistwidget.h"

class FirmwareUpdater : public QWidget
{
    Q_OBJECT
public:
    explicit FirmwareUpdater(HeadstageSettings settings, QWidget *parent = nullptr);

signals:
    void startFirmwareUpload(QString device, QString file);

public slots:
    void deviceSelected(LoggerRecording device);
    void browseFirmware();
    void uploadFirmware(QString file);
    void firmwareUploadFinished();
private:
    QLineEdit *browseText;
    QComboBox *deviceDropdown;
    QLabel *firmwareversion;
    QString loggerfirmwareText;
    QString dockfirmwareText;
    QString firmwaretype;
    QPushButton *updateFirmwareBtn;
    void dropdownItemChanged(const QString &text);
};

#endif // FIRMWAREUPDATER_H
