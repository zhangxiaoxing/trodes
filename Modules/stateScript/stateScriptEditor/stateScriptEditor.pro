
TARGET = stateScriptEditor
include(../../module_defaults.pri)
QT       += core gui widgets

TEMPLATE = app

#########################################
#Source files and libraries
#########################################
HEADERS  += mainWindow.h\
            ../scripteditor.h \
            ../highlighter.h \
            customApplication.h

SOURCES += main.cpp\
            mainWindow.cpp\
            ../scripteditor.cpp \
            ../highlighter.cpp \
            customApplication.cpp

win32 {
    RC_ICONS += trodesWindowsIcon_sseditor.ico
}

macx {
    ICON        = trodesMacIcon_sseditor.icns
    QMAKE_INFO_PLIST += Info.plist
}

