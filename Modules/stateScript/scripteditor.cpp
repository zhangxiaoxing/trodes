/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "scripteditor.h"
#include <QScrollBar>
#include <QFile>

scriptEditor::scriptEditor(QWidget *parent)
    : QTextEdit(parent) {

    unsavedData = false;
    fileName = "";

    //document()->setMaximumBlockCount(100);
    QPalette p = palette();
    p.setColor(QPalette::Window, Qt::gray);
    //p.setColor(QPalette::Text, Qt::black);
    setPalette(p);

    setTabStopDistance(20);
    scriptHighlighter = new Highlighter(document());
    setEnabled(false);

}

scriptEditor::~scriptEditor() {
    delete scriptHighlighter;
}

void scriptEditor::asHtml(QString &html) {
    scriptHighlighter->asHtml(this,html);
}

void scriptEditor::trackIfSaved() {
    connect(this,SIGNAL(textChanged()),this,SLOT(setUnsavedFile()));
}

void scriptEditor::setFileName(QString name) {
    fileName = name;
    emit newFileName(fileName);
}

QString scriptEditor::getFileName() {
    return fileName;
}

void scriptEditor::setUnsavedFile() {
    unsavedData = true;
    emit changesNotSaved();
}

void scriptEditor::setSavedFile() {
    unsavedData = false;
    emit saved();
}

bool scriptEditor::isUnSaved() {
    return unsavedData;
}

void scriptEditor::setData(const QByteArray &data) {

    QPalette p = palette();
    p.setColor(QPalette::Window, Qt::white);
    //p.setColor(QPalette::Text, Qt::black);
    setPalette(p);
    setPlainText(data);
    setEnabled(true);


}

void scriptEditor::removeData() {
    QPalette p = palette();
    p.setColor(QPalette::Window, Qt::gray);
    //p.setColor(QPalette::Text, Qt::black);
    setPalette(p);
    setPlainText(QByteArray());
    setEnabled(false);
}

void scriptEditor::keyPressEvent(QKeyEvent *e) {


    QTextEdit::keyPressEvent(e);


}


