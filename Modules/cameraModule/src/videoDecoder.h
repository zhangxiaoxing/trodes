/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QVIDEODECODER_H
#define QVIDEODECODER_H


#include <QIODevice>
//#include <QIODevice>
#include <QtGui>
//#include <QtWidgets>
#include <QFile>
#include <QImage>

#define H264_INBUF_SIZE 16384                                                           // number of bytes we read per chunk

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
//#include <tinylib.h>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavutil/avutil.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libavutil/mathematics.h>
#include <libavutil/dict.h>
}


class H264_Decoder {

 public:
  H264_Decoder();

  ~H264_Decoder();                                    //cleans up the allocated objects and closes the codec context
  bool load(QString fileName, float fps = 0.0f);      // load a video file which is encoded with x264
  bool readFrame();                                   // read a frame from file and decode
  void createQImageFromFrame(QImage*& img);           //Creates a QImage from the current frame
  bool seekFrame(int64_t frameNum);
  int getFrameRate();

 private:

  bool decodeCurrentFrame(AVPacket* packet);
  QImage *lastFrame;
  bool fileIsOpen;
  int videoStream;
  int fr;


 public:
  AVCodec* codec;                        // the AVCodec* which represents the H264 decoder
  AVCodecContext* codec_context;         // the context; keeps generic state
  AVCodecParserContext* parser;          // parser that is used to decode the h264 bitstream
  AVFrame* picture;                      // will contain a decoded picture
  AVFormatContext *format_context;
  int frame;                             // the number of decoded frames
  std::vector<uint8_t> buffer;           // buffer we use to keep track of read/unused bitstream data
};



#endif // __QVIDEODECODER_H
