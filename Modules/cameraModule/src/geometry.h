#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <QDataStream>
#include <QLineF>

struct LineNodeIndex {
    LineNodeIndex():startNodeIndex(0), endNodeIndex(0),zone(0),line(){}
    int startNodeIndex;
    int endNodeIndex;
    int zone;
    QLineF line;
};
QDataStream & operator<< (QDataStream& stream, const LineNodeIndex& line);
QDataStream & operator>> (QDataStream& stream, LineNodeIndex& line);

#endif // GEOMETRY_H
