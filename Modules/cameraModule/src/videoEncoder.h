/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VideoEncoder_H
#define VideoEncoder_H


#include <QIODevice>
#include <QtGui>
#include <QtWidgets>
#include <QFile>
#include <QImage>
#include "abstractCamera.h"

extern "C" {
#include <x264.h>
}

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavutil/avutil.h>
#include <libswscale/swscale.h>

#include "libavutil/mathematics.h"
#include "libavformat/avformat.h"
#include "libavutil/dict.h"
}


class X264VideoEncoder: public QObject {
Q_OBJECT

public:
    X264VideoEncoder(QObject *parent);
    ~X264VideoEncoder() override;
    bool createFile(QString filename,unsigned width,unsigned height,AbstractCamera::videoFmt format,unsigned fps=25, unsigned scaleDownFactor = 1);
    bool createTimestampFile(QString filename, quint32 clockRate);
    bool close();




private:
    unsigned Width,Height;
    unsigned Bitrate;
    unsigned Gop;
    bool videoFileOpen;
    bool timestampFileOpen;

    QFile *timestampFile;
    QFile *hwSyncFile;
    QFile *videoFile;

    //x264 stuff
    x264_param_t param;
    struct SwsContext* convertCtx;
    x264_t* encoder;
    x264_picture_t* pic_in;
    x264_picture_t* pic_out;
    AVFrame *ppicture;
    uint8_t *picture_buf;
    int pts;

    QString fileName;
    QString timestampFileName;

public slots:
    bool encodeImage(const QImage &);
    void writeTimestamp(quint32 t, quint32 hwFrameCount, quint64 hwTimestamp);
    void endThread();
signals:
    void finished();

};




#endif // VideoEncoder_H
