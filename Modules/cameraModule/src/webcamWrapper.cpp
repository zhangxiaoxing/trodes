#include "webcamWrapper.h"

WebcamWrapper::WebcamWrapper()
    : cameraInput(NULL) {
    //surface = new WebcamDisplaySurface(this);
    numFramesRecieved = 0;

    //connect(surface,SIGNAL(newFrame(QImage*, uint32_t, uint64_t, bool, qint64)),this,SLOT(sendFrameSignals(QImage*,uint32_t,uint64_t,bool,qint64)));
    //connect(surface,SIGNAL(activeChanged(bool)),this,SLOT(cameraStateChanged(bool)));




    isCameraOpen = false;
    isCameraAcquiring = false;

}

WebcamWrapper::~WebcamWrapper() {
}

QStringList WebcamWrapper::availableCameras() {
    QStringList out;
    //QList<QCameraInfo> availCameras = QCameraInfo::availableCameras();
    availCameras = QMediaDevices::videoInputs();
    qDebug() << "Webcams:" << availCameras;
    for (int i = 0; i < availCameras.length(); i++) {
        out.append(availCameras[i].description());
    }
    return out;
}

bool WebcamWrapper::open(int cameraID) {
    //QList<QCameraInfo> availCameras = QCameraInfo::availableCameras();
    //if (availableCameras.length() > cameraID) {

    cameraInput = new QCamera(availCameras[cameraID]);
    captureSession.setCamera(cameraInput);
    camSink = new QVideoSink;
    captureSession.setVideoOutput(camSink);
    connect(camSink, SIGNAL(videoFrameChanged(const QVideoFrame &)),this,SLOT(videoFrameChanged(const QVideoFrame &)));

    //camSink->videoFrameChanged()

    //cameraInput->setViewfinder(videoSurface());
    isCameraOpen = true;
    numFramesRecieved = 0;

        return true;
    /*} else {
        qDebug() << "Error in opening webcam";
        return false;
    }*/


}

bool WebcamWrapper::start() {
    if (isCameraOpen) {
         cameraInput->start(); // to start the viewfinder
         return true;
    } else {
        return false;
    }

}

void WebcamWrapper::videoFrameChanged(const QVideoFrame &currentFrame) {


    QImage *sendImage = new QImage(currentFrame.toImage());
    /*QImage *sendImage = new QImage(currentFrame.bits(),
                     currentFrame.width(),
                     currentFrame.height(),
                     currentFrame.bytesPerLine(),
                     imageFormat);*/

    if (!isCameraAcquiring) {
        isCameraAcquiring = true;
        qDebug() << "Video format is: " << sendImage->format();
        //Log the video format
        switch (sendImage->format()) {
        case QImage::Format_RGB32:
            qDebug() << "RGB32 video format";
            setFormat(AbstractCamera::Fmt_RGB32);
            break;
        case QImage::Format_RGB888:
            qDebug() << "RGB24 video format";
            setFormat(AbstractCamera::Fmt_RGB24);
            break;
        case QImage::Format_ARGB32:
            qDebug() << "ARGB32 video format";
            setFormat(AbstractCamera::Fmt_ARGB32);
            break;
        default:
            qDebug() << "Video format not supported";
            setFormat(AbstractCamera::Fmt_Invalid);
            break;

        }
    }
    //emit newFrame(sendImage,0, 0, true, -1); //send image to image processor, flip frame
    emit newFrame(sendImage,0, 0, false, -1); //send image to image processor, do not flip frame
}

void WebcamWrapper::close() {
    stop();
    isCameraAcquiring = false;

    if (cameraInput != NULL) {
        //cameraInput->unload();
        //cameraInput->

        /* Do not delete the cameraInput pointer
         * It's a memory leak, but on linux, deleting a qcamera causes
         * a crash later down the road for some reason. Even breaks when
         * deleting later on a timer
         */
//        cameraInput->deleteLater();
//        delete cameraInput;
//        cameraInput = NULL;
    }
    isCameraOpen = false;
}

void WebcamWrapper::stop() {
    if (isCameraAcquiring) {
        cameraInput->stop();
        isCameraAcquiring = false;
    }

}

/*void WebcamWrapper::cameraStateChanged(bool active) {
    if (active) {
        isCameraAcquiring = true;
        qDebug() << "Video format is: " << (QImage::Format)surface->getFormat();
        //Log the video format
        switch (surface->getFormat()) {
        case QImage::Format_RGB32:
            qDebug() << "RGB32 video format";
            setFormat(AbstractCamera::Fmt_RGB32);
            break;
        case QImage::Format_RGB888:
            qDebug() << "RGB24 video format";
            setFormat(AbstractCamera::Fmt_RGB24);
            break;
        case QImage::Format_ARGB32:
            qDebug() << "ARGB32 video format";
            setFormat(AbstractCamera::Fmt_ARGB32);
            break;
        default:
            qDebug() << "Video format not supported";
            setFormat(AbstractCamera::Fmt_Invalid);
            break;

        }
    } else {
        isCameraAcquiring = false;
    }
}*/


//----------------------------------------------------



/*
WebcamDisplaySurface::WebcamDisplaySurface(QObject *parent)
    : QAbstractVideoSurface(parent)
    , imageFormat(QImage::Format_Invalid) {
    frameTimer.start();
}

QList<QVideoFrame::PixelFormat> WebcamDisplaySurface::supportedPixelFormats(
        QAbstractVideoBuffer::HandleType handleType) const {
    if (handleType == QAbstractVideoBuffer::NoHandle) {
        return QList<QVideoFrame::PixelFormat>()
                << QVideoFrame::Format_RGB32
                << QVideoFrame::Format_ARGB32
                << QVideoFrame::Format_ARGB32_Premultiplied
                << QVideoFrame::Format_RGB565
                << QVideoFrame::Format_RGB555;
    } else {
        return QList<QVideoFrame::PixelFormat>();
    }
}

QImage::Format WebcamDisplaySurface::getFormat() {
    return imageFormat;
}

bool WebcamDisplaySurface::isFormatSupported(
        const QVideoSurfaceFormat &format, QVideoSurfaceFormat *similar) const
{
    Q_UNUSED(similar);

    const QImage::Format imageFormat = QVideoFrame::imageFormatFromPixelFormat(format.pixelFormat());
    const QSize size = format.frameSize();

    return imageFormat != QImage::Format_Invalid
            && !size.isEmpty()
            && format.handleType() == QAbstractVideoBuffer::NoHandle;
}

bool WebcamDisplaySurface::start(const QVideoSurfaceFormat &format)
{
    const QImage::Format imageFormat = QVideoFrame::imageFormatFromPixelFormat(format.pixelFormat());
    const QSize size = format.frameSize();

    if (imageFormat != QImage::Format_Invalid && !size.isEmpty()) {
        this->imageFormat = imageFormat;

        imageSize = size;
        sourceRect = format.viewport();

        QAbstractVideoSurface::start(format);

        //widget->updateGeometry();
        //updateVideoRect();

        return true;
    } else {
        return false;
    }
}

void WebcamDisplaySurface::stop()
{
    currentFrame = QVideoFrame();
    targetRect = QRect();

    QAbstractVideoSurface::stop();

    //widget->update();
}

bool WebcamDisplaySurface::present(const QVideoFrame &frame) {

    if (surfaceFormat().pixelFormat() != frame.pixelFormat()
            || surfaceFormat().frameSize() != frame.size()) {
        setError(IncorrectFormatError);
        stop();

        return false;
    } else {

        if(!frame.isValid()){
            return true;
        }



        currentFrame = frame;
        if(QVideoFrame::imageFormatFromPixelFormat(currentFrame.pixelFormat()) == QImage::Format_Invalid ||
           QVideoFrame::imageFormatFromPixelFormat(currentFrame.pixelFormat()) != imageFormat){
            return true;
        }
        //widget->update();

        if (currentFrame.map(QAbstractVideoBuffer::ReadOnly)) {
#ifdef __APPLE__
            QImage *sendImage = new QImage(currentFrame.bits(),
                             currentFrame.width(),
                             currentFrame.height(),
                             currentFrame.bytesPerLine(),
                             imageFormat);
#else
            QImage *sendImage = new QImage(currentFrame.width(),
                             currentFrame.height(),
                             imageFormat);
            memcpy(sendImage->bits(), frame.bits(), frame.mappedBytes());
#endif

            //widget->newImage(*sendImage);


#ifdef WIN32
               emit newFrame(sendImage,0, 0, true, -1); //send image to image processor, flip frame
#else
               emit newFrame(sendImage,0, 0, false, -1); //send image to image processor
#endif

            //emit newFrame(); //for time request via tcp server
            currentFrame.unmap();
        }

        return true;
    }
}*/

