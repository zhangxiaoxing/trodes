Untethered Recordings Using Datalogging Headstages
===================================================

Overview
--------

Some of the SpikeGadgets headstages have the ability to
record neural data directly onto an SD card on the headstage. This
allows completely untethered operation, which can enable a wide range of
experiments that are not practical with tethers. However, this creates
an issue: if the neural signals are recorded on the headstage, how can
we now correlate those signals to events in the environment (lever
presses, beam breaks, video, lights, sounds)? During tethered recording,
we combine such signals with the neural recording and timestamp video
frames as data are collected. But during untethered recordings, the
environmental record is recorded separately and must be registered with
the neural data on the SD card after the recording has finished. Here we
cover the main things to consider and how to use the Trodes toolbox to
perform critical operations. |untethered_diagram.png|

.. figure:: https://bitbucket.org/repo/d7xGjL/images/1555535034-tethered_diagramai.png
   :alt: tethered_diagramai.png

   tethered_diagramai.png

Synchronization
---------------

Synchronization between the environmental record and
the data recorded on the headstage depends on a radio signal that is
sent to the headstage at regular intervals (most setups use a 10-second
interval). This signal originates from SpikeGadgets hardware (there are
multiple options) that also acquires environmental data and connects to
your computer. Trodes connects to this hardware to display and save the
environmental data during the recording session. If you need video, the
camera module is used the same way you would during tethered recordings.
You will not have access to the neural data during the recording. After
the recording session is done, you will have two files: one on the
headstage SD card and one on the computer. These two files will be
aligned and merged using the synchronization signals that were recorded
in both files.

Recording
---------

If you are using the MCU without a connected ECU for this
purpose, you should use the MCUonly_RF workspace (located in the
SampleWorkspaces folder). If you have an ECU connected to the MCU for
extended environmental control, you should use the ECUonly workspace. In
either case, make sure that the SpikeGadgets radio transmitter is
plugged into the correct auxiliary slot in the front of the MCU. See the
hardware manual for more details. Once you have opened up the workspace,
connect to the MCU. If you are using the MCUonly_RF workspace you should
use USB to connect to the MCU. If an ECU is connected, you can use
either USB or Ethernet. Next, power on the headstage and make sure that
the light indicators on the headstage indicate that it is ready to start
recording. You are now ready to start the session. When you start
acquiring data in Trodes, the MCU will send a “start” signal to the
headstage to start recording. Next, create a new recording file from the
Trodes menu and start recording. When the recording session is complete,
stop the recording in Trodes and close the .rec file. Then, stop
acquisition (this will also stop the headstage recording) and power off
the headstage. At this point the .rec file on the computer will contain
the environmental record. The SD card on the headstage will contain the
neural data. Note: the SD card on the headstage will be locked from
further recording in order to protect the existing recording from
accidental erasing. After data has been transferred to the computer, the
card must be enabled for recording again using the transfer utility.

Transferring Data to Computer and Merging Files
-----------------------------------------------

To merge to the two
files into a single .rec file, you will need three files: 1) The file
from the SD card recording, 2) the .rec file containing the
environmental record, and 3) a workspace to append to the merged file.
The workspace should be the same workspace you would have used if the
recording was performed in tethered mode. Note: this is NOT the same
workspace you used to collect the environmental record. We provide a
graphical utility to perform the operations of transferring data from
the SD card and merging the two files here: :doc:`Data Logger
GUI </advanced/DataLoggerGUI>`

Getting Started First-time steps to set up data logging:
--------------------------------------------------------

1)  Plug the RF Sync dongle into the Aux 1 port in the front of the MCU,
    connect the ECU to the MCU, and connect the MCU to the computer via
    USB. Do not connect the headstage to the MCU. Turn the MCU and ECU
    on. If you do not have an ECU, ignore the ECU part.

2)  Open up Trodes with the ECU_only.trodesconf workspace or the
    (MCUonly_RF.trodesconf workspace if you do not have an ECU), connect
    to the MCU in the connection menu, and start streaming data. On the
    Aux tab, you should see the ECU’s inputs streaming to the screen.

3)  Once you have verified that streaming from the MCU works ok, stop
    streaming (Connection->Disconnect).

4)  Insert an SD card into the headstage that is configured for the
    correct number of channels. If the card is not yet configured (new
    card), you will need to add the configuration to the SD card using
    the Write Config feature of the DataLogger GUI using the correct
    config file. If you have already done a recording using the SD card,
    make sure to re-enable it for recording using the DataLogger GUI.
    Plug the charged battery into the headstage. The headstage will
    power on and the orange light should go solid on (indicating that it
    is waiting for the start signal from the MCU). If the red light is
    flashing, it means that the SD card is not enabled for recording or
    that there is no config file on the SD card.

5)  In Trodes, start streaming again. This should send a radio command
    to the headstage to start collecting data. The orange light should
    turn off on the headstage.

6)  In trodes, create a file to save the environmental data and start
    recording (like you do during tethered recordings).

7)  Collect 1-2 minutes of data, then end the environmental recording in
    Trodes.

8)  Stop streaming from the MCU (Connection->Disconnect). The orange
    light should turn back on on the headstage indicating that it is
    paused.

9)  Unplug the battery from the headstage and remove the SD card.

10) Plug the SD card into your computer (or the SpikeGadgets docking
    station)and use the Data Logger GUI to download the data from the SD
    card and merge the data with the environmental record you just
    recorded. You will need to provide the right workspace, otherwise
    you will have the error that you descibed (clock jumping around and
    you just see noise during playback). The merge workspace should be
    the same workspace you have been using during tethered recordings
    with the MCU and ECU connected to each other.

11) Play back the merged file in Trodes to make sure that you used the
    correct workspace.

.. |untethered_diagram.png| image:: https://bitbucket.org/repo/d7xGjL/images/4230814158-untethered_diagram.png

