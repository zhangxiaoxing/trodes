===================================
Importing data for Matlab or Octave
===================================

These are instructions for importing information from Trodes ``.rec`` files
into Matlab or Octave.

In Matlab or Octave, the function *readTrodesExtractedDataFile* is used to read the .dat files that result from using the trodesexport utility. The exact contents of the file depends on the type of data that was exported. In this example below, we are importing a file with spike snippets (spikes processor).

|

``imported_data = readTrodesExtractedDataFile('example_recording.spikes_nt2.dat');``


Will create a structure containing the fields:

    - description: 1x30 sq_string
    - byte_order: 1x13 sq_string
    - original_file: 1x21 sq_string
    - clockrate: 1x1 scalar
    - trodes_version: 1x5 sq_string
    - compile_date: 1x11 sq_string
    - compile_time: 1x8 sq_string
    - qt_version: 1x5 sq_string
    - commit_tag: 1x26 sq_string
    - controller_firmware: 1x1 scalar
    - headstage_firmware: 1x1 scalar
    - controller_serialnum: 1x1 scalar
    - headstage_serialnum: 1x1 scalar
    - autosettle: 1x1 scalar
    - smartref: 1x1 scalar
    - gyro: 1x1 scalar
    - accelerometer: 1x1 scalar
    - magnetometer: 1x1 scalar
    - time_offset: 1x1 scalar
    - system_time_at_creation: 1x1 scalar
    - timestamp_at_creation: 1x1 scalar
    - first_timestamp: 1x1 scalar
    - ntrode_id: 1x1 scalar
    - num_channels: 1x1 scalar
    - voltage_scaling: 1x1 scalar
    - threshold: 1x1 scalar
    - spike_invert: 1x3 sq_string
    - reference: 1x3 sq_string
    - filter: 1x2 sq_string
    - lowpassfilter: 1x1 scalar
    - highpassfilter: 1x1 scalar
    - spike_trigger_on_1: 1x1 scalar
    - fields: 1x2 struct

Most of these fields will exist for all of the exported data types, but some are specific to the type. In this case, 'threshold' is specific to spike trigger events, for example.

The data are always stored in 'fields':

|

``imported_data.fields``

ans =

  1x2 struct array containing the fields:

    name

    type

    columns

    bytesPerItem

    data

imported_data.fields(1).name is 'time' and imported_data.fields(2).name is 'waveformCh1'. If the nTrode has more than one channel, the array will be have one extra entry per channel. Similarly, imported_data.fields(1).data contains each spike's time, and imported_data.fields(2).data contains each spikes' waveform data. 

**Note: the data type is given in the 'type' field. To save memory, this is usually not 'double', which will create problems for some Matlab functions.** To convert to double, you can do this: myDoubleVar = double(originalVar);

Similar structures will be created for all other data types.
