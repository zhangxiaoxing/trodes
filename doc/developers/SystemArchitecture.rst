System Architecture
===================

Trodes is seperated into two parts: 1) central multithreaded process
that handles the basics of electrophysiology, such as stream and spike
visualization, audio, and saving; 2) a number of independent processes
that communicate through POSIX sockets with the central process. Here is
a diagram of architecture of the central part of Trodes:

.. image:: https://bitbucket.org/repo/d7xGjL/images/1685924650-trodesDiagram.png