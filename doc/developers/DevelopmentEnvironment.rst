======================================
Setting up the development environment
======================================


Qt Installation (All platforms)
===============================

The graphical user interface is using Qt. Building Trodes
requires installing Qt.

For all platforms, download the `Qt online installer <https://www.qt.io/download-qt-installer>`__.

Make sure to download the right installer for your platform.

It will prompt you login with a Qt account. You create one and verify your email.

After it finishes loading, it will prompt you to select which packages to install.

Select a custom installation.

Deselect the defaults and only select ``LTS``.

We want the latest ``5.x.x`` version. Under the latest ``5.x.x`` version,
select only:

1. Desktop gcc 64 bit
2. Sources
3. Qt Debug Information Files

.. note::
   For Windows users, it may display Desktop (MSVC) or something similar. Select that.

Leave any packages under "Development tools" selected. Those defaults are fine.

Agree to the licenses and install.

Platform-specific instructions
==============================

Linux (Ubuntu)
--------------

Code is tested on Ubuntu 20.04 and Ubuntu 18.04.

Most of the code has been tested in Ubuntu 18.04. Newer code has been
tested in 20.04. Both versions should work and are supported.

Install the following packages:

1. ``sudo apt-get install git`` (to pull the Trodes repo from Bitbucket)

2. ``sudo apt-get install chrpath`` (used during the build +
   installation process)

3. ``sudo apt-get install mesa-common-dev libglu1-mesa-dev``. (Note that
   this may not actually be necessary; if you are using a graphics card
   and installed its driver, then OpenGL is probably set up already.
   Many manufacturers such as Nvidia have implemented OpenGL for their
   hardware, so installing the appropriate driver will pull in the
   necessary dependencies).

4. ``sudo apt-get install libpulse-dev`` (needed for audio)

5. ``sudo apt-get install libgstreamer-plugins-base0.10-0`` (optional,
   for camera module)

Mac
---

1. `Install Homebrew <https://brew.sh/>`__. Then execute
   ``brew install git``

2. `Go to Apple’s developer
   website <https://developer.apple.com/download>`__. Search for Xcode.
   Download and install that as well as Xcode Command Line Tools. Then
   from the top menu, Xcode->Preferences->Locations. Select the
   appropriate kit for the Command Line Tools.

Windows
-------

1. Recommended: Select mingw32. This will install a shell that sets up
   the Qt build environment, so you won’t have to worry about adding
   qmake to a PATH environment variable.

2. Alternatively, you can install one of the msvc pre-built packages if
   you really want 64 bit, but you will have to do some additional
   things to get Trodes to compile. Don’t worry, it’s not terribly
   difficult to do. It’s mostly changing variable length arrays to
   conform to the features that the Microsoft C++ compiler will support.
   Next, install the Microsoft Visual Studio version that matches the Qt
   package version you selected (example: install Qt msvc2017 and then
   Microsoft Visual Studio Community 2017). We also recommend you
   install Qt’s `jom <https://wiki.qt.io/Jom>`__ executable to
   parallelize the compilation process.
