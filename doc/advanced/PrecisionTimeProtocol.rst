Precision Time Protocol (PTP)
=============================

`Precision time
protocol <https://en.wikipedia.org/wiki/Precision_Time_Protocol>`__ is a
protocol used to synchronize clocks over a computer network. With a
small amount of setup (and an PTP-compatible Allied Vision camera!), you
can achieve microsecond level synchronization between camera frames and
Trodes data packets.

Please read ahead, as there are limitations on which cameras this works
with and which operating systems this has been tested on.

Contributors and Sponsors
-------------------------

Thanks to the Frank Lab at UCSF for sponsoring the development time for
this feature. Thanks to Tom Davidson, Abhilasha Joshi, and Alison Comrie
for contributing to the initial development, testing of the feature, and
lending of cameras!

Allied Vision Camera setup
--------------------------

Only a few models of Allied Vision cameras support PTP, specifically
`Manta, Prosilica GT and Prosilica GC cameras <https://www.alliedvision.com/fileadmin/content/documents/products/cameras/various/appnote/GigE/PTP_IEEE1588_with_GigE.pdf>`__.

After setting up your ethernet network with the camera (See our hardware
setup guide), open VimbaViewer, AVT’s proprietary software to tweak
settings. Follow the Properties tab and go to settings GigE -> PTP. Set
**PtpMode** to “Slave”. Continue setting up the rest of your camera
settings, such as exposure, brightness, etc.

.. figure:: https://bitbucket.org/repo/d7xGjL/images/1974791267-ptpmode_slave.png
   :alt: ptpmode_slave.png

   ptpmode_slave.png

Trodes setup
------------

1. Open up Trodes and edit your workspace in the Workspace Editor.
2. In the first tab, make sure as **SysClock** is added in the list of
   devices. If not, do so now. |sysclock_editor.png|
3. In the last tab, add the module “cameraModule” to the list of
   automatically launched modules.
4. Highlight cameraModule, and click on **+Add Argument**. In the *Flag*
   field, type in ``ptpEnabled`` and leave the *Value* field empty.
   |cammod_ptp_arg.png|
5. Save and Open the workspace. CameraModule should open automatically,
   and the extra flag enables a series of checks for PTP.

Computer and network setup
--------------------------

Ubuntu
~~~~~~

1. Install ptpd: ``sudo apt-get install ptpd``
2. Find the ethernet network interface your camera module is located on
   (e.g eth0 or enp0s25)

   -  Make sure this is not the MCU!

3. Start the PTP daemon in master mode: ``sudo ptpd -M -i eth0``

And you’re done! If you set up the camera correctly, it should be
attempting to calibrate and sync with the computer. Open Trodes and
CameraModule, and the status should have changed to “Calibrating”. It
takes a few minutes (5-20) to sync.

The PTP daemon must be restarted every time the computer shuts down or
logs off.

Windows
~~~~~~~

There currently is no tested solution for Windows. In theory, any PTP
service should work, as it is a standardized protocol. There are some
free PTP services for Windows available, but none have been tested.
Contact us and we would be glad to help if this is something you need.

.. |sysclock_editor.png| image:: https://bitbucket.org/repo/d7xGjL/images/2149001629-sysclock_editor.png
.. |cammod_ptp_arg.png| image:: https://bitbucket.org/repo/d7xGjL/images/935245254-cammod_ptp_arg.png

