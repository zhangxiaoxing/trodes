module Trodes.Network.Cpp.Print.JuliaStruct where

import Data.Text (Text)
import qualified Data.Text as T
import Trodes.Network.Cpp.Ast
import Trodes.Network.Cpp.Print.Types

-- iterate through each constructor and make it a struct
prettyShowType :: CppDefinition -> Text
prettyShowType def = T.unlines
    [ "# module " <> name
    , ""
    , structDef
    , funDef
    ]
    where
        (structDef, funDef) = if length (cppConstructors def) == 1
                        then
                            let singleCons = head $ cppConstructors def in
                            (prettyShowConstructor singleCons, renderFunction singleCons)
                        else ("# not defined on sum types", "")
        name = cppName def

prettyShowConstructor :: CppConstructor -> Text
prettyShowConstructor constructor = T.unlines
    [ "# constructor " <> cppConstructorName constructor
    , "struct " <> cppConstructorName constructor
    , T.unlines $ (map ("   " <>) fields) ++ ["end"]
    ]
    where
        fields = map prettyShowField $ cppConstructorVariables constructor

appendCommaAllButLast :: [Text] -> [Text]
appendCommaAllButLast [] = []
appendCommaAllButLast xs = (map (<> ",") $ init xs) ++ [last xs]

renderFunction :: CppConstructor -> Text
renderFunction constructor = T.unlines
    [ "function read" <> cppConstructorName constructor <> "(dict)"
    , "    " <> cppConstructorName constructor <> "("
    , T.intercalate "\n" $ map ("        " <>) fieldsParens
    , "end"
    ]
    where
        fieldsParens = fields ++ [")"]
        fields = appendCommaAllButLast $ map showExtractDict $ cppConstructorVariables constructor
        showExtractDict :: CppRecordField -> Text
        showExtractDict field = "dict[\"" <> (cppRecordFieldName field) <> "\"]"


prettyShowField :: CppRecordField -> Text
prettyShowField field = printField field
    where
        printField :: CppRecordField -> Text
        printField f = (printFieldName f) <> " :: " <> (printJuliaType f)

        printFieldName :: CppRecordField -> Text
        printFieldName f = cppRecordFieldName f