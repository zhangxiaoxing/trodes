module Trodes.Network.Types.ITime where

import Data.Word
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp, CppType)

data ITime = ITimInfoRequeste
    { time :: Word32
    } deriving (Generic, Cpp, CppType)
