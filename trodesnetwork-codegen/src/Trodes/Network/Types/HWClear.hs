module Trodes.Network.Types.HWClear where

import Data.Word
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp, CppType)

data HWClear = HWClear
    { number :: Word16
    } deriving (Generic, Cpp, CppType)
