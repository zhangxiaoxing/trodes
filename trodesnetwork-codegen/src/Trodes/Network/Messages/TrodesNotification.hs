module Trodes.Network.Messages.TrodesNotification where

import Data.Text (Text)
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data TrodesNotification = TrodesNotification
    { tag :: Text
    , who :: Text
    } deriving (Generic, Cpp)
