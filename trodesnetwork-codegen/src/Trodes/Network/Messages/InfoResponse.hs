module Trodes.Network.Messages.InfoResponse where

import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

import qualified Trodes.Network.Types.ITime as I
import qualified Trodes.Network.Types.ITimerate as I

data InfoResponse
    = IRTime I.ITime
    | IRTimerate I.ITimerate
    | IRTrodesConfig
    deriving (Generic, Cpp)
