module Trodes.Network.Messages.DataCommand where

import Data.Text (Text)
import GHC.Generics (Generic)
import Trodes.Network.Cpp.Generic (Cpp)

data DataCommand = DataCommand
    { command :: Text
    , streamname :: Text
    } deriving (Generic, Cpp)
