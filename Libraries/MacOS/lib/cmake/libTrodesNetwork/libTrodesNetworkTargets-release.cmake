#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "libTrodesNetwork::TrodesNetwork" for configuration "Release"
set_property(TARGET libTrodesNetwork::TrodesNetwork APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(libTrodesNetwork::TrodesNetwork PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libTrodesNetwork.0.1.1.dylib"
  IMPORTED_SONAME_RELEASE "@rpath/libTrodesNetwork.0.1.1.dylib"
  )

list(APPEND _IMPORT_CHECK_TARGETS libTrodesNetwork::TrodesNetwork )
list(APPEND _IMPORT_CHECK_FILES_FOR_libTrodesNetwork::TrodesNetwork "${_IMPORT_PREFIX}/lib/libTrodesNetwork.0.1.1.dylib" )

# Import target "libTrodesNetwork::trodesnetwork" for configuration "Release"
set_property(TARGET libTrodesNetwork::trodesnetwork APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(libTrodesNetwork::trodesnetwork PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/spikegadgets_python/spikegadgets/trodesnetwork.0.1.1.so"
  IMPORTED_SONAME_RELEASE "@rpath/trodesnetwork.0.1.1.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS libTrodesNetwork::trodesnetwork )
list(APPEND _IMPORT_CHECK_FILES_FOR_libTrodesNetwork::trodesnetwork "${_IMPORT_PREFIX}/spikegadgets_python/spikegadgets/trodesnetwork.0.1.1.so" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
