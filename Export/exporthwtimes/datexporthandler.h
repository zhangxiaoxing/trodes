#ifndef DATEXPORTHANDLER_H
#define DATEXPORTHANDLER_H
#include "abstractexporthandler.h"

enum dioFormat{
    DIODAT, //DIO dat file provided
    DIOREC  //DIO from rec file to be used
};

enum videoTSFormat{
    HWSYNC, //camHWsync file has frame and timestamps
    HWVID   //camhwsync only has frame data
};

class DATExportHandler: public AbstractExportHandler
{
    Q_OBJECT

public:
    DATExportHandler(QStringList arguments);
    int processData();
    ~DATExportHandler();

protected:

    void parseArguments();
    void printHelpMenu();

    //void printCustomMenu();
    //void parseCustomArguments(int &argumentsProcessed);

private slots:


private:
    bool parseFields(QString fieldLine);
    int setUpPulses();
    int setUpVideoAndFrames();

    QString dioFileName;
    QString dioChannel;
    QString videoTimeStampsFileName;
    QString hwSyncFileName;
    QString h264FileName;
    QVector<quint32> frames;
    QVector<quint32> videoTimes;
    QVector<quint32> pulseTimes;
    //abstract: recfileName
    bool olderVideoFormat;
    qreal avgIncrease;
};


#endif // DATEXPORTHANDLER_H
