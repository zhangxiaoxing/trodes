#ifndef LFPEXPORTHANDLER_H
#define LFPEXPORTHANDLER_H
#include "abstractexporthandler.h"
#include "iirFilter.h"
#include <stdio.h>


class DATExportHandler: public AbstractExportHandler
{
    Q_OBJECT

public:
    DATExportHandler(QStringList arguments);
    int processData();
    ~DATExportHandler();

protected:

    void parseArguments();
    void printHelpMenu();

    //void printCustomMenu();
    //void parseCustomArguments(int &argumentsProcessed);

private:
    TrodesConfiguration mergeConfigData; //Final config placed at top of merged file
    TrodesConfiguration environmentalConfigData; //Config used duyring the experiment to log enironmental data

    QString sdFileName;
    QFile *sdFilePtr;
    QVector<char> sdFilePacketBuffer;
    QVector<uint32_t> sdSyncValues;
    QVector<uint32_t> sdTimeDuringSync;
    QVector<int64_t> sdPacketDuringSync;

    QVector<uint32_t> recSyncValues;
    QVector<int64_t>  recPacketDuringSync;

    //char recInterleavedData[8];
    QList<QByteArray> recInterleavedData;
    char sdInterleavedData[8];
    char recInterleavedFlag;
    char sdInterleavedFlag;
    bool firstSample;
    int packLengthAdjustmentofSD;




    QList<int> deviceListToUse;
    int sdFilePacketSize;
    int sdFilePacketStartDataLoc;
    int sdFilePacketFlagByteLoc;
    int sdFilePacketSyncByteLoc;
    int sdFilePacketTimeByteLoc;

    int trodesRFPacketByte;
    int trodesRFInterleavedFlagByte; //where the interleaved flag byte is in the packet
    int trodesRFInterleavedFlagBit; //the bit in the flag byte that indicates that a sync came in


    uint32_t currentSDTimeStamp;
    uint32_t lastSDTimeStamp;
    uint32_t currentRecSyncStamp;

    int64_t currentRecPacket = 0;
    int64_t currentSDPacket = 0;

    QString mergedConfFileName;

    int SDRecNumChan;

    QList<QDataStream*> neuroStreamPtrs;
    QVector<bool> writeSDChannel;

    int syncOffsetTime;
    int syncOffsetPackets;
    int currentSyncInd;


    bool openSDInputFile();
    bool scanRecForSyncs();
    int  scanSDFileForChannelCount();
    bool writeMergedPacket();
    bool readNextSDPacket();
    bool readNextRecPacket();
    bool registerFiles();
    int readMergeConfig();
    QString getHumanReadableTime(uint32_t timestamp, uint32_t samplingrate);



};

#endif // LFPEXPORTHANDLER_H
