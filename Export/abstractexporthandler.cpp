#include "abstractexporthandler.h"



AbstractExportHandler::AbstractExportHandler(QStringList arguments):
    supportedDataCategories(0)
    ,embeddedWorkspace()
    ,loadedWorkspacePtr(nullptr)

{

    enableAVX = false;

    globalConf = &embeddedWorkspace.globalConf;
    hardwareConf = &embeddedWorkspace.hardwareConf;
    headerConf = &embeddedWorkspace.headerConf;
    streamConf = &embeddedWorkspace.streamConf;
    spikeConf = &embeddedWorkspace.spikeConf;
    moduleConf = &embeddedWorkspace.moduleConf;
    benchConfig = &embeddedWorkspace.benchConfig;
    conf_ptrs.globalConf = globalConf;
    conf_ptrs.hardwareConf = hardwareConf;
    conf_ptrs.headerConf = headerConf;
    conf_ptrs.streamConf = streamConf;
    conf_ptrs.spikeConf = spikeConf;
    conf_ptrs.moduleConf = moduleConf;
    conf_ptrs.benchConfig = benchConfig;

    numChannelsInFile = 0;
    packetTimeLocation = -1;
    packetHeaderSize = -1;
    filePacketSize = -1;
    defaultMenuEnabled = true;

    argumentList = arguments;

    _fileNameFound = false;
    helpMenuPrinted = false;
    argumentReadOk = true;
    _argumentsSupported = true;
    _configReadOk = true;

    filterLowPass_SpikeBand = -1;
    filterHighPass_SpikeBand = -1;
    filterLowPass_LFPBand = -1;
    override_Spike_Refs = false;
    override_LFPRefs = false;
    override_RAWRefs = false;
    override_useSpikeFilters = false;
    override_useLFPFilters = false;
    override_useNotchFilters = false;
    useRefs = true;
    useLFPRefs = true;
    useRAWRefs = true;
    useSpikeFilters = true;
    useLFPFilters = false;
    useNotchFilters = true;
    maxGapSizeInterpolation = -1; //default number of points to interpolate over (-1 = inf)
    outputSamplingRate = -1; //default of -1 means to use the same as the input
    invertSpikes = true;
    threshOverride = false;
    //recFileName = "";
    externalWorkspaceFile = "";
    outputFileName = "";
    outputDirectory = "";

    argumentsProcessed = 0;
    abortOnBackwardsTimestamp = false;

    paddingBytes = 0;

    requireRecFile = true;

    currentTimeStamp = 0;
    lastTimeStampInFile = 0;
    firstRecProcessed = false;

}

AbstractExportHandler::~AbstractExportHandler()
{

}

bool AbstractExportHandler::fileNameFound() {
    return _fileNameFound;
}

bool AbstractExportHandler::argumentsOK() {
    return argumentReadOk;
}

bool AbstractExportHandler::argumentsSupported() {
    return _argumentsSupported;
}

bool AbstractExportHandler::wasHelpMenuDisplayed() {
    return helpMenuPrinted;
}

void AbstractExportHandler::writeDefaultHeaderInfo(QFile *filePtr) {
    QFileInfo fi(recFileName);
    QString infoLine;

    infoLine = QString("Byte_order: little endian\n");
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Original_file: ") + fi.fileName() + "\n";
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Clockrate: %1\n").arg(hardwareConf->sourceSamplingRate);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Trodes_version: %1\n").arg(globalConf->trodesVersion);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Compile_date: %1\n").arg(globalConf->compileDate);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Compile_time: %1\n").arg(globalConf->compileTime);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("QT_version: %1\n").arg(globalConf->qtVersion);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Commit_tag: %1\n").arg(globalConf->commitHeadStr);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Controller_firmware: %1\n").arg(globalConf->controllerFirmwareVersion);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Headstage_firmware: %1\n").arg(globalConf->headstageFirmwareVersion);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Controller_serialnum: %1\n").arg(globalConf->controllerSerialNumber);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Headstage_serialnum: %1\n").arg(globalConf->headstageSerialNumber);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("AutoSettle: %1\n").arg(globalConf->autoSettleOn);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("SmartRef: %1\n").arg(globalConf->smartRefOn);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Gyro: %1\n").arg(globalConf->gyroSensorOn);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Accelerometer: %1\n").arg(globalConf->accelSensorOn);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Magnetometer: %1\n").arg(globalConf->magSensorOn);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Time_offset: %1\n").arg(startOffsetTime);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("System_time_at_creation: %1\n").arg(globalConf->systemTimeAtCreation);
    filePtr->write(infoLine.toLocal8Bit());
    infoLine = QString("Timestamp_at_creation: %1\n").arg(globalConf->timestampAtCreation);
    filePtr->write(infoLine.toLocal8Bit());

}

bool AbstractExportHandler::fileConfigOK() {
    return _configReadOk;
}

void AbstractExportHandler::printHelpMenu() {
    if (defaultMenuEnabled) {

        printf("\n"
               "--General--\n"
               "-rec <filename>                 -- Recording filename. Required. Muliple -rec <filename> entries can be used to append data in output.\n"
               "-output <basename>              -- The base name for the output files. If not specified, the base name of the first .rec file is used. \n"
               "-outputdirectory <directory>    -- A root directory to extract output files to (default is directory of .rec file). \n"
               "-reconfig <filename>            -- Use a different workspace than the one embedded in the recording file. \n"
               "-v or -version                  -- Prints the current executable's version information to the terminal. \n"
               "-interp <integer>               -- Maximum number of dropped packets to interpolate over (linear). A value of -1 is infinite. 0 is no interpolation. "
               "-avx <1 or 0>                   -- Whether or not to use AVX CPU intrinsics to speed up processing."
               "-abortbaddata <1 or 0>          -- Whether or not to abort export if data appears corrupted. \n"
               "-paddingbytes <integer>         -- Used to add extra bytes to the expected packet size if an override is required\n\n\n"
               "\n"
               "Setting the following datatype-specific options will override the settings defined in the workspace.\n");


        if (supportedDataCategories & DataCategories::LfpBand) {
            printf("\n"
                   "--LFP data band--\n"
                   "-lfplowpass <integer>           -- Low pass filter value for the LFP band. Overrides settings in workspace for all nTrodes.\n"
                   "-uselfprefs <1 or 0>            -- Override what is in the workspace for lfp band reference on/off settings for all nTrodes.\n"
                   "-uselfpfilters <1 or 0>         -- Override what is in the workspace for lfp filter on/off settings for all nTrodes. \n");
        }
        if (supportedDataCategories & DataCategories::SpikeBand) {
            printf("\n"
                   "--Spike data band--\n"
                   "-highpass <integer>             -- High pass filter value for the spike band. Overrides settings in workspace for all nTrodes. \n"
                   "-lowpass <integer>              -- Low pass filter value for the spike band. Overrides settings in workspace for all nTrodes.\n"
                   "-invert <1 or 0>                -- Sets whether or not to invert spikes to go upward.\n"
                   "-userefs <1 or 0>               -- Override what is in the workspace for spike band reference on/off settings for all nTrodes.\n"
                   "-usespikefilters <1 or 0>       -- Override what is in the workspace for spike filter on/off settings for all nTrodes. \n");
        }
        if (supportedDataCategories & DataCategories::SpikeEvents) {
            printf("\n"
                   "--Spike event processing--\n"
                   "-thresh <integer>               -- Overrides the thresholds for each nTrode stored in the workspace and applies the new threshold to all nTrodes.\n");
        }
        if (supportedDataCategories & DataCategories::RawBand) {
            printf("\n"
                   "--Raw data band--\n"
                   "-userawrefs <1 or 0>            -- Override what is in the workspace for raw band reference on/off settings for all nTrodes.\n");
        }
        if (supportedDataCategories & DataCategories::ContinuousOutput) {
            printf("\n"
                   "--Continuous output files--\n"
                   "-outputrate <integer>           -- Define the output sampling rate in the output file(s).\n");

        }

    }
}

void AbstractExportHandler::displayVersionInfo() {
    printf("%s", qPrintable(GlobalConfiguration::getVersionInfo(false)));
    printf("\n");
}

void AbstractExportHandler::parseArguments() {


    QString AVX_string = "";
    QString filterLowPass_string = "";
    QString filterHighPass_string = "";
    QString maxGapSizeInterpolation_string = "";
    QString ref_string = "";
    QString use_lfp_ref_string = "";
    QString use_raw_ref_string = "";
    QString notch_string = "";
    QString outputrate_string = "";
    QString spikefilter_string = "";
    QString reconfig_string = "";
    QString outputname_string = "";
    QString outputdirectory_string = "";
    QString abort_string = "";
    QString padding_string = "";
    QString lfpfilter_string = "";
    QString lfpfilter_lowpass_string = "";

    //For spikes
    QString invertSpikes_string = "";
    QString threshOverride_string = "";


    int optionInd = 1;
    while (optionInd < argumentList.length()) {
        if ((argumentList.at(optionInd).compare("-rec",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            recFileNameList.push_back(argumentList.at(optionInd+1));
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-avx",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            AVX_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-highpass",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            filterHighPass_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-lowpass",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            filterLowPass_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-lfplowpass",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            lfpfilter_lowpass_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-interp",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            maxGapSizeInterpolation_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-outputrate",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            outputrate_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-userefs",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            ref_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-uselfprefs",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            use_lfp_ref_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-userawrefs",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            use_raw_ref_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-usespikefilters",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            spikefilter_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-uselfpfilters",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            lfpfilter_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        /*} else if ((argumentList.at(optionInd).compare("-usenotchfilters",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            notch_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;*/
        } else if ((argumentList.at(optionInd).compare("-reconfig",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            reconfig_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        }  else if ((argumentList.at(optionInd).compare("-output",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            outputname_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        }  else if ((argumentList.at(optionInd).compare("-outputdirectory",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            outputdirectory_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-abortbaddata",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            abort_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-paddingbytes",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            padding_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-version",Qt::CaseInsensitive)==0) || (argumentList.at(optionInd).compare("-v",Qt::CaseInsensitive)==0)) {
            AbstractExportHandler::displayVersionInfo();
            helpMenuPrinted = true; //we're going to consider version info the same as the help menu to terminate the main loop
            argumentsProcessed = argumentsProcessed+1;
            return;
        } else if ((argumentList.at(optionInd).compare("-h",Qt::CaseInsensitive)==0)) {
            AbstractExportHandler::printHelpMenu();
            helpMenuPrinted = true;
            argumentsProcessed = argumentsProcessed+1;
            return;
        } else if ((argumentList.at(optionInd).compare("-invert",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            invertSpikes_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-thresh",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            threshOverride_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        }
        optionInd++;
    }


    //Read the AVX argument
    if (!AVX_string.isEmpty()) {
        bool ok1;

        enableAVX = AVX_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "-avx arument must be followed by a 1 (on) or 0 (off).";

            argumentReadOk = false;
            return;
        }
    }

    //Read the low pass filter argument
    if (!filterLowPass_string.isEmpty()) {
        bool ok1;

        filterLowPass_SpikeBand = filterLowPass_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Low pass filter could not be resolved into an integer.";
            //filterLowPass_SpikeBand = -1;
            argumentReadOk = false;
            return;
        } else {

            //setting the filter automatically overrides what is in the file and turns on all filters
            override_useSpikeFilters = true;
            useSpikeFilters = true;
            //useSpikeFilters = false; // This setting overrides the file's settings
            //useLFPFilters = false;
            //if (filterHighPass_SpikeBand == -1) {
                //Set the high pass filter to 0
            //    filterHighPass_SpikeBand = 0;
            //}
        }
    }

    //Read the high pass filter argument
    if (!filterHighPass_string.isEmpty()) {
        bool ok1;

        filterHighPass_SpikeBand = filterHighPass_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "High pass filter could not be resolved into an integer.";
            //filterHighPass_SpikeBand = -1;
            argumentReadOk = false;
            return;
        } else {

            //setting the filter automatically overrides what is in the file and turns on all filters
            override_useSpikeFilters = true;
            useSpikeFilters = true;

            //useSpikeFilters = false; // This setting overrides the file's settings
            //if (filterLowPass_SpikeBand == -1) {
                //Set the low pass filter to highest supported value
            //    filterLowPass_SpikeBand = 6000;
            //}
        }
    }

    //Read the low pass filter argument
    if (!lfpfilter_lowpass_string.isEmpty()) {
        bool ok1;

        filterLowPass_LFPBand = lfpfilter_lowpass_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Low pass filter for LFP could not be resolved into an integer.";
            argumentReadOk = false;
            return;
        } else {
            //setting the filter automatically overrides what is in the file and turns on all filters
            override_useLFPFilters = true;
            useLFPFilters = true;

        }
    }

    //TODO: notch filter settings

    if (!outputrate_string.isEmpty()) {
        bool ok1;

        outputSamplingRate = outputrate_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Output rate could not be resolved into an integer.";
            outputSamplingRate = -1;
            argumentReadOk = false;
            return;
        }

        if (((outputSamplingRate < 1)&&(outputSamplingRate != -1)) || (outputSamplingRate > 30000))  {
            qDebug() << "Outputrate must be between 1 and 30000. Or, it can be -1 to match the original rate.";
            outputSamplingRate = -1;
            argumentReadOk = false;
            return;
        }
    }

    if (!ref_string.isEmpty()) {
        bool ok1;

        useRefs = ref_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Reference mode could not be resolved into an integer (1 or 0).";
            useRefs = true;
            argumentReadOk = false;
            return;
        } else {
            override_Spike_Refs = true;
        }
    }

    if (!use_lfp_ref_string.isEmpty()) {
        bool ok1;

        useLFPRefs = use_lfp_ref_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "LFP reference mode could not be resolved into an integer (1 or 0).";

            argumentReadOk = false;
            return;
        } else {
            override_LFPRefs = true;
        }
    }

    if (!use_raw_ref_string.isEmpty()) {
        bool ok1;

        useRAWRefs = use_raw_ref_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "RAW reference mode could not be resolved into an integer (1 or 0).";

            argumentReadOk = false;
            return;
        } else {
            override_RAWRefs = true;
        }
    }

    if (!abort_string.isEmpty()) {
        bool ok1;

        abortOnBackwardsTimestamp = abort_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Abort mode could not be resolved into an integer (1 or 0).";
            abortOnBackwardsTimestamp = false;
            argumentReadOk = false;
            return;
        }
    }

    if (!spikefilter_string.isEmpty()) {
        bool ok1;

        useSpikeFilters = spikefilter_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Spikefilter mode could not be resolved into an integer (1 or 0).";
            useSpikeFilters = true;
            filterHighPass_SpikeBand = -1;
            filterLowPass_SpikeBand = -1;
            argumentReadOk = false;
            return;
        } else {
            override_useSpikeFilters = true;
        }
    }

    if (!lfpfilter_string.isEmpty()) {
        bool ok1;

        useLFPFilters = lfpfilter_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "LFP filter mode could not be resolved into an integer (1 or 0).";
            useLFPFilters = true;
            argumentReadOk = false;
            return;
        } else {
            override_useLFPFilters = true;
        }
    }

    if (!notch_string.isEmpty()) {
        bool ok1;

        useNotchFilters = notch_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Notch filter mode could not be resolved into an integer (1 or 0).";

            argumentReadOk = false;
            return;
        } else {
            override_useNotchFilters = true;
        }
    }

    //Read the interpolation argument
    if (!maxGapSizeInterpolation_string.isEmpty()) {
        bool ok1;

        maxGapSizeInterpolation = maxGapSizeInterpolation_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Max interpolation gap value could not be resolved into an integer.";
            maxGapSizeInterpolation = -1;
            argumentReadOk = false;
            return;
        }
    }

    //Read the high pass filter argument
    if (!padding_string.isEmpty()) {
        bool ok1;

        paddingBytes = padding_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Padding could not be resolved into an integer.";
            paddingBytes = 0;
            argumentReadOk = false;
            return;
        } else {
            if (paddingBytes < 0) {
                qDebug() << "Padding must be an integer greater than or equal to 0";
                paddingBytes = 0;
                argumentReadOk = false;
                return;
            }

        }
    }

    if (!reconfig_string.isEmpty()) {

        externalWorkspaceFile = reconfig_string;
        QFileInfo fi(externalWorkspaceFile);
        if (!fi.exists()) {
            qDebug() << "File could not be found: " << externalWorkspaceFile;
            argumentReadOk = false;
        }

    }

    //Read the invert argument
    if (!invertSpikes_string.isEmpty()) {
        bool ok1;

        invertSpikes = invertSpikes_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Invert spikes argument could not be resolved into an integer. Use 0 (false) or 1 (true)";
            argumentReadOk = false;
            return;
        }
    }

    //Read the thresh argument
    if (!threshOverride_string.isEmpty()) {
        bool ok1;

        threshOverrideVal = threshOverride_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "Thresh argument could not be resolved into an integer.";
            argumentReadOk = false;
            return;
        } else if ((threshOverrideVal < 10) || (threshOverrideVal > 2000)) {
            qDebug() << "A threshold override value of" << threshOverride << "is not allowed. Value must be >= 10 uV and <= 2000 uV.";
            argumentReadOk = false;
            return;
        } else {
            threshOverride = true; //when set to true, the threshOverrideVal is used for all nTrodes
            //threshOverrideVal_rangeConvert = (threshOverrideVal * 65536) / AD_CONVERSION_FACTOR; //convert the threshold value to the full-range value
        }
    }


    if (!outputname_string.isEmpty()) {

        outputFileName = outputname_string;
    }

    if (!outputdirectory_string.isEmpty()) {
        if(outputdirectory_string.endsWith('/'))
            outputDirectory = outputdirectory_string;
        else
            outputDirectory = outputdirectory_string.append('/');
    }

    if (recFileNameList.isEmpty()) {
        if (requireRecFile) {
            qDebug() << "No filename(s) given.  Must include -rec <filename> in arguments.";
            argumentReadOk = false;
            return;
        }
    } else {
        //Make sure the first file exists
        recFileName = recFileNameList.at(0);

        QFileInfo fi(recFileName);

        if (fi.exists()) {
            _fileNameFound = true;
            if (readConfig() < 0) {
                _configReadOk = false;
            }
        } else {
            qDebug() << "File could not be found: " << recFileName;
            argumentReadOk = false;
        }

    }

    if (conf_ptrs.spikeConf->deviceType == "neuropixels1") {

        if (maxGapSizeInterpolation != 0) {
            qDebug() << "Error: Workspaces using neuropixels1 does not support interpolation during export. Set -interp flag to 0.";
            argumentReadOk = false;
        }



    }


}

bool AbstractExportHandler::getNextPacket() {

    if (neuralDataHandler->getSamplesLeftToInterpolate() > 0) {
        neuralDataHandler->stepPreviousSample();
        currentTimeStamp++;
        return true;

    } else {
        //We have no gaps that need to be interpolated over, so we now read the next packet in the file
        //Read in a packet of data to make sure everything looks good
        if (!(filePtr->read(buffer.data(),filePacketSize) == filePacketSize)) {
            //If we can't read, we have reached the end of the file
            return false;
        }
        //Find the time stamp
        bufferPtr = buffer.data()+packetTimeLocation;
        tPtr = (uint32_t *)(bufferPtr);
        lastTimeStampInFile = nextTimeStampInFile;
        nextTimeStampInFile = *tPtr+startOffsetTime;


        if (channelPacketLocations.length() > 0) {
            bufferPtr = buffer.data()+channelPacketLocations[0];
            tempDataPtr = (int16_t*)(bufferPtr);
            calculateCARDataInPacket(tempDataPtr);            
            neuralDataHandler->processNextSamples(nextTimeStampInFile,tempDataPtr,CARData.data(),buffer.data());
        }



        //The number of expected data points between this time stamp and the last (1 if no data is missing)
        int64_t timestampJump = ((int64_t)nextTimeStampInFile-(int64_t)lastTimeStampInFile)-1;
        if (timestampJump < -1) {
            qDebug() << "Error with file: backwards timestamp at time " << nextTimeStampInFile << "and the last timestamp was" << lastTimeStampInFile;
            if (abortOnBackwardsTimestamp) {
                qDebug() << "Aborting";
                return false;
            }
        }

        if (neuralDataHandler->getSamplesLeftToInterpolate() > 0) {
            //Interpolation needed, so we recursively call this function
            qDebug() << "Interpolating data during gap of" << neuralDataHandler->getSamplesLeftToInterpolate() << "points after timestamp" << lastTimeStampInFile;
            //numberOfPacketsToInterpolate_done = 0;
            return getNextPacket();
        } else if (neuralDataHandler->gapOccured()) {
            qDebug() << "A gap of" << nextTimeStampInFile-lastTimeStampInFile << "packets occured in the file.";
        }

        currentTimeStamp = nextTimeStampInFile; //No interpolation needed, so we update the timestamp using that value in the file
    }



    return true;



}


void AbstractExportHandler::createFilters() {

    //Default filter creation-- use the filters that are in the config for each nTrode
    //Redefine in inheriting class if different behavior is required
    for (int i=0; i < channelPacketLocations.length(); i++) {
        channelFilters.push_back(new BesselFilter);
        channelFilters.last()->setSamplingRate(hardwareConf->sourceSamplingRate);

        if (useSpikeFilters) {
            //The user has given new filters to override the file's settings
            filterOn[i] = true;
            channelFilters.last()->setFilterRange(filterHighPass_SpikeBand,filterLowPass_SpikeBand);
        } //else if (useSpikeFilters && highPassFilters[i] != -1 && lowPassFilters[i] != -1) {
            //Use the spike filter settings in the file
          //  channelFilters.last()->setFilterRange(highPassFilters[i],lowPassFilters[i]);}
        if (useLFPFilters) {
            //Use the lfp filter settings in the file
            lfpFilterOn[i] = true;

            //Should be lfp filter
            channelFilters.last()->setFilterRange(0,lfpLowPassFilters[i]);
        }else {
            //Turn off filters
            filterOn[i] = false;
        }
    }
}

void AbstractExportHandler::setDefaultMenuEnabled(bool enabled) {
    defaultMenuEnabled = enabled;
}

bool AbstractExportHandler::openInputFile() {
    filePtr = new QFile;
    filePtr->setFileName(recFileName);
    dataStartLocBytes = findEndOfConfigSection(recFileName);
    packetsRead = 0;

    //open the raw data file
    if (!filePtr->open(QIODevice::ReadOnly)) {
        delete filePtr;
        qDebug() << "Error: could not open file for reading.";
        return false;
    }

    buffer.resize(filePacketSize*2); //we make the input buffer the size of two packets
    currentPacket.resize(filePacketSize*2);
    lastReadPacket.resize(filePacketSize*2);
    dataLastTimePoint.resize(channelPacketLocations.size()); //Stores the last data point.
    dataLastTimePoint.fill(0);
    pointsSinceLastLog=-1;

    //----------------------------------

    //skip past the config header
    if (!filePtr->seek(dataStartLocBytes)) {
        delete filePtr;
        qDebug() << "Error seeking in file";
        return false;
    }

    //Read in a packet of data to make sure everything looks good
    if (!(filePtr->read(buffer.data(),filePacketSize) == filePacketSize)) {
        delete filePtr;
        qDebug() << "Error: could not read from file";
        return false;
    }

    if (firstRecProcessed) {

        int readCode = readNextConfig(recFileName);
        if (readCode < 0) {
            return false;
        }
    }


    //Look in the trodesComments file to see if time was reset.  If so, create a time offset.
    QFileInfo fi(recFileName);
    QString commentsCheckName = fi.absolutePath() + "/"+ fi.completeBaseName() + ".trodesComments";
    QFileInfo commentsFileInfo(commentsCheckName);
    bool offsetDefined = false;
    startOffsetTime = 0;
    if (commentsFileInfo.exists()) {
        QFile commentsFile;
        commentsFile.setFileName(commentsFileInfo.absoluteFilePath());
        if (commentsFile.open(QIODevice::ReadOnly)) {
            QString line;

            while (!commentsFile.atEnd()) {
                line += commentsFile.readLine();

                int resetKeywordPos = line.indexOf("time reset");
                if (resetKeywordPos > -1) {

                    qDebug() << "Creating offset for file.";
                    startOffsetTime = currentTimeStamp+1;
                    offsetDefined = true;

                    break;

                }
                line = "";
            }
        }

    }


    //Find first time stamp
    bufferPtr = buffer.data()+packetTimeLocation;
    tPtr = (uint32_t *)(bufferPtr);

    if (firstRecProcessed && !offsetDefined) {
        qDebug() << "Calculating time offset for appended file...";
        if  (globalConf->systemTimeAtCreation == systemTimeAtCreation) {
            //Same file-- this happens when a single environmental file was merged with multiple sequential logger files
            qDebug() << "According to the date and time when the file was created, it was created at the same time as the previous file. No offset will be applied to the timestamps.";
           startOffsetTime = 0;
        } else if ((globalConf->systemTimeAtCreation > 0) && (globalConf->systemTimeAtCreation < systemTimeAtCreation)) {
            qDebug() << "WARNING: this file was created BEFORE the first file. Stitching files anyway with no time offset.";
            startOffsetTime = 0;
        } else if (globalConf->systemTimeAtCreation < 1) {
            qDebug() << "WARNING: this file does not contain information about the date and time when the file was created, so if the system clock was reset there is no way of knowing how much time elapsed between the two files. No offset will be applied.";
            startOffsetTime = 0;
        } else if ((globalConf->systemTimeAtCreation > 0) && (globalConf->systemTimeAtCreation > systemTimeAtCreation)) {
            //The user is stitching together two or more rec files, and the next rec file has a time stamp reset.
            //We need to use system time to calculate the offset.
            qDebug() << "Appending files using system time to calculate offset.";
            //Make sure the creation time of the next file is after the creation time of the first file.


            uint64_t tempOffset = timestampAtCreation+(uint64_t)((globalConf->systemTimeAtCreation-systemTimeAtCreation) * ((double)hardwareConf->sourceSamplingRate / 1000));
            if (globalConf->timestampAtCreation > tempOffset) {
                qDebug() << "Error in file offset calculation using system time. Stitching files anyway, ignoring system time.";
                startOffsetTime = 0;
            }
            tempOffset = tempOffset - globalConf->timestampAtCreation;
            if (tempOffset > lastTimeStampInFile) {
                qDebug() << "Created new timestamps based on computer's time when file was created. There was a" << (tempOffset-lastTimeStampInFile)/hardwareConf->sourceSamplingRate << "second period from the end of the last file to when this file was created.";
                startOffsetTime = tempOffset;
            } else {
                qDebug() << "ERROR: system clock when file created is before the end of the last file. Stitching files anyway, ignoring system time.";
                startOffsetTime = 0;

            }

        }

    }

    currentTimeStamp = *tPtr + startOffsetTime;
    lastTimeStampInFile = currentTimeStamp-1;

    //Seek back to begining of data
    filePtr->seek(dataStartLocBytes);

    //Initialize info about progress
    progressMarker = filePtr->size()/20;
    currentProgressMarker = 0;
    lastProgressMod = 0;
    if (progressMarker == 0) {
        progressMarker = 1;
    }

    //Calculate the number of points to skip to achieve the output rate
    if (outputSamplingRate == -1) {
        decimation = 1;
    } else {
        decimation = hardwareConf->sourceSamplingRate/outputSamplingRate;
        qDebug() << "Output rate downsampled to" << outputSamplingRate << "samples per sec.";
    }

    firstRecProcessed = true; //if multiple files are being stitched, this flag will be true for the next file.

    return true;

}

void AbstractExportHandler::printProgress() {
    //Print progress (based on location in input file)
    if ((filePtr->pos()%progressMarker) < lastProgressMod) {
        //printf("\r%d%%",currentProgressMarker);
        //fflush(stdout);
        qDebug() << currentProgressMarker << "%";
        currentProgressMarker = 100.0*((double)filePtr->pos()/(double)filePtr->size());
        //currentProgressMarker+=10;
    }
    lastProgressMod = (filePtr->pos()%progressMarker);
}

int AbstractExportHandler::findEndOfConfigSection(QString configFileName) {


    QFile file;
    int filePos = -1;

    if (!configFileName.isEmpty()) {
        file.setFileName(configFileName);
        if (!file.open(QIODevice::ReadOnly)) {
            return -1;
        }

        QFileInfo fi(configFileName);
        QString ext = fi.suffix();
        if (ext.compare("rec") == 0) {
            //this is a rec file with a configuration in the header

            QString configContent;
            QString configLine;
            bool foundEndOfConfig = false;

            while (file.pos() < 1000000) {
                configLine += file.readLine();
                configContent += configLine;
                if (configLine.indexOf("</Configuration>") > -1) {
                    foundEndOfConfig = true;
                    break;
                }
                configLine = "";
            }

            if (foundEndOfConfig) {
                filePos = file.pos();
            }
        }
        file.close();
        return filePos;
    }
    return -1;
}

int AbstractExportHandler::readConfig() {
    //Read the config .xml file

    QString configFileName = recFileName;
    startOffsetTime = 0;

    QFileInfo fI(configFileName);
    QString baseName = fI.completeBaseName();

    QString workspaceCheckName = fI.absolutePath() + "/"+ baseName + ".trodesconf";
    QFileInfo workspaceFile(workspaceCheckName);

    bool externalChannelSettings = false;

    QDomDocument doc("TrodesConf"); //Channel settings. Can be reconfigured with external workspce
    QDomDocument sessionAttributes("TrodesConf"); //Settings that were logged at the time of the recording. Can only be retrived from the rec file

    if (!externalWorkspaceFile.isEmpty()) {
        //An external workspace file should be used
        //this is a normal xml-based config file

        QString errString = externalWorkspace.readTrodesConfig(externalWorkspaceFile);
        if (!errString.isEmpty()) {
            qDebug() << "There was an error parsing" << externalWorkspaceFile << "." << errString;
            return -1;
        }
        //filePos = findEndOfConfigSection(recFileName);
        externalChannelSettings = true;

    } else if (workspaceFile.exists()) {
        //Check if there is a .trodeconf file with the same name in the same directory.
        //If so, use that.

        qDebug() << "Using the following workspace file: " << workspaceFile.fileName();


        externalWorkspaceFile = workspaceFile.absoluteFilePath();
        QString errString = externalWorkspace.readTrodesConfig(workspaceFile.absoluteFilePath());
        if (!errString.isEmpty()) {
            qDebug() << "There was an error parsing" << externalWorkspaceFile << "." << errString;
            return -1;
        }

        externalChannelSettings = true;
    }


    //Load in workspace embedded in the rec file

    QString errorString = embeddedWorkspace.readTrodesConfig(recFileName);
    if (!errorString.isEmpty()) {
        qDebug() << "There is an issue reading in the workspace embedded in the rec file: " << errorString;
        return -1;
    }


    if (!externalChannelSettings) {
        //No external workspace, so load all settings from those embedded in the rec file
        globalConf = &embeddedWorkspace.globalConf;
        hardwareConf = &embeddedWorkspace.hardwareConf;
        headerConf = &embeddedWorkspace.headerConf;
        streamConf = &embeddedWorkspace.streamConf;
        spikeConf = &embeddedWorkspace.spikeConf;
        moduleConf = &embeddedWorkspace.moduleConf;
        benchConfig = &embeddedWorkspace.benchConfig;
        loadedWorkspacePtr = &embeddedWorkspace;
        conf_ptrs.globalConf = globalConf;
        conf_ptrs.hardwareConf = hardwareConf;
        conf_ptrs.headerConf = headerConf;
        conf_ptrs.streamConf = streamConf;
        conf_ptrs.spikeConf = spikeConf;
        conf_ptrs.moduleConf = moduleConf;
        conf_ptrs.benchConfig = benchConfig;
    } else {
        //The global settings are still grabbed from the rec file, but everything else comes from the external file.
        globalConf = &embeddedWorkspace.globalConf;
        hardwareConf = &externalWorkspace.hardwareConf;
        headerConf = &externalWorkspace.headerConf;
        streamConf = &externalWorkspace.streamConf;
        spikeConf = &externalWorkspace.spikeConf;
        moduleConf = &externalWorkspace.moduleConf;
        benchConfig = &externalWorkspace.benchConfig;
        loadedWorkspacePtr = &externalWorkspace;
        conf_ptrs.globalConf = globalConf;
        conf_ptrs.hardwareConf = hardwareConf;
        conf_ptrs.headerConf = headerConf;
        conf_ptrs.streamConf = streamConf;
        conf_ptrs.spikeConf = spikeConf;
        conf_ptrs.moduleConf = moduleConf;
        conf_ptrs.benchConfig = benchConfig;

    }

    systemTimeAtCreation = globalConf->systemTimeAtCreation;
    timestampAtCreation = globalConf->timestampAtCreation;

    dataStartLocBytes = findEndOfConfigSection(recFileName);
    return dataStartLocBytes; //return the position of the file where data begins

}


int AbstractExportHandler::readNextConfig(QString configFileName) {
    //Read the next config .xml file.  Since critical info had already been calulated from the first file, we only need to
    //get info unique to this file.

    int filePos = 0;
    //startOffsetTime = 0;



    QFileInfo fI(configFileName);
    QString baseName = fI.completeBaseName();

    QString workspaceCheckName = fI.absolutePath() + "/"+ baseName + ".trodesconf";
    QFileInfo workspaceFile(workspaceCheckName);



    QDomDocument doc("TrodesConf");


    if (!externalWorkspaceFile.isEmpty()) {
        //An external workspace file should be used
        //this is a normal xml-based config file

        QFile file;
        file.setFileName(externalWorkspaceFile);
        if (!file.open(QIODevice::ReadOnly)) {
            qDebug() << QString("File %1 not found").arg(externalWorkspaceFile);
            return -1;
        }

        if (!doc.setContent(&file)) {
            file.close();
            qDebug("XML didn't read properly in external workspace file.");
            return -1;
        }
        filePos = findEndOfConfigSection(recFileName);

    } else if (workspaceFile.exists()) {
        //Check if there is a .trodeconf file with the same name in the same directory.
        //If so, use that.

        qDebug() << "Using the following workspace file: " << workspaceFile.fileName();
        QFile file;
        file.setFileName(workspaceFile.absoluteFilePath());
        if (!file.open(QIODevice::ReadOnly)) {
            qDebug() << QString("File %1 not found").arg(workspaceFile.absoluteFilePath());
            return -1;
        }

        if (!doc.setContent(&file)) {
            file.close();
            qDebug("XML didn't read properly in external workspace file.");
            return -1;
        }
        filePos = findEndOfConfigSection(recFileName);
    } else if (!configFileName.isEmpty()) {
        //If the other conditions are not met, use the settings embedded in the recording file.
        QFile file;
//        bool isRecFile = false;
        file.setFileName(configFileName);
        if (!file.open(QIODevice::ReadOnly)) {
            qDebug() << QString("File %1 not found").arg(configFileName);
            return -1;
        }

        QFileInfo fi(configFileName);
        QString ext = fi.suffix();

        if (ext.compare("rec") == 0) {
            //this is a rec file with a configuration in the header
//            isRecFile = true;
            QString configContent;
            QString configLine;
            bool foundEndOfConfig = false;

            while (!file.atEnd()) {
                configLine += file.readLine();
                configContent += configLine;
                if (configLine.indexOf("</Configuration>") > -1) {
                    //qDebug() << "End of config header found at " << file.pos();
                    foundEndOfConfig = true;
                    break;
                }
                configLine = "";
            }

            if (foundEndOfConfig) {
                filePos = file.pos();
                if ((externalWorkspaceFile.isEmpty()) && (!doc.setContent(configContent))) {
                    file.close();
                    qDebug("Config header in didn't read properly.");
                    return -1;
                }
            }
        }
        file.close();
    }

    //Load in workspace
    QString errorString = embeddedWorkspace.readTrodesConfig(doc);
    if (!errorString.isEmpty()) {
        qDebug() << "There is an issue reading in the workspace: " << errorString;
        return -1;
    }

    globalConf = &embeddedWorkspace.globalConf; //We only care about the global info, since the rest was already loaded before


    dataStartLocBytes = filePos;
    return filePos; //return the position of the file where data begins



    /*QDomElement root = doc.documentElement();
    if (root.tagName() != "Configuration") {
        qDebug("Configuration not root node.");
        return -1;
    }

    QDomNodeList globalConfigList = root.elementsByTagName("GlobalConfiguration");
    if (globalConfigList.length() > 1) {
        qDebug() << "Config file error: multiple GlobalConfiguration sections found in configuration file.";
        return -7;
    }
    QDomNode globalnode = globalConfigList.item(0);
    int globalErrorCode = globalConf->loadFromXML(globalnode);
    if (globalErrorCode != 0) {
        qDebug() << "configuration: nsParseTrodesConfig(): Failed to load global configuration err: " << globalErrorCode;
        return globalErrorCode;
    }

    dataStartLocBytes = filePos;
    return filePos; //return the position of the file where data begins
    */

}

/*void AbstractExportHandler::calculateChannelInfo() {
    //bool useAllChannelsPerNtrode = true;


    QList<int> devicePrefOrder;

    for (int i=0;i<hardwareConf->devices.length();i++) {

        int insertInd = 0;
        for (int sortInd= 0; sortInd < sortedDeviceList.length(); sortInd++) {
            if (hardwareConf->devices[i].packetOrderPreference < sortedDeviceList[sortInd]) {
                break;
            } else {
                insertInd++;
            }
        }


        devicePrefOrder.insert(insertInd,hardwareConf->devices[i].packetOrderPreference);
        sortedDeviceList.insert(insertInd,i);
    }

    if(hardwareConf->sysTimeIncluded) {
        packetHeaderSize = (2*hardwareConf->headerSize)+12; //Aux info plus 4-byte timestamp and 8-byte system clock
    } else {
        packetHeaderSize = (2*hardwareConf->headerSize)+4; //Aux info plus 4-byte timestamp
    }

    packetTimeLocation = packetHeaderSize-4; //timestamp are always the last 4 bytes of the packet header


    QList<int> sortedChannelList;
    //Gather all HW channels

    for (int n = 0; n < spikeConf->ntrodes.length(); n++) {

        spikeDetectors.push_back(new ThresholdSpikeDetector(this,n,loadedWorkspacePtr));
        nTrodeInfo nInf;
        nInf.numChannels = spikeConf->ntrodes[n]->hw_chan.length();
        nInf.lfpChannel = spikeConf->ntrodes[n]->moduleDataChan; //This should really be called lfpDataChan...       
        if (useRefs) {
            //User wants to use the reference settings in the file
            nInf.nTrodeRefNtrode = spikeConf->ntrodes[n]->refNTrode;
            nInf.nTrodeRefNtrodeChannel = spikeConf->ntrodes[n]->refChan;
            nInf.nTrodeRefOn = spikeConf->ntrodes[n]->refOn;
            nInf.lfpRefOn = spikeConf->ntrodes[n]->lfpRefOn;
        } else {
            //User wants to to turn off all referencing
            nInf.nTrodeRefNtrode = -1;
            nInf.nTrodeRefNtrodeChannel = -1;
            nInf.nTrodeRefOn = false;
            nInf.lfpRefOn = false;
        }


        if (useSpikeFilters) {
            //User wants to use spike filter settings in file
            nInf.filterOn = spikeConf->ntrodes[n]->filterOn;
            nInf.nTrodeLowPassFilter = spikeConf->ntrodes[n]->highFilter;
            nInf.nTrodeHighPassFilter = spikeConf->ntrodes[n]->lowFilter;

        } else if (useLFPFilters) {
            //User wants to use spike filter settings in file
            nInf.lfpFilterOn = spikeConf->ntrodes[n]->lfpFilterOn;
            nInf.nTrodeLowPassFilter = spikeConf->ntrodes[n]->moduleDataHighFilter;
            nInf.nTrodeHighPassFilter = 0;
        } else if (filterHighPass_SpikeBand != -1 || filterLowPass_SpikeBand != -1){
            //User has entered new filter settings
            nInf.filterOn = true;
            nInf.nTrodeLowPassFilter = filterLowPass_SpikeBand;
            nInf.nTrodeHighPassFilter = filterHighPass_SpikeBand;

        } else {
            //User wants no filters
            nInf.filterOn = false;
            nInf.lfpFilterOn = false;
            nInf.nTrodeLowPassFilter = -1;
            nInf.nTrodeHighPassFilter = -1;
        }


        nTrodeSettings.push_back(nInf);
        for (int c = 0; c < spikeConf->ntrodes[n]->hw_chan.length(); c++) {
            bool channelalreadyused = false;

            int tempHWRead = spikeConf->ntrodes[n]->hw_chan[c];
            //make sure there is not a repeat channel
            for (int ch=0;ch<sortedChannelList.length();ch++) {
                if (sortedChannelList[ch]==tempHWRead) {
                    channelalreadyused = true;
                }
            }
            //qDebug() << "Read" << spikeConf->ntrodes[n]->unconverted_hw_chan[c] << tempHWRead;
            if (!channelalreadyused) {
                sortedChannelList.push_back(tempHWRead);
            }

        }
    }

    //Sort the channels
    std::sort(sortedChannelList.begin(),sortedChannelList.end());

    //Remember how many channels are actually saved in the file
    //(may be different from how many channels came from recording hardware)

    if (globalConf->saveDisplayedChanOnly) {
        //numChannelsInFile = sortedChannelList.length();
        numChannelsInFile = streamConf->nChanConfigured;
    } else {
        numChannelsInFile = hardwareConf->NCHAN;
    }

    int lastChannel = -1;
    int unusedChannelsSoFar = 0;
    for (int i=0;i<sortedChannelList.length();i++) {
        if ((sortedChannelList[i]-lastChannel > 1) && (globalConf->saveDisplayedChanOnly)){
            unusedChannelsSoFar = unusedChannelsSoFar + (sortedChannelList[i]-lastChannel-1);

        }
        channelPacketLocations.push_back(packetHeaderSize+(2*(sortedChannelList[i]-unusedChannelsSoFar)));
        lastChannel = sortedChannelList[i];
    }

    if (globalConf->saveDisplayedChanOnly) {
        filePacketSize = 2*(numChannelsInFile) + packetHeaderSize+paddingBytes;
    } else {
        filePacketSize = 2*(hardwareConf->NCHAN) + packetHeaderSize+paddingBytes;
    }


    //Now we go back and gather some more info about each channel...
    for (int i=0;i<sortedChannelList.length();i++) {
        bool foundthischannel = false;
        for (int n = 0; n < spikeConf->ntrodes.length(); n++) {
            if (!foundthischannel) {
            for (int c = 0; c < spikeConf->ntrodes[n]->hw_chan.length(); c++) {
                int tempHWRead = spikeConf->ntrodes[n]->hw_chan[c];
                if (tempHWRead == sortedChannelList[i]) {

                    //Keep a record of the channel's nTrode ID
                    nTrodeForChannels.push_back(spikeConf->ntrodes[n]->nTrodeId);
                    //qDebug() << tempHWRead << spikeConf->ntrodes[n]->unconverted_hw_chan[c] << "ntrode" << spikeConf->ntrodes[n]->nTrodeId << c;
                    nTrodeIndForChannels.push_back(n);
                    //Keep a record of the channel within the nTrode
                    channelInNTrode.push_back(c);
                    //Keep a record of whether the reference is on for this channel
                    refOn.push_back(spikeConf->ntrodes[n]->refOn);
                    lfpRefOn.push_back(spikeConf->ntrodes[n]->lfpRefOn);
                    groupRefOn.push_back(spikeConf->ntrodes[n]->groupRefOn);
                    refGroup.push_back(spikeConf->ntrodes[n]->refGroup);

                    //Keep a record of the filter settings for each nTrode
                    filterOn.push_back(spikeConf->ntrodes[n]->filterOn);
                    lfpFilterOn.push_back(spikeConf->ntrodes[n]->lfpFilterOn);

                    if (spikeConf->ntrodes[n]->filterOn) {
                        lowPassFilters.push_back(spikeConf->ntrodes[n]->highFilter);
                        highPassFilters.push_back(spikeConf->ntrodes[n]->lowFilter);
                    } else {
                        lowPassFilters.push_back(-1);
                        highPassFilters.push_back(-1);
                    }

                    lfpLowPassFilters.push_back(spikeConf->ntrodes[n]->moduleDataHighFilter);

                    //if (spikeConf->ntrodes[n]->lfpFilterOn) {
                    //    lfpLowPassFilters.push_back(spikeConf->ntrodes[n]->moduleDataHighFilter);
                    //} else {
                    //    lfpLowPassFilters.push_back(-1);

                    //}

                    //Find the packet location in the file of the channel's reference
                    int tempRefHWChan = spikeConf->ntrodes[spikeConf->ntrodes[n]->refNTrode]->hw_chan[spikeConf->ntrodes[n]->refChan];
                    int foundRefIndex = -1;
                    for (int refLookup=0; refLookup < sortedChannelList.length(); refLookup++) {
                        if (sortedChannelList[refLookup] == tempRefHWChan) {
                            foundRefIndex = refLookup;
                            refPacketLocations.push_back(channelPacketLocations[foundRefIndex]);
                            break;
                        }
                    }
                    if (foundRefIndex == -1) {
                        //No ref match was found
                        refPacketLocations.push_back(-1);
                    }

                    //qDebug() << QString("nt%1ch%2").arg(spikeConf->ntrodes[n]->nTrodeId).arg(c+1);
                    foundthischannel = true;
                    break;
                }
            }
            }
        }
    }

}*/

void AbstractExportHandler::calculateChannelInfo() {

    QList<int> devicePrefOrder;

    for (int i=0;i<hardwareConf->devices.length();i++) {

        int insertInd = 0;
        for (int sortInd= 0; sortInd < sortedDeviceList.length(); sortInd++) {
            if (hardwareConf->devices[i].packetOrderPreference < sortedDeviceList[sortInd]) {
                break;
            } else {
                insertInd++;
            }
        }
        devicePrefOrder.insert(insertInd,hardwareConf->devices[i].packetOrderPreference);
        sortedDeviceList.insert(insertInd,i);
    }

    if(hardwareConf->sysTimeIncluded) {
        packetHeaderSize = (2*hardwareConf->headerSize)+12; //Aux info plus 4-byte timestamp and 8-byte system clock
    } else {
        packetHeaderSize = (2*hardwareConf->headerSize)+4; //Aux info plus 4-byte timestamp
    }

    packetTimeLocation = packetHeaderSize-4; //timestamp are always the last 4 bytes of the packet header

    QList<int> sortedChannelList;
    //Gather all HW channels

    for (int n = 0; n < spikeConf->ntrodes.length(); n++) {

        nTrodeIndexList.push_back(n);

        spikeDetectors.push_back(new ThresholdSpikeDetector(this,n,loadedWorkspacePtr));

        if (override_Spike_Refs) {
            if (useRefs) {
                //User wants to turn on all refs for the spike band, overriding settings in the file
                spikeConf->ntrodes[n]->refOn = true;
            } else {
                //User wants to to turn off all referencing
                spikeConf->ntrodes[n]->refOn = false;
            }
        }

        if (override_LFPRefs) {
            if (useLFPRefs) {
                //User wants to turn on all refs for the lfp band, overriding settings in the file
                spikeConf->ntrodes[n]->lfpRefOn = true;
            } else {
                //User wants to to turn off all referencing  for lfp
                spikeConf->ntrodes[n]->lfpRefOn = false;
            }
        }

        if (override_RAWRefs) {
            if (useRAWRefs) {
                //User wants to turn on all refs for the lfp band, overriding settings in the file
                spikeConf->ntrodes[n]->rawRefOn = true;
            } else {
                //User wants to to turn off all referencing  for lfp
                spikeConf->ntrodes[n]->rawRefOn = false;
            }
        }

        //TODO: override_rawRefs...

        if (override_useSpikeFilters) {

            if (useSpikeFilters) {
                //User wants to turn on all filters for the spike band, overriding settings in the file
                spikeConf->ntrodes[n]->filterOn = true;
            } else {
                //User wants to to turn off all spike filters
                spikeConf->ntrodes[n]->filterOn = false;
            }

            if (filterLowPass_SpikeBand != -1) {
                spikeConf->ntrodes[n]->highFilter = filterLowPass_SpikeBand;
            }
            if (filterHighPass_SpikeBand != -1) {
                spikeConf->ntrodes[n]->lowFilter = filterHighPass_SpikeBand;
            }
        }

        if (override_useLFPFilters) {
            if (useLFPFilters) {
                //User wants to turn on all filters for the lfp band, overriding settings in the file
                spikeConf->ntrodes[n]->lfpFilterOn = true;
            } else {
                //User wants to to turn off all lfp filters
                spikeConf->ntrodes[n]->lfpFilterOn = false;
            }
            if (filterLowPass_LFPBand != -1) {
                spikeConf->ntrodes[n]->moduleDataHighFilter = filterLowPass_LFPBand;
            }
        }

        if (override_useNotchFilters) {
            if (useNotchFilters) {
                //User wants to turn on all notch filters, overriding settings in the file
                spikeConf->ntrodes[n]->notchFilterOn = true;
            } else {
                //User wants to to turn off all notch filters
                spikeConf->ntrodes[n]->notchFilterOn = false;
            }
        }

        //nTrodeSettings.push_back(nInf);
        for (int c = 0; c < spikeConf->ntrodes[n]->hw_chan.length(); c++) {
            bool channelalreadyused = false;

            int tempHWRead = spikeConf->ntrodes[n]->hw_chan[c];
            //make sure there is not a repeat channel
            for (int ch=0;ch<sortedChannelList.length();ch++) {
                if (sortedChannelList[ch]==tempHWRead) {
                    channelalreadyused = true;
                }
            }
            //qDebug() << "Read" << spikeConf->ntrodes[n]->unconverted_hw_chan[c] << tempHWRead;
            if (!channelalreadyused) {
                sortedChannelList.push_back(tempHWRead);
            }

        }
    }

    //Sort the channels
    std::sort(sortedChannelList.begin(),sortedChannelList.end());

    //Remember how many channels are actually saved in the file
    //(may be different from how many channels came from recording hardware)

    if (globalConf->saveDisplayedChanOnly) {
        //numChannelsInFile = sortedChannelList.length();
        numChannelsInFile = streamConf->nChanConfigured;
    } else {
        numChannelsInFile = hardwareConf->NCHAN;
    }

    int lastChannel = -1;
    int unusedChannelsSoFar = 0;
    for (int i=0;i<sortedChannelList.length();i++) {
        if ((sortedChannelList[i]-lastChannel > 1) && (globalConf->saveDisplayedChanOnly)){
            unusedChannelsSoFar = unusedChannelsSoFar + (sortedChannelList[i]-lastChannel-1);

        }
        channelPacketLocations.push_back(packetHeaderSize+(2*(sortedChannelList[i]-unusedChannelsSoFar)));
        lastChannel = sortedChannelList[i];
    }

    if (globalConf->saveDisplayedChanOnly) {
        filePacketSize = 2*(numChannelsInFile) + packetHeaderSize+paddingBytes;
    } else {
        filePacketSize = 2*(hardwareConf->NCHAN) + packetHeaderSize+paddingBytes;
    }


    //Now we go back and gather some more info about each channel...
    for (int i=0;i<sortedChannelList.length();i++) {
        bool foundthischannel = false;
        for (int n = 0; n < spikeConf->ntrodes.length(); n++) {
            if (!foundthischannel) {
                for (int c = 0; c < spikeConf->ntrodes[n]->hw_chan.length(); c++) {
                    int tempHWRead = spikeConf->ntrodes[n]->hw_chan[c];
                    if (tempHWRead == sortedChannelList[i]) {
                        if (globalConf->saveDisplayedChanOnly) {
                            spikeConf->ntrodes[n]->hw_chan[c] = i; //Change the actual value in the workspace
                        }
                        //qDebug() << "Original HW" << sortedChannelList[i] << "Packet ch location" << i << "nTrode ID" << spikeConf->ntrodes[n]->nTrodeId << "channel" << c;


                        //Keep a record of the channel's nTrode ID
                        nTrodeForChannels.push_back(spikeConf->ntrodes[n]->nTrodeId);
                        //qDebug() << tempHWRead << spikeConf->ntrodes[n]->unconverted_hw_chan[c] << "ntrode" << spikeConf->ntrodes[n]->nTrodeId << c;
                        nTrodeIndForChannels.push_back(n);
                        //Keep a record of the channel within the nTrode
                        channelInNTrode.push_back(c);

                        foundthischannel = true;
                        break;
                    }
                }
            }
        }
    }


    for(int i = 0; i < spikeConf->carGroups.length(); ++i){

        for (int j = 0; j < spikeConf->carGroups[i].chans.length(); j++) {

            int tmpCARHWChan = sortedChannelList.indexOf(spikeConf->carGroups[i].chans[j].hw_chan);
            if (tmpCARHWChan == -1) {
                qDebug() << "There is a problem with the Common Average Referencing (CAR) channel list. One of the designated CAR channels was not recorded.  CAR group" << i << "Channel" << j;
                //TODO: remove all use of this CAR group from the workspace
                return;
            } else {
                if (tmpCARHWChan != spikeConf->carGroups[i].chans[j].hw_chan) {
                    //The channel HW channel needs to be modified becuase not all channels were recorded.
                    spikeConf->carGroups[i].chans[j].hw_chan = tmpCARHWChan;
                }

            }
        }
        CARData.push_back(0); //Where the computed CAR data will be stored for each packet
    }


    //avxsupported = false;
    avxsupported = enableAVX && detectAVXCapabilities();
    if (avxsupported) {
        qDebug() << "AVX support detected. Using AVX processing.";
    } else {
        //qDebug() << "Either AVX is turned off or no AVX support detected. Using basic processing.";
    }
    if(avxsupported){
        uint32_t extraSteps = 0;

        if (conf_ptrs.spikeConf->deviceType == "neuropixels1") {
            extraSteps |= AbstractNeuralDataHandler::ExtraProcessorType::NeuroPixels1;
        }

        extraSteps |= AbstractNeuralDataHandler::ExtraProcessorType::Export;

        neuralDataHandler = new VectorizedNeuralDataHandler(conf_ptrs, nTrodeIndexList,extraSteps);
    }
    else {

        if (conf_ptrs.spikeConf->deviceType == "neuropixels1") {
            qDebug() << "Error: Workspaces using neuropixels1 hardware MUST use the -avx flag set to 1.";
            exit(1);
        }

        neuralDataHandler = new NeuralDataHandler(conf_ptrs,nTrodeIndexList);
    }


    if ((maxGapSizeInterpolation < 0) || (maxGapSizeInterpolation > 4294967295)) {
        maxGapSizeInterpolation = 4294967295; //infinite interpolation
    }
    if (maxGapSizeInterpolation > 0) {
        neuralDataHandler->setInterpMode(true,(uint32_t)maxGapSizeInterpolation); //With this, the handler will interpolate across data gaps
    }
    neuralDataHandler->setSpikeInvert(invertSpikes);

}

void AbstractExportHandler::calculateCARDataInPacket(int16_t *startOfNeuralData) {

    //Calculate the common average reference values to be subtracted from samples that use them
    for (int i=0; i < spikeConf->carGroups.length(); ++i){
    if(spikeConf->carGroups[i].chans.length() == 0){
        //if this car group is empty, skip it
        continue;
    }
    int32_t accum = 0;
    for(auto const &chan : spikeConf->carGroups[i].chans){
        accum += startOfNeuralData[chan.hw_chan];
    }

    //divide by 0 exception not possible since if chans is empty, skip calculations
    CARData[i] = accum/spikeConf->carGroups[i].chans.length();

    }

}
