#include <QtCore>
#include <QCoreApplication>
#include "txtexporthandler.h"

int main(int argc, char *argv[]) {
    QCoreApplication a(argc, argv);
    TXTExportHandler h(a.arguments());

    if (h.wasHelpMenuDisplayed()) {
        return 0;
    }

    if (!h.fileNameSpecified()) {
        qDebug() << "Error: No input filename given.  Must include -i or --input <filename> in arguments.";
        return -1;
    }

    if (!h.fileNameFound()) {
        qDebug() << "Error: Input filename is not a file. Exiting.";
        return -1;
    }

    if (!h.argumentsSupported()) {
        qDebug() << "Error: one or more argument flags is not supported. Use -h flag to print usage.";
        return -1;
    }

    return h.processData();

}

