#include "datexporthandler.h"
#include <algorithm>
//#include <iostream>

DATExportHandler::DATExportHandler(QStringList arguments):
    AbstractExportHandler(arguments)
{

    supportedDataCategories = DataCategories::SpikeBand | DataCategories::ContinuousOutput;

    //Set default behavior
    combineNeuralChannels = true;
    combineAuxChannels = false;
    maxGapSizeInterpolation = 30000;
    invertSpikes = false;  //offline sorter does not want inverted spikes.



    parseArguments();

    /*
    Do custom argument checks here like this:
    if (ARGVAL != REQUIREMENT) {
        qDebug() << "Error: ....";
        argumentReadOk = false;
    }   
    */

    //Parse custom arguments
    //parseCustomArguments(argumentsProcessed);

    if (argumentsProcessed != argumentList.length()-1) {
        _argumentsSupported = false;
        return;
    }
}

DATExportHandler::~DATExportHandler()
{

}

void DATExportHandler::printHelpMenu() {
    //printf("-outputrate <integer>  -- The sampling rate of the output file. \n"
    //       );

    printf("\nUsed to extract continous data from a raw rec file and save to binary file that can be imported with Offline Sorter. \n");
    printf("Usage:  exportofflinesorter -rec INPUTFILENAME OPTION1 VALUE1 OPTION2 VALUE2 ...  \n\n"
           "Input arguments \n");
    printf("Defaults:\n"
           "    -outputrate -1 (full) \n"          
           "    -interp 30000 (Will interpolate gaps up to 30000 points) \n"
           "    -combinechannels 1 (combine neural channels into one file. Instead use one file per channel.) \n"
           "    -appendauxdata 0 (do not append DIO and analog data into the file)\n");

    //AbstractExportHandler::printHelpMenu();
}

void DATExportHandler::parseArguments() {
    //Parse extra arguments not handled by the base class

    int optionInd = 1;
    QString combine_string = "";
    QString appendauxdata_string = "";
    while (optionInd < argumentList.length()) {

        if ((argumentList.at(optionInd).compare("-h",Qt::CaseInsensitive)==0)) {
            //printCustomMenu();
            //return;
            printHelpMenu();
        } else if ((argumentList.at(optionInd).compare("-combinechannels",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            combine_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        } else if ((argumentList.at(optionInd).compare("-appendauxdata",Qt::CaseInsensitive)==0) && (argumentList.length() > optionInd+1)) {
            appendauxdata_string = argumentList.at(optionInd+1);
            optionInd++;
            argumentsProcessed = argumentsProcessed+2;
        }
        optionInd++;
    }
    if (!combine_string.isEmpty()) {
        bool ok1;

        combineNeuralChannels = combine_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "combinechannels argument could not be resolved into an integer. Use 0 (false) or 1 (true)";
            argumentReadOk = false;
            return;
        }
    }
    if (!appendauxdata_string.isEmpty()) {
        bool ok1;

        combineAuxChannels = appendauxdata_string.toInt(&ok1);
        if (!ok1) {
            //Conversion to int didn't work
            qDebug() << "appendauxdata argument could not be resolved into an integer. Use 0 (false) or 1 (true)";
            argumentReadOk = false;
            return;
        }
    }

    if (!combineNeuralChannels && combineAuxChannels) {
        qDebug() << "appendauxdata can only be set to 1 if combinechannels is also set to 1";
        argumentReadOk = false;
        return;
    }

    AbstractExportHandler::parseArguments();
}


int DATExportHandler::processData() {
    qDebug() << "Exporting to Offline Sorter format file...";

    //Calculate the packet positions for each channel that we are extracting, plus
    //other critical info (number of saved channels, reference info, etc).
    calculateChannelInfo();
    //createFilters();

    qint32 numPointsWrittenToFile = 0;

    if (!openInputFile()) {
        return -1;
    }

    int16_t tmpVal;

    //Create a directory for the output files located in the same place as the source file
    QFileInfo fi(recFileName);
    QString fileBaseName;
    if (outputFileName.isEmpty()) {
        fileBaseName = fi.completeBaseName();
    } else {
        fileBaseName = outputFileName;
    }

    QString directory;
    if(outputDirectory.isEmpty()) {
        directory = fi.absolutePath();
    } else {
        directory = outputDirectory;
    }

    QString saveLocation = directory+QString(QDir::separator())+fileBaseName+".offlinesorter"+QString(QDir::separator());
    QDir dir(saveLocation);
    if (!dir.exists()) {
        if (!dir.mkpath(".")) {
            qDebug() << "Error creating OfflineSorter directory.";
            return -1;
        }
    }

    QList<QFile*> neuroFilePtrs;
    QList<QDataStream*> neuroStreamPtrs;

    QList<QFile*> timeFilePtrs;
    QList<QDataStream*> timeStreamPtrs;



    //Create an output file for the timestamps
    //*****************************************
    timeFilePtrs.push_back(new QFile);
    timeFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".timestamps.dat"));
    if (!timeFilePtrs.last()->open(QIODevice::WriteOnly)) {
        qDebug() << "Error creating output file.";
        return -1;
    }
    timeStreamPtrs.push_back(new QDataStream(timeFilePtrs.last()));
    timeStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);


    QFile* infoTextFile = new QFile;
    QString infoLine;
    if (combineNeuralChannels) {
        //Create a channel info text file

        infoTextFile->setFileName(saveLocation+fileBaseName+QString(".info.txt"));
        if (!infoTextFile->open(QIODevice::WriteOnly)) {
            qDebug() << "Error creating output file.";
            return -1;
        }
        infoLine = QString("Binary file contents\n\nData offset: 0\nDigitizing frequency: %1\nA/D Converter resolution: 16 bits\nMaximum voltage: 6.39 mV\n\n").arg(hardwareConf->sourceSamplingRate);
        infoTextFile->write(infoLine.toLocal8Bit());
    }



    //Create an output file for the neural data
    //*****************************************

    int totalChannelsInComboFile = 0;
    int totalAuxDataChannelsInComboFile = 0;

    //Create the output files
    if (combineNeuralChannels) {
        //All neural channels into one file
        neuroFilePtrs.push_back(new QFile);
        neuroFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".os.dat"));
        if (!neuroFilePtrs.last()->open(QIODevice::WriteOnly)) {
            qDebug() << "Error creating output file.";
            return -1;
        }
        neuroStreamPtrs.push_back(new QDataStream(neuroFilePtrs.last()));
        neuroStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);

        totalChannelsInComboFile += spikeConf->ntrodes.length();

        for (int i=0; i<spikeConf->ntrodes.length(); i++) {
            infoLine = QString("Channel %1: NTrode %2\n").arg(i+1).arg(spikeConf->ntrodes[i]->nTrodeId);
            infoTextFile->write(infoLine.toLocal8Bit());
        }



    } else {
        //One file per ntrode
        for (int i=0; i<spikeConf->ntrodes.length(); i++) {
            neuroFilePtrs.push_back(new QFile);

            neuroFilePtrs.last()->setFileName(saveLocation+fileBaseName+QString(".os_nt%1.dat").arg(spikeConf->ntrodes[i]->nTrodeId));

            if (!neuroFilePtrs.last()->open(QIODevice::WriteOnly)) {
                qDebug() << "Error creating output file.";
                return -1;
            }
            neuroStreamPtrs.push_back(new QDataStream(neuroFilePtrs.last()));
            neuroStreamPtrs.last()->setByteOrder(QDataStream::LittleEndian);
        }
    }

    QVector<int16_t> lastValues;
    QVector<int> interleavedDataBytes;
    uint8_t *interleavedDataIDBytePtr;

    if (combineAuxChannels) {
        //Append aux channels into the file

        for (int auxChInd = 0; auxChInd < headerConf->headerChannels.length(); auxChInd++) {
            lastValues.push_back(0);
            if (headerConf->headerChannels[auxChInd].dataType == DeviceChannel::INT16TYPE) {
                interleavedDataBytes.push_back(headerConf->headerChannels[auxChInd].interleavedDataIDByte);
            } else {
                interleavedDataBytes.push_back(-1);
            }
            infoLine = QString("Channel %1: Aux channel %2\n").arg(auxChInd+1+totalChannelsInComboFile).arg(headerConf->headerChannels[auxChInd].idString);
            infoTextFile->write(infoLine.toLocal8Bit());
        }
        totalAuxDataChannelsInComboFile += headerConf->headerChannels.length();
        totalChannelsInComboFile += headerConf->headerChannels.length();

    }

    if (combineNeuralChannels) {
        infoTextFile->flush();
        infoTextFile->close();
    }


    //************************************************

    int inputFileInd = 0;

    while (inputFileInd < recFileNameList.length()) {
        if (inputFileInd > 0) {
            //There are multiple files that need to be stiched together. It is assumed that they all have
            //the exact same header section.
            recFileName = recFileNameList.at(inputFileInd);
            uint32_t lastFileTStamp = currentTimeStamp;

            qDebug() << "\nAppending from file: " << recFileName;
            QFileInfo fi(recFileName);

            if (!fi.exists()) {
                qDebug() << "File could not be found: " << recFileName;
                break;
            }
            if (!openInputFile()) {
                qDebug() << "Error: it appears that the file does not have an identical header to the last file. Cannot append to file.";
                return -1;
            }
            for (int i=0; i < channelFilters.length(); i++) {
                channelFilters[i]->resetHistory();
            }
            if (currentTimeStamp < lastFileTStamp) {
                qDebug() << "Error: timestamps do not begin with greater value than the end of the last file. Aborting.";
                return -1;
            }

        }




        //Process the data and stream results to output files
        while(!filePtr->atEnd()) {

            if (!getNextPacket()) {
                //We have reached the end of the file
                break;
            }



            numPointsWrittenToFile++;

            int fileInd = 0;
            if ((currentTimeStamp%decimation) == 0) { //if we are subsampling the data, make sure this is a point we want to save in the first place
                //save data in time file
                *timeStreamPtrs.at(0) << currentTimeStamp;

                for (int ntInd=0; ntInd < spikeConf->ntrodes.length(); ntInd++) {
                    const int16_t* tmpNtrodeData = neuralDataHandler->getNTrodeSamples(ntInd,AbstractNeuralDataHandler::SPIKE);
                    for (int chInd = 0; chInd < spikeConf->ntrodes[ntInd]->hw_chan.length(); chInd++) {
                        *neuroStreamPtrs.at(fileInd) << tmpNtrodeData[chInd]; //Write the data to disk
                    }
                    if (!combineNeuralChannels) {
                        fileInd++; //separate file per nTrode
                    }
                }

                if (combineAuxChannels) {
                    for (int auxChInd = 0; auxChInd < headerConf->headerChannels.length(); auxChInd++) {

                        if (headerConf->headerChannels[auxChInd].dataType == DeviceChannel::INT16TYPE) {
                            //Analog channel
                            if (interleavedDataBytes[auxChInd] != -1) {
                                //This location in the packet has interleaved data from multiple sources that are sampled at a lower rate
                                //Find the time stamp
                                interleavedDataIDBytePtr = (uint8_t*)buffer.data()+interleavedDataBytes[auxChInd];
                                //interleavedDataIDBytePtr = (uint8_t*)(currentPacket.data()) + interleavedDataBytes[auxChInd];

                                if (*interleavedDataIDBytePtr & (1 << headerConf->headerChannels[auxChInd].interleavedDataIDBit)) {
                                    //This interleaved data point belongs to this channel, so update the channel. Otherwise, no update occurs.
                                    bufferPtr = buffer.data()+headerConf->headerChannels[auxChInd].startByte;
                                    //bufferPtr = currentPacket.data()+headerConf->headerChannels[auxChInd].startByte;
                                    tmpVal = (int16_t)(*bufferPtr);
                                    *neuroStreamPtrs.at(0) << tmpVal;
                                    lastValues[auxChInd] = tmpVal;

                                } else {
                                    //Use the last data point received
                                    *neuroStreamPtrs.at(0) << lastValues[auxChInd];

                                }
                            } else {
                                //No interleaving
                                bufferPtr = buffer.data()+headerConf->headerChannels[auxChInd].startByte;
                                //bufferPtr = currentPacket.data()+headerConf->headerChannels[auxChInd].startByte;
                                tmpVal = (int16_t)(*bufferPtr);
                                *neuroStreamPtrs.at(0) << tmpVal;
                                lastValues[auxChInd] = tmpVal;
                            }

                        } else {
                            //Digital channel
                            bufferPtr = buffer.data()+headerConf->headerChannels[auxChInd].startByte;
                            //bufferPtr = currentPacket.data()+headerConf->headerChannels[auxChInd].startByte;
                            tmpVal =  (int16_t)((*bufferPtr & (1 << headerConf->headerChannels[auxChInd].digitalBit)) >> headerConf->headerChannels[auxChInd].digitalBit);
                            *neuroStreamPtrs.at(0) << tmpVal;
                            lastValues[auxChInd] = tmpVal;

                        }
                    }
                }
            }

            //Print the progress to stdout
            printProgress();

            lastTimeStampInFile = currentTimeStamp;

        }

        printf("\rDone\n");
        filePtr->close();
        inputFileInd++;
    }

    for (int i=0; i < neuroFilePtrs.length(); i++) {
        neuroFilePtrs[i]->flush();
        neuroFilePtrs[i]->close();
    }

    for (int i=0; i < timeFilePtrs.length(); i++) {
        timeFilePtrs[i]->flush();
        timeFilePtrs[i]->close();
    }

    /*for (int i=0; i < analogFilePtrs.length(); i++) {
        analogFilePtrs[i]->flush();
        analogFilePtrs[i]->close();
    }

    for (int i=0; i < DIOFilePtrs.length(); i++) {
        DIOFilePtrs[i]->flush();
        DIOFilePtrs[i]->close();
    }*/





    return 0; //success
}
