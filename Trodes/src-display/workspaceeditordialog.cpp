#include "workspaceeditordialog.h"

#if defined (__linux__)
#define EDITOR_STYLE_SHEET "#main {background-color:white;}"
#else
#define EDITOR_STYLE_SHEET "#main {background-color:white;} #editor::pane {border: 1px solid #e2e2e2;}"
#endif

//QDialog inherits QWidget. Only useful for exec() function which blocks Trodes until closed
WorkspaceEditorDialog::WorkspaceEditorDialog(QWidget *parent) : QDialog(parent) {
    workspaceGui = new WorkspaceEditor(M_EMBEDDED);
    workspaceGui->setObjectName("editor");

    QMenu *menuSaveLoad = new QMenu();
    menuSaveLoad->setTitle("File");
    menuSaveLoad->setEnabled(true);

    QMenuBar *menuBar = new QMenuBar();
    menuBar->addAction(menuSaveLoad->menuAction());
    menuBar->setStyleSheet("background-color:black");

    QMenu *menuSave = new QMenu();
    menuSave->setTitle("Save");
    menuSaveLoad->addAction(menuSave->menuAction());

    actionSave = new QAction(0);
    actionSave->setText("Save");
    actionSave->setShortcut(QKeySequence(tr("Ctrl+S")));
    addAction(actionSave);
    actionSaveAs = new QAction(0);
    actionSaveAs->setText("Save As");
    actionSaveAs->setShortcut(QKeySequence(tr("Ctrl+Shift+S")));
    addAction(actionSaveAs);
    menuSave->addAction(actionSave);
    menuSave->addAction(actionSaveAs);

    actionLoad = new QAction(0);
    actionLoad->setText("Load");
    actionLoad->setShortcut(QKeySequence(tr("Ctrl+O")));
    addAction(actionLoad);
    menuSaveLoad->addAction(actionLoad);

    connect(actionSave, SIGNAL(triggered(bool)), workspaceGui, SLOT(saveToXML()));
    connect(actionSaveAs, SIGNAL(triggered(bool)), workspaceGui, SLOT(saveAsToXML()));
    connect(actionLoad, SIGNAL(triggered(bool)), workspaceGui, SLOT(buttonLoadPressed()));
    connect(workspaceGui,SIGNAL(newTitle(QString)),this,SLOT(setWindowTitle(QString)));


    mainLayout = new QVBoxLayout();

    buttonBar = new QGridLayout();
    buttonCancel = new QPushButton(tr("Close"));
    buttonSave = new QPushButton(tr("Save"));
    buttonSaveAs = new QPushButton(tr("Save as"));
    buttonOpen = new QPushButton(tr("Open"));

    buttonCancel->setMaximumSize(buttonCancel->sizeHint());
    buttonSave->setMaximumSize(buttonSave->sizeHint());
    buttonSaveAs->setMaximumSize(buttonSaveAs->sizeHint());   
    buttonOpen->setMaximumSize(buttonOpen->sizeHint());

    connect(buttonCancel, SIGNAL(released()),this,SLOT(buttonCancelPressed()));
    connect(buttonSave, SIGNAL(released()),this,SLOT(buttonSavePressed()));
    connect(buttonSaveAs, SIGNAL(released()),this,SLOT(buttonSaveAsPressed()));


    connect(buttonOpen, &QPushButton::released, this, &WorkspaceEditorDialog::buttonApplyOrOpenPushed);

    buttonBar->addWidget(buttonCancel, 0,0, Qt::AlignLeft);
    buttonBar->addWidget(buttonSaveAs, 0,3, Qt::AlignRight);
    buttonBar->addWidget(buttonSave, 0,4, Qt::AlignRight);
    buttonBar->addWidget(buttonOpen, 0, 5, Qt::AlignRight);
//    buttonOpen->hide();
    buttonBar->setColumnStretch(1,1);


    mainLayout->addWidget(menuBar, 0);
    mainLayout->addWidget(workspaceGui, 1);
    mainLayout->addLayout(buttonBar, 1);
    this->setObjectName("main");

    setStyleSheet(EDITOR_STYLE_SHEET);
    setLayout(mainLayout);

}

//void WorkspaceEditorDialog::enableOpenButton(){
//    buttonOpen->show();
//}

void WorkspaceEditorDialog::setCurrentFileName(QString filename)
{
    currentFileName = filename;
}

void WorkspaceEditorDialog::loadFileIntoWorkspaceGui(const TrodesConfiguration &c)
{
    setWindowTitle("Edit current workspace");
    workspaceGui->loadedWorkspace = c;
    workspaceGui->curLoadedFile = "";
    workspaceGui->loadWorkspaceToDisplay();

}
void WorkspaceEditorDialog::loadFileIntoWorkspaceGui(QString filename)
{
    if (!filename.isEmpty()) {
        setWindowTitle(filename);
        workspaceGui->loadFromXML(filename);
        currentFileName = filename;
    } else {
        workspaceGui->loadDefaultUnconfigured();
    }
}

/*void WorkspaceEditorDialog::fillInWorkspace(HeadstageSettings headstageSettings, bool ecuconnected, bool rfconnected, int chansperntrode, int psize, bool useSysClock){

    //STUB:  DELETE THIS FUNCTION

    qDebug() << "Filling workspace editor from Editor Dialog";
//    WorkspaceEditorDialog *workspaceeditor = new WorkspaceEditorDialog;
//    WorkspaceEditor *workspaceeditor = new WorkspaceEditor(M_EMBEDDED);
    this->hide();
    this->loadFileIntoWorkspaceGui("");

    if(ecuconnected){
        this->workspaceGui->addECU();
    }

    if(rfconnected){
        //this->workspaceGui->addRF();
    }

    if(!useSysClock){
        this->workspaceGui->removeSysClock();
    }

//    if(headstageSettings.accelSensorAvailable
//            || headstageSettings.gyroSensorAvailable
//            || headstageSettings.magSensorAvailable)
//    {
    if(headstageSettings.valid && headstageSettings.numberOfChannels){
        int workspacepsize= headstageSettings.numberOfChannels*sizeof(int16_t) + sizeof(uint32_t) + 2 + ecuconnected*32;
        if(psize-workspacepsize == 8){
            this->workspaceGui->addHSSensors();//Add in sensors
        }
    }

    if(headstageSettings.samplingRate){
        this->workspaceGui->setSamplingRate(headstageSettings.samplingRate);
    }

    this->workspaceGui->displayAllAux();

    int numchans = headstageSettings.valid ? headstageSettings.numberOfChannels : 0;
    int ntrodes = numchans/chansperntrode;
    this->workspaceGui->setNumChannels(numchans);

    QList<int> channelmapping;
    for(int i = 0; i < numchans/chansperntrode; ++i){
        for(int j = 0; j < chansperntrode; ++j){
            channelmapping.append(i+1);
        }
    }
    this->workspaceGui->setChannelMap(channelmapping);

    int cols, pages;
    if(ntrodes == 2){
        cols = 1;
        pages = 1;
    }
    else if(numchans < 256){
        cols = 2;
        pages = 2;
    }
    else{
        cols = 4;
        pages = 4;
    }
    this->workspaceGui->configureDisplay(cols, pages);

}*/

int WorkspaceEditorDialog::openEditor(void)
{

    return(this->exec());
}

int WorkspaceEditorDialog::openFileEditor(void)
{
    setFileEditState();
    return(openEditor());

}

int WorkspaceEditorDialog::openReconfigEditor(void)
{
    setReconfigState();
    //workspaceGui->setForReconfigure();
    return(openEditor());
}

void WorkspaceEditorDialog::setReconfigState()
{
    actionSave->setEnabled(false);
    actionSaveAs->setEnabled(false);
    actionLoad->setEnabled(false);

    buttonCancel->setText("Cancel");
    buttonSave->setVisible(false);
    buttonSaveAs->setVisible(false);
    buttonOpen->setText("Apply");

}
void WorkspaceEditorDialog::setFileEditState()
{
    actionSave->setEnabled(true);
    actionSaveAs->setEnabled(true);
    //actionLoad->setEnabled(true);

    buttonCancel->setText("Close");
    buttonSave->setVisible(true);
    buttonSaveAs->setVisible(true);

    buttonOpen->setText("Open");
}

void WorkspaceEditorDialog::clearGUI() {
    //no good alternative to deleteing the WorkspaceEditor and replacing it with a new one
    WorkspaceEditor *newEditor = new WorkspaceEditor(M_EMBEDDED);
    mainLayout->replaceWidget(workspaceGui, newEditor);
    delete workspaceGui;
    workspaceGui = newEditor;
    workspaceGui->setObjectName("editor");
    setStyleSheet(EDITOR_STYLE_SHEET);
    connect(actionSave, SIGNAL(triggered(bool)), workspaceGui, SLOT(saveToXML()));
    connect(actionSaveAs, SIGNAL(triggered(bool)), workspaceGui, SLOT(saveAsToXML()));
    connect(actionLoad, SIGNAL(triggered(bool)), workspaceGui, SLOT(buttonLoadPressed()));
}


void WorkspaceEditorDialog::buttonCancelPressed() {
    QString filePathOutput = workspaceGui->askToSaveUnsavedChanges();

    if (filePathOutput.compare("---") == 0) {
        //Close was cancelled
        return;
    }
    this->hide();
    clearGUI(); //we clear the gui because we don't want previous settings loaded into the gui to persist
    QDialog::reject();
}

void WorkspaceEditorDialog::buttonSaveAsPressed() {
    /*QString tempFilePath = workspaceGui->askToSaveUnsavedChanges();
    if(tempFilePath.isEmpty()){
        tempFilePath = QString("%1/tempWorkspace.trodesconf").arg(QCoreApplication::applicationDirPath());
    }*/
    QString tempFilePath = workspaceGui->saveAsToXML();
    if (workspaceGui->saveToXML(tempFilePath,false)) { //only execute if the save was successful
        setWindowTitle(tempFilePath);
        currentFileName = tempFilePath;
        //emit sig_openTempWorkspace(workspaceGui->curSaveFile);
        //this->hide();
        //clearGUI();
        //QDialog::accept();
    }

}

void WorkspaceEditorDialog::buttonApplyOrOpenPushed() {

    TrodesConfiguration t;
    bool ok = workspaceGui->getWorkspaceIfValid(t);

    if (ok) {


        t.writeTrodesConfig(QCoreApplication::applicationDirPath() + "/" + "temp.trodesconf");
        emit this->sig_openTempWorkspace(QCoreApplication::applicationDirPath()+"/"+"temp.trodesconf", currentFileName);

        //emit sig_openWorkspace(t);
        this->hide();
        clearGUI();
        QDialog::accept();
    }


    /*workspaceGui->saveToXML("temp.trodesconf", true);
    emit this->sig_openTempWorkspace("temp.trodesconf");
    accept();*/
}

void WorkspaceEditorDialog::buttonSavePressed() {
    /*QString tempFilePath = workspaceGui->askToSaveUnsavedChanges();
    if(tempFilePath.isEmpty()){
        tempFilePath = QString("%1/tempWorkspace.trodesconf").arg(QCoreApplication::applicationDirPath());
    }*/

    if (currentFileName.isEmpty()) {
        QString tempFilePath = workspaceGui->saveAsToXML();
        if (workspaceGui->saveToXML(tempFilePath,false)) { //only execute if the save was successful
            setWindowTitle(tempFilePath);
            currentFileName = tempFilePath;
        }
    } else {

        if (!workspaceGui->saveToXML("",false)) { //only execute if the save was successful
            qDebug() << "Error saving to file.";
        }

    }

}

void WorkspaceEditorDialog::closeEvent(QCloseEvent * event){
    buttonCancelPressed();
}
