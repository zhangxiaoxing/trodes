/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <QObject>
//#include <QWidget>
#include <QColor>
#include <QDomNode>
#include <QXmlStreamWriter>
#include <QHash>
#include <QDebug>
//includes for Helper class
#include <QPalette>
#include <QCheckBox>
#include "../src-display/cargrouppanel.h"
#define TRODES_VERSION 230 //2.3.0
#define BETA_VERSION 1 //IF BETA_VERSION = 0, NOT BETA, else, is BETA
#include "hardwaresettings.h"
#include <thread>




//#define TRODES_CODE

//#define TRODES_CODE //REMOVE LATER!!!

#ifdef TRODES_CODE
#include "iirFilter.h"
#endif


#ifdef PHOTOMETRY_CODE
#include "iirFilter.h"
#endif
//This this the value that defines the uV step per LSB
#define AD_CONVERSION_FACTOR 12780  //uV =  Input value*(12780/65535)
//#define AD_CONVERSION_FACTOR 12500

//#define DIGITALTYPE 0
//#define INT16TYPE   1

/* **** Benchmarking Frequency Defines **** */
#define BENCH_FREQ_SPIKE_DETECT_DEFAULT 10
#define BENCH_FREQ_SPIKE_SENT_DEFAULT 10
#define BENCH_FREQ_SPIKE_RECEIVE_DEFAULT 10
#define BENCH_FREQ_POS_STREAM_DEFAULT 1
#define BENCH_FREQ_EVENTSYS_DEFAULT 1

#define MAX_HARDWARE_CHANNELS 3084  // just a guess; this will have to be changed if we ever go to higher channel counts
#define MAX_CHAN_PER_NTRODE 1024
//Spike waveform parameters
#define POINTSINWAVEFORM 40 //Total number of points in waveform snippet
#define PEAKALIGNADJUSTMENT 5 //+/- padding in the detection window to allow slight alignment
#define PEAKALIGNIND 13 //Points into window where peaks are aligned (needs to be > POINTSTOREWIND)
#define POINTSTOREWIND 10 //Number of points to collect before threshold crossing
#define MAX_SPIKE_POINTS (POINTSINWAVEFORM * MAX_CHAN_PER_NTRODE)

//#define MAXCARGROUPS 32
//#define MAXCHANPERGROUP 16
////qDebug Message Handler external definitions
//extern void moduleMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg);
//extern void setModuleName(QString name);

//forward decalrations
class SingleSpikeTrodeConf;
class SpikeConfiguration;

class Helper { //helper class to hold common static functions
public:
    static void setWidgetTextPaletteColor(QWidget *widget, QColor newColor);
    static void setWidgetTextPaletteColor(QCheckBox *widget, QColor newColor);

    static QList<QList<QString> > getSortedTagList(SingleSpikeTrodeConf *nTrode);

};

class XMLContainer {
public:
    XMLContainer(const QString &name = "");
    ~XMLContainer();
    void clear();
    void setName(const QString &name);
    QString name();
    void appendField(const QString &fName, const QVariant &fdata);
    void appendVersionFields();
    void appendChild(XMLContainer *container);
    QDomElement toXML(QDomDocument &doc) const;
    bool fromXML(const QDomElement &XMLElem);
    QHash<QString,QVariant> getAttributes();
    QList<XMLContainer*> getChildElements();

private:
    QString tagName;
    //QStringList fieldNames;
    //QList<QVariant> fieldData;

    QHash<QString,QVariant> attributes;
    QList<XMLContainer*> childElements;
};

class GlobalConfiguration : public QObject
{
    Q_OBJECT
public:
    GlobalConfiguration(QObject *parent);
    int loadFromXML(QDomNode &globalConfNode);
    void saveToXML(QDomDocument &doc, QDomElement &rootNode);
    void saveToXMLNoSessionData(QDomDocument &doc, QDomElement &rootNode);
    void saveToXML(QDomDocument &doc, QDomElement &rootNode, qint64 currentTimeStamp);
    static QString getVersionInfo(bool withSpaces = true, bool richtext = false);

    void applyHeadstageSettings(HeadstageSettings headstageSettings);
    void applyControllerSettings(HardwareControllerSettings controllerSettings);

    QString filePrefix;
    QString filePath;
    QString configfilepath;
    bool saveDisplayedChanOnly;  // save only the displayed data (note that all Digital IO channels are always saved
    bool realTimeMode;
    unsigned int processorCount;

    int MBPerFileChunk; //if set to -1, no chunking occurs

    bool suppressModuleAbsPathWarning;// = 0;

    //These values get filled by Trodes and are used as info during playback and data export of recorded data
    //**********************
    qint64 timestampAtCreation;
    qint64 systemTimeAtCreation;
    QString trodesVersion;
    QString compileDate;
    QString compileTime;
    QString qtVersion;
    QString headstageSerialNumber;
    QString controllerSerialNumber;
    QString controllerFirmwareVersion;
    QString headstageFirmwareVersion;
    int autoSettleOn;
    int smartRefOn;
    int gyroSensorOn;
    int accelSensorOn;
    int magSensorOn;
    QString commitHeadStr;
    //**********************

private:
    void loadCurrentVersionInfo(void);
};

struct DeviceChannel {
    QString idString;
    enum {DIGITALTYPE, INT16TYPE, UINT32TYPE} dataType;
    int startByte;
    int digitalBit;
    int port;
    int interleavedDataIDByte;
    int interleavedDataIDBit;
    bool input;
};

struct DeviceInfo {
    QString name;
    int packetOrderPreference;
    int numBytes;
    bool available;
    QList<DeviceChannel> channels;
    int byteOffset;
    QString wsDisplayName;//workspace editor display name
};

struct CustomSourceOption {
    QString name;
    QList<double> data;
};

struct SourceControl {
    QString name;
    int unitNumber;
    QList<CustomSourceOption> customOptions;
};

class HardwareConfiguration : public QObject
{
    Q_OBJECT
public:
    HardwareConfiguration(QObject *parent);
    //~HardwareConfiguration();
    int loadFromXML(QDomNode &moduleConfNode);
    void saveToXML(QDomDocument &doc, QDomElement &rootNode);
    int NCHAN;  // Number of configured hardware channels
    //int numStimChan; //Number of channels that are stimulation capable
    int sourceSamplingRate; // sampling rate of Intan chips
    unsigned int lfpSubsamplingInterval;
    int headerSize; // number of bytes in header for this hardware
    bool headerSizeManuallyDefined;
    int useIntrinsics;

    QList<DeviceInfo> devices;
    QList<SourceControl> sourceControls;
    bool ECUConnected;
    bool sysTimeIncluded;
#ifdef PHOTOMETRY_CODE
    int APDsamplingRate;
    int NFIBER;  // Number of configured hardware channels
#endif
};



// The module configuration specifies which modules are launched and which need to exchange data
class SingleModuleConf
{
public:
     // default values are set in SingleModuleConf::loadFromXML
     QString    moduleName; // the program name with path (relative paths are relative to Trodes/Trodes.app)
     int        sendTrodesConfig; //if 1, the path to the config file is sent in the arguments
     int        sendNetworkInfo;//if 1, the address and port for the trodes server are sent
     QString    hostName; // the name of the host on which this module is run;
     QStringList moduleArguments; //the command line argument list for the module
};


class ModuleConfiguration : public QObject
{
  Q_OBJECT
public:
    ModuleConfiguration(QObject *parent);
    ~ModuleConfiguration();
    int loadFromXML(QDomNode &moduleConfNode);
    void saveToXML(QDomDocument &doc, QDomElement &rootNode);
    bool modulesDefined;

    qint8 myID;  // The number for this program (-1 for trodes, 0 - n for modules)
    QString trodesConfigFileName;
    QList<SingleModuleConf> singleModuleConf;

    bool       modulePresent(QString modName);
    int        findModule(QString modName);


};

class NetworkConfiguration : public QObject
{
    Q_OBJECT
public:
    NetworkConfiguration();
    ~NetworkConfiguration();
    enum NetworkType {qsocket_based, zmq_based};
    int loadFromXML(QDomNode &moduleConfNode);
    void saveToXML(QDomDocument &doc, QDomElement &rootNode);

    NetworkType networkType;
    NetworkType defaultNetworkType;

    QString trodesHost;  // the hostname of the machine running trodes
    quint16 trodesPort;  // the port number for the trodes server that all modules connect to

    QString hardwareAddress;  // the broadcast address for the network that the hardware is attached to
    quint16 hardwarePort;  // the port number to communicate with the hardware.  This is fixed.
    quint16 ecuDirectPort;  // the port number to communicate to the ECU via the MCU.  This is fixed


    qint32  dataSocketType; // TCPIP, UDP or LOCAL
    qint8   myModuleID;  // the ID for this module
    bool    networkConfigFound;
    bool    networkTypeConfiguredinWorkspace;
    bool    networkAddressConfiguredinWorkspace;

    qint8 numModulesConnected;
    //int lfpRate;
};


class NTrode
{
public:
   int number;
   QList<int> hw_chan;
};

class NTrodeTable : public QObject
{
  Q_OBJECT
public:
  QList<NTrode *> ntrodes;
  NTrodeTable() {}

};


class streamConfiguration : public QObject
{
  Q_OBJECT
public:
  int nColumns; //number of columns per page
  int nTabs; //number of pages of neural display data
  bool horizontalLayout;
  QList<int> pageBreaks;
  double tLength;
  double FS;
  QColor backgroundColor;

  QList<int> trodeIndexLookupByHWChan;
  QList<int> trodeChannelLookupByHWChan;
  int nChanConfigured; // the total number of configured channels;


  //bool saveOnlyDisplayedChannels;
  QVector<bool> saveHWChan;  // true for hardware channels that are displayed

  //Spike and LFP filters for the stream display.
  //QVector<bool> spikeModeOn;
  //QVector<bool> lfpModeOn;

/*#ifdef TRODES_CODE
  // We only include the filters if we are compiling this within TRODES.
  //ButterworthFilter* dataFilters;
  QVector<BesselFilter*> spikeFilters;
  QVector<BesselFilter*> lfpFilters;
  QVector<NotchFilter*> notchFilters;
#endif*/

#ifdef PHOTOMETRY_CODE
  QList<int> fiberIndexLookupByHWChan;
  QList<int> fiberChannelLookupByHWChan;
  ButterworthFilter *photometryFilters;
#endif

  streamConfiguration(bool dontCreateFilter = false);
  ~streamConfiguration();

  //void setSpikeModeOn(int ntrode, bool on, bool emitchange = true);
  //void setLFPModeOn(int ntrode, bool on, bool emitchange = true);
  //void setAllModesOn(bool spike, bool lfp);
  int loadFromXML(QDomNode &eegDispConfNode);
  //int loadFromXML(QDomNode &eegDispConfNode, HardwareConfiguration *hconf, SpikeConfiguration *spConf);
  bool createIndexData(HardwareConfiguration *hconf, SpikeConfiguration *spConf);
  void saveToXML(QDomDocument &doc, QDomElement &rootNode);
  void setChanToSave(int numHWChan);
  void listChanToSave(int numHWChan); // for debugging


public slots:
  void setTLength(double newTLength);
  void setBackgroundColor(QColor c);

signals:
  void updatedTLength(double);
  void updatedBackgroundColor(QColor c);
  void updatedLFPMode();
  void updatedSpikeMode();
  void updatedAllModes();
};


//The header portion of each packet can contain extra information, such as digital inputs and DC-coupled analog inputs
class headerChannel
{
public:
   QString idString; //what is displayed (doesn't have to be a number)
   int dataType;
   bool input; // true for inputs, false for outputs
   bool storeStateChanges;
   int port; // the statescript port number
   int startByte;
   int digitalBit;
   int interleavedDataIDByte;
   int interleavedDataIDBit;
   QColor color;
   int maxDisp;
   QString deviceName;
   headerChannel() :
       idString(""),
       storeStateChanges(false),
       deviceName("")
   {

   }
};

class headerDisplayConfiguration : public QObject {
    Q_OBJECT
public:
    headerDisplayConfiguration(QObject *parent);
    ~headerDisplayConfiguration();
    int loadFromXML(QDomNode &headerConfNode);
    int loadFromXML(QDomNode &headerConfNode, HardwareConfiguration *h);
    void saveToXML(QDomDocument &doc, QDomElement &rootNode);
    void saveToXML(QDomDocument &doc, QDomElement &rootNode,HardwareConfiguration *h);
    QList<headerChannel> headerChannels;

    int maxDigitalPort(bool input);
    int minDigitalPort(bool input);
    bool digitalPortValid(int port, bool input);
    bool digitalIDValid(QString ID, bool input);

    QStringList digInIDList;
    QList<int> digInPortList;
    QStringList digOutIDList;
    QList<int> digOutPortList;

};

struct HardwareChannelSettings {
    int ID;
    int thresh;
    int maxDisp;
    int streamingChannelLookup;
    bool triggerOn;
    bool stimCapable;
};

struct GroupingTag {
    QString category;
    QString tag;
    void operator= (const GroupingTag &g) {category = g.category; tag = g.tag;}
    bool operator== (const GroupingTag &g) const { if(category == g.category && tag == g.tag){return(true);} else{return(false);}  }

    inline QString toTagString(void) {
        return(QString("[%1] %2").arg(category).arg(tag));
    }

    inline void readInTagStr(QString tagStr) {
        QString _cata = tagStr.split("]").first();
        _cata = _cata.right(_cata.length()-1); //remove the '[' from the string
        QString _tag = tagStr.split("]").last();
        while(_tag.length() > 1 && _tag.at(0) == ' ') {
            _tag.remove(0,1);
        }
//        _tag = _tag.right(tag.length()-1); //remove the blank space from the string
        category = _cata;
        tag = _tag;
    }
};

// global qhash overload for GroupingTag.  This enables putting GroupingTag objects into QHash structures
inline uint qHash(const GroupingTag &key, uint seed) {
    return qHash(key.tag, seed) ^ qHash(key.category, seed);
}

class CategoryDictionary {
public:
    explicit CategoryDictionary(void);

    void operator= (const CategoryDictionary &cd) {categories = cd.categories; all = cd.all;}

    inline void setCategories(QHash<QString, QHash<QString,QString> > newCategories) {categories = newCategories;}

    bool categoryExists(QString category);
    bool tagExists(QString tag); //*DEPRECIATED
    bool gTagExists(GroupingTag gt);
    bool isTagInCategory(QString category, QString tag); //*DEPRECIATED

    QString getTagsCategory(QString tag);
    QList<GroupingTag> getSortedAllTagList(void);
    QHash<GroupingTag, int> getAllTags(void);
    QHash<QString,QString> getCategorysTags(QString category);
    QHash<QString, QHash<QString,QString> > getCategories(void);

    bool addCategory(QString newCategory);
    bool addTagToCategory(QString category, QString tag);
    bool addTag(GroupingTag gt); //adds the grouping tag to the dictionary
    bool addTagToALL(GroupingTag gt); // adds the grouping tag to the 'All' category

    void clear(void);
    void printAll(void);

private:
    QHash<QString, QHash<QString,QString> >categories;
    QHash<GroupingTag, int> all; //hash that contains ALL grouping tags, regardless of category

};

class SingleSpikeTrodeConf
{
public:
    SingleSpikeTrodeConf();
    SingleSpikeTrodeConf(const SingleSpikeTrodeConf * s);



    int nTrodeId; // arbitrary ID
    int nTrodeIndex;
    int refNTrode; // default -1 = ground
    int refNTrodeID; // default -1
    int refChan; // default 0
    int refChanID; //default to blank
    int lowFilter;
    int highFilter;
    int moduleDataChan;
    int moduleDataHighFilter;
    bool refOn; //For spikes
    bool filterOn;//For spikes
    bool moduleDataOn;
    bool lfpRefOn; //For lfp
    bool lfpFilterOn; //for lfp
    bool notchFilterOn; // for notch filter
    int notchFreq;
    int notchBW;
    double spike_scaling_to_uV;
    double raw_scaling_to_uV;
    double lfp_scaling_to_uV;

    bool spikeViewMode;
    bool lfpViewMode;
    bool stimViewMode;


    bool rawRefOn;
    int refGroup;
    bool groupRefOn; //if false, use refntrode+refchan. if true, use refGroup as group

    HardwareChannelSettings channelSettings;

    QHash<QString, int> tags;
    QHash<GroupingTag, int> gTags;

    QColor color;
    QList<int> maxDisp;
    QList<int> thresh;
    QList<int> hw_chan;
    QList<int> unconverted_hw_chan;  //hw channel actually come in interleaved, so we need to convert from the non-interleaved numbers
    QList<int> thresh_rangeconvert;
    QList<int> streamingChannelLookup;
    //QList<int> stimBitLookup;  //If stim channels exists, a section in the packet will have a bit saying whether each stim-capable channel is stimulating. Value is -1 if the channel is not stim enabled.
    QList<bool> triggerOn;
    QList<bool> stimCapable;
    QList<int> coord_ml;
    QList<int> coord_ap;
    QList<int> coord_dv;
    QList<int> spikeSortingGroup;

};

class SpikeConfStructure{
private:
    QHash<int, SingleSpikeTrodeConf*> hash;
    //QHash<int, SingleSpikeTrodeConf> hash;
    //QList<SingleSpikeTrodeConf*> list;
    QList<SingleSpikeTrodeConf> list;
public:
    SpikeConfStructure(int nitems);
    SpikeConfStructure(){}
    void setSize(int nitems);
    int nitems;

    int      size();
    int      length();
    int      index(int id);
    bool     isEmpty();
    bool     exists(int id);

    //These are used all over and in the data processing thread. Should be as fast and minimal as possible
    inline SingleSpikeTrodeConf*   at(const int i)          {return &list[i];}
    inline SingleSpikeTrodeConf*   operator[](const int i)  {return &list[i];}
    inline SingleSpikeTrodeConf*   refOfIndex(int ind)      {return &list[list[ind].refNTrode];}

    //These two may cause slight latency increase due to hash tables being slower than instant array accesses
    inline SingleSpikeTrodeConf*   ID(const int i)          {return hash[i];}
    inline SingleSpikeTrodeConf*   refOfID(int id)          {return &list[hash[id]->refNTrode];}
    //inline SingleSpikeTrodeConf*   ID(const int i)          {return &hash[i];}
    //inline SingleSpikeTrodeConf*   refOfID(int id)          {return list[hash[id].refNTrode];}

    SingleSpikeTrodeConf*   takeLast();

    void clear();
    void copyFrom(const SpikeConfStructure& cs);
    void append(int id, SingleSpikeTrodeConf* ntrodeconf);
    void append(int id);
};


class SpikeConfiguration : public QObject
{
  Q_OBJECT
public:
//  QList<SingleSpikeTrodeConf *> ntrodes;
    enum ReturnCode
    {
        configurationComplete,
        autoConfigNeeded_neuropixel1
    };


    SpikeConfStructure ntrodes;
    CategoryDictionary groupingDict;
    QVector<CARGroup > carGroups;
    QString deviceType;


  SpikeConfiguration(QObject *parent, int numberOfDisplayedNTrodes) :
      QObject(parent),
      ntrodes(numberOfDisplayedNTrodes)
  {

  }

  SpikeConfiguration(QObject *parent) : QObject(parent) {}

  //SpikeConfiguration(QObject *parent, QDomDocument &doc);
  ~SpikeConfiguration();

  int loadFromXML(QDomNode &spikeConfNode);
  int loadFromXML(QDomNode &spikeConfNode, int totalNumChannels);
  void saveToXML(QDomDocument &doc, QDomElement &rootNode);
  bool convertNTrodeRefToIDs(void);
  void convertNTrodeIDsToRef(void);
  int convertHWchan(int hwchan, int totalchan);
  void setSpikeModeOn(int ntrode, bool on, bool emitchange = true);
  void setStimViewModeOn(int ntrode, bool on, bool emitchange = true);
  void setLFPModeOn(int ntrode, bool on, bool emitchange = true);
  void setAllModesOn(bool spike, bool lfp, bool stim);



  SingleSpikeTrodeConf* operator[](int i) { return ntrodes.at(i); }
private:
  int numChanPerChip;

signals:

  void updatedModuleData(void);
  void updatedModuleDataFilter(QList<int>);
  void updatedRef(void);
  void updatedNotchFilter(void);
  void updatedLFPFilter(void);
  void updatedMaxDisplay(void);
  void newMaxDisplay(int nTrode, int newMaxVal);
  void newThreshold(int nTrode, int newThresh);
  void newTriggerMode(int hwChannel, bool triggerOn);
  void newThreshold(int nTrode, int chan, int newThresh);
  void newTriggerMode(int nTrode, int chan, bool triggerOn);
  void updatedTraceColor(void);
  void updatedFilter(QList<int>);
  void updatedFilterOn(void);
  void updatedThresh(void);
  void updatedTriggerMode(void);
  void changeAllMaxDisp(int);
  void changeAllThresh(int);
  void updatedLFPMode();
  void updatedSpikeMode();
  void updatedStimViewMode();
  void updatedAllModes();

public slots:

#ifdef TRODES_CODE
  void setModuleDataSwitch(int nTrode, bool on, bool emitchange = true);
  void setRefSwitch(int nTrode, bool on, bool emitchange = true);
  void setNotchFilterSwitch(int nTrode, bool on, bool emitchange = true);
  void setLFPRefSwitch(int nTrode, bool on, bool emitchange = true);
  void setLFPFilterSwitch(int nTrode, bool on, bool emitchange = true);
  void setFilterSwitch(int nTrode, bool on, bool emitchange = true);
  void setModuleDataChan(int nTrode, int newChan, bool emitchange = true);
  void setMaxDisp(int nTrode, int chan, int newMaxDisp, bool emitchange = true);
  void setThresh(int nTrode, int chan, int newThresh, bool emitchange = true);
  void setMaxDisp(int nTrode, int newMaxDisp, bool emitchange = true);
  void setThresh(int nTrode,  int newThresh, bool emitchange = true);
  void setTriggerMode(int nTrode, bool triggerOn);
  void setTriggerMode(int nTrode, int chan, bool newTriggerMode);
  void setReference(int nTrode, int newRefNTrode, int newRefNTrodeChan, bool emitchange = true);
  void setColor(int nTrode, QColor newColor, bool emitchange = true);
  void setLowFilter(int nTrode, int cutoff, bool emitchange = true);
  void setHighFilter(int nTrode, int cutoff, bool emitchange = true);
  void setModuleDataHighFilter(int nTrode, int cutoff, bool emitchange = true);
  void setNotchFilterFreq(int nTrode, int notchFreq, bool emitchange = true);
  void setNotchFilterBW(int nTrode, int notchBW, bool emitchange = true);
  void setGroupRef(int nTrode, bool toggle, int group, bool emitchange = true);
  void setRawRefOn(int nTrode, bool on, bool emitchange = true);
#endif
};

#ifdef PHOTOMETRY_CODE
class SinglePhotometryFiberConf
{
public:
  int nFiberId; // arbitrary ID
  int carrierFreq[2];
  QColor color;

  QList<int> maxDisp;
  QList<int> hw_chan;
  QList<int> cidx;
  QList<bool> filterOn;
  QList<int> lowFilter;
  QList<int> highFilter;

};

class PhotometryConfiguration : public QObject
{
  Q_OBJECT
public:
  QList<SinglePhotometryFiberConf *> nfibers;

  PhotometryConfiguration(QObject *parent, int numberOfDisplayedNfibers) : QObject(parent) {
    for (int i = 0; i < numberOfDisplayedNfibers; i++) {
      nfibers.append(new SinglePhotometryFiberConf);
      nfibers[i]->nFiberId = i;
      nfibers[i]->color = QColor("#0000FF");
      nfibers[i]->carrierFreq[0] = 300;
      nfibers[i]->carrierFreq[1] = 6000;
    }
  }

  ~PhotometryConfiguration();

  int loadFromXML(QDomNode &spikeConfNode);
  void saveToXML(QDomDocument &doc, QDomElement &rootNode);
  SinglePhotometryFiberConf* operator[](int i) { return nfibers.at(i); }

signals:

  void updatedMaxDisplay(void);
  void newMaxDisplay(int nTrode, int newMaxVal);
  void updatedTraceColor(void);
  void changeAllMaxDisp(int);

public slots:

  void setMaxDisp(int nTrode, int newMaxDisp);
  void setColor(int nTrode, QColor newColor);

};
#endif

class BenchmarkConfig {

public:
    BenchmarkConfig();
    BenchmarkConfig(bool recSysTime, bool pSpikeDetect, bool pSpikeSent, bool pSpikeReceived, bool pPositionStreaming, bool pEventSys);

    void setAllBenchmarking(bool on);

    int loadFromXML(QDomNode &globalConfNode);
    void saveToXML(QDomDocument &doc, QDomElement &rootNode);

    void resetDefaultFreq(void);
    void resetDefaultBools();

    inline void editedByUser(void) {userRuntimeEdited = true;}
    inline void intiatedFromCommandLine(void) {iniFromCmdLine = true;}

    inline bool isRecordingSysTime(void) {return(recordSysTime);}
    inline bool wasEditedByUser(void) {return(userRuntimeEdited);}
    inline bool wasInitiatedFromCommandLine(void) {return(iniFromCmdLine);}

    inline bool printSpikeDetect(void) {return(spikeDetect);}
    inline bool printSpikeSent(void) {return(spikeSent);}
    inline bool printSpikeReceived(void) {return(spikeReceived);}
    inline bool printPositionStreaming(void) {return(positionStreaming);}
    inline bool printEventSys(void) {return(eventSys);}

    inline int getFreqSpikeDetect(void) {return(freqSpikeDetect);}
    inline int getFreqSpikeSent(void) {return(freqSpikeSent);}
    inline int getFreqSpikeReceived(void) {return(freqSpikeReceived);}
    inline int getFreqPositionStream(void) {return(freqPositionStreaming);}
    inline int getFreqEventSys(void) {return(freqEventSys);}

    inline void setBoolRecordSysTime(bool setVal) {recordSysTime = setVal;}
    inline void setBoolSpikeDetect(bool setVal) {spikeDetect = setVal;}
    inline void setBoolSpikeSent(bool setVal) {spikeSent = setVal;}
    inline void setBoolSpikeReceived(bool setVal) {spikeReceived = setVal;}
    inline void setBoolPositionStreaming(bool setVal) {positionStreaming = setVal;}
    inline void setBoolEventSys(bool setVal) {eventSys = setVal;}

    inline void setFreqSpikeDetect(int setVal) {freqSpikeDetect = setVal;}
    inline void setFreqSpikeSent(int setVal) {freqSpikeSent = setVal;}
    inline void setFreqSpikeReceived(int setVal) {freqSpikeReceived = setVal;}
    inline void setFreqPositionStream(int setVal) {freqPositionStreaming = setVal;}
    inline void setFreqEventSys(int setVal) {freqEventSys = setVal;}

    inline QString getFreqStr(void) { return(QString("%1 %2 %3 %4 %5").arg(freqSpikeDetect).arg(freqSpikeSent).arg(freqSpikeReceived).arg(freqPositionStreaming).arg(freqEventSys)); }
    inline QString getBoolStr(void) { return(QString("%1 %2 %3 %4 %5 %6").arg(recordSysTime).arg(spikeDetect).arg(spikeSent).arg(spikeReceived).arg(positionStreaming).arg(eventSys)); }

private:
    bool userRuntimeEdited;
    bool iniFromCmdLine;
    bool recordSysTime;
    bool spikeDetect;
    bool spikeSent;
    bool spikeReceived;
    bool positionStreaming;
    bool eventSys;

    int freqSpikeDetect;
    int freqSpikeSent;
    int freqSpikeReceived;
    int freqPositionStreaming;
    int freqEventSys;

};

extern BenchmarkConfig *benchConfig;

class TrodesConfiguration {

public:


    //Methods
    TrodesConfiguration();
    QString readTrodesConfig(QString configFileName);
    QString readTrodesConfig(QDomDocument doc, QString configFileName = "");
    bool writeTrodesConfig(QString configFileName);
    bool writeRecConfig(QString configFileName, qint64 currentTimestamp);
    void clear();

    bool autoPopulate_neuropixels1();
    void getNeuropixelsHardwareCommandData(NeuroPixelsSettings &s);
    void generateNeuropixelsWorkspace(int numProbes);
    bool addNeuropixelsNtrodes(QList<QVector<bool> > channelsOn, QList<QList<QColor> > colors, QList<int> spikeGains, QList<int> lfpGains, QList<QPointF> channelLocations, QList<QVector<int> > probeLocations);
    void removeNeuropixelsConfig();
    void printChannelOrderInPacket();

    bool isValid();

    //Configuration objects
    GlobalConfiguration globalConf;
    streamConfiguration streamConf;
    SpikeConfiguration spikeConf;
    headerDisplayConfiguration headerConf;
    ModuleConfiguration moduleConf;
    NetworkConfiguration networkConf;
    HardwareConfiguration hardwareConf;
    BenchmarkConfig benchConfig;



    //Extra data
    int dataStartLoc; //the position of the file where data begins

    // Overloaded assignment
    TrodesConfiguration& operator= (const TrodesConfiguration &tc);



};

struct TrodesConfigurationPointers {

    //Pointers to configuration objects
    GlobalConfiguration* globalConf;
    streamConfiguration* streamConf;
    SpikeConfiguration* spikeConf;
    headerDisplayConfiguration* headerConf;
    ModuleConfiguration* moduleConf;
    NetworkConfiguration* networkConf;
    HardwareConfiguration* hardwareConf;
    BenchmarkConfig* benchConfig;

};

//int nsParseTrodesConfig(QString configFileName);


bool writeTrodesConfig(QString configFileName);
bool writeRecConfig(QString configFileName, quint32 currentTimestamp);




/*class TrodesDataStream : public QDataStream  {
public:
    TrodesDataStream();
    TrodesDataStream(QIODevice *d);
    TrodesDataStream(QByteArray *a, QIODevice::OpenMode mode);
    TrodesDataStream(const QByteArray & a);
};*/

//***********************************************************
//avgCalculator Class, useful for benchmark latency testing

#define MOVING_AVG_SET_SIZE 10

template <class T>
class avgCalculator {
public:
    avgCalculator() { sum = 0; movAvgSum = 0; movAvgMaxSetSize = MOVING_AVG_SET_SIZE; min = -1; max = 0;}
    void insert(T obj);
    inline float average(void) { return(sum/data.length()); }
    inline float movingAverage(void) { return(movAvgSum/movAvgData.length()); }
    inline T getMax(void) {return(max);}
    inline T getMin(void) {return(min);}

private:
    T sum;
    T movAvgSum;
    T min;
    T max;
    int movAvgMaxSetSize;
    QVector<T> data;
    QVector<T> movAvgData;
};



#endif // CONFIGURATION_H
