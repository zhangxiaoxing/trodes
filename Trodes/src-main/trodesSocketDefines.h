#ifndef TRODESSOCKETDEFINES_H
#define TRODESSOCKETDEFINES_H

#include<stdint.h>

#define TRODES_ID                       -1  // -1 is the ID for trodes, modules go from 0 to n-1

/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define TRODESSOCKETTYPE_TCPIP   1
#define TRODESSOCKETTYPE_UDP     2
#define TRODESSOCKETTYPE_LOCAL   3

// test
#define TRODESSOCKET_MAXMESSAGESIZE  100000

#define TRODESMESSAGE_MODULEID          1
#define TRODESMESSAGE_REQUESTMODULEID   2
#define TRODESMESSAGE_DATATYPEAVAILABLE  3  // specifies the type of data this module can provide
#define TRODESMESSAGE_SENDDATATYPEAVAILABLE 4 // request the current list of available datatypes from trodes
#define TRODESMESSAGE_MODULENAME 5 //used to attach a name to the module
#define TRODESMESSAGE_TURNONDATASTREAM   6
#define TRODESMESSAGE_TURNOFFDATASTREAM   7
#define TRODESMESSAGE_CURRENTTIME 8
#define TRODESMESSAGE_CURRENTTIMEREQUEST 9
#define TRODESMESSAGE_SETDATATYPE 10
#define TRODESMESSAGE_SETDECIMATION 11 // set the decimation factor for continuous data
#define TRODESMESSAGE_NTRODEMODULEDATACHAN 12 // set the channel of the nTrode to send for continuous data
#define TRODESMESSAGE_NUMNTRODES 13 // the number of nTrodes
#define TRODESMESSAGE_NUMPOINTSPERSPIKE 14  // message for user modules
#define TRODESMESSAGE_NUMCHANPERNTRODE 15 // message for user modules
#define TRODESMESSAGE_SETSCRIPTFUNCTIONVALID 16  // indicate that the  function number is or is not valid (uint32 bool message content)
#define TRODESMESSAGE_MODULEREADY 17 //every module should send this once fully setup (helpful for testing and debugging)

//benchmarkingConfig define goes here
#define TRODESMESSAGE_PLAYBACK_COMMAND 19

#define TRODESMESSAGE_BENCHMARKCONFIG 18


#define TRODESMESSAGE_SETTLECOMMAND 20
#define TRODESMESSAGE_SOURCECONNECT 21
#define TRODESMESSAGE_OPENFILE 22
#define TRODESMESSAGE_CLOSEFILE 23
#define TRODESMESSAGE_STARTAQUISITION 24
#define TRODESMESSAGE_STOPAQUISITION 25
#define TRODESMESSAGE_STARTSAVE 26
#define TRODESMESSAGE_STOPSAVE 27
#define TRODESMESSAGE_TIMERATEREQUEST 28
#define TRODESMESSAGE_TIMERATE 29
#define TRODESMESSAGE_CURRENTSTATEREQUEST 30


#define TRODESMESSAGE_STATESCRIPTCOMMAND 31
#define TRODESMESSAGE_STATESCRIPTEVENT 32
#define TRODESMESSAGE_INSTANCENUM 33

#define TRODESMESSAGE_POSITION0 34
#define TRODESMESSAGE_POSITION1 35
#define TRODESMESSAGE_POSITION2 36
#define TRODESMESSAGE_CAMERAIMAGE0 37
#define TRODESMESSAGE_CAMERAIMAGE1 38
#define TRODESMESSAGE_CAMERAIMAGE2 39
#define TRODESMESSAGE_NTRODESPIKE  40

#define TRODESMESSAGE_SPIKESERVERREQUEST 50
#define TRODESMESSAGE_SPIKECLIENTINFO 51
#define TRODESMESSAGE_STARTDATACLIENT 52
#define TRODESMESSAGE_ECUHARDWAREINFO 53
#define TRODESMESSAGE_MCUHARDWAREINFO 54

#define TRODESMESSAGE_ENABLECONTDATASOCKET 55
#define TRODESMESSAGE_ENABLESPIKEDATASOCKET 56
#define TRODESMESSAGE_ENABLEPOSDATASOCKET 57
#define TRODESMESSAGE_ENABLEDIGITALIODATASOCKET 58
#define TRODESMESSAGE_ENABLEANALOGIODATASOCKET 59
#define TRODESMESSAGE_NDIGITALPORTS 60

#define TRODESMESSAGE_ERRORMESSAGE 90
#define TRODESMESSAGE_STATUSMESSAGE 91

#define TRODESMESSAGE_MODULEDISCONNECTED 99
#define TRODESMESSAGE_QUIT 100

//FSData Specific Messages (temporary, will be removed with refactor)
#define TRODESMESSAGE_SETAUTOSETTLE 110 //messages associated with toggling auto settle on stimulation

//For event system
#define TRODESMESSAGE_EVENT 150
#define TRODESMESSAGE_EVENTLIST 151
#define TRODESMESSAGE_EVENTSUBSCRIBE 152
#define TRODESMESSAGE_EVENTNAMEREQUEST  153
#define TRODESMESSAGE_EVENTREMOVALREQUEST  154
#define TRODESMESSAGE_REQUESTEVENTLIST  155
#define TRODESMESSAGE_EVENTUNSUBSCRIBE  156
#define TRODESMESSAGE_REQUESTSUBSCRIPTIONLIST  157
#define TRODESMESSAGE_SUBSCRIPTIONLIST  158
#define TRODESMESSAGE_EVENTSENT 159





// NOTE that message numbers > 100 are reserved for modules

//List of data types for dedicated sockets
//If > 0, the socket will only be used to stream one data
//type, and all other messaging will be disabled
// To add a datatype, create a new #define, make the number the next power of 2 and change MAXDATATYPE below to reflect the change


#define TRODESDATATYPE_MESSAGING 0
#define TRODESDATATYPE_SPIKES 1
#define TRODESDATATYPE_DIGITALIO 2
#define TRODESDATATYPE_ANALOGIO 4
#define TRODESDATATYPE_CONTINUOUS 8
#define TRODESDATATYPE_POSITION 16
#define TRODESDATATYPE_RIPPLEDETECTION 32
#define TRODESDATATYPE_BLOCK_CONTINUOUS 64



#define TRODESDATATYPE_MAXDATATYPE 64

#define TRODESHARDWARE_DEFAULTIP    "192.168.0.254"
#define TRODESHARDWARE_CONTROLPORT   8100
#define TRODESHARDWARE_DATAPORT      8200
#define TRODESHARDWARE_STATESCRIPTPORT 8110 //To the ECU
#define TRODESHARDWARE_STATESCRIPTRETURNPORT 8111 //From the ECU
#define TRODESHARDWARE_ECUDIRECTPORT    8120


//TRODESMESSAGE_PLAYBACK_COMMAND specifier flags
enum PlaybackCommand {PC_NULL, PC_SEEK, PC_PAUSE, PC_PLAY, PC_STOP, PC_RECORD, PC_STOPRECORD};

//TRODESDATATYPE_POSITION specifier flags
enum PPacketType {PPT_NULL, PPT_Header, PPT_2DPos, PPT_Lin, PPT_LinTrack, PPT_Zone, PPT_Velocity};
enum DataType { DT_NULL, DT_int, DT_qreal, DT_uint8_t, DT_uint32_t, DT_int16_t };


typedef struct _DataClientInfo {
    int     socketType;
    uint8_t dataType;
    uint16_t nTrodeId;
    int nTrodeIndex;
    uint16_t decimation;
    unsigned short port;
    char hostName[80];
} DataClientInfo;

typedef struct _HardwareNetworkInfo {
    unsigned short port;
    char address[80];
} HardwareNetworkInfo;

typedef struct _DIOBuffer {
    uint32_t timestamp;
    int     port;
    char    input;
    char    value;
} DIOBuffer;


#endif // TRODESSOCKETDEFINES_H
