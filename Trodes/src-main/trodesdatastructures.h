#ifndef TRODESDATASTRUCTURES_H
#define TRODESDATASTRUCTURES_H
#include <QVariant>
#include <QVector>
#include <QDebug>
#include <stdint.h>
#include <QTime>
#include "trodesSocketDefines.h"

/* ********** DOCUMENTATION
 * NOTE: if you want to send and receive these data types as signals and slots in your
 *       module, you must declare them in the main.cpp file (after initializing the
 *       QApplication) of that application via the following code:
 *       "qRegisterMetaType<dataStruct>("dataStruct")", where dataStruct is the name of
 *       the class/struct that you want to use.
*/

//TRODESDATATYPE_POSITION specifier flags
/**
enum PPacketType {PPT_NULL, PPT_Header, PPT_2DPos, PPT_Lin, PPT_LinTrack, PPT_Velocity};
enum DataType { DT_NULL, DT_int, DT_qreal, DT_uint8_t, DT_uint32_t, DT_int16_t };
**/


//***********************************************************
//dataSend Class

class dataSend {

public:

    dataSend();
    template <typename T>
    //dataSend(DataType ty, T cont) { init(ty,QVariant(cont),sizeof(T)); }
    dataSend(DataType ty, T cont) { init(ty,QVariant(cont),sizeOfType(ty)); }
    dataSend(const dataSend &obj); //copy constructor

    template <typename T>
    inline void insert(T obj) {container = QVariant(obj); bytes = sizeof(obj); };
    inline void setType(DataType ty) { type = ty; };

    inline QVariant getObj(void) const { return(container); };
    inline DataType getType(void) const { return(type); };
    inline int getSize(void) const { return(bytes); };
    int sizeOfType(DataType ty);

    void printAtr(void);

private:
    DataType type;
    QVariant container;
    int bytes;
    void init(DataType ty, QVariant cont, int by);
};
Q_DECLARE_METATYPE(dataSend);

//***********************************************************
//dataPacket Class

class dataPacket {
public:
    dataPacket();
    dataPacket(PPacketType ty);
    dataPacket(const dataPacket &obj); //copy contructor
    ~dataPacket();

    void insert(dataSend someData);
    inline PPacketType getType(void) const { return(type); };
    inline int dataLength(void) const { return(data.length()); };
    inline int getSize(void) const { return(totalBytes); };
    dataSend getDataAt(int index) const;
    void printAtr(void) const;

private:
    PPacketType type;
    QVector<dataSend> data;
    int totalBytes;
};
Q_DECLARE_METATYPE(dataPacket);  //enables this class in signals

//***********************************************************
//TrodesEvent Class

class TrodesEvent {
    /*
     * The TrodesEvent class holds all information necessary for the Trodes main server to easily store
     * particular event data.
     */
public:
    TrodesEvent();
    TrodesEvent(QString name, QString mod, qint8 modID);
    TrodesEvent(const TrodesEvent &obj); //copy constructor

    inline void setEventName(QString name) { eventName = name; };
    inline void setModuleName(QString mod) { parentModule = mod; };
    inline void setModuleID(qint8 modID) { parentModuleID = modID; };
    inline QString getEventName(void) const {return(eventName);};
    inline QString getParentModule(void) const {return(parentModule);};
    inline qint8 getParentModuleID(void) const {return(parentModuleID);};

    void printEventInfo(void) const;

private:
    QString eventName;
    QString parentModule;
    qint8 parentModuleID;


};
Q_DECLARE_METATYPE(TrodesEvent);

//***********************************************************
//TrodesEventMessage Class

class TrodesEventMessage {
    /*
     * The TrodesEventMessage class was created so that when events are broadcast, a system timer
     * value would also be recorded at the time of broadcast to help facilitate latency testing
     * and also sync events accurately to logged data.
     */
public:
    TrodesEventMessage();
    TrodesEventMessage(QString eventMess);

    inline void setEventMessage(QString eM) {eventMessage = eM;};
    inline void setEventTimeToCur(void) {eventTime = QTime::currentTime().msecsSinceStartOfDay();};

    inline QString getEventMessage(void) {return(eventMessage);};
    inline int getTime(void) {return(eventTime);};

private:
    QString eventMessage;
    int eventTime;
};
Q_DECLARE_METATYPE(TrodesEventMessage);

//#include "trodesdatastructures.cpp"
#endif // TRODESDATASTRUCTURES_H
