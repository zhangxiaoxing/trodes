#ifndef TRODESSPLASHSCREEN_H
#define TRODESSPLASHSCREEN_H

#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QLayoutItem>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGroupBox>
#include <QPushButton>
#include <QSettings>
#include <QSpacerItem>
#include <QPropertyAnimation>
#include <QGraphicsOpacityEffect>
#include <QGraphicsItemAnimation>
#include <QCoreApplication>
#include <QPainter>
#include <QPixmap>
#include <QBitmap>
#include <QMovie>
#include <QFontDatabase>
#include <QEvent>
#include <QResizeEvent>
#include <QFileDialog>
#include <QDebug>
#include <QtNetwork>
#include "configuration.h"

//SS_PIXEL_SIZE_MODIFIER is just a simple transformation added to certain defined values to make the UI scale properly across platforms
#if defined (__APPLE__)
#define SS_PIXEL_SIZE_MODIFIER 3
#else
#define SS_PIXEL_SIZE_MODIFIER 0
#endif

#define SPLASH_SCREEN_DEFAULT_FADE_TIME 1000
#define SPLASH_SCREEN_NUM_REMEMBERED_FILES 5
#define SS_VERSION_ERROR_IMG_DIM 24
#define SS_EF_ABSOLUTE_MAX_WIDTH 500
#define SS_EF_BASE_VERTICAL_SPACING 30 //base vert padding (non-expanded)
#define SS_EF_BASE_HORIZONTAL_SPACING 50 //base horizontal padding (expanded)
#define SS_VERTICAL_SPACING (5+SS_PIXEL_SIZE_MODIFIER) //each EF element's vertical padding margin
#define SS_EF_HORIZONTAL_SPACING (25+SS_PIXEL_SIZE_MODIFIER)

//different UI design options
#define UI_FILL false //buttons fill on hover and open
#define JUST_BORDERS false //borders color on hover and open
#define ANIMATED_BORDER_FILL true //buttons fill on hover, but go white when opened.  Borders color on open.

#define UI_TEMPLATE_LIST false //template workspaces in 'CreateWorkspace' arranged in a list
#define UI_TEMPLATE_BROWSE true //'browse template' option in 'CreateWorkspace'

#define SS_EF_FRAME_LABEL_FONT "Leelawadee UI"
#define SS_EF_FRAME_LABEL_FONT_SIZE (12+SS_PIXEL_SIZE_MODIFIER)
#define SS_VERSION_INFO_FONT_SIZE (12+SS_PIXEL_SIZE_MODIFIER)
#define SS_INFO_TEXT_FONT_SIZE (10+SS_PIXEL_SIZE_MODIFIER)
#define CLOSE_FRAME_FULL_ANIMATION true //true = closing all frames will default to just white.  False = closing all frames will use animation fade

class AnimatedIcon : public QLabel {
    Q_OBJECT
public:
    explicit AnimatedIcon(QWidget *parent = 0);
    bool isRotating(void) {return(rotating);};

    void setImage(QString imgPath, QSize imgSize);

public slots:
    void startRotating(int angle);
    void stopRotating(void);

protected:
    void paintEvent(QPaintEvent *ev);

private:
    int rotationInterval;
    int currentAngle;
    bool rotating;
    QString path;
    QSize imageSize;

    void rotate(int angle); //rotates the image by the given angle

};

class LoadingScreen : public QFrame {
    Q_OBJECT
    void newParent() {
        if (!parent()) return;
        parent()->installEventFilter(this);
        raise();
    }
public:
    explicit LoadingScreen(QWidget *parent = 0);

    void debug(void);

public slots:
    void fadeIn(void);
    void fadeOut(void);

    void runAnimation_fade(int time, bool fadeIn);

protected:
    bool eventFilter(QObject *obj, QEvent *ev);
    bool event(QEvent *ev);

private:
    AnimatedIcon *trodesIcon;
    QPropertyAnimation *a_fade;
    QGraphicsItemAnimation *a_backgroundFade;
    bool visible;

private slots:
    void sendFadeCompleteSig(void);

signals:
    void sig_fadeAnimationComplete(bool isVisible);
};

class EmbeddedButton : public QLabel {
    Q_OBJECT
public:
    explicit EmbeddedButton( const QString& text = "", int maxTextLen = -1, QWidget* parent = 0);

    void setButtonText(QString text);
    void setHyperLink(QString link);
    void setMaxWidth(int width);
    void truncateText(void);

    bool isTextEmpty(void);

    virtual void setVisible(bool visible);

private:
    QString unformatedText;
    QString hyperLink;
    int maxWidth;

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

signals:
    void sig_clicked(void);
    void sig_sendLink(QString link);

};


class ExpandableFrame : public QFrame {
    Q_OBJECT
    Q_PROPERTY(QColor borderColor READ borderColor WRITE setBorderColor)
    Q_PROPERTY(QColor backgroundColor READ backgroundColor WRITE setBackgroundColor)
public:
    explicit ExpandableFrame(bool nonExpandable = false, QWidget *parent = 0);

    //property field declarations for color animations
    QColor borderColor() {
        return (Qt::black);
    }

    QColor backgroundColor() {
        return(Qt::black);
    }

//    void setExpandableFrameStyleSheet(const QString &styleSheet);
    void addWidget(QWidget *widget, int spacing=0, Qt::Alignment alignment = Qt::Alignment());

    inline bool isExpanded(void) {return(expanded);}
    void setExpanded(bool expand);
    void resizeFrame(void);

    void borderColorAnimation(int duration, QColor startColor, QColor endColor);
    void backgroundColorAnimation(int duration, QColor startColor, QColor endColor);

    void setBorderColor(QColor color);
    void setBackgroundColor(QColor color);
    void setBackgroundColor(QString color);
    void replaceStyleSheetPropertyValue(QString property, QString newValue);
    inline void setBaseColor(QString color) {defaultColor = color;}
    inline void setHoverColor(QString color) {hoverColor = color;}

    void setSize(QSize size);
    bool isWidgetInFrame(QWidget *ptr);
    inline QSize getBaseSize(void) {return(QSize(baseWidth,baseHeight));}
    inline QSize getExpandedSize(void) {return(QSize(maxWidth, maxHeight));}


protected:
    void mousePressEvent(QMouseEvent *event);
    void enterEvent(QEvent *event);
    void leaveEvent(QEvent *event);

private:
    bool verifyWidth(void);

    bool isStaticFrame;
    bool expanded;
    bool widgetsVisible;
    int baseWidth;
    int baseHeight;
    int maxHeight;
    int maxWidth;
    QString defaultColor;
    QString hoverColor;
    QColor curBackgroundColor;

    QList<QWidget*> widgets;

    //animations
    QPropertyAnimation *a_geometry;
    QPropertyAnimation *a_maxSize;
    QPropertyAnimation *a_minSize;
    QPropertyAnimation *a_borderColor;
    QPropertyAnimation *a_backgroundColor;

    QVBoxLayout *mainLayout;

private slots:
    void resizeAnimationFinished(void);
    void backgroundColorAnimationFinished(void);
    void setWidgetsVisible(bool visible);

signals:
    void sig_clicked(void);
    void sig_frameExpanding(ExpandableFrame *self);
};

class TrodesSplashScreen : public QGroupBox
{
    Q_OBJECT
    void newParent() { //if overlain on a parent widget, give the parent this widgets event filter
        if (!parent()) return;
        parent()->installEventFilter(this);
        raise();
    }
public:
    explicit TrodesSplashScreen(QWidget *parent = 0);
    ~TrodesSplashScreen();

    void initializeAnimations(void);
    void expandQuickStart();
protected:
    void mousePressEvent(QMouseEvent *event);

    //overload the eventFilter to intercept the parent's events and resize accordingly
    bool eventFilter(QObject *obj, QEvent *ev);
    bool event(QEvent *ev);

private:
    // *** UI Objects ***
    QLabel      *trodesLogo;
    QLabel      *labelLogoText;
    QHBoxLayout *trodesLogoLayout;
    QVBoxLayout *mainLayout;
    QVBoxLayout *verticalButtonLayout;

    //MARK: flaming
    bool flameOn;
    QMovie *flamingGif;

    //Expandable Frames
    ExpandableFrame *infoFrame;
    QLabel          *labelInfoFrameTitle;
    QFrame          *versionFrame;
    QLabel          *labelWarningIcon;
    QLabel          *labelInfoFrameTrodesVersion;
    QLabel          *labelGeneralInfo;

    ExpandableFrame *createWorkspaceFrame;
    QLabel          *labelCreateWorkspaceSelection;
    QLabel          *labelTemplateWorkpaces;

    ExpandableFrame *workspaceFrame;
    QLabel          *labelWorkspaceSelection;
    QLabel          *labelPrevWorkspaceSelection;

    ExpandableFrame *playbackFrame;
    QLabel          *labelPlaybackFileSelection;
    QLabel          *labelPrevPlaybackFileSelection;

    ExpandableFrame *quickstartFrame;
    QLabel          *labelQuickStart;

    QList<EmbeddedButton*> templateLabels;
    QList<EmbeddedButton*> workspaceLabels;
    QList<EmbeddedButton*> playbackFileLabels;

    QList<ExpandableFrame*> expandingFrames;

    //buttons
    EmbeddedButton *buttonCreateNewScratch;
    EmbeddedButton *buttonUseTemplate;
    EmbeddedButton *buttonLoad;
    EmbeddedButton *buttonPlayback;
    EmbeddedButton *buttonEthConnect;
    EmbeddedButton *buttonUsbConnect;

    // *** Properties ***    
    bool canLoadFile; //bool to prevent Trodes from trying to load multiple files at once

    //animations
    bool isFadedOut;
    QPropertyAnimation *animation;
    QPropertyAnimation *fadeOut;

    QPropertyAnimation *a_fade;

public slots:
    void versionChecker(bool isLatest, QString version, bool maintenancetool);
    void setCanLoadFile(bool canLoadF = true);

    void setVisible(bool visible);

    void runAnimation_windowFade(int time = 0, bool fadeIn = false );    
//    void runAnimation_fade(int time = 0);

private slots:
    void loadWorkspace(QString file);
    void loadFile(QString file);

    void closeOtherFrames(ExpandableFrame *expFrame);
    void closeAllFrames(void);
    void fadeAnimationFinished(void);
    void setTemplateLabelsAndLinks(void);
    void setFileLabelsAndLinks(void);
    void buttonCreateNewPressed(void);
    void buttonLoadPressed(void);
    void buttonPlaybackPressed(void);
    void chooseTemplateFile(void);

    void buttonEthPressed();
    void buttonUsbPressed();

signals:
    void sig_fadeAnimationCompleted(void);
    void sig_createNewWorkspaceButtonPressed(void);
    void sig_loadWorkspaceButtonPressed(void);
    void sig_loadFileForPlaybackButtonPressed(void);
    void sig_loadTemplateWorkspace(QString filePath);
    void sig_loadWorkspace(QString filePath);
    void sig_initializeFilePlayback(QString filePath);

    void quickstartEthernet();
    void quickstartUSB();

    void launchUpdater();
};

#endif // TRODESSPLASHSCREEN_H
