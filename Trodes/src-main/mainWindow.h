﻿/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef UI_MAIN_H
#define UI_MAIN_H

#include <sys/types.h>
#include <QtGui>
#include "spikeDisplay.h"
#include "streamDisplay.h"
#include "streamProcessorThread.h"
#include <QAudioOutput>
#include <QAudioFormat>
#include "audioThread.h"
#include "triggerThread.h"
#include "recordThread.h"
#include "trodesSocket.h"
#include "networkMessage.h"
#include "dialogs.h"
#include "globalObjects.h"
#include "sourceController.h"
#include "trodesSplashScreen.h"
#include "benchmarkWidget.h"

#include <TrodesCentralServer.h>
#include <AbstractModuleClient.h>


#include "quicksetup.h"
#include "rfwidget.h"
#include "probelayoutpanel.h"

// part of trodesnetwork-v2
#include <memory>
#include <TrodesModule.h>

QT_BEGIN_NAMESPACE


class Style_tweaks : public QProxyStyle
{
    public:

        void drawPrimitive(PrimitiveElement element, const QStyleOption *option,
                           QPainter *painter, const QWidget *widget) const
        {
            /* do not draw focus rectangles - this permits modern styling */
            if (element == QStyle::PE_FrameFocusRect)
                return;

            QProxyStyle::drawPrimitive(element, option, painter, widget);
        }
};

//NOTE: scaling for the backgroundFrame will sometimes be incorrect...
class BackgroundFrame : public QFrame {
    Q_OBJECT
    void newParent() {
        if (!parent()) return;
        parent()->installEventFilter(this);
        raise();
    }
public:
    explicit BackgroundFrame(QWidget *parent = 0);

protected:
    bool eventFilter(QObject *obj, QEvent *ev);
    bool event(QEvent *ev);
};
class SourceButton : public TrodesButton{
    Q_OBJECT
public:
    SourceButton() : TrodesButton(nullptr){
        setIcon(QIcon(":/buttons/cable2.png"));
        setMenu(new QMenu());
        setIconSize(QSize(15, 15));
        setFixedSize(50, 20);
    }
    void addAction(QAction *action){
        menu()->addAction(action);
    }
};

class RecordFileButton : public TrodesButton{
    Q_OBJECT
public:
    //https://wiki.qt.io/PushButton_Based_On_Action

    RecordFileButton() : TrodesButton(nullptr){
//        setIcon(QIcon(":/buttons/new-file.png"));
//        setIconSize(QSize(15, 15));
        setFixedSize(50, 20);
        openAction = nullptr;
        closeAction = nullptr;
    }
    void setOpenRecordAction(QAction *openrec){
        if (openAction && openAction != openrec) {
            disconnect(openAction, &QAction::changed, this, &RecordFileButton::updateButtonFromOpenAction);
            disconnect(this, &RecordFileButton::clicked, openAction, &QAction::trigger);
        }

        openAction = openrec;
        updateButtonFromOpenAction();

        connect(openrec, &QAction::changed, this, &RecordFileButton::updateButtonFromOpenAction);
        connect(this, &RecordFileButton::clicked, openAction, &QAction::trigger);
    }
    void setCloseFileAction(QAction *closefile){
        if (closeAction && closeAction != closefile) {
            disconnect(closeAction, &QAction::changed, this, &RecordFileButton::updateButtonFromCloseAction);
            disconnect(this, &RecordFileButton::clicked, closeAction, &QAction::trigger);
        }

        closeAction = closefile;
        updateButtonFromCloseAction();

        connect(closefile, &QAction::changed, this, &RecordFileButton::updateButtonFromCloseAction);
        connect(this, &RecordFileButton::clicked, closeAction, &QAction::trigger);
    }
    void updateButtonFromOpenAction(){
        if (!openAction)
            return;
        setText(openAction->text());
        setStatusTip(openAction->statusTip());
        setToolTip(openAction->toolTip());
        setIcon(openAction->icon());
        setEnabled(openAction->isEnabled());
        setCheckable(openAction->isCheckable());
        setChecked(openAction->isChecked());
    }
    void updateButtonFromCloseAction(){
        if (!closeAction)
            return;
        setText(closeAction->text());
        setStatusTip(closeAction->statusTip());
        setToolTip(closeAction->toolTip());
        setIcon(closeAction->icon());
        setEnabled(closeAction->isEnabled());
        setCheckable(closeAction->isCheckable());
        setChecked(closeAction->isChecked());
    }
private:
    QAction *openAction;
    QAction *closeAction;
};

class MainWindow : public QMainWindow
{
  Q_OBJECT

private:
    std::unique_ptr<trodes::TrodesModule> trodesModule;

    bool isSplashScreenVisible;
    bool isAudioOn;
    QHash<int, int> selectedNTrodes;
    int prevSelectedNTrodeIndex;
    bool analysisModeOn;
    bool closeInitiated;

    NTrodeSelectionDialog *nTrodeSelectWindow;

private slots:
    void about();
    void aboutHardware();
    void aboutCurConfig();
    void aboutVersion();
    void aboutPlayback();
    bool checkMultipleInstances(QString errorMessage);
    void saveWorkspacePathToSystem(QString workspacePath);
    void savePlaybackFilePathToSystem(QString playbackFilePath);
    void swapSplashScreenAndTabView(bool loadSplash);
    void setBackgroundFrameVisibility(void);
    void changeAudioDevice(QString);
    void SetUpStreamManager();
    void SetUpStreamDisplay(StreamProcessorManager* streamManager);
    void SetUpSpikeDisplay(StreamProcessorManager* streamManager);

    void fillSourceOptionsMenu();
    void clearSourceOptionsMenu();
    void openCustomSourceOptionsGUI();
    void enableSourceMenu(QString menuName);
    void setDebugCheckpoint(bool on, QString name = "");
    void printDebugCheckpoint(QString custom = "");

protected:
    void keyPressEvent(QKeyEvent *event);
    void closeEvent(QCloseEvent* event);
    void resizeEvent(QResizeEvent *);
    void moveEvent(QMoveEvent *);
    void paintEvent(QPaintEvent *);

public:
    void setSplashScreenAnimation(bool v);

    int                         findEndOfConfigSection(QString filename);

    bool                        animateSplash;
    TrodesSplashScreen          *splashScreen;
    WorkspaceEditorDialog       *workspaceEditor;
    BackgroundFrame             *tabsBackground;

    LoadingScreen               *loadingScreen;
    QThread                     *lsThread; //loadingScreen thread;

    QGridLayout                 *playbackLayout;
    QSlider                     *playbackSlider;
    QLabel                      *playbackStartTimeLabel;
    QLabel                      *playbackEndTimeLabel;
//    QTime                       *playbackStartClock;
//    QTime                       *playbackEndClock;
    QString                     timeFormatString;
    uint32_t                    playbackStartTime;
    uint32_t                    playbackEndTime;

    uint32_t                    totalTimeRecorded;
    uint32_t                    lastPlayPauseTime;

    QTabWidget                  *tabs;
    int                         lastTabIndex;
    QTabWidget                  *eventTabs;
    //QList<NtrodeDisplayWidget*> ntrodeDisplayWidgetPtrs;
    QVBoxLayout                 *ultraLayout;
    QGridLayout                 *mainLayout;
    QGridLayout                 *headerLayout;
    QLabel                      *timeLabel;
    QLabel                      *fileLabel;
    QLabel                      *fileStatusColorIndicator;
    QLabel                      *streamStatusColorIndicator;
    QTimer                      *statusErrorBlinkTimer;
    TrodesButton                 *soundSettingsButton;
    TrodesButton                 *trodeSettingsButton;
    TrodesButton                 *commentButton;
    TrodesButton                 *spikesButton;
    TrodesButton                 *videoButton;
    TrodesButton                 *statescriptButton;
    TrodesButton                 *recordButton;
    TrodesButton                 *pauseButton;
    TrodesButton                 *playButton;
    TrodesButton                 *playbackInfoButton;
//    TrodesButton                 *linkChangesButton;
    TrodesButton                 *uncoupleDisplayButton;
    TrodesButton                 *streamDisplayButton;

    QString                     fileString;
    int                         currentFilePart;
    bool                        soundDialogOpen;
    bool                        recordFileOpen;
    QString                     recordFileName;
    QString                     queuedRecordFileName;
    QString                     recordFileBaseName;
    QString                     lastRecordFileName;
    bool                        dataStreaming;
    bool                        dataWasStreamingBeforeLiveReconfig;
    QString                     oldConfigFile;
    bool                        liveReconfigHappening;
    bool                        moduleDataStreaming;
    bool                        recording;
    int                         timerTick;
    bool                        eventTabWasChanged;
    bool                        statusBlinkRed;
    bool                        noDataErrFlag;
    bool                        packetErrFlag;
    bool                        unresponsiveHeadstageFlag;
    bool                        noHeadstageErrFlag;
    bool                        tempWorkspaceLoading;

    TrodesConfiguration         playbackEmbeddedWorkspace; //workspace embedded in the playback file
    TrodesConfiguration         playbackExternalWorkspace; //replacement workspace for playback file
    TrodesConfiguration         acquisitionWorkspace; //workspace for acquiring data

    TrodesConfigurationPointers activeWorkspacePointers;

    QString                     calcTimeString(uint32_t tmpTimeStamp);
    QString                     currentConfigFileName;
    QString                     loadedConfigFile;
    int64_t                     visibleTime;
    bool                        eventTabsInitialized[64];
    int                         currentTrodeSelected;
    bool                        singleTriggerWindowOpen;
    bool                        channelsConfigured;
    int                         sourceMenuState;
    QTimer                      *pullTimer;
    QTimer                      *workspaceShutdownCheckTimer;
    TrodesConfiguration         stagedWorkspaceForLoad;
    QString                     stagedPlaybackFileForLoad;
    bool                        quitting;  //True if the program is in the process of quitting
    bool                        unitTestFlag;
    bool                        configSettingsChanged;
    bool                        spectrumAnalysisWindowOpen;
    bool                        impedanceToolOpen;
    bool                        RMSPlotOpen;
    QString                     debugCheckpointStatement;  //used for checkpoint debug statments
    int                         debugCheckpointNumber;
    bool                        debugCheckpointsOn;
    bool                        usingExternalWorkspace;

    int settleChannelByteInPacket;
    quint8 settleChannelBit;
    quint8 settleChannelTriggerState;
    int settleChannelDelay;


    HeadstageSettings headstageSettings;
    HardwareControllerSettings controllerSettings;

    bool                        enableAVXFlag;
    StreamProcessorManager      *streamManager;
    SourceController            *sourceControl;
    StreamDisplayManager        *eegDisp;
    //SpikeDisplayWidget          *spikeDisp;
    MultiNtrodeDisplayWidget    *spikeDisp;
    //SDDisplay                   *sdDisp;
//    SDDisplayPanel              *sdDisp;
    AudioController             *soundOut;
    soundDialog                 *newSoundDialog;
    RecordThread                *recordOut;
    TrodesModuleNetwork         *trodesNet;
    QThread                     *trodesNetThread;
    //mark: Network

    BenchmarkWidget             *benchmarkingControlPanel;
    CommentDialog               *newCommentDialog;
    NTrodeSettingsWidget  *triggerSettings;
    PreferencesPanel            *preferencesPanel;
    NetworkPanel                *networkPanel;
    RFWidget                    *rfwidget;
    QString timeStampToString(uint32_t);


    void startThreads();

    QMenu *menuFile;
    QMenu *menuConfig;
    QAction *actionAboutConfig;
    QAction *actionLoadConfig;
    QAction *actionCloseConfig;
    QAction *actionSaveConfig;
    QAction *actionReConfig;
    QAction *actionOpenRecordDialog;
    QAction *actionPlaybackOpen;
    QAction *actionCloseFile;
    QAction *menuExport;
    QAction *actionSaveClusters;
    QAction *actionLoadClusters;
    QAction *actionRecord;
    QAction *actionPause;
    QAction *actionPlay;
    QAction *actionQuit;
    QAction *actionMerge;


    QMenu *menuSystem;
    QMenu *sourceMenu;
    QMenu *sourceOptionsMenu;

    QMenu *menuSimulationSource;
    QMenu *menuSpikeGadgetsSource;

    QAction *actionSourceNone;
    QAction *actionSourceFake;
    QAction *actionSourceFakeSpikes;
    QAction *actionSourceFile;
    QAction *actionSourceUSB;
    QAction *actionSourceUSB3;
    QAction *actionSourceRhythm;
    QAction *actionSourceEthernet;
    QAction *actionSourceDockUSB;
    QAction *actionSound;



    QAction *actionConnect;
    QAction *actionDisconnect;
    QAction *actionClearBuffers; //a debugging tool
    QAction *actionSendSettle;
    QAction *actionOpenGeneratorDialog;

    QAction *actionRestartModules;


    QMenu *menuHelp;
    QAction *actionAboutTrodes;
    QAction *actionAboutQT;
    QAction *actionAboutHardware;
    QAction *actionAboutVersion;
    QAction *actionAboutPlaybackFile;
    bool isLatestVersion;

    QMenu   *menuDisplay;
    QMenu   *menuEEGDisplay;
    QMenu   *menuSetTLength;
    QAction *actionSetTLength0_2;
    QAction *actionSetTLength0_5;
    QAction *actionSetTLength1_0;
    QAction *actionSetTLength2_0;
    QAction *actionSetTLength5_0;
    QMenu   *streamFilterMenu;
    QAction *streamLFPFiltersOn;
    QAction *streamSpikeFiltersOn;
    QAction *streamStimViewOn;
    QAction *streamNoFiltersOn;
    QAction *actionDisplayAllPETHs;
    QMenu   *menuTools;
    QAction *actionDisplaySpectrumAnalysis;
    QAction *actionDisplayImpedanceTool;
    QAction *actionDisplayRMSPlot;
    QAction *actionDisplayRFWidget;


    QMenu   *menuSettings;
    QMenu   *menuSelect;
    QAction *actionSelectAll;
    QAction *actionSelectByTag;

    QMenu   *menuDebug;
    QMenu   *menuNTrode;
    QAction *actionShowCurrentTrode;
//    QMenu   *menuLinkChanges;
//    QAction *actionLinkChanges;
//    QAction *actionUnLinkChanges;
    QAction *actionUncoupleDisplay;
    QAction *actionRealtimeDisplay;

    QAction *clearAllNTrodes;
    QMenu   *menuHeadstage;
    QAction *actionHeadstageSettings;
    QAction *actionControllerSettings;
    QAction *actionSettleDelay;
    QAction *actionBenchmarkingSettings;

    QAction   *menuPreferences;
    QAction   *menuNetworkPanel;
    QElapsedTimer *recordTimer;
    int msecRecorded;
    QStatusBar *statusbar;
    QWidget *centralwidget;

    QProcess* mergeProcess;
    bool mergeSucessful;

    MainWindow(bool audioOn = true);
    ~MainWindow();
    void retranslateUi();
    // retranslateUi

    void setEnableAVXFlag(bool value);

    QList<QDockWidget*> dockingStations;
    void dockingStationConnected();
    void dockingStationDisconnected();

signals:
    void closeRMSDialog();
    void closeAnalysisDialog();
    void gotversion(bool, QString, bool);
    void configFileLoaded();
    void setAudioChannel(int hwchannel);
    void setAudioDevice(QString device);
    void updateAudio();
    void closeAllWindows();
    void closeSoundDialog();
    void closeWaveformDialog();
    void endAllThreads();
    void endAudioThread();
    void newTraceLength(double);
    void newTimeStamp(uint32_t);
    void recordFileOpened(QString filename);
    void sourceConnected(int);
    //void sourceConnected(QString source);
    void recordFileClosed();
    void recordingStarted();
    void recordingStopped();
    void messageForModules(TrodesMessage *tm);
    void clearDataAvailable(void); //This signal tells moduleNet to remove all non-trodes data available markers
    void signalSaveClusters(QString filename);
    void signalLoadClusters(QString fiename);
    void sendEvent(uint32_t t, int evTime, QString eventName);

    void jumpFileTo(qreal value);
    void signal_sendPlaybackCommand(qint8 cmdFlg, qint32 ts);
    void sig_sendPlaybackCommand(QString cmd, uint32_t ts);

    void sig_channelColorChanged(QColor color);
    void sig_fadeInLoadscreen(void);
    void sig_fadeOutLoadscreen(void);
    void openallpeths();

public slots:

    void checkForUpdate();
    void spectrumAnalysisWindowClosed();
    void impedanceToolClosed();
    void RMSPlotClosed();
    void parseReceivedVersion(QString version, bool maintenancetool);
    void launchUpdater();
    void sourcePacketSizeError(bool on);
    void sourceUnresponsiveHeadstage(bool on);
    void sourceNoDataError(bool on);
    void sourceNoHeadstageError(bool on);
    void toggleErrorBlink(bool blink);
    void updateErrorBlink();
    void errorBlinkTmrFunc();
    void audioChannelChanged(int hwchannel);
    void nTrodeClicked(int nTrodeInd, Qt::KeyboardModifiers mods);
    void processNTrodeRightClickAction(QString actionStr);
    void selectTrode(int nTrode);
    void streamTabChanged(int tabIndex);
    bool openPlaybackFile(void);
    bool openPlaybackFile(const QString fileName);
    bool openWorkspaceFileForAcquisition(void);
    bool openWorkspaceFileForAcquisition(const QString fileName);
    bool openWorkspaceFileForAcquisition(const TrodesConfiguration &c);
    void eventTabChanged(int);
    void setSourceMenuState(int);
    void sourceStateChanged(int);
    void resetAllAudioButtons();
    void updateTime();
    void lfpFiltersOn();
    void spikeFiltersOn();
    void mixedVisualFiltersOn();
    void stimViewOn();
    void noFiltersOn();
//    void linkChanges(bool link);
//    void linkChanges();
//    void unLinkChanges();
    void uncoupleDisplay(bool uncouple);
    void setRealTimeDisplayMode(bool realtime);
    void streamdisplaybuttonPressed();
    void openSoundDialog();
    void openGeneratorDialog();
    void openHeadstageDialog();
    void openControllerSettingsDialog();
    void headstageSettingsChanged(HeadstageSettings s);
    void controllerSettingsChanged(HardwareControllerSettings s);
    void openSettleChannelDelayDialog();
    void setSettleChannelDelay(int delay);
    void openTrodeSettingsWindow();
    void openTrodeWindow();
    void enableHeadstageDialogMenu();
    void enableControllerDialogMenu();
    void enableGeneratorDialogMenu();
    void soundButtonPressed();
    void audioDeviceChanged(QAudioSink *);
    void commentButtonPressed();
    void spikesButtonToggled(bool on);
    void spikesPanelClosed();
    void videoButtonPressed(QString videoString = "");
    void statescriptButtonPressed();
    void exportActionSelected();
    void openExportMenu();
    void saveCurrentClusters();
    void loadClusterFile(QString filename);
    void loadClusterFile();
    void selectedNTrodesUpdated(void);
    void trodesButtonToggled(bool on);
    void trodeSettingsPanelClosed(void);
    void saveTrodeSettingPanelVisible(bool wasOpen);
    void saveSpikesPanelVisible(bool wasOpen);
    void saveTrodeSettingPanelPos(QRect geo);
    void setMaxDisp(int newMaxDisp); //this function sets all selected nTrodes max displays to the specified integer value
    void setAllRefs(int nTrode, int channel);
    void setAllMaxDisp(int newMaxDisp);
    void setAllThresh(int newThresh);
    void setAllFilters(int low, int high);
    void toggleAllFilters(bool on);
    void toggleAllRefs(bool on);
    void toggleAllTriggers(bool on);
    void setTLength();
    void toggleSpectralAnalysis(bool on);
    void toggleImpedanceTool(bool on);
    void toggleRMSPlot(bool on);
    void setSource();
    void setSource(DataSource source);
    void connectToSource();
    bool disconnectFromSource();
    void sendSettleCommand();
    void clearAll();
    bool loadWorkspaceToDisplay();
    void loadWorkspaceToDisplayWhenReady(const TrodesConfiguration &tc);
    void checkReadyForWorkspaceLoad();
    void openPlaybackFileLater();
    void exitProgramLater();
    void closeWorkspace(); //wraper binding for closeConfig
    void closeConfig(bool openSplashScreen = true);
    void settingsChanged();
    void settingsWarning();
    void saveConfig();
    void saveConfig(QString fname);
    void reConfig_live_close();
    void reConfig_live_reopen(const TrodesConfiguration &newWorkspace);
    void reConfig();
    void openRecordDialog();
    void createNextFilePart();
    void closeFile();
    void openRecordFile(QString filename);
    void openQueuedRecordFile();
    void recordButtonPressed();
    void pauseButtonPressed();
    void playButtonPressed();
    void recordButtonReleased();
    void pauseButtonReleased();
    void playButtonReleased();
    void actionRecordSelected();
    void actionPauseSelected();
    void actionPlaySelected();

    //Merge functions and process
    void actionMergeSelected();
    void readDataFromMergeProcess();
    void openMergedFile();

    void exportData(bool spikesOn, bool ModuleDataon, int triggerSetting, int noiseSetting, int ModuleDataChannelSetting, int ModuleDataFilterSetting);
    void cancelExport();
    void removeFromOpenNtrodeList(int nTrodeNum);
    void checkRestartModules(void);
    void quitModules(void);
    void bufferOverrunHandler();
    void showErrorMessage(QString msg);
    void showWarningMessage(QString msg);
    void errorSaving();
    void noSpaceLeftWhileSaving();
    void lowDiskSpaceWarning();
    void restartCrashedModule(QString modName);

    //mark: network new stuff
    void startTrodesNetwork(void);
    void endTrodesNetwork(void);
    void loadConfigIntoNetworkObject(void);
    void processAquisitionCommand(QString cmd, qint32 time);
    void sendAquisitionCommand(qint8 cmd, qint32 time);
    void playbackAcquisitionStopped();

    //old network
    void startMainNetworkMessaging();
    void startModules(QString fileName);
    QProcess* startSingleModule(SingleModuleConf s, QProcess* moduleProcess);

    void sendModuleDataChanToModules(int nTrode, int chan);

    void setModuleDataStreaming(bool);
    bool isModuleDataStreaming(void);

    void threadError(QString errorString);
    void forwardProcessOutput();

    void setSettleControlChannel(int byteInPacket, quint8 bit, quint8 triggerState); //receives signal from streamDisplay

    void setTimeStampLabels(uint32_t playbackStartTimeStamp, uint32_t playbackEndTimeStamp);
    void updateSlider(qreal);
    void sliderisPressed();
    void sliderIsReleased();
    void movingSlider(int value);
    void updateTimeFromSlider(int value);
    void pausePlaybackSignal();
    void playPlaybackSignal();
    void seekPlaybackSignal(uint32_t t);
    void approxSliderMove(uint32_t);


    void broadcastEvent(TrodesEventMessage ev);


    void raiseWorkspaceGui(void);
    void loadFileInWorkspaceGui();
    void loadFileInWorkspaceGui(QString filePath);
    void openTempWorkspace(QString tempPath, QString filenmame);

    //MARK: select by dialog
    void openSelectionDialog(void);
    void receivedSelectByTagCommand(int operation, QHash<GroupingTag,int> selectedTags);
    void clearAllSelected(void);
    void selectAllNTrodes(void);

    //MARK: benchmarking dialog
    void openBenchmarkingDialog(void);
    void processPlaybackCommand(qint8 flg, qint32 timestamp);

    //void quickstartEthernet();
    //void quickstartUSB();

    void generateWorkspace(bool ecuconnected, bool rfconnected, int chansperntrode, int psize, int samplingRate);
    WorkspaceEditorDialog* fillInWorkspaceEditor(bool ecuconnected, bool rfconnected, int chansperntrode, int psize, int samplingRate);
    void launchWorkspaceEditorFromQuickstart(bool ecuconnected, bool rfconnected, int chansperntrode, int psize, int samplingRate);
    void finalizeFromQuickstart();
};


  QT_END_NAMESPACE

#endif // MAIN_H
