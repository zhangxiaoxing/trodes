#include "analyses.h"



SpectralAnalysis::SpectralAnalysis(QObject *parent) :
    QObject(parent)
    ,   totalReadHead(0)
    ,   totalWriteHead(0)
    ,   writeHead(0)
    ,   readHead(0)
    ,   bufferLength(SpectrumLengthSamples*5)
    ,   inputFreqHz(30000)
    ,   analysisWindowSamples(SpectrumLengthSamples)


{




    dataBuffer = new int16_t[bufferLength];

    qRegisterMetaType<FrequencySpectrum>("FrequencySpectrum");
    qRegisterMetaType<WindowFunction>("WindowFunction");
    qRegisterMetaType<QList<QPointF> >("QList<QPointF>");


    qDebug() << "Creating spectrum analyser";

    m_spectrumAnalyser = new SpectrumAnalyser(this);
    m_spectrumAnalyser->setWindowFunction(HannWindow);

    m_spectrumBufferLength = analysisWindowSamples*2;
    m_spectrumBuffer.resize(m_spectrumBufferLength);
    //m_spectrumBuffer.fill(0);




    CHECKED_CONNECT(m_spectrumAnalyser,
                    SIGNAL(spectrumChanged(FrequencySpectrum)),
                    this,
                    SLOT(spectrumChanged(FrequencySpectrum)));

}


SpectralAnalysis::~SpectralAnalysis() {
    delete [] dataBuffer;
}

void SpectralAnalysis::calculateSpectrum()
{

    QAudioFormat m_format;
    m_format.setSampleFormat(QAudioFormat::Int16);



    if (m_spectrumAnalyser->isReady()) {


        char *dataPtr = (char*)(dataBuffer+readHead);
        QByteArray sendData;
        sendData.resize(m_spectrumBufferLength);

        if ((bufferLength-readHead) > (analysisWindowSamples)) {
            //We are not going to reach the end of the ring buffer, so only one copy needed
            sendData = QByteArray::fromRawData(dataPtr,m_spectrumBufferLength);

        } else {
            //We are going to reach the end of the ring buffer, so we will need to loop back and copy the rest

            int sampToCpy = 2*(bufferLength-readHead);

            sendData = QByteArray::fromRawData(dataPtr,sampToCpy-2);
            dataPtr = (char*)(dataBuffer);
            sendData.append(dataPtr,m_spectrumBufferLength-sampToCpy);

        }
        //m_spectrumAnalyser->calculate(m_spectrumBuffer, m_format);
        //sendData = QByteArray::fromRawData(sendBuffer,m_spectrumBufferLength);
        m_spectrumAnalyser->calculate(sendData, m_format);
        //m_spectrumAnalyser->calculate(sendBuffer, m_format);


    }

    //Even if the analyser was not ready, we need to advance the read head
    readHead = (readHead+analysisWindowSamples)%bufferLength;
    totalReadHead = totalReadHead+analysisWindowSamples;

}


void SpectralAnalysis::resetBuffer() {
    writeHead = 0;
    readHead = 0;

    totalReadHead = 0;
    totalWriteHead = 0;

    for (int i = 0; i < bufferLength; i++) {
        dataBuffer[i] = 0;
    }

}


void SpectralAnalysis::addValue(int16_t sample) {
    dataBuffer[writeHead] = sample;
    writeHead = (writeHead + 1) % bufferLength;
    totalWriteHead += 1;

    if (totalWriteHead-totalReadHead > analysisWindowSamples) {
        calculateSpectrum();
    }

}

void SpectralAnalysis::setSamplingRate(int rate) {
    inputFreqHz = rate;
}


void SpectralAnalysis::spectrumChanged(const FrequencySpectrum &spectrum)
{

    QList<QPointF> dataPoints;
    for (int i=0;i<spectrum.count();i++) {
        if (spectrum[i].frequency > 0.0) {
            /*if (spectrum[i].frequency < 200) {
                qDebug() << spectrum[i].frequency;
            }*/
            dataPoints.append(QPointF(spectrum[i].frequency,spectrum[i].amplitude));
        }
    }

    emit newSpectrum(dataPoints);

}





