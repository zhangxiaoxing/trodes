﻿#include <TrodesNetwork/Resources/ServiceProvider.h>
#include <TrodesNetwork/Generated/DataCommand.h>

#include <thread>

#include "abstractTrodesSource.h"
#include "globalObjects.h"
#include <QtNetwork>
#include "CZHelp.h"
#include <cmath>

extern bool unitTestMode;
eegDataBuffer rawData;

//Simple ring buffer with individual write blocks of equal size. Designed to be thread safe if
//one thread is write-only and another is read-only.
TransferBlockRingBuffer::TransferBlockRingBuffer(int maxTransferSize, int ringSize)
    :currentWriteBlock(0),
     currentReservedWriteBlock(0),
     unfinishedWriteJobs(0),
     currentReadBlock(0),
     currentReadBlockByte(0),
     totalBytesWritten(0),
     totalBytesRead(0),
     totalBlocksWritten(0),
     totalBlocksRead(0)
{
    for (int i = 0;i < ringSize; i++) {
        buffers.push_back(QVector<uchar>(maxTransferSize));
        transferSizes.push_back(0);
    }
}
TransferBlockRingBuffer::~TransferBlockRingBuffer()
{
    clear();
}
void TransferBlockRingBuffer::setBufferSize(int maxTransferSize, int ringSize)
{
    clear();
    for (int i = 0;i < ringSize; i++) {
        buffers.push_back(QVector<uchar>(maxTransferSize));
        transferSizes.push_back(0);
    }
}

uchar *TransferBlockRingBuffer::getNextWritePtr(bool *ok)
{
    //Returns a pointer to the beginning of the current block for writing

    if (totalBlocksWritten+unfinishedWriteJobs-totalBlocksRead < (buffers.size()-1)) {
        uchar* rVal =  buffers[currentReservedWriteBlock].data();
        unfinishedWriteJobs++;
        currentReservedWriteBlock = (currentReservedWriteBlock+1) % buffers.size();
        *ok = true;
        return rVal;

    } else {
        //We can't advance the write marker because we would overwrite unread data.
        //This is the main check for buffer overrun
        *ok = false;
        return nullptr;
    }

    //return buffers[currentWriteBlock].data();
}
void TransferBlockRingBuffer::setLastWriteTransferSize(int size)
{
    //Once a block transfer is complete, we need to remember how large it was (it may not have reached the full block size).
    transferSizes[currentWriteBlock] = size;
    totalBytesWritten += transferSizes[currentWriteBlock];
    currentWriteBlock = (currentWriteBlock+1) % buffers.size();
    totalBlocksWritten++;
    unfinishedWriteJobs--;
    if (unfinishedWriteJobs < 0) {
        unfinishedWriteJobs = 0;
    }

}
bool TransferBlockRingBuffer::advanceWriteMarker()
{
    //Advance the index to the next write buffer. If at the end of the ring, go to beginning.
    if (totalBlocksWritten-totalBlocksRead < (buffers.size()-1)) {
        totalBytesWritten += transferSizes[currentWriteBlock];
        currentWriteBlock = (currentWriteBlock+1) % buffers.size();
        /*if (currentWriteBlock == 0) {
            qDebug() << "Write buffer looped" << currentReadBlock << currentWriteBlock << totalBytesRead << totalBytesWritten << totalBlocksRead << totalBlocksWritten;
        }*/
        totalBlocksWritten++;
        return true;
    } else {
        //We can't advance the write marker because we would overwrite unread data.
        //This is the main check for buffer overrun
        return false;
    }
}
int TransferBlockRingBuffer::bytesAvailable()
{
    return totalBytesWritten-totalBytesRead;
}
int TransferBlockRingBuffer::read(uchar *outBuffer, int size)
{
    //Copy over the requested number of bytes. This operation needs to deal with block boundaries.
    int copyIndex = 0;
    int bAvail = bytesAvailable();

    if (size > bAvail) {
        size = bAvail;
    }

    int tSize = size;

    while (size > 0) {
        int remainingBlockData = transferSizes[currentReadBlock]-currentReadBlockByte;
        if (size > remainingBlockData) {
            //size is larger than the data left in the current read block, so we copy over the rest of the current read block.
            std::copy (buffers[currentReadBlock].data()+currentReadBlockByte, buffers[currentReadBlock].data()+transferSizes[currentReadBlock], outBuffer+copyIndex);
            size -= remainingBlockData;
            copyIndex += remainingBlockData;
            currentReadBlock = (currentReadBlock+1)%buffers.size();
            /*if (currentReadBlock == 0) {
                qDebug() << "Read buffer looped" << currentReadBlock << currentWriteBlock << totalBytesRead << totalBytesWritten << totalBlocksRead << totalBlocksWritten;
            }*/
            currentReadBlockByte = 0;
            totalBlocksRead++;
        } else {
            //size is less than the data left in the current read block
            std::copy (buffers[currentReadBlock].data()+currentReadBlockByte, buffers[currentReadBlock].data()+currentReadBlockByte+size, outBuffer+copyIndex);
            currentReadBlockByte += size;
            size = 0;
        }

    }

    totalBytesRead += tSize;
    return tSize; //return the actual number of bytes read
}
void TransferBlockRingBuffer::clear()
{
    for (int i=0;i<buffers.size();i++) {
        buffers[i].clear();
    }
    buffers.clear();
    currentReadBlock = 0;
    currentReadBlockByte = 0;
    currentWriteBlock = 0;
    currentReservedWriteBlock = 0;
    unfinishedWriteJobs = 0;
    totalBytesRead = 0;
    totalBytesWritten = 0;
    totalBlocksRead = 0;
    totalBlocksWritten = 0;
}
//----------------------------------------

RawDataPublisher::RawDataPublisher(QString host, int port, int PSize, int hsize)
    : host(host),
      port(port),
      PACKET_SIZE(PSize),
      headerSize(hsize),
      neuralDataRequested(false),
      digitalDataRequested(false),
      analogDataRequested(false),
      timeDataRequested(false)
{
    packetBuff = NULL;
}

//------------------------------------------------------------------
RawDataPublisher::~RawDataPublisher(){
}

void RawDataPublisher::setHeaderSize(int hsize)
{
    headerSize = hsize;
}

void RawDataPublisher::setSendNeuralData(bool send)
{
    neuralDataRequested = send;
}
void RawDataPublisher::setSendDigitalData(bool send)
{
    digitalDataRequested = send;
}
void RawDataPublisher::setSendAnalogData(bool send)
{
    analogDataRequested = send;
}
void RawDataPublisher::setSendTimeData(bool send)
{
    timeDataRequested = send;
}

// returns true if successful
bool RawDataPublisher::startDedicatedDataPub(){
        allocatePacketBuffer();

        std::string address = host.toStdString();

        timestampTrodesPub = std::move(std::unique_ptr<trodes::network::SourcePublisher<trodes::network::TrodesTimestampData>>(new trodes::network::SourcePublisher<trodes::network::TrodesTimestampData>(address, port, "source.timestamps")));
        neuralTrodesPub = std::move(std::unique_ptr<trodes::network::SourcePublisher<trodes::network::TrodesNeuralData>>(new trodes::network::SourcePublisher<trodes::network::TrodesNeuralData>(address, port, "source.neural")));
        analogTrodesPub = std::move(std::unique_ptr<trodes::network::SourcePublisher<trodes::network::TrodesAnalogData>>(new trodes::network::SourcePublisher<trodes::network::TrodesAnalogData>(address, port, "source.analog")));
        digitalTrodesPub = std::move(std::unique_ptr<trodes::network::SourcePublisher<trodes::network::TrodesDigitalData>>(new trodes::network::SourcePublisher<trodes::network::TrodesDigitalData>(address, port, "source.digital")));

        std::shared_ptr<RawDataPublisher> thisDataPub(this);

        data_service_thread = std::thread([ dataPub = thisDataPub ]() {
            std::string address = dataPub->host.toStdString();
            trodes::network::ServiceProvider<trodes::network::DataCommand, std::string> service(address, dataPub->port, "trodes.data.service");
            while (true) {
                service.handle([ dataPub ](trodes::network::DataCommand input) -> std::string {
                    if (input.command == "enable" || input.command == "disable") {
                        bool setting = input.command == "enable";

                        if (input.streamname == "neural") {
                            dataPub->setSendNeuralData(setting);
                        } else if (input.streamname == "digital") {
                            dataPub->setSendDigitalData(setting);
                        } else if (input.streamname == "analog") {
                            dataPub->setSendAnalogData(setting);
                        } else if (input.streamname == "timestamps") {
                            dataPub->setSendTimeData(setting);
                        } else {
                            return "error";
                        }
                        return "success";
                    }
                    return "error";
                });
            }
        });
        data_service_thread.detach();

        qDebug() << "[RawDataPublisher::startDedicatedDataPub] Set up publishers \"source.timestamps\", \"source.neural\", \"source.analog\", \"source.digital\""
                 << "to publish at address" << QString::fromStdString(address) << "port" << port;
        configureHFData();

        return true;
}

void RawDataPublisher::deleteDedicatedDataPub(){
    freePacketBuffer();

    timestampTrodesPub.reset();
    neuralTrodesPub.reset();
    analogTrodesPub.reset();
    digitalTrodesPub.reset();
}

void RawDataPublisher::allocatePacketBuffer() {
    PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + (2*headerSize);
    packetBuff = new unsigned char[PACKET_SIZE];
}

void RawDataPublisher::freePacketBuffer() {
    if (packetBuff != NULL)
        delete [] packetBuff;
    packetBuff = NULL;
}

void RawDataPublisher::configureHFData(){
    calculateIndices();

    int digitallen = 0;
    std::string digformat("t,");
    for(auto const& dig : digitalIndices){
        digitallen += dig.length;
        digformat += std::to_string(dig.start) + ",";
        digformat += std::to_string(dig.length) + ",";
    }
    digformat.pop_back();

    prevDigital = new unsigned char[digitallen];
    memset(prevDigital, 0, digitallen);

    int analoglen = 0;
    std::string anaformat("t,");
    for(auto const& ana: analogIndices){
        analoglen += ana.length;
        anaformat += std::to_string(ana.start) + ",";
        anaformat += std::to_string(ana.length) + ",";
    }
    anaformat.pop_back();
}

//Calculate start and length bytes of digital and analog parts of each
void RawDataPublisher::calculateIndices(){
    QVector<DeviceChannel> channels;
    for(int i = 0; i < hardwareConf->devices.length(); i++){
        for(int j = 0; j < hardwareConf->devices[i].channels.length(); j++){
            channels.append(hardwareConf->devices[i].channels[j]);
        }
    }

    if(channels.length()){
        int prevType = channels[0].dataType;
        if(prevType == DeviceChannel::DIGITALTYPE){
            digitalIndices.push_back({0, 9}); // 8 bits sync, then 1 bit for the first channel
        }
        else if(prevType == DeviceChannel::INT16TYPE){
            analogIndices.push_back({0, 1});
        }

        for(int i = 1; i < channels.size(); i++){
            if(channels[i].dataType == prevType){
                //continuation of last type, increment block length
                if(prevType == DeviceChannel::DIGITALTYPE){
                    ++digitalIndices.back().length;
                }
                else if(prevType == DeviceChannel::INT16TYPE){
                    ++analogIndices.back().length;
                }
            }
            else if(prevType != channels[i].dataType){
                //finish last block, then start new one
                if(prevType == DeviceChannel::DIGITALTYPE){
                    //we have been adding bits for digital, so divide by 8 bits in a byte
                    digitalIndices.back().length /= 8;
                }
                else if(prevType == DeviceChannel::INT16TYPE){
                    //we have been counting number of analog channels, each one 2 bytes, multiply by 2
                    analogIndices.back().length *= sizeof(int16_t);
                }

                //Manage new type. Ignore unknown types
                if(channels[i].dataType == DeviceChannel::DIGITALTYPE){
                    digitalIndices.push_back({channels[i].startByte, 1});
                }
                else if(channels[i].dataType == DeviceChannel::INT16TYPE){
                    analogIndices.push_back({channels[i].startByte, 1});
                }
                prevType = channels[i].dataType;
            }
            if(i == channels.size()-1){
                if(prevType == DeviceChannel::DIGITALTYPE){
                    //we have been adding bits for digital, so divide by 8 bits in a byte
                    digitalIndices.back().length /= 8;
                }
                else if(prevType == DeviceChannel::INT16TYPE){
                    //we have been counting number of analog channels, each one 2 bytes, multiply by 2
                    analogIndices.back().length *= sizeof(int16_t);
                }
            }
        }
    }
}

void RawDataPublisher::publishDataPacket(unsigned char *rData, size_t dataSize, int64_t systime){



    //Data: [header ... timestamp ... neural]
    uint32_t timestamp = *(uint32_t*)(rData+2*headerSize);
    size_t offset = 0;

    if (timeDataRequested) {
        trodes::network::TrodesTimestampData timestampData{timestamp, systime};
        timestampTrodesPub->publish(timestampData);
    }


    if (digitalDataRequested) {
        //timestamp, followed by digital data
        offset = 0;
        memcpy(packetBuff+offset, &timestamp, sizeof(uint32_t));
        offset += sizeof(uint32_t);
        for(size_t i = 0; i < digitalIndices.size(); i++){
            memcpy(packetBuff+offset, &(rData)[digitalIndices[i].start], digitalIndices[i].length);
            offset += digitalIndices[i].length;
        }

        // trodesnetwork-specific publishing
        std::vector<std::vector<uint8_t>> digitalDataBuffer;
        for(size_t i = 0; i < digitalIndices.size(); i++){
            std::vector<uint8_t> digitalDataBufferPart;
            for (auto j = 0; j < digitalIndices[i].length; j++) {
                digitalDataBufferPart.push_back(rData[digitalIndices[i].start + j]);
            }
            digitalDataBuffer.push_back(digitalDataBufferPart);
        }
        trodes::network::TrodesDigitalData digitalData{timestamp, digitalDataBuffer, systime};
        digitalTrodesPub->publish(digitalData);
    }


    if (analogDataRequested) {
        //timestamp, followed by analog data
        offset = 0;
        memcpy(packetBuff+offset, &timestamp, sizeof(uint32_t));
        offset += sizeof(uint32_t);
        for(size_t i = 0; i < analogIndices.size(); i++){
            memcpy(packetBuff+offset, &(rData)[analogIndices[i].start], analogIndices[i].length);
            offset += analogIndices[i].length;
        }

        // trodesnetwork-specific publishing
        std::vector<std::vector<uint8_t>> analogDataBuffer;
        for(size_t i = 0; i < analogIndices.size(); i++){
            std::vector<uint8_t> analogDataBufferPart;
            for (auto j = 0; j < analogIndices[i].length; j++) {
                analogDataBufferPart.push_back(rData[analogIndices[i].start + j]);
            }
            analogDataBuffer.push_back(analogDataBufferPart);
        }
        trodes::network::TrodesAnalogData analogData{timestamp,analogDataBuffer,systime};
        analogTrodesPub->publish(analogData);
    }


    if (neuralDataRequested) {
        //Publish neural data [timestamp ... , data]
        std::vector<int16_t> neuralDataBuffer;
        int16_t* neuralPointer = (int16_t*)((uint8_t*)rData + 2*headerSize + sizeof(uint32_t));
        for (auto c = 0; c < hardwareConf->NCHAN; c++) {
            int16_t amplitude = *neuralPointer;
            neuralDataBuffer.push_back(amplitude);
            // advance pointer by 2 bytes each time
            neuralPointer += 1;
        }

        trodes::network::TrodesNeuralData neuralData{timestamp, neuralDataBuffer, systime};
        neuralTrodesPub->publish(neuralData);
    }
}

void RawDataPublisher::publishDataPacket(int rawIdx){



    //int64_t systime = CZHelp::systemTimeMSecs();
    unsigned char* hdata = (unsigned char*)(rawData.digitalInfo+rawIdx*headerSize);
    //unsigned char* hdata = (unsigned char*)(rawData.digitalInfo+rawData.writeIdx*headerSize);
    uint32_t timestamp = rawData.timestamps[rawIdx];

    //qDebug() << "Time:" << timestamp;

    int64_t systime = rawData.sysTimestamps[rawIdx];
    //size_t offset = 0;

    if (timeDataRequested) {
        trodes::network::TrodesTimestampData timestampData{timestamp, systime};
        timestampTrodesPub->publish(timestampData);
    }

    //timestamp, followed by digital data


    if (digitalDataRequested) {


        std::vector<std::vector<uint8_t>> digitalDataBuffer;
        for(size_t i = 0; i < digitalIndices.size(); i++){
            std::vector<uint8_t> digitalDataBufferPart;
            for (auto j = 0; j < digitalIndices[i].length; j++) {
                digitalDataBufferPart.push_back(hdata[digitalIndices[i].start + j]);
            }
            digitalDataBuffer.push_back(digitalDataBufferPart);
        }
        trodes::network::TrodesDigitalData digitalData{timestamp, digitalDataBuffer, systime};
        digitalTrodesPub->publish(digitalData);
    }



    if (analogDataRequested) {


        // trodesnetwork-specific data publishing
        std::vector<std::vector<uint8_t>> analogDataBuffer;
        for(size_t i = 0; i < analogIndices.size(); i++){
            std::vector<uint8_t> analogDataBufferPart;
            for (auto j = 0; j < analogIndices[i].length; j++) {
                analogDataBufferPart.push_back(hdata[analogIndices[i].start + j]);
            }
            analogDataBuffer.push_back(analogDataBufferPart);
        }
        trodes::network::TrodesAnalogData analogData{timestamp,analogDataBuffer,systime};
        analogTrodesPub->publish(analogData);
    }


    if (neuralDataRequested) {

        // trodesnetwork-specific data publishing
        //unsigned char* hdata = (unsigned char*)(rawData.digitalInfo+rawIdx*headerSize);
        std::vector<int16_t> neuralDataBuffer;
        //int16_t* neuralPointer = (int16_t*)((uint8_t*)hdata + 2*headerSize + sizeof(uint32_t));
        int16_t* neuralPointer = rawData.data+(rawIdx*hardwareConf->NCHAN);

        for (auto c = 0; c < hardwareConf->NCHAN; c++) {
            int16_t amplitude = *neuralPointer;
            neuralDataBuffer.push_back(amplitude);
            // advance pointer by 2 bytes each time
            neuralPointer += 1;
        }

        trodes::network::TrodesNeuralData neuralData{timestamp, neuralDataBuffer, systime};
        neuralTrodesPub->publish(neuralData);
    }
}

bool AbstractTrodesSource::convertToStateMachineCommand(GlobalStimulationSettings &self, QByteArray *array) {
    //Convert the data in a global stimulation settings command to a byte array to be sent to the state machine

    //Here is the format of the array:
    /*
    u8 command (0x88)
    u16 data
    u8 checksum (sum of all bytes in the command, the command will be ignored if check-sum mismatch)

    data has the following contents (bits 9-15 are not used):
        settle_all_headstages <= data[0];
        settle_this_headstage <= data[1];
        amp_settle_mode <= data[2];
        charge_recov_mode <= data[3];
        stim_stepsize <= data[7:4]
        DC_amp_convert <= data[8];

   */

    array->resize(4); //total message size in bytes
    QDataStream dataStream(array,QIODevice::ReadWrite);
    dataStream.setByteOrder(QDataStream::LittleEndian);

    //Serialize the data
    dataStream << (quint8)command_setGlobalStimSettings;
    uint16_t data = 0;
    data |= self.settleAll();
    data |= (self.settleChip() << 1);
    data |= (self.useFastSettle() << 2);
    data |= (self.chargeRecoverySwitch() << 3);
    data |= (self.currentScaling() << 4);
    data |= (self.useDCAmp() << 8);
    dataStream << data;

    //Calculate a checksum
    uint8_t checksum = 0;
    for (int i = 0;i < array->length()-1; i++) {
        checksum += array->at(i);
    }
    dataStream << checksum;

    return true;
}

HardwareControllerSettings AbstractTrodesSource::convertHardwareControllerSettings(QByteArray &array)
{
    HardwareControllerSettings s(array.data());
//    parseMCUSettings(array.data(), &s);
    return s;
}

HeadstageSettings AbstractTrodesSource::convertHeadstageSettings(QByteArray &array)
{
    HeadstageSettings s(array.data());
//    parseHSSettings(array.data(), &s);
    return s;
}

bool AbstractTrodesSource::convertToStateMachineCommand(GlobalStimulationCommand &self, QByteArray *array) {
    //Convert the data in a global stimulation command to a byte array to be sent to the state machine

    //Here is the format of the array:
    /*
    u8 command (0x88)
    u16 data
    u8 checksum (sum of all bytes in the command, the command will be ignored if check-sum mismatch)

    data has the following contents (bits 4-15 are not used):
        DSP_settle <= data[0];
        reset_sequencers <= data[1];
        stim_shutdown <= data[2];
        stim_cmd_en <= data[3];

   */

    array->resize(4); //total message size in bytes
    QDataStream dataStream(array,QIODevice::ReadWrite);
    dataStream.setByteOrder(QDataStream::LittleEndian);

    //Serialize the data
    dataStream << (quint8)command_setGlobalStimAction;
    uint16_t data = 0;
    data |= self.dspOffsetCleared();
    data |= (self.sequencerReset() << 1);
    data |= (self.stimulationAborted() << 2);
    data |= (self.stimEnabled() << 3);
    dataStream << data;

    //Calculate a checksum
    uint8_t checksum = 0;
    for (int i = 0;i < array->length()-1; i++) {
        checksum += array->at(i);
    }
    dataStream << checksum;

    return true;
}

bool AbstractTrodesSource::convertToStateMachineCommand(StimulationCommand &self, QByteArray *array){
    //Convert the data in a stimulation pattern command to a byte array to be sent to the state machine

    //Here is the format of the array:
    /*
    u8 command (0x85)
    u8 stim sequencer unit to program (most likely we will end up with 8 or 16 stim sequencers instead of the full 256 stim sequencer that a direct mapping would imply)
    u8 stim trigger source (bits 4-0 has the stim trigger bit, bit 5 has edge or level sensitibity, bit 6 has polarity, bit 7 has trigger enable)
    u16 stim num pulses
    u8 stim pulse shape (bits 0-1 has the pulse shape (00 biphasic, 01 biphasic with dwell period, 11 triphasic), bit 3 has the pulse polarity (0 means cathode leading) )
    u16 event settle on (settle will start when the stim seqencer counter match this value)
    u16 event settle off (settle will stop when the stim seqencer counter match this value)
    u16 event stim start ph1 (stim phase 1 will start when the stim seqencer counter match this value)
    u16 event stim start ph2 (stim phase 2 will start when the stim seqencer counter match this value)
    u16 event stim start ph3 (stim phase 3 will start when the stim seqencer counter match this value)
    u16 event stim end (stim will end when the stim seqencer counter match this value)
    u16 event stim repeat (stim pulse will repeat when the stim seqencer counter match this value if number of remaining stim pulses > 0)
    u16 event charge recovery on (charge recovery will start when the stim seqencer counter match this value)
    u16 event charge recovery off (charge recovery will stop when the stim seqencer counter match this value)
    u16 event settle on repeat (settle will start when the stim seqencer counter match this value if number of remaining stim pulses > 0)
    u16 event settle off repeat (settle will stop when the stim seqencer counter match this value if number of remaining stim pulses > 0)
    u16 event end (sequence will end when the stim seqencer counter match this value)
    u16 cathode channel
    u16 anode channel
    u8 cathode pulse current
    u8 anode pulse current
    u8 check-sum (sum of all bytes in the command, the command will be ignored if check-sum mismatch)
    */

    if (!self.isValid()) {
        //We will only create the output if the settings are valid
        return false;
    }

    array->resize(37); //total message size in bytes
    QDataStream dataStream(array,QIODevice::ReadWrite);
    dataStream.setByteOrder(QDataStream::LittleEndian);

    uint16_t numPulses = self.getNumPulsesInTrain();
    if (numPulses < 1) {
        //We need at least one pulse
        return false;
    }
    uint16_t numRepeats = numPulses-1;

    //Serialize the data
    dataStream << (quint8)command_setStimulateParams;
    dataStream << self.getSlot(); //self.slot;
    dataStream << self.get_group();//self._group;
    dataStream << numRepeats;
    dataStream << self.getPulseMode(); //self._pulseMode;
    dataStream << self.getSettleOnMark(); //self._settleOnMark;
    dataStream << self.getSettleOffMark(); //self._settleOffMark;
    dataStream << self.getStartPhaseOneMark(); //self._startPhaseOneMark;
    dataStream << self.getStartPhaseTwoMark(); //self._startPhaseTwoMark;
    dataStream << self.getStartPhaseThreeMark(); //self._startPhaseThreeMark;
    dataStream << self.getStimEndMark(); //self._stimEndMark;
    dataStream << self.getStimRepeatMark(); //self._stimRepeatMark;
    dataStream << self.getChargeRecoveryOnMark(); //self._chargeRecoveryOnMark;
    dataStream << self.getChargeRecoveryOffMark(); //self._chargeRecoveryOffMark;
    dataStream << self.getRepeatedSettleOnMark(); //self._repeatedSettleOnMark;
    dataStream << self.getRepeatedSettleOffMark(); //self._repeatedSettleOffMark;
    dataStream << self.getEndMark(); //self._endMark;


    //Calculate the HW channels for the cathode and anode
    uint16_t cathodeHWchan = 0;
    uint16_t anodeHWchan = 0;
    bool foundCathNT = false;
    bool foundAnNT = false;

    qDebug() << "Converting stimulation command to state machine command";
    uint16_t stim_chips;
    if (spikeConf->deviceType == "neurolight") {
        qDebug() << "Mapping channels for Neurolight headstage";
        stim_chips = hardwareConf->NCHAN/16;
    } else{
        stim_chips = hardwareConf->NCHAN/32;
    }
    //qDebug() << "Cathode ntrode ID" << self.getCathodeNTrodeID();
    //qDebug() << "Anode ntrode ID" << self.getAnodeNTrodeID();




    for (int NT = 0; NT < spikeConf->ntrodes.length(); NT++) {
        if (spikeConf->ntrodes[NT]->nTrodeId == self.getCathodeNTrodeID()) {
            if ((self.getCathodeChannel() > 0) && (spikeConf->ntrodes[NT]->hw_chan.length() >= self.getCathodeChannel())) {
                foundCathNT = true;
                cathodeHWchan = spikeConf->ntrodes[NT]->hw_chan[self.getCathodeChannel()-1]; //channel is 1-based!!
                //Convert to channel number in stim engine format from the packet order format

                if (spikeConf->deviceType == "neurolight") {
                    //Neurolight headstages have one 32 channel recording chip and one 16 record/stim, and this changes the mapping
                    cathodeHWchan = (((cathodeHWchan%(stim_chips/2)) << 4) | cathodeHWchan/stim_chips);
                } else {

                    cathodeHWchan = (((cathodeHWchan%stim_chips) << 5) | cathodeHWchan/stim_chips);

                }

            }
        }
        if (spikeConf->ntrodes[NT]->nTrodeId == self.getAnodeNTrodeID()) {
            if ((self.getAnodeChannel() > 0) && spikeConf->ntrodes[NT]->hw_chan.length() >= self.getAnodeChannel()) {
                foundAnNT = true;
                anodeHWchan = spikeConf->ntrodes[NT]->hw_chan[self.getAnodeChannel()-1]; //channel is 1-based!!
                //Convert to channel number in stim engine format

                if (spikeConf->deviceType == "neurolight") {
                   //Neurolight headstages have one 32 channel recording chip and one 16 record/stim, and this changes the mapping
                    anodeHWchan = (((anodeHWchan%(stim_chips/2)) << 4) | anodeHWchan/stim_chips);
                } else {

                    anodeHWchan = (((anodeHWchan%stim_chips) << 5) | anodeHWchan/stim_chips);
                }

            }
        }
    }

    if (!foundCathNT || !foundAnNT) {
        return false;
    }


    dataStream << cathodeHWchan; //cathodeChannel;
    dataStream << anodeHWchan; //anodeChannel;
    if (self.getAnodePulseLeading()) {
        dataStream << self.getSecondPulseAmplitude(); //self.secondPulseAmplitude
        dataStream << self.getLeadingPulseAmplitude(); //self.leadingPulseAmplitude
    } else {
        dataStream << self.getLeadingPulseAmplitude();
        dataStream << self.getSecondPulseAmplitude();
    }


    //Calculate a checksum
    uint8_t checksum = 0;
    for (int i = 0;i < array->length()-1; i++) {
        checksum += array->at(i);
    }
    dataStream << checksum;

    return true;
}

bool AbstractTrodesSource::getDataStreamFromHardware(QByteArray* data, int numBytes) {
    //This function should be redefined for each inheriting class
    //It sends the start command 'sCommand' to the hardware, then puts all of the streaming bytes that
    //come back into *data until numBytes have been collected.
    //Returns true if all went well and false if something went wrong.

    return false;
}

bool AbstractTrodesSource::sendCommandToHardware(QByteArray &command) {
    //This function should be redefined for each inheriting class

    return false;
}

//----------------------------------------

AbstractSourceRuntime::AbstractSourceRuntime(){
    quitNow = false;
    acquiring = false;
    runLoopActive = false;
    lastTimeStamp = 0;

    numConsecJumps = 0;
    packetSizeErrorThrown = false;
    totalDroppedPacketEvents = 0;
    totalUnresponsiveHeadstagePackets = 0;
    badFrameAlignment = false;
    unresponsiveHeadstage = false;


    directDataPub = false; //Do we want this thread to be responsible for publishing data? Packets might get dropped if we do.

    //initialization went ok, so start the runtime thread
    runtimeHelper = new SourceRuntimeHelper();
    setUpHelperThread(runtimeHelper);

    if (directDataPub && networkConf->networkType==NetworkConfiguration::zmq_based) {

        datapub = new RawDataPublisher(networkConf->trodesHost, networkConf->trodesPort, PACKET_SIZE, hardwareConf->headerSize);
        datapub->setSendAnalogData(false);
        datapub->setSendDigitalData(false);
        datapub->setSendTimeData(false);
        datapub->setSendNeuralData(false);
    }
    resetStreamingStats();
}

AbstractSourceRuntime::~AbstractSourceRuntime() {
    if (directDataPub && networkConf->networkType==NetworkConfiguration::zmq_based) {
        deleteDedicatedDataPub();
        delete datapub;
        datapub = NULL;
    }

    //runtimeHelper->deleteLater();
}

void AbstractSourceRuntime::setUpHelperThread(SourceRuntimeHelper *rtPtr) {

    //    qDebug() << "##### Starting Helper Thread";
    if (networkConf->networkType==NetworkConfiguration::zmq_based) {
        rtPtr->startDedicatedDataPub();
    }
    //rtPtr->appendSysClock = appendSysClock; //Should the source thread append the computer's system clock to each packet?
    workerThread = new QThread();
    rtPtr->moveToThread(workerThread);
    rtPtr->connect(this, SIGNAL(startHelperThread()), SLOT(Run()));
    connect(this, SIGNAL(startHelperThread()), this, SLOT(setHelperThreadRunning()));
    connect(rtPtr, SIGNAL(finished()), workerThread, SLOT(quit()));
    connect(rtPtr,SIGNAL(finished()), rtPtr, SLOT(deleteLater()));
    connect(rtPtr,SIGNAL(finished()), this, SLOT(setHelperThreadNotRunning()));
    connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));
    connect(rtPtr,SIGNAL(deregisterHighFreqData(HighFreqDataType)), this, SIGNAL(deregisterHighFreqData(HighFreqDataType)));
    connect(rtPtr,SIGNAL(timeStampError(bool)),this,SIGNAL(timeStampError(bool)));
    connect(rtPtr,SIGNAL(signal_UnresponsiveHeadstage(bool)),this,SIGNAL(signal_UnresponsiveHeadstage(bool)));
    connect(rtPtr,SIGNAL(failure()),this,SLOT(RunTimeError()));
//    connect(rtPtr, SIGNAL(publishNeuroData(unsigned char*,size_t)), this, SLOT(publishNeuroData(unsigned char*,size_t)));

    workerThread->setObjectName("DataSourceHelper");
    workerThread->start(QThread::TimeCriticalPriority);
    helperThreadCreated = true;
}

void AbstractSourceRuntime::setHelperThreadRunning() {
    helperThreadRunning = true;

}

void AbstractSourceRuntime::setHelperThreadNotRunning() {
    helperThreadRunning = false;
    helperThreadCreated = false;

}

bool AbstractSourceRuntime::isHelperThreadRunning() {
    return helperThreadRunning;
}

bool AbstractSourceRuntime::startDedicatedDataPub() {

    if (directDataPub && networkConf->networkType==NetworkConfiguration::zmq_based) {
        return datapub->startDedicatedDataPub();
    } else {
        return false;
    }

}

void AbstractSourceRuntime::deleteDedicatedDataPub() {
    if (directDataPub && networkConf->networkType==NetworkConfiguration::zmq_based) {
        datapub->deleteDedicatedDataPub();
    }
}

void AbstractSourceRuntime::calculatePacketSize() {
    /*stimPacketSize = ceil(hardwareConf->numStimChan/8.0);
    if (stimPacketSize > 0) {
        stimPacketSize = stimPacketSize+2; //The first two bytes in the stim section is used to report commands recieved
    }
    //qDebug() << "Stim packet size:" << stimPacketSize;
    PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + (2*hardwareConf->headerSize) + stimPacketSize; //Packet size in bytes*/
    appendSysClock = false;

    PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + (2*hardwareConf->headerSize); //Packet size in bytes
}


void AbstractSourceRuntime::setHeaderSize(int hsize)
{
    headerSize = hsize;
    if (directDataPub && networkConf->networkType==NetworkConfiguration::zmq_based) {
        datapub->setHeaderSize(hsize);
    }
}

void AbstractSourceRuntime::resetStreamingStats() {
    numPacketsProcessed = 0;
    numPacketsDropped = 0;
    largestPacketDrop = 0;
    totalUnresponsiveHeadstagePackets = 0;
}

void AbstractSourceRuntime::printStreamingStats() {

    if (runtimeHelper->isUsingTransferBuffer()) {
        //Get the stats from the helper thread.
        numPacketsProcessed = runtimeHelper->numPacketsProcessed;
        totalDroppedPacketEvents = runtimeHelper->totalDroppedPacketEvents;
        totalUnresponsiveHeadstagePackets = runtimeHelper->totalUnresponsiveHeadstagePackets;
        numPacketsDropped = runtimeHelper->numPacketsDropped;
        largestPacketDrop = runtimeHelper->largestPacketDrop;
    }

    QString printOutput;

    if (numPacketsProcessed > 0) {
        printOutput.append("\n\n---------Streaming stats---------\n");
        printOutput.append(QString("%1 total packets dropped.\n").arg(numPacketsDropped));
        printOutput.append(QString("%1 percent packet drops.\n").arg(((double)numPacketsDropped/(double)numPacketsProcessed)*100.0));
        printOutput.append(QString("Largest drop event: %1 packets.\n").arg(largestPacketDrop));

        printOutput.append(QString("%1 total unresponsive headstage packets.\n").arg(totalUnresponsiveHeadstagePackets));
        printOutput.append(QString("%1 percent unresponsive headstage packets.\n").arg(((double)totalUnresponsiveHeadstagePackets/(double)numPacketsProcessed)*100.0));

        QString ss = QString(":%1").arg((numPacketsProcessed/hardwareConf->sourceSamplingRate)%60,2,10,QLatin1Char('0'));
        QString mm = QString(":%1").arg((numPacketsProcessed/(hardwareConf->sourceSamplingRate*60))%60,2,10,QLatin1Char('0'));
        QString hh = QString("%1").arg(numPacketsProcessed/(hardwareConf->sourceSamplingRate*60*60),2,10,QLatin1Char('0'));
        printOutput.append(QString("Streaming time: ")+hh+mm+ss + "\n");

        printOutput.append(QString("---------------------------------\n\n"));
        qDebug().noquote() << printOutput;
    }

   /* qDebug() << "----Streaming stats----";
    qDebug() << ((double)numPacketsDropped/(double)numPacketsProcessed)*100.0 << "percent packet drops.";
    qDebug() << "Largest drop event:" << largestPacketDrop << "packets.";

    QString ss = QString(":%1").arg((numPacketsProcessed/hardwareConf->sourceSamplingRate)%60,2,10,QLatin1Char('0'));
    QString mm = QString(":%1").arg((numPacketsProcessed/(hardwareConf->sourceSamplingRate*60))%60,2,10,QLatin1Char('0'));
    QString hh = QString("%1").arg(numPacketsProcessed/(hardwareConf->sourceSamplingRate*60*60),2,10,QLatin1Char('0'));
    qDebug() << "Streaming time:" << hh+mm+ss;
    qDebug() << "-----------------------";*/
}

void AbstractSourceRuntime::checkHardwareStatus(const uchar *statusPtr) {

    if (*statusPtr & 0x40) { //bit 6 set high
        if (currentTimeStamp > 0) {
            totalUnresponsiveHeadstagePackets++;
            if (!unresponsiveHeadstage) {
                qDebug() << "Warning-- packets(s) with unresponsive headstage at timestamp" << currentTimeStamp;
                emit signal_UnresponsiveHeadstage(true);
            }
            unresponsiveHeadstage = true;
        }
    } else {
        if (unresponsiveHeadstage) {
            emit signal_UnresponsiveHeadstage(false);
        }
        unresponsiveHeadstage = false;
    }
}

bool AbstractSourceRuntime::checkForCorrectTimeSequence() {

    bool returnVal = true;
    //If the time does not increase by 1 throw an error
    if ((currentTimeStamp-lastTimeStamp) != 1) {
        if (totalDroppedPacketEvents > 0) {
            //The first packet is often a jump (playback) so we only write a statement after the 2nd (or higher) timestamp jumps
            //qDebug() << "Jump in timestamps: " << ((double)currentTimeStamp-(double)lastTimeStamp) << currentTimeStamp;
        }
        numConsecJumps++;
        //totalDroppedPacketEvents = totalDroppedPacketEvents + 1;

        if ((currentTimeStamp > lastTimeStamp) && (lastTimeStamp > 1000) && (currentTimeStamp > 1000)) {

            uint32_t diff = (currentTimeStamp-lastTimeStamp);
            numPacketsProcessed = numPacketsProcessed + diff;
            numPacketsDropped = numPacketsDropped + diff - 1;
            totalDroppedPacketEvents = totalDroppedPacketEvents + diff - 1;

            if ((diff-1) > largestPacketDrop) {
                largestPacketDrop = (diff-1);
            }

            qDebug() << "Warning-- packets dropped:" << (currentTimeStamp-lastTimeStamp) - 1 << "at timestamp" << currentTimeStamp;


            if (currentTimeStamp-lastTimeStamp > 30000) {
                //Huge jump in timestamps might mean that the packet is corrupted. Probably best to discard.
                returnVal = false;
            }


        } else if (lastTimeStamp > currentTimeStamp) {
            //Backwards time.  Probably a corrupted packet. Best to discard.
            returnVal = false;
            if (!packetSizeErrorThrown && numConsecJumps > 5) {

                qDebug() << "Signal error-- packet size does not match workspace. Check workspace file.";
                packetSizeErrorThrown = true;
                emit timeStampError(true);
            }
        }


    } else {
        numConsecJumps = 0;
        numPacketsProcessed++;
        if (packetSizeErrorThrown) {
            packetSizeErrorThrown = false;

            emit timeStampError(false); //reset the error indicator
        }
    }
    lastTimeStamp = currentTimeStamp;

    return returnVal;
}

void AbstractSourceRuntime::calculateHeaderSize() {
//    if (appendSysClock) {
//        headerSize = hardwareConf->headerSize-4;  //Remove the 8 bytes (4 16-bit values) defined in the config for sys clock
//    } else {
        headerSize = hardwareConf->headerSize;
//    }
}


bool AbstractSourceRuntime::checkFrameAlignment(unsigned char *packet) {
    if (*packet != 0x55) {
        //We don't have alignment!  So now we drop everything
        //and try to find it. We need two 0x55's separated by PACKET_SIZE.
        if ((!packetSizeErrorThrown) && (lastTimeStamp > 1000) && (currentTimeStamp > 1000)) {
            qDebug() << "Bad frame alignment in source packet";
        }
        badFrameAlignment = true;
        tempSyncByteLocation = 0;
        findNextSyncByte(packet);
        return false;
    }

    badFrameAlignment = false;
    return true;
}


void AbstractSourceRuntime::findNextSyncByte(unsigned char *packet) {
    //Look for the next sync byte in the current packet
    for (unsigned int i = 0; i < PACKET_SIZE; i++) {
        if (*(packet+i) == 0x55) {
            tempSyncByteLocation = i;
            qDebug() << "Found possible sync byte at byte" << i;
            break;
        }
    }
}

//use this function to directly copy a packet's data buffer
//is faster than publishDataPacket(void)
//NOTE: always call this directly after the packet's alignment has been verified
void AbstractSourceRuntime::publishDataPacket(unsigned char *rData, size_t dataSize, int64_t systime) {
    if (directDataPub && networkConf->networkType==NetworkConfiguration::zmq_based) {
        datapub->publishDataPacket(rData, dataSize, systime);
    }
}

//use this if the data packet was not received but generated
//NOTE: always call this function directly before the rawData.writeIdx variable is iterated
void AbstractSourceRuntime::publishDataPacket() {
    if (directDataPub && networkConf->networkType==NetworkConfiguration::zmq_based) {
        //datapub->publishDataPacket();
    }
}


void AbstractSourceRuntime::calculateReferences(){
    for(int i = 0; i < spikeConf->carGroups.length(); ++i){
        if(spikeConf->carGroups[i].useCount == 0){
            //if this car group is not being used, skip it
            continue;
        }
        if(spikeConf->carGroups[i].chans.length() == 0){
            //if this car group is empty, skip it
            continue;
        }
        int32_t accum = 0;
        for(auto const &chan : spikeConf->carGroups[i].chans){
            accum += rawData.data[(rawData.writeIdx*hardwareConf->NCHAN) + chan.hw_chan];
        }
        const int carvalind = rawData.writeIdx*spikeConf->carGroups.length() + i;
        //divide by 0 exception not possible since if chans is empty, skip calculations
        rawData.carvals[carvalind] = accum/spikeConf->carGroups[i].chans.length();
    }
}
//---------------------------------------
SourceRuntimeHelper::SourceRuntimeHelper(){
    quitNow = false;
    acquiring = false;
    lastTimeStamp = 0;

    numConsecJumps = 0;
    packetSizeErrorThrown = false;

    badFrameAlignment = false;
    unresponsiveHeadstage = false;
    useTransferBuffer = false;
    transferBuffer = nullptr;
    runLoopActive = false;

    totalDroppedPacketEvents = 0;
    totalUnresponsiveHeadstagePackets = 0;
    numPacketsDropped = 0;
    numPacketsProcessed = 0;
    largestPacketDrop = 0;


    sourceDataAvailable = new QSemaphore;
    if (networkConf->networkType==NetworkConfiguration::zmq_based) {

        datapub = new RawDataPublisher(networkConf->trodesHost, networkConf->trodesPort, PACKET_SIZE, hardwareConf->headerSize);
        datapub->setSendAnalogData(true);
        datapub->setSendDigitalData(true);
        datapub->setSendTimeData(true);
        datapub->setSendNeuralData(false); //This should only be turned on if set to it (TODO: workspace setting)
    }

}

void SourceRuntimeHelper::Run() {


  rawIdx = 0;
  calculateHeaderSize();
  PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + (2*headerSize); //Packet size in bytes
  QVector<uchar> buffer;
  buffer.resize(PACKET_SIZE*2);
  QHostAddress sender;
  quint16 senderPort;



  quitNow = false;
  acquiring = true;
  runLoopActive = true;


  packetSizeErrorThrown = false;
  lastTimeStamp = 0;
  int numConsecErrors = 0;
  int remainingSamples = 0;
  double dTimestamp = 0.0;

  qDebug() << "Source helper thread running....";



  while (quitNow != true) {

      //QCoreApplication::processEvents();
      if (useTransferBuffer) {


          //Process the received data packet.
          //The packet contains a 16-bit header for binary info,
          //a 32-bit time stamp, and hardwareConf->NCHAN 16-bit samples.


          while ( (transferBuffer->bytesAvailable() > PACKET_SIZE) && (quitNow != true)) {


              uchar* RxBuffer = buffer.data();
              transferBuffer->read(RxBuffer,PACKET_SIZE);
              int leftInBuffer = PACKET_SIZE;

              //The first byte is always 0x55.  Check to make sure we have
              //the correct frame alignment

              if (!checkFrameAlignment(RxBuffer)) {
                  //We don't have alignment!  So now we drop everything
                  //and try to find it.
                  numConsecErrors++;
                  if (badFrameAlignment && tempSyncByteLocation > 0) {
                      //We have lost packet alignment, but we have a candidate sync byte location. We dump
                      //everything up to that point in the current packet.
                      if (transferBuffer->bytesAvailable() > tempSyncByteLocation+PACKET_SIZE)  {
                          qDebug() << "Fast-forwarding to candidate sync location.";

                          uchar* RxBuffer = buffer.data();
                          transferBuffer->read(RxBuffer,tempSyncByteLocation+PACKET_SIZE);
                      }

                  }

                  if (!packetSizeErrorThrown && numConsecErrors > 5) {
                      //if we have had many bad alignments in a row,
                      //consider it a fatal error and stop

                      //quitNow = true;
                      //emit failure();
                      qDebug() << "Wrong incoming packet size";

                      packetSizeErrorThrown = true;
                      emit timeStampError(true);
                      emit failure();

                  }
                  //We dump the rest of the processing for this packet
                  continue;
              }



              numConsecErrors = 0; //Packet looks good

              checkHardwareStatus((const uchar*)RxBuffer+1);

              //publishDataPacket(RxBuffer, PACKET_SIZE, CZHelp::systemTimeMSecs());

              //Read header info
              memcpy(&(rawData.digitalInfo[rawData.writeIdx*headerSize]), \
                      RxBuffer, headerSize*sizeof(uint16_t));

              RxBuffer += (2*headerSize);
              leftInBuffer -= (2*headerSize);



              //Process time stamp
              uint32_t* dataPtr = (uint32_t *)(RxBuffer);
              currentTimeStamp = *dataPtr;
              //qDebug() << "Time" << currentTimeStamp;


              if (checkForCorrectTimeSequence() && !packetSizeErrorThrown) {
                  rawData.timestamps[rawData.writeIdx] = *dataPtr;

                  rawData.sysTimestamps[rawData.writeIdx] = QTime::currentTime().msecsSinceStartOfDay();

                  RxBuffer += 4;
                  leftInBuffer -= 4;
                  dTimestamp += 1.0;
                  rawData.dTime[rawData.writeIdx] = dTimestamp;
                  // system time in nanoseconds
                  //                  rawData.sysClock[rawData.writeIdx] = QDateTime::currentMSecsSinceEpoch() * 1000000;
                  rawData.sysClock[rawData.writeIdx] = AbstractSourceRuntime::getSystemTimeNanoseconds();

                  //Process the channels in the packet
                  remainingSamples = hardwareConf->NCHAN;
                  memcpy(&(rawData.data[rawData.writeIdx*hardwareConf->NCHAN]), \
                          RxBuffer, remainingSamples*sizeof(uint16_t));
                  RxBuffer += remainingSamples * 2;
                  leftInBuffer -= remainingSamples * 2;
                  remainingSamples = 0;

                  //This should ideally go in a seperate thread
                  calculateReferences();

                  //Advance the write markers and release a semaphore
                  writeMarkerMutex.lock();
                  rawData.writeIdx = (rawData.writeIdx + 1) % EEG_BUFFER_SIZE;
                  rawDataWritten++;
                  writeMarkerMutex.unlock();



                  for (int a = 0; a < rawDataAvailable.length(); a++) {
                      rawDataAvailable[a]->release(1);
                  }
                  sourceDataAvailable->release(1);

              }

          }
      }

      if ((quitNow != true) && (sourceDataAvailable->tryAcquire(1, 100))) {

          int samplesToCopy = sourceDataAvailable->available();

          if (!sourceDataAvailable->tryAcquire(samplesToCopy))
              qDebug() << "[SourceHelper] Error acquiring available samples";

          samplesToCopy += 1; // we acquired one at the beginning

          for (int s = 0; s < samplesToCopy && !quitNow; s++) {


              publishDataPacket();
              rawIdx = (rawIdx + 1) % EEG_BUFFER_SIZE;

          }

      }

  }

  emit timeStampError(false);
  emit signal_UnresponsiveHeadstage(false);
  qDebug() << "Source helper thread finished.";
  runLoopActive = false;

  emit finished();


}


SourceRuntimeHelper::~SourceRuntimeHelper() {
    while (sourceDataAvailable->available() > 0) {
        sourceDataAvailable->tryAcquire();
    }
    if (networkConf->networkType==NetworkConfiguration::zmq_based) {
        deleteDedicatedDataPub();
        delete datapub;
        datapub = NULL;
    }
    qDebug() << "Source runtime helper deleted.";
}

bool SourceRuntimeHelper::startDedicatedDataPub() {

    if (networkConf->networkType==NetworkConfiguration::zmq_based) {
        return datapub->startDedicatedDataPub();
    } else {
        return false;
    }

}

void SourceRuntimeHelper::setTransferBuffer(TransferBlockRingBuffer *bf) {
    useTransferBuffer = true;
    transferBuffer = bf;
}

bool SourceRuntimeHelper::isUsingTransferBuffer() {
    return useTransferBuffer;
}

void SourceRuntimeHelper::deleteDedicatedDataPub() {
    if (networkConf->networkType==NetworkConfiguration::zmq_based) {
        datapub->deleteDedicatedDataPub();
    }
}

void SourceRuntimeHelper::calculatePacketSize() {
    appendSysClock = false;

    PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + (2*hardwareConf->headerSize); //Packet size in bytes
}

void SourceRuntimeHelper::setHeaderSize(int hsize)
{
    headerSize = hsize;
    if (networkConf->networkType==NetworkConfiguration::zmq_based) {
        datapub->setHeaderSize(hsize);
    }
}


void SourceRuntimeHelper::calculateHeaderSize() {
//    if (appendSysClock) {
//        headerSize = hardwareConf->headerSize-4;  //Remove the 8 bytes (4 16-bit values) defined in the config for sys clock
//    } else {
        headerSize = hardwareConf->headerSize;
//    }
}


//use this function to directly copy a packet's data buffer
//is faster than publishDataPacket(void)
//NOTE: always call this directly after the packet's alignment has been verified
void SourceRuntimeHelper::publishDataPacket(unsigned char *rData, size_t dataSize, int64_t systime) {
    if (networkConf->networkType==NetworkConfiguration::zmq_based) {
        datapub->publishDataPacket(rData, dataSize, systime);
    }
}

//use this if the data packet was not received but generated
//NOTE: always call this function directly before the rawData.writeIdx variable is iterated
void SourceRuntimeHelper::publishDataPacket() {
    if (networkConf->networkType==NetworkConfiguration::zmq_based) {
        datapub->publishDataPacket(rawIdx);
    }
}


void SourceRuntimeHelper::calculateReferences(){
    for(int i = 0; i < spikeConf->carGroups.length(); ++i){
        if(spikeConf->carGroups[i].useCount == 0){
            //if this car group is not being used, skip it
            continue;
        }
        if(spikeConf->carGroups[i].chans.length() == 0){
            //if this car group is empty, skip it
            continue;
        }
        int32_t accum = 0;
        for(auto const &chan : spikeConf->carGroups[i].chans){
            accum += rawData.data[(rawData.writeIdx*hardwareConf->NCHAN) + chan.hw_chan];
        }
        const int carvalind = rawData.writeIdx*spikeConf->carGroups.length() + i;
        //divide by 0 exception not possible since if chans is empty, skip calculations
        rawData.carvals[carvalind] = accum/spikeConf->carGroups[i].chans.length();
    }
}

void SourceRuntimeHelper::checkHardwareStatus(const uchar *statusPtr) {

    if (*statusPtr & 0x40) { //bit 6 set high
        if (currentTimeStamp > 0) {
            totalUnresponsiveHeadstagePackets++;
            if (!unresponsiveHeadstage) {
                qDebug() << "Warning-- packets(s) with unresponsive headstage at timestamp" << currentTimeStamp;
                emit signal_UnresponsiveHeadstage(true);
            }
            unresponsiveHeadstage = true;
        }
    } else {
        if (unresponsiveHeadstage) {
            emit signal_UnresponsiveHeadstage(false);
        }
        unresponsiveHeadstage = false;
    }
}

bool SourceRuntimeHelper::checkForCorrectTimeSequence() {

    bool returnVal = true;
    //If the time does not increase by 1 throw an error
    if ((currentTimeStamp-lastTimeStamp) != 1) {
        if (totalDroppedPacketEvents > 0) {
            //The first packet is often a jump (playback) so we only write a statement after the 2nd (or higher) timestamp jumps
            //qDebug() << "Jump in timestamps: " << ((double)currentTimeStamp-(double)lastTimeStamp) << currentTimeStamp;
        }
        numConsecJumps++;
        //totalDroppedPacketEvents = totalDroppedPacketEvents + 1;

        if ((currentTimeStamp > lastTimeStamp) && (lastTimeStamp > 1000) && (currentTimeStamp > 1000)) {

            uint32_t diff = (currentTimeStamp-lastTimeStamp);
            numPacketsProcessed = numPacketsProcessed + diff;
            numPacketsDropped = numPacketsDropped + diff - 1;
            totalDroppedPacketEvents = totalDroppedPacketEvents + diff - 1;

            if ((diff-1) > largestPacketDrop) {
                largestPacketDrop = (diff-1);
            }

            qDebug() << "Warning-- packets dropped:" << (currentTimeStamp-lastTimeStamp) - 1 << "at timestamp" << currentTimeStamp;


            if (currentTimeStamp-lastTimeStamp > 30000) {
                //Huge jump in timestamps might mean that the packet is corrupted. Probably best to discard.
                returnVal = false;
            }


        } else if (lastTimeStamp > currentTimeStamp) {
            //Backwards time.  Probably a corrupted packet. Best to discard.
            returnVal = false;
            if (!packetSizeErrorThrown && numConsecJumps > 5) {

                qDebug() << "Signal error-- packet size does not match workspace. Check workspace file.";
                packetSizeErrorThrown = true;
                emit timeStampError(true);
            }
        }


    } else {
        numConsecJumps = 0;
        numPacketsProcessed++;
        if (packetSizeErrorThrown) {
            packetSizeErrorThrown = false;

            emit timeStampError(false); //reset the error indicator
        }
    }
    lastTimeStamp = currentTimeStamp;

    return returnVal;
}


bool SourceRuntimeHelper::checkFrameAlignment(unsigned char *packet) {
    if (*packet != 0x55) {
        //We don't have alignment!  So now we drop everything
        //and try to find it. We need two 0x55's separated by PACKET_SIZE.
        if ((!packetSizeErrorThrown) && (lastTimeStamp > 1000) && (currentTimeStamp > 1000)) {
            qDebug() << "Bad frame alignment in source packet";
        }
        badFrameAlignment = true;
        tempSyncByteLocation = 0;
        findNextSyncByte(packet);
        return false;
    }

    badFrameAlignment = false;
    return true;
}


void SourceRuntimeHelper::findNextSyncByte(unsigned char *packet) {
    //Look for the next sync byte in the current packet
    for (unsigned int i = 0; i < PACKET_SIZE; i++) {
        if (*(packet+i) == 0x55) {
            tempSyncByteLocation = i;
            qDebug() << "Found possible sync byte at byte" << i;

        }
    }
}


//---------------------------------------
//AbstractTrodesSource

AbstractTrodesSource::AbstractTrodesSource () {
//    neuroDataPub = NULL;
    startCommandValue = command_startNoECU; // start data capture without ECU

    //startCommandValue = 0x64; // start data capture with ECU
    connectErrorThrown = false;
    reinitMode = false;

    appendSysClock = false;
    restartThreadAfterShutdown = false;
    threadCreated = false;
    threadRunning = false;




}

AbstractTrodesSource::~AbstractTrodesSource() {
//    deleteNeuroDataPub();
}

void AbstractTrodesSource::setECUConnected(bool ECUconnected) {
    if (ECUconnected) {
        startCommandValue = command_startWithECU;
    } else {
        startCommandValue = command_startNoECU;
    }
}

void AbstractTrodesSource::setAppendSysClock(bool clOn) {

    appendSysClock = clOn;

}


void AbstractTrodesSource::setUpThread(AbstractSourceRuntime *rtPtr) {

    //    qDebug() << "##### Starting Source Thread!";
    if (networkConf->networkType==NetworkConfiguration::zmq_based) {
        rtPtr->startDedicatedDataPub();
    }
    rtPtr->appendSysClock = appendSysClock; //Should the source thread append the computer's system clock to each packet?
    workerThread = new QThread();
    rtPtr->moveToThread(workerThread);
    rtPtr->connect(this, SIGNAL(startRuntime()), SLOT(Run()));
    connect(this, SIGNAL(startRuntime()), this, SLOT(setThreadRunning()));
    connect(rtPtr, SIGNAL(finished()), workerThread, SLOT(quit()));
    connect(rtPtr,SIGNAL(finished()), rtPtr, SLOT(deleteLater()));
    connect(rtPtr,SIGNAL(finished()), this, SLOT(setThreadNotRunning()));
    connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));
    connect(rtPtr,SIGNAL(timeStampError(bool)),this,SIGNAL(timeStampError(bool)));
    connect(rtPtr,SIGNAL(signal_UnresponsiveHeadstage(bool)),this,SIGNAL(signal_UnresponsiveHeadstage(bool)));
    connect(rtPtr,SIGNAL(failure()),this,SLOT(RunTimeError()));
    connect(rtPtr,SIGNAL(deregisterHighFreqData(HighFreqDataType)), this, SIGNAL(deregisterHighFreqData(HighFreqDataType)));
//    connect(rtPtr, SIGNAL(publishNeuroData(unsigned char*,size_t)), this, SLOT(publishNeuroData(unsigned char*,size_t)));

    workerThread->setObjectName("DataSource");
    workerThread->start(QThread::TimeCriticalPriority);
    threadCreated = true;
}

void AbstractTrodesSource::setThreadRunning() {
    threadRunning = true;

}

void AbstractTrodesSource::setThreadNotRunning() {
    threadRunning = false;
    threadCreated = false;
    if (restartThreadAfterShutdown) {
        //restartThread();
    }

}

bool AbstractTrodesSource::isThreadRunning() {
    return threadRunning;
}

quint64 AbstractTrodesSource::getTotalDroppedPacketEvents() {
    return 0;
}

void AbstractTrodesSource::MeasureImpedance(HardwareImpedanceMeasureCommand s) {
    //Used to request impedance measurements from hardware.

    //Currently only Intan supported
    MeasureImpedance_Intan(s);
}

void AbstractTrodesSource::MeasureImpedance_Intan(HardwareImpedanceMeasureCommand s) {



    if (!lastHeadstageSettings.valid) {
        printf("Error: no valid headstage detected to measure impendace from. \n");
        return;
    }

    double impTestFreq = s.frequency;
    double sampleRate =  (double)lastHeadstageSettings.samplingRate;
    int period = round(sampleRate / impTestFreq);


    int bytesInPacket = lastControllerSettings.packetSize;
    int packetsPerCycle = ceil(lastHeadstageSettings.samplingRate/s.frequency);
    int numBytesToRequest = s.numberOfCycles * packetsPerCycle * bytesInPacket;
    QList<QByteArray> dataStream;

    //printf("Sending Intan impedance command. \n");
    QByteArray impedanceModeCommand;
    impedanceModeCommand.resize(5);

    bool abort = false;

    //Intan chips have three different capacitors that determine the implitude of the test signal.
    //We will try all three, and afterward determine which is best to use...
    for (char amplitudeMode = 0; amplitudeMode < 3; amplitudeMode++) {

        dataStream.push_back(QByteArray());

        impedanceModeCommand[0] = command_startImpedanceTest;
        char amp;
        if (amplitudeMode == 0) {
            amp = 0;
        } else if (amplitudeMode == 1) {
            amp = 1;
        } else {
            amp = 3;
        }

        //Send a command to hardware to put it into impedance testing mode
        impedanceModeCommand[1] = 1; //0 for regular mode, 1 for impedance mode
        impedanceModeCommand[2] = amp; //Valid values are 0, 1 or 3
        impedanceModeCommand[3] = s.channel & 0xff;
        impedanceModeCommand[4] = s.channel>>8;
        bool success = sendCommandToHardware(impedanceModeCommand);
        if (!success) {
            qDebug() << "Failure in setting impedance test mode.";
            abort = true;
             break;
        }

        //Give some time for headstage programming to go through
        QThread::msleep(100);

        //Stream data from the headstage and save
        success = getDataStreamFromHardware(&dataStream.last(), numBytesToRequest);
        if (!success) {
            qDebug() << "Failure in getting impedance values.";
            abort = true;
            break;
        }
    }

    if (!abort) {

        QList<double> impedanceMagnitude;
        QList<double> impedancePhase;

        //We got all the data
        //qDebug() << "Sizes:" << lastHeadstageSettings.auxbytes << lastHeadstageSettings.packetSize << lastHeadstageSettings.numberOfChannels << lastControllerSettings.packetSize;
        int auxSize = lastControllerSettings.packetSize - (2*lastHeadstageSettings.numberOfChannels);
        for (int amp = 0; amp < 3; amp ++) {

            QVector<double> dbldata;

            //Scale the data to uV
            for (int dindex = lastHeadstageSettings.auxbytes+auxSize; dindex<dataStream.at(amp).length();dindex = dindex+lastControllerSettings.packetSize) {
                char* ptr = dataStream[amp].data()+dindex;
                double dblval = (double)(*((int16_t*)(ptr))) * 0.195;
                dbldata.push_back(dblval);
            }


            //Run the data through a notch filter with 10 Hz stopband. This will either be 60 Hz (US) or 50 Hz (Europe, etc)
            double fNotch = s.notchHz;
            double bandwidth = 10.0;

            double d = exp(-1.0 * M_PI * bandwidth / sampleRate);
            double b = (1.0 + d * d) * cos(2.0 * M_PI * fNotch / sampleRate);
            double b0 = (1.0 + d * d) / 2.0;
            double b1 = -b;
            double b2 = b0;
            double a1 = b1;
            double a2 = d * d;
            int length = dbldata.size();

            double prevPrevIn = dbldata[0];
            double prevIn = dbldata[1];
            for (int t = 2; t < length; ++t) {
                double in = dbldata[t];
                dbldata[t] = b0 * in + b1 * prevIn + b2 * prevPrevIn - a1 * dbldata[t - 1] - a2 * dbldata[t - 2];  // Direct Form 1 implementation
                prevPrevIn = prevIn;
                prevIn = in;
            }


            //We only look at the end of the data stream beacause it takes a while to settle
            int startIndex = 0;
            int endIndex = startIndex + (s.numberOfCycles-50) * period - 1;

            // Move the measurement window to the end of the waveform to ignore start-up transient.
            while (endIndex < dbldata.size() - period) {
                startIndex += period;
                endIndex += period;
            }


            //Now we calculate the impedance (magitude and phase)
            const double K = 2.0 * M_PI * (impTestFreq / sampleRate);  // precalculate for speed

            // Perform correlation with sine and cosine waveforms.
            double meanI = 0.0;
            double meanQ = 0.0;
            for (int t = startIndex; t <= endIndex; ++t) {
                meanI += dbldata[t] * cos(K * t);
                meanQ += dbldata[t] * -1.0 * sin(K * t);
            }
            double dlength = (double)(endIndex - startIndex + 1);
            meanI /= dlength;
            meanQ /= dlength;

            double realComponent = 2.0 * meanI;
            double imagComponent = 2.0 * meanQ;


            impedanceMagnitude.push_back(sqrt(realComponent * realComponent + imagComponent * imagComponent));
            impedancePhase.push_back((180.0/3.141592653589793238463) * atan2(imagComponent, realComponent));
        }


        //We now decide which of the three tests to use.
        double highCutoff = 7500.0;
        double saturationVoltage;
        double relativeFreq = impTestFreq / (double)lastHeadstageSettings.samplingRate;

        if (impTestFreq < 0.2 * highCutoff) {
            saturationVoltage = 5000.0;
        } else {
            saturationVoltage =  5000.0 * sqrt(1.0 / (1.0 + pow(3.3333 * impTestFreq / highCutoff, 4.0)));
        }

        int bestAmplitudeIndex;
        if (impedanceMagnitude[2] < saturationVoltage) {
            bestAmplitudeIndex = 2;
        } else if (impedanceMagnitude[1] < saturationVoltage) {

            bestAmplitudeIndex = 1;
        } else {
            bestAmplitudeIndex = 0;
        }

        // If C2 and C3 are too close, C3 is probably saturated. Ignore C3.
        double capRatio = impedanceMagnitude[1] / impedanceMagnitude[2];
        if (capRatio > 0.2) {
            if (bestAmplitudeIndex == 2) {

                bestAmplitudeIndex = 1;
            }
        }


        //Which capacitor value should be used?
        double cSeries = 0.0;
        switch (bestAmplitudeIndex) {
        case 0:
            cSeries = 0.1e-12;
            break;
        case 1:
            cSeries = 1.0e-12;
            break;
        case 2:
            cSeries = 10.0e-12;
            break;
        }


        const double DacVoltageAmplitude = 128.0 * (1.225 / 256.0); // this assumes the DAC amplitude was set to 128
        bool stimChip = false;
        double parasiticCapacitance;  // Estimate of on-chip parasitic capicitance, including effective amplifier input capacitance.
        if (stimChip) {
            parasiticCapacitance = 12.0e-12;  // 12 pF
        } else {
            parasiticCapacitance = 15.0e-12;  // 15 pF
        }

        // Calculate current amplitude produced by on-chip voltage DAC
        double current = 2.0 * M_PI * impTestFreq * DacVoltageAmplitude * cSeries;

        double finalMagnitude;
        double finalPhase;


        // Calculate impedance magnitude from calculated current and measured voltage.
        finalMagnitude = 1.0e-6 * (impedanceMagnitude[bestAmplitudeIndex] / current) *
                (18.0 * relativeFreq * relativeFreq + 1.0);

        // Calculate impedance phase, with small correction factor accounting for the 3-command SPI pipeline delay.
        finalPhase = impedancePhase[bestAmplitudeIndex] + (360.0 * (3.0 / period));


        // Factor out on-chip parasitic capacitance from impedance measurement.
        // First, convert from polar coordinates to rectangular coordinates.
        double measuredR = finalMagnitude * cos(0.0174533 * finalPhase);
        double measuredX = finalMagnitude * sin(0.0174533 * finalPhase);

        double capTerm = 2.0 * M_PI * impTestFreq * parasiticCapacitance;
        double xTerm = capTerm * (measuredR * measuredR + measuredX * measuredX);
        double denominator = capTerm * xTerm + 2.0 * capTerm * measuredX + 1.0;
        double trueR = measuredR / denominator;
        double trueX = (measuredX + xTerm) / denominator;

        // Now, convert from rectangular coordinates back to polar coordinates.

        finalMagnitude = sqrt(trueR * trueR + trueX * trueX);
        finalPhase = (180.0/3.141592653589793238463) * atan2(trueX, trueR);


        // Empirical tests indicate that Intan chips usually underestimate impedance
        // by about 9%
        finalMagnitude = 1.09 * finalMagnitude;




        //qDebug() << s.channel << finalMagnitude << bestAmplitudeIndex << impedanceMagnitude[0] << impedanceMagnitude[1] << impedanceMagnitude[2];

        emit impedanceValueReturned(s.channel,(int)finalMagnitude);

        //qDebug() << "AMP" << magnitude;
        /*for (int q=0;q<132;q++) {
                qDebug() << dbldata.at(q);
            }*/



    }

    //Switch system back to normal mode
    impedanceModeCommand[0] = command_startImpedanceTest;
    impedanceModeCommand[1] = 0; //0 for regular mode, 1 for impedance mode
    impedanceModeCommand[2] = 0; //Valid values are 0, 1 or 2
    impedanceModeCommand[3] = 0;
    impedanceModeCommand[4] = 0;
    bool success = sendCommandToHardware(impedanceModeCommand);


}



void AbstractTrodesSource::SendImpendaceMeasureCommand(HardwareImpedanceMeasureCommand s) {
    qDebug() << "Impedance measure request received, but this hardware interface does not support impedance measurements.";
}

quint64 AbstractTrodesSource::getTotalUnresponsiveHeadstagePackets()
{

    return 0;
}

//void AbstractTrodesSource::startDedicatedDataPub() {
//    //create neurological data publisher
//    neuroDataPub = new HighFreqPub();
//    neuroDataPub->initialize();

//    HighFreqDataType contData;
//    contData.setName(hfType_NEURO);
//    contData.setDataFormat("bsfmt");
//    contData.setSockAddr(neuroDataPub->getAddress());
//    contData.setByteSize((2*(hardwareConf->NCHAN) + 4 + (2*hardwareConf->headerSize)));
//    emit newContinuousData(contData);
//}

void AbstractTrodesSource::RunTimeError() {

    //Here we should display a message that something has gone wrong,
    //but perhaps not stop data acquisition altogether?
    //connectErrorThrown = true;
    //StopAcquisition();
    qDebug() << "An error occured during data acquisition.";

    if (!unitTestMode) {
        QMessageBox messageBox;
        messageBox.warning(0,"Error","An error occured during data acquisition.");
        messageBox.setFixedSize(500,200);
    }

}

void AbstractTrodesSource::restartThread() {

}

void AbstractTrodesSource::StartSimulation() {

}

void AbstractTrodesSource::SendSettleCommand() {

}

void AbstractTrodesSource::SendSettleChannel(int byteInPacket, quint8 bit, int delay, quint8 triggerState) {

}

void AbstractTrodesSource::SendFunctionTrigger(int funcNum) {

}

void AbstractTrodesSource::SendHeadstageSettings(HeadstageSettings s) {

}

bool AbstractTrodesSource::SendNeuroPixelsSettings(NeuroPixelsSettings s) {
    return true;
}

void AbstractTrodesSource::SendSaveHeadstageSettings() {

}

NeuroPixelsSettings AbstractTrodesSource::GetNeuroPixelsSettings() {
    NeuroPixelsSettings s;
    return s;
}

HeadstageSettings AbstractTrodesSource::GetHeadstageSettings() {
    HeadstageSettings s;
    return s;
}

HardwareControllerSettings AbstractTrodesSource::GetControllerSettings() {
    HardwareControllerSettings s;
    return s;
}

void AbstractTrodesSource::SetStimulationParams(StimulationCommand s) {

}

bool AbstractTrodesSource::SendGlobalStimulationSettings(GlobalStimulationSettings s) {
    return true;
}

void AbstractTrodesSource::SendGlobalStimulationAction(GlobalStimulationCommand s) {

}


void AbstractTrodesSource::ClearStimulationParams(uint16_t slot) {

}

void AbstractTrodesSource::SendStimulationStartSlot(uint16_t slot) {

}

void AbstractTrodesSource::SendStimulationStartGroup(uint16_t group) {

}

void AbstractTrodesSource::SendStimulationStopSlot(uint16_t slot) {

}

void AbstractTrodesSource::SendStimulationStopGroup(uint16_t group) {

}

void AbstractTrodesSource::EnableECUShortcutMessages() {

}

void AbstractTrodesSource::SendECUShortcutMessage(uint16_t function) {

}

bool AbstractTrodesSource::RunDiagnostic(QByteArray *data, HeadstageSettings &hsSettings, HardwareControllerSettings &cnSettings) {
    return false;
}

void AbstractTrodesSource::SendControllerSettings(HardwareControllerSettings s) {

}

void AbstractTrodesSource::SendSDCardUnlock() {

}

void AbstractTrodesSource::ConnectToSDCard() {

}

void AbstractTrodesSource::ReconfigureSDCard(int numChannels) {

}

int AbstractTrodesSource::MeasurePacketLength(HeadstageSettings settings){
    return -1;
}

