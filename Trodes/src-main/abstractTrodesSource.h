#ifndef ABSTRACTTRODESSOURCE_H
#define ABSTRACTTRODESSOURCE_H

#include <QtCore>
#include <QMessageBox>
#include <stdint.h>


#include "highfreqclasses.h"


//#include "dialogs.h"
#include "trodesglobaltypes.h"
#include "hardwaresettings.h"
#include <chrono>

#include <memory>
#include <thread>

#include <TrodesNetwork/Resources/SourcePublisher.h>
#include <TrodesNetwork/Generated/TrodesAnalogData.h>
#include <TrodesNetwork/Generated/TrodesDigitalData.h>
#include <TrodesNetwork/Generated/TrodesNeuralData.h>
#include <TrodesNetwork/Generated/TrodesTimestampData.h>


typedef struct alignas(64) {
  int16_t  data[EEG_BUFFER_SIZE * 4096];
  int16_t carvals[EEG_BUFFER_SIZE*MAXCARGROUPS];
  uint32_t  timestamps[EEG_BUFFER_SIZE]; //hardware timestamps (sample number)
  int64_t sysTimestamps[EEG_BUFFER_SIZE]; //computer's timestamps in ms
  int64_t sysClock[EEG_BUFFER_SIZE];
  int16_t  digitalInfo[EEG_BUFFER_SIZE*2048];


  double   dTime[EEG_BUFFER_SIZE];
  QAtomicInt   writeIdx;
} eegDataBuffer;

extern eegDataBuffer rawData;

class TransferBlockRingBuffer
{
public:

    TransferBlockRingBuffer(int maxTransferSize = 0, int RingSize = 0);
    ~TransferBlockRingBuffer();
    void setBufferSize(int maxTransferSize, int RingSize);
    uchar* getNextWritePtr(bool *ok);
    void setLastWriteTransferSize(int size);
    bool advanceWriteMarker();
    int read(uchar*, int size);
    int bytesAvailable();
    void clear();


private:

    QList<QVector<uchar> > buffers;
    QVector<int> transferSizes;
    int currentWriteBlock;
    int currentReservedWriteBlock;
    int unfinishedWriteJobs;
    int currentReadBlock;
    int currentReadBlockByte;

    uint64_t totalBytesWritten;
    uint64_t totalBytesRead;

    uint64_t totalBlocksWritten;
    uint64_t totalBlocksRead;


};

//Network data publisher
class RawDataPublisher
{
public:
    RawDataPublisher(QString host, int port, int PSize, int hsize);
    ~RawDataPublisher();
    void setHeaderSize(int hsize);
    void setSendNeuralData(bool send);
    void setSendDigitalData(bool send);
    void setSendAnalogData(bool send);
    void setSendTimeData(bool send);

    // starts the zmq-based pub
    bool startDedicatedDataPub(void);
    // destroys the zmq-based pub
    void deleteDedicatedDataPub(void);

    void publishDataPacket(unsigned char *rData, size_t dataSize, int64_t systime);
    void publishDataPacket(int rawIdx);

private:
    void allocatePacketBuffer(void);
    void freePacketBuffer(void);

    //Network publishers and related
    struct segment
    {
        segment() {}
        segment(int s, int l) : start(s), length(l){}
        int start;
        int length;
    };
    QString host;
    int port;
    int PACKET_SIZE;
    int headerSize;
    unsigned char *packetBuff; //data packet buffer
    unsigned char *prevDigital;



    std::unique_ptr<trodes::network::SourcePublisher<trodes::network::TrodesTimestampData>> timestampTrodesPub;
    std::unique_ptr<trodes::network::SourcePublisher<trodes::network::TrodesNeuralData>> neuralTrodesPub;
    std::unique_ptr<trodes::network::SourcePublisher<trodes::network::TrodesAnalogData>> analogTrodesPub;
    std::unique_ptr<trodes::network::SourcePublisher<trodes::network::TrodesDigitalData>> digitalTrodesPub;

    std::thread data_service_thread;

    void configureHFData();
    void calculateIndices();

    bool neuralDataRequested;
    bool digitalDataRequested;
    bool analogDataRequested;
    bool timeDataRequested;

    std::vector<segment> neuralIndices;
    std::vector<segment> digitalIndices;
    std::vector<segment> analogIndices;
};

class SourceRuntimeHelper : public QObject {
  Q_OBJECT
public:
    static int64_t getSystemTimeNanoseconds(){
        return std::chrono::duration_cast<std::chrono::nanoseconds>(
                    std::chrono::system_clock::now().time_since_epoch()
                    ).count();
    }
  SourceRuntimeHelper();
  virtual ~SourceRuntimeHelper();

  bool startDedicatedDataPub();
  void deleteDedicatedDataPub(void);
  void setTransferBuffer(TransferBlockRingBuffer *bf);
  bool isUsingTransferBuffer();



  bool quitNow;
  bool runLoopActive;
  bool acquiring;
  //QAtomicInt totalDroppedPacketEvents;
  //void printStreamingStats();
  //void resetStreamingStats();
  bool appendSysClock;
  QAtomicInt totalDroppedPacketEvents;
  QAtomicInt totalUnresponsiveHeadstagePackets;
  uint64_t      numPacketsProcessed;
  uint64_t      numPacketsDropped;
  uint64_t      largestPacketDrop;


protected:

  void publishDataPacket(unsigned char *rData, size_t dataSize, int64_t systime);
  void publishDataPacket(void);
  void calculatePacketSize();

  bool checkForCorrectTimeSequence();
  void checkHardwareStatus(const uchar *statusPtr);
  bool checkFrameAlignment(unsigned char*);
  void findNextSyncByte(unsigned char*);


  void setHeaderSize(int hsize);
  bool badFrameAlignment;
  bool unresponsiveHeadstage;
  unsigned int tempSyncByteLocation;
  unsigned int PACKET_SIZE;
  unsigned int headerSize;
  uint32_t      lastTimeStamp;
  bool          packetSizeErrorThrown;


  void calculateReferences();
  virtual void  calculateHeaderSize();

private:
  int           numConsecJumps;
  RawDataPublisher* datapub;
  int rawIdx;
  TransferBlockRingBuffer* transferBuffer;
  bool useTransferBuffer;




public slots:
  void Run(void);

signals:
  void NewData(void);
  void finished();
  void timeStampError(bool error);
  void failure();
  void signal_UnresponsiveHeadstage(bool u);

  void deregisterHighFreqData(HighFreqDataType dType);

//  void publishNeuroData(unsigned char *data, size_t size);
//  void newContinuousData(HighFreqDataType dType);

};

class AbstractSourceRuntime : public QObject {
  Q_OBJECT
public:
    static int64_t getSystemTimeNanoseconds(){
        return std::chrono::duration_cast<std::chrono::nanoseconds>(
                    std::chrono::system_clock::now().time_since_epoch()
                    ).count();
    }
  AbstractSourceRuntime();
  virtual ~AbstractSourceRuntime();

  bool startDedicatedDataPub();
  void deleteDedicatedDataPub(void);
  bool isHelperThreadRunning();

  //QAtomicInt quitNow;

  bool quitNow;
  //QAtomicInt acquiring;
  bool acquiring;
  QAtomicInt totalDroppedPacketEvents;
  QAtomicInt totalUnresponsiveHeadstagePackets;
  void printStreamingStats();
  void resetStreamingStats();
  bool appendSysClock;
  bool runLoopActive;




protected:
  bool checkForCorrectTimeSequence();
  void checkHardwareStatus(const uchar *statusPtr);
  bool checkFrameAlignment(unsigned char*);
  void findNextSyncByte(unsigned char*);
  void publishDataPacket(unsigned char *rData, size_t dataSize, int64_t systime);
  void publishDataPacket(void);
  void calculatePacketSize();
  void setHeaderSize(int hsize);
  bool badFrameAlignment;
  bool unresponsiveHeadstage;
  unsigned int tempSyncByteLocation;
  unsigned int PACKET_SIZE;
  unsigned int headerSize;
  uint32_t      lastTimeStamp;
  bool          packetSizeErrorThrown;
  uint64_t      numPacketsProcessed;
  uint64_t      numPacketsDropped;
  uint64_t      largestPacketDrop;

  SourceRuntimeHelper *runtimeHelper;

  void calculateReferences();
  virtual void  calculateHeaderSize();

private:
  int           numConsecJumps;
  bool          directDataPub;
  RawDataPublisher* datapub;
  QThread       *workerThread;
  void          setUpHelperThread(SourceRuntimeHelper* rtPtr);
  bool          helperThreadCreated;
  bool          helperThreadRunning;


public slots:
  virtual void Run(void) = 0;

private slots:
  void setHelperThreadRunning();
  void setHelperThreadNotRunning();

signals:
  void NewData(void);
  void finished();
  void timeStampError(bool error);
  void failure();
  void startHelperThread(void);
  void signal_UnresponsiveHeadstage(bool u);
  void deregisterHighFreqData(HighFreqDataType dType);

//  void publishNeuroData(unsigned char *data, size_t size);
//  void newContinuousData(HighFreqDataType dType);

};



class AbstractTrodesSource : public QObject {
  Q_OBJECT

public:
    AbstractTrodesSource();
    ~AbstractTrodesSource();
    void        setECUConnected(bool);
    void        setAppendSysClock(bool);
    bool        isThreadRunning();
    virtual quint64 getTotalDroppedPacketEvents();
    virtual quint64 getTotalUnresponsiveHeadstagePackets();
    virtual void SendImpendaceMeasureCommand(HardwareImpedanceMeasureCommand s);
    void MeasureImpedance(HardwareImpedanceMeasureCommand s);

    HardwareControllerSettings lastControllerSettings;
    HeadstageSettings          lastHeadstageSettings;

    /*enum CommandMessageCodes : quint8 {
        command_startNoECU = 0x61,
        command_stop = 0x62,
        command_startWithECU = 0x64,
        command_sdCardUnlock = 0x65,
        command_settle = 0x66,
        command_writeHSSettingsToFlash = 0x67,
        command_trigger = 0x69,
        command_setSettleTriggerChannel = 0x6A,
        command_startSimulation = 0x6B,
        command_connectToSD = 0x6C,
        command_configureSD = 0x6D,
        command_getHeadstageSettings = 0x81,
        command_setHeadstageSettings = 0x82,
        command_getControllerSettings = 0x83,
        command_setControllerSettings = 0x84,
        command_setStimulateParams = 0x85, //Set stim pattern in one slot
        command_clearStimulateParams = 0x86, //Clear a stim pattern in one memory slot
        command_stimulateStart = 0x87,  //Start a single defined stim pattern
        command_stimulateStartGroup = 0x88,//Start all defined stim patterns
        command_stimulateStop = 0x89,  //Stop a single running stim pattern
        command_stimulateStopGroup = 0x8A,//Stop all running stim patterns
        command_getStimulationParams = 0x8B, //Get stimulation parameters in one memory slot
        command_setGlobalStimSettings = 0x8C, //Send global stimulation settings
        command_setGlobalStimAction = 0x8D //Send one-time global stimulation command

    };

    enum ReceivedMessageCodes {
        received_headstageSettings = 0x81,
        received_controllerSettings = 0x83,
        received_stimulationParams = 0x74
    };*/


protected:
  static bool convertToStateMachineCommand(StimulationCommand &self, QByteArray *array);
  static bool convertToStateMachineCommand(GlobalStimulationCommand &self, QByteArray *array);
  static bool convertToStateMachineCommand(GlobalStimulationSettings &self, QByteArray *array);

  virtual bool getDataStreamFromHardware(QByteArray* data, int numBytes);
  virtual bool sendCommandToHardware(QByteArray &command);

  static HardwareControllerSettings convertHardwareControllerSettings(QByteArray &array);
  static HeadstageSettings convertHeadstageSettings(QByteArray &array);

  void MeasureImpedance_Intan(HardwareImpedanceMeasureCommand s);

  QThread       *workerThread;
  void          setUpThread(AbstractSourceRuntime* rtPtr);
  char          startCommandValue;
  bool          connectErrorThrown;
  bool          restartThreadAfterShutdown;
  bool          threadRunning;
  bool          threadCreated;
  bool          reinitMode;
  bool          appendSysClock;


protected slots:
  virtual void RunTimeError(void);
  void setThreadNotRunning();
  void setThreadRunning();
  virtual void restartThread();


public slots:
  virtual void InitInterface(void) = 0;  // This gets called after constructor
  virtual void StartAcquisition(void) = 0;
  virtual void StartSimulation(void); //Hardware will generate fake data
  virtual void StopAcquisition(void) = 0;
  virtual void CloseInterface(void) = 0;
  virtual void SendSettleCommand(void);
  virtual void SendSettleChannel(int byteInPacket, quint8 bit, int delay, quint8 triggerState);
  virtual void SendFunctionTrigger(int funcNum);
  virtual void SendHeadstageSettings(HeadstageSettings s);
  virtual bool SendNeuroPixelsSettings(NeuroPixelsSettings s);
  virtual void SendSaveHeadstageSettings();
  virtual void SetStimulationParams(StimulationCommand s);
  virtual bool SendGlobalStimulationSettings(GlobalStimulationSettings s);
  virtual void SendGlobalStimulationAction(GlobalStimulationCommand s);
  virtual void ClearStimulationParams(uint16_t slot);
  virtual void SendStimulationStartSlot(uint16_t slot);
  virtual void SendStimulationStartGroup(uint16_t group);
  virtual void SendStimulationStopSlot(uint16_t slot);
  virtual void SendStimulationStopGroup(uint16_t group);

  virtual void EnableECUShortcutMessages();
  virtual void SendECUShortcutMessage(uint16_t function);
  bool RunDiagnostic(QByteArray *data, HeadstageSettings &hsSettings, HardwareControllerSettings &cnSettings);

  virtual NeuroPixelsSettings GetNeuroPixelsSettings();
  virtual HeadstageSettings GetHeadstageSettings();
  virtual HardwareControllerSettings GetControllerSettings();
  virtual void SendControllerSettings(HardwareControllerSettings s);
  virtual void SendSDCardUnlock(void);
  virtual void ConnectToSDCard(void);
  virtual void ReconfigureSDCard(int numChannels);
  virtual int MeasurePacketLength(HeadstageSettings settings);


signals:
  void stateChanged(int);
  void startRuntime(void);
  void acquisitionStarted(void);
  void acquisitionStopped(void);
  void acquisitionPaused(void);
  void timeStampError(bool error);
  void signal_UnresponsiveHeadstage(bool u);
  void SDCardStatus(bool cardConnected,int numChan, bool unlocked, bool hasData);
  void headstageSettingsReturned(HeadstageSettings s);
  void controllerSettingsReturned(HardwareControllerSettings s);
  void setTimeStamps(uint32_t, uint32_t);
  void workerThreadStopped();
  void impedanceValueReturned(int HWChan, int value);

  void newContinuousData(HighFreqDataType dType);
  void deregisterHighFreqData(HighFreqDataType dType);

  void newDiagnosticMessage(QString msg);
};


#endif // ABSTRACTTRODESSOURCE_H
