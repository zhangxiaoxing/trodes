/*
   Trodes is a free, open-source neuroscience data collection and experimental control toolbox

   Copyright (C) 2012 Mattias Karlsson

   This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// part of trodesnetwork-v2
#include <memory>
#include <TrodesModule.h>

#include "mainWindow.h"
//#include <QGLFormat>
#include "rmsplot.h"
//QGLFormat qglFormat;
bool linkChangesBool;
bool exportMode;
extern QString lastDebugMsg;
extern bool unitTestMode;


BackgroundFrame::BackgroundFrame(QWidget *parent) : QFrame(parent) {
    setAttribute(Qt::WA_NoSystemBackground);
    newParent();
    setStyleSheet("background-color:white");
    QVBoxLayout *mainLayout = new QVBoxLayout();

    QLabel *newLabel = new QLabel(tr(""));
    mainLayout->addWidget(newLabel);
    setLayout(mainLayout);

}

bool BackgroundFrame::eventFilter(QObject *obj, QEvent *ev) {
    if (obj == parent()) {
        if (ev->type() == QEvent::Resize) {

            resize(static_cast<QResizeEvent*>(ev)->size());
        }
        else if (ev->type() == QEvent::ChildAdded) {
            raise();
        }
    }
    return(QFrame::eventFilter(obj, ev));
}

bool BackgroundFrame::event(QEvent *ev) {
    if (ev->type() == QEvent::ParentAboutToChange) {
        if(parent())
            parent()->removeEventFilter(this);
    }
    else if (ev->type() == QEvent::ParentChange) {
        newParent();
    }
    return(QFrame::event(ev));
}

MainWindow::MainWindow(bool audioOn)
{
    setWindowIcon(QIcon(":/trodesIcon.png"));
    nTrodeSelectWindow = nullptr;
    isAudioOn = audioOn;
    prevSelectedNTrodeIndex = 0;
    tempWorkspaceLoading = false;
    sourceMenuState = SOURCE_STATE_NOT_CONNECTED;
    unresponsiveHeadstageFlag = false;
    closeInitiated = false;

    networkConf=nullptr;
    moduleConf=nullptr;
    streamConf=nullptr;
    spikeConf=nullptr;
    headerConf=nullptr;
    hardwareConf=nullptr;
    globalConf=nullptr;
    //benchConfig=NULL;

    qDebug().noquote() << "Trodes Version Info:\n" << GlobalConfiguration::getVersionInfo(false); //print version info to debug log

    quitting = false;
    unitTestFlag = false;  //set to true if we are doing unit testing
    enableAVXFlag = false;
    debugCheckpointsOn = false;
    debugCheckpointStatement = "";
    debugCheckpointNumber = 0;
    currentFilePart = 0;


  if (objectName().isEmpty())
    setObjectName(QString("Main"));

  resize(800, 600);
  exportMode = false; //Whether or not data displays should be updated

  //hardwareConf = new HardwareConfiguration(nullptr); // this is requried for creating the audio generator, but this object
                                                  // is overwritten when the config file is read
  preferencesPanel = nullptr;
//  sdDisp = nullptr;
  spikeDisp = nullptr;

  setAutoFillBackground(true);
  soundDialogOpen = false;
  spectrumAnalysisWindowOpen = false;
  impedanceToolOpen = false;
  RMSPlotOpen = false;
  recordFileOpen = false;
  dataStreaming = false;
  dataWasStreamingBeforeLiveReconfig = false;
  liveReconfigHappening = false;
  recording = false;
  timerTick = 0;
  channelsConfigured = false;
  visibleTime = 0;
  eventTabWasChanged = false;
  for (int i = 0; i < 64; i++) {
    eventTabsInitialized[i] = false;
  }
  eventTabsInitialized[0] = true;
  currentTrodeSelected = 0;
  singleTriggerWindowOpen = false;

  settleChannelByteInPacket = 0;
  settleChannelBit = 0;
  settleChannelTriggerState = 0;
  settleChannelDelay = 0;

  headstageSettings.autoSettleOn = false;
  headstageSettings.percentChannelsForSettle = 0;
  headstageSettings.threshForSettle = 0;
  headstageSettings.smartRefOn = false;
  headstageSettings.accelSensorOn = false;
  headstageSettings.gyroSensorOn = false;
  headstageSettings.magSensorOn = false;

  headstageSettings.smartRefAvailable = false;
  headstageSettings.autosettleAvailable = false;
  headstageSettings.accelSensorAvailable = false;
  headstageSettings.gyroSensorAvailable = false;
  headstageSettings.magSensorAvailable = false;
  headstageSettings.rfAvailable = false;


  newCommentDialog = nullptr;
  triggerSettings = nullptr;
  configSettingsChanged = false;
  //qglFormat.setVersion(3,2);
  //qglFormat.setProfile(QGLFormat::CoreProfile);
  //qglFormat.setDoubleBuffer(true);


  //Statusbar setup---------------------------
  statusbar = new QStatusBar(this);
  TrodesFont statusFont;
//  statusFont.setPixelSize(14);
  statusbar->setFont(statusFont);
  setStatusBar(statusbar);
  //statusbar = statusbar;
  statusbar->showMessage(tr("Not connected to device"));
  //------------------------------------------

  //Time format to be displayed
  timeFormatString = "HH:mm:ss.zzz";


  //File menu--------------------------------
  //Any shortcuts should be added in section below for workaround
  menuFile = new QMenu;
  menuFile->setTitle("File");
  menuBar()->addAction(menuFile->menuAction());

  menuConfig = new QMenu;
  menuFile->addAction(menuConfig->menuAction());

  actionLoadConfig = new QAction(this);
  actionLoadConfig->setShortcut(QKeySequence(tr("Ctrl+O")));
  menuConfig->addAction(actionLoadConfig);
  actionCloseConfig = new QAction(this);
  actionCloseConfig->setEnabled(false);
  actionCloseConfig->setShortcut(QKeySequence(tr("Shift+Ctrl+O")));
  menuConfig->addAction(actionCloseConfig);
  actionSaveConfig = new QAction(this);
  actionSaveConfig->setShortcut(QKeySequence(tr("Ctrl+S")));
  actionSaveConfig->setEnabled(false);
  menuConfig->addAction(actionSaveConfig);
  actionReConfig = new QAction(this);
  actionReConfig->setEnabled(false);
  actionReConfig->setShortcut(QKeySequence(tr("Ctrl+R")));
  menuConfig->addAction(actionReConfig);

  actionAboutConfig = new QAction(this);
  actionAboutConfig->setEnabled(false);
  actionAboutConfig->setText("About Workspace");
  menuConfig->addAction(actionAboutConfig);


  actionPlaybackOpen = new QAction(this);
  actionPlaybackOpen->setShortcut(QKeySequence(tr("Ctrl+P")));
  actionPlaybackOpen->setData(QVariant::fromValue(SourceFile));
  menuFile->addAction(actionPlaybackOpen);
  actionOpenRecordDialog = new QAction(this);
  actionOpenRecordDialog->setShortcut(QKeySequence(tr("Ctrl+N")));
  menuFile->addAction(actionOpenRecordDialog);
  actionOpenRecordDialog->setEnabled(false);
  actionCloseFile = new QAction(this);
  actionCloseFile->setShortcut(QKeySequence(tr("Ctrl+W")));
  actionCloseFile->setEnabled(false);
  menuFile->addAction(actionCloseFile);
  menuFile->addSeparator();
  actionRecord = new QAction(this);
  actionRecord->setEnabled(false);
  menuFile->addAction(actionRecord);
  actionPause = new QAction(this);
  actionPause->setEnabled(false);
  menuFile->addAction(actionPause);
  actionPlay = new QAction(this);
  actionPlay->setEnabled(false);
  menuFile->addAction(actionPlay);
  actionMerge = new QAction(this);
  actionMerge->setEnabled(false);
  menuFile->addAction(actionMerge);
  menuExport = new QAction(this);
  menuExport->setEnabled(false);

  /*QStringList exportMenus;
  QStringList exportPrograms;
  exportMenus << "Spikes" << "LFP" << "Aux Analog" << "Aux Digital" << "Raw binary (for offline sorter)" << "Klusta-Suite" << "MountainSort";
  exportPrograms << "exportspikes" << "exportlfp" << "exportanalog" << "exportdio" << "exportofflinesorter" << "exportphy" << "exportmda";
  for (int mInd = 0;mInd < exportMenus.length();mInd++) {
      QAction *actionPtr = new QAction(this);
      actionPtr->setText(exportMenus.at(mInd));
      actionPtr->setData(QVariant(exportPrograms.at(mInd)));
      connect(actionPtr,SIGNAL(triggered()),this,SLOT(exportActionSelected()));
      menuExport->addAction(actionPtr);
  }*/



  menuFile->addAction(menuExport);
  connect(menuExport,SIGNAL(triggered()),this,SLOT(openExportMenu()));

  actionSaveClusters = new QAction(this);
  actionSaveClusters->setEnabled(false);
  menuFile->addAction(actionSaveClusters);

  actionLoadClusters = new QAction(this);
  actionLoadClusters->setEnabled(false);
  menuFile->addAction(actionLoadClusters);


  menuFile->addSeparator();
  // Module Menu

  actionRestartModules = new QAction(this);
  menuFile->addAction(actionRestartModules);
  actionRestartModules->setEnabled(false);

  menuFile->addSeparator();

  actionQuit = new QAction(this);
  actionQuit->setShortcut(Qt::CTRL + Qt::Key_Q);
  //actionQuit->setMenuRole(QAction::QuitRole);
  menuFile->addAction(actionQuit);


  //****************************
  //Workaround for qt 5.7+ bug in linux: action shortcuts in linux do not work
  addAction(actionLoadConfig);
  addAction(actionCloseConfig);
  addAction(actionSaveConfig);
  addAction(actionPlaybackOpen);
  addAction(actionOpenRecordDialog);
  addAction(actionCloseFile);
  addAction(actionQuit);
  //****************************

  connect(actionLoadConfig, SIGNAL(triggered()), this, SLOT(openWorkspaceFileForAcquisition()));
  connect(actionReConfig, SIGNAL(triggered()), this, SLOT(reConfig()));
  QObject::connect(actionPlaybackOpen,SIGNAL(triggered()),this,SLOT(setSource()));
  connect(actionCloseConfig, SIGNAL(triggered()), this, SLOT(closeWorkspace()));
  connect(actionSaveConfig, SIGNAL(triggered()), this, SLOT(saveConfig()));
  connect(actionAboutConfig, SIGNAL(triggered()), this, SLOT(aboutCurConfig()));


  QObject::connect(actionOpenRecordDialog, SIGNAL(triggered()), this, SLOT(openRecordDialog()));
  QObject::connect(actionCloseFile, SIGNAL(triggered()), this, SLOT(closeFile()));
  connect(actionRecord,SIGNAL(triggered()),this,SLOT(actionRecordSelected()));
  connect(actionPause,SIGNAL(triggered()),this,SLOT(actionPauseSelected()));
  connect(actionPlay,SIGNAL(triggered()),this,SLOT(actionPlaySelected()));
  connect(actionMerge,SIGNAL(triggered()),this,SLOT(actionMergeSelected()));
  //connect(actionExport,SIGNAL(triggered()),this,SLOT(openExportDialog()));
  connect(actionSaveClusters,SIGNAL(triggered()),this,SLOT(saveCurrentClusters()));
  connect(actionLoadClusters,SIGNAL(triggered()),this,SLOT(loadClusterFile()));
  connect(actionQuit, SIGNAL(triggered()), this, SLOT(close()));
  connect(actionRestartModules, SIGNAL(triggered()), this, SLOT(checkRestartModules()));


  //----------------------------------------------



  //Connection menu--------------------------------
  menuSystem = new QMenu;
  menuSystem->setTitle("Connection");
  menuBar()->addAction(menuSystem->menuAction());
  //mainMenuBar->addAction(menuSystem->menuAction());
  //actionReconfigure = new QAction(this);
  //menuSystem->addAction(actionReconfigure);
  sourceMenu = new QMenu;
  menuSystem->addAction(sourceMenu->menuAction());
  menuSystem->addSeparator();
  menuSimulationSource = new QMenu;
  menuSpikeGadgetsSource = new QMenu;
  actionSourceNone = new QAction(this);
  actionSourceFake = new QAction(this);
  actionSourceFakeSpikes = new QAction(this);
  menuSimulationSource->addAction(actionSourceFake);
  menuSimulationSource->addAction(actionSourceFakeSpikes);
  actionSourceFile = new QAction(this);
  actionSourceEthernet = new QAction(this);
  actionSourceUSB = new QAction(this);
  actionSourceUSB3 = new QAction(this);
  actionSourceDockUSB = new QAction(this);
  menuSpikeGadgetsSource->addAction(actionSourceUSB);
  menuSpikeGadgetsSource->addAction(actionSourceEthernet);
  menuSpikeGadgetsSource->addAction(actionSourceDockUSB);
  menuSpikeGadgetsSource->addAction(actionSourceUSB3);
  actionSourceRhythm = new QAction(this);
  actionSourceNone->setCheckable(true);
  actionSourceNone->setChecked(true);
  actionSourceFake->setCheckable(true);
  actionSourceFake->setChecked(false);
  actionSourceFakeSpikes->setCheckable(true);
  actionSourceFakeSpikes->setChecked(false);
  actionSourceFile->setCheckable(true);
  actionSourceFile->setChecked(false);
  actionSourceUSB->setCheckable(true);
  actionSourceUSB->setChecked(false);
  actionSourceUSB3->setCheckable(true);
  actionSourceUSB3->setChecked(false);
  actionSourceDockUSB->setCheckable(true);
  actionSourceDockUSB->setChecked(false);
  actionSourceRhythm->setCheckable(true);
  actionSourceRhythm->setChecked(false);
  actionSourceEthernet->setCheckable(true);
  actionSourceEthernet->setChecked(false);
  sourceMenu->addAction(actionSourceNone);
  //sourceMenu->addAction(actionSourceFake);
  //sourceMenu->addAction(actionSourceFakeSpikes);
  sourceMenu->addAction(actionSourceFile);
  //sourceMenu->addAction(actionSourceEthernet);
  //sourceMenu->addAction(actionSourceUSB);
  sourceMenu->addAction(menuSimulationSource->menuAction());
  sourceMenu->addAction(menuSpikeGadgetsSource->menuAction());
  sourceMenu->addAction(actionSourceRhythm);
  actionSourceNone->setData(QVariant::fromValue(SourceNone));
  actionSourceFake->setData(QVariant::fromValue(SourceFake));
  actionSourceFakeSpikes->setData(QVariant::fromValue(SourceFakeSpikes));
  actionSourceFile->setData(QVariant::fromValue(SourceFile));
  actionSourceEthernet->setData(QVariant::fromValue(SourceEthernet));
  actionSourceUSB->setData(QVariant::fromValue(SourceUSBDAQ));
  actionSourceUSB3->setData(QVariant::fromValue(SourceUSB3));
#ifndef USE_D3XX
  actionSourceUSB3->setVisible(false);
#endif
  actionSourceDockUSB->setData(QVariant::fromValue(SourceDockUSB));
  actionSourceRhythm->setData(QVariant::fromValue(SourceRhythm));
#ifndef RHYTHM
    actionSourceRhythm->setVisible(false);
#endif
  actionConnect = new QAction(this);
  menuSystem->addAction(actionConnect);
  actionConnect->setEnabled(false);
  actionDisconnect = new QAction(this);
  menuSystem->addAction(actionDisconnect);
  actionDisconnect->setEnabled(false);
  actionClearBuffers = new QAction(this);
  menuSystem->addAction(actionClearBuffers);
  actionClearBuffers->setEnabled(false);
  actionSendSettle = new QAction(this);
  menuSystem->addAction(actionSendSettle);
  actionSendSettle->setEnabled(false);
  menuSystem->addSeparator();

  sourceOptionsMenu = new QMenu;
  menuSystem->addAction(sourceOptionsMenu->menuAction());
  actionOpenGeneratorDialog = new QAction(this);
  menuSystem->addAction(actionOpenGeneratorDialog);
  actionOpenGeneratorDialog->setEnabled(false);

  //fillSourceOptionsMenu(); //this will go somewhere else later




  QObject::connect(actionSourceNone,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceFake,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceFakeSpikes,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceFile,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceEthernet,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceUSB,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceUSB3,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionSourceDockUSB, SIGNAL(triggered()), this, SLOT(setSource()));
  QObject::connect(actionSourceRhythm,SIGNAL(triggered()),this,SLOT(setSource()));
  QObject::connect(actionConnect, SIGNAL(triggered()), this, SLOT(connectToSource()));
  QObject::connect(actionDisconnect, SIGNAL(triggered()), this, SLOT(disconnectFromSource()));
  QObject::connect(actionSendSettle, SIGNAL(triggered()), this, SLOT(sendSettleCommand()));

  QObject::connect(actionOpenGeneratorDialog, SIGNAL(triggered()), this, SLOT(openGeneratorDialog()));

  //-----------------------------------------------------



  //Display menu-------------------------------------------
  menuDisplay = new QMenu;
  menuDisplay->setTitle("View");
  menuBar()->addAction(menuDisplay->menuAction());
  //mainMenuBar->addAction(menuDisplay->menuAction());
  actionShowCurrentTrode = new QAction(this);
  actionShowCurrentTrode->setEnabled(false);
  menuDisplay->addAction(actionShowCurrentTrode);
  actionUncoupleDisplay = new QAction(this);
  actionUncoupleDisplay->setCheckable(true);
  actionUncoupleDisplay->setChecked(false);
  actionUncoupleDisplay->setShortcut(QKeySequence(Qt::Key_Space));
  menuDisplay->addAction(actionUncoupleDisplay);
  connect(actionUncoupleDisplay,SIGNAL(toggled(bool)),this,SLOT(uncoupleDisplay(bool)));

  actionRealtimeDisplay = new QAction(this);
  actionRealtimeDisplay->setCheckable(true);
  actionRealtimeDisplay->setChecked(false);
  menuDisplay->addAction(actionRealtimeDisplay);
  connect(actionRealtimeDisplay,SIGNAL(toggled(bool)),this,SLOT(setRealTimeDisplayMode(bool)));

  menuEEGDisplay = new QMenu;
  menuDisplay->addAction(menuEEGDisplay->menuAction());
  menuSetTLength = new QMenu(this);
  actionSetTLength0_2 = new QAction(this);
  actionSetTLength0_2->setData(0.2);
  actionSetTLength0_2->setCheckable(true);
  actionSetTLength0_2->setChecked(false);
  actionSetTLength0_5 = new QAction(this);
  actionSetTLength0_5->setData(0.5);
  actionSetTLength0_5->setCheckable(true);
  actionSetTLength0_5->setChecked(false);
  actionSetTLength1_0 = new QAction(this);
  actionSetTLength1_0->setData(1.0);
  actionSetTLength1_0->setCheckable(true);
  actionSetTLength1_0->setChecked(true);
  actionSetTLength2_0 = new QAction(this);
  actionSetTLength2_0->setData(2.0);
  actionSetTLength2_0->setCheckable(true);
  actionSetTLength2_0->setChecked(false);
  actionSetTLength5_0 = new QAction(this);
  actionSetTLength5_0->setData(5.0);
  actionSetTLength5_0->setCheckable(true);
  actionSetTLength5_0->setChecked(false);
  menuSetTLength->addAction(actionSetTLength0_2);
  menuSetTLength->addAction(actionSetTLength0_5);
  menuSetTLength->addAction(actionSetTLength1_0);
  menuSetTLength->addAction(actionSetTLength2_0);
  menuSetTLength->addAction(actionSetTLength5_0);
  menuEEGDisplay->addMenu(menuSetTLength);
  streamFilterMenu = new QMenu;
  menuEEGDisplay->addAction(streamFilterMenu->menuAction());
  streamLFPFiltersOn = new QAction(this);
  streamLFPFiltersOn->setCheckable(true);
  streamLFPFiltersOn->setChecked(false);
  streamSpikeFiltersOn = new QAction(this);
  streamSpikeFiltersOn->setCheckable(true);
  streamSpikeFiltersOn->setChecked(true);
  streamStimViewOn = new QAction(this);
  streamStimViewOn->setCheckable(true);
  streamStimViewOn->setChecked(false);
  streamNoFiltersOn = new QAction(this);
  streamNoFiltersOn->setCheckable(true);
  streamNoFiltersOn->setChecked(false);
  streamFilterMenu->addAction(streamLFPFiltersOn);
  streamFilterMenu->addAction(streamSpikeFiltersOn);
  streamFilterMenu->addAction(streamNoFiltersOn);
  streamFilterMenu->addAction(streamStimViewOn);
  actionSound = new QAction(this);
  menuDisplay->addAction(actionSound);
  actionDisplayAllPETHs = new QAction(this);
  actionDisplayAllPETHs->setText("Open All PETH plots");
  menuDisplay->addAction(actionDisplayAllPETHs);

  menuTools = new QMenu;
  menuDisplay->addAction(menuTools->menuAction());
  menuTools->setTitle("Tools");
  actionDisplaySpectrumAnalysis = new QAction(this);
  actionDisplaySpectrumAnalysis->setText("Power spectral density");
  actionDisplaySpectrumAnalysis->setCheckable(true);
  menuTools->addAction(actionDisplaySpectrumAnalysis);

  actionDisplayImpedanceTool = new QAction(this);
  actionDisplayImpedanceTool->setText("Impedance measurement");
  actionDisplayImpedanceTool->setCheckable(true);
  actionDisplayImpedanceTool->setVisible(true); //Until tool is ready.
  menuTools->addAction(actionDisplayImpedanceTool);

  actionDisplayRMSPlot = new QAction(this);
  actionDisplayRMSPlot->setText("RMS Noise plot");
  actionDisplayRMSPlot->setCheckable(true);
  menuTools->addAction(actionDisplayRMSPlot);

  actionDisplayRFWidget = new QAction(this);
  actionDisplayRFWidget->setText("RF Display Widget");
  actionDisplayRFWidget->setCheckable(true);
  menuTools->addAction(actionDisplayRFWidget);

  connect(actionDisplayRMSPlot, &QAction::triggered, this, &MainWindow::toggleRMSPlot);

//  actionDisplayRMSPlot->trigger();

  connect(actionDisplayAllPETHs, &QAction::triggered, this, &MainWindow::openallpeths);
  connect(actionDisplaySpectrumAnalysis,&QAction::triggered,this,&MainWindow::toggleSpectralAnalysis);
  connect(actionDisplayImpedanceTool,&QAction::triggered,this,&MainWindow::toggleImpedanceTool);
  connect(actionSetTLength0_2,SIGNAL(triggered()),this,SLOT(setTLength()));
  connect(actionSetTLength0_5,SIGNAL(triggered()),this,SLOT(setTLength()));
  connect(actionSetTLength1_0,SIGNAL(triggered()),this,SLOT(setTLength()));
  connect(actionSetTLength2_0,SIGNAL(triggered()),this,SLOT(setTLength()));
  connect(actionSetTLength5_0,SIGNAL(triggered()),this,SLOT(setTLength()));
  connect(actionShowCurrentTrode,SIGNAL(triggered()),this,SLOT(openTrodeWindow()));
  connect(streamLFPFiltersOn, &QAction::triggered, this, &MainWindow::lfpFiltersOn);
  connect(streamSpikeFiltersOn, &QAction::triggered, this, &MainWindow::spikeFiltersOn);
  connect(streamStimViewOn, &QAction::triggered, this, &MainWindow::stimViewOn);
  connect(streamNoFiltersOn, &QAction::triggered, this, &MainWindow::noFiltersOn);
  if (isAudioOn)
      connect(actionSound, &QAction::triggered, this, &MainWindow::soundButtonPressed);
//    QObject::connect(actionSound, SIGNAL(triggered()), this, SLOT(openSoundDialog()));

  //--------------------------------------------------------
  //Settings menu
  menuSettings = new QMenu;
  menuBar()->addAction(menuSettings->menuAction());

  //nTrode settings menu-------------------------------------------
  menuNTrode = new QMenu;
  menuSettings->addAction(menuNTrode->menuAction());
  //mainMenuBar->addAction(menuNTrode->menuAction());
//  menuLinkChanges = new QMenu;
//  menuNTrode->addAction(menuLinkChanges->menuAction());
//  actionLinkChanges = new QAction(this);
//  actionLinkChanges->setCheckable(true);
//  actionLinkChanges->setChecked(false);
//  actionUnLinkChanges = new QAction(this);
//  actionUnLinkChanges->setCheckable(true);
//  actionUnLinkChanges->setChecked(true);
//  menuLinkChanges->addAction(actionUnLinkChanges);
//  menuLinkChanges->addAction(actionLinkChanges);
  linkChangesBool = false;

  clearAllNTrodes = new QAction(this);
  menuNTrode->addAction(clearAllNTrodes);

  menuHeadstage = new QMenu;
  menuSettings->addAction(menuHeadstage->menuAction());
  actionHeadstageSettings = new QAction(this);
  actionHeadstageSettings->setEnabled(false);
  menuHeadstage->addAction(actionHeadstageSettings);

  actionSettleDelay = new QAction(this);
  menuHeadstage->addAction(actionSettleDelay);

  actionControllerSettings = new QAction(this);
  actionControllerSettings->setEnabled(false);
  menuSettings->addAction(actionControllerSettings);

  menuPreferences = new QAction("Preferences");
  menuSettings->addAction(menuPreferences);

  menuNetworkPanel = new QAction("Network Panel");
  menuSettings->addAction(menuNetworkPanel);




//  QObject::connect(actionUnLinkChanges, SIGNAL(triggered()), this, SLOT(unLinkChanges()));
//  QObject::connect(actionLinkChanges, SIGNAL(triggered()), this, SLOT(linkChanges()));
  QObject::connect(clearAllNTrodes, SIGNAL(triggered()), this , SLOT(clearAll()));
  connect(actionHeadstageSettings,SIGNAL(triggered()),this,SLOT(openHeadstageDialog()));
  connect(actionControllerSettings,SIGNAL(triggered()),this,SLOT(openControllerSettingsDialog()));
  connect(actionSettleDelay, SIGNAL(triggered()),this,SLOT(openSettleChannelDelayDialog()));
  //-----------------------------------------------------

  //Select menu ----------------------------------------
  menuSelect = new QMenu;
  menuBar()->addAction(menuSelect->menuAction());
  actionSelectAll = new QAction(this);
  actionSelectAll->setShortcut(QKeySequence(tr("Ctrl+A")));
  actionSelectByTag = new QAction(this);
  actionSelectByTag->setShortcut(QKeySequence(tr("Ctrl+F")));
  menuSelect->setTitle("Select");
  actionSelectAll->setText("All");
  actionSelectByTag->setText("By Grouping Tag");
  menuSelect->setEnabled(true);
  actionSelectAll->setEnabled(true);
  actionSelectByTag->setEnabled(true);
  menuSelect->addAction(actionSelectAll);
  menuSelect->addAction(actionSelectByTag);

  connect(actionSelectAll, SIGNAL(triggered(bool)), this, SLOT(selectAllNTrodes()));
  connect(actionSelectByTag, SIGNAL(triggered(bool)), this, SLOT(openSelectionDialog()));

  //----------------------------------------------------

  //Debug menu -----------------------------------------
  menuDebug = new QMenu;
  menuBar()->addAction(menuDebug->menuAction());
  actionBenchmarkingSettings = new QAction(this);
  actionBenchmarkingSettings->setShortcut(QKeySequence(tr("Ctrl+B")));
  addAction(actionBenchmarkingSettings);
  menuDebug->setTitle("Debug");
  actionBenchmarkingSettings->setText("Benchmarking");
  menuDebug->setEnabled(true);
  actionBenchmarkingSettings->setEnabled(true);
  menuDebug->addAction(actionBenchmarkingSettings);

  connect(actionBenchmarkingSettings, SIGNAL(triggered()), this, SLOT(openBenchmarkingDialog()));

  //----------------------------------------------------

  //Help menu-----------------------------------------

  actionAboutTrodes = new QAction(this);
  actionAboutTrodes->setMenuRole(QAction::AboutRole);
  actionAboutQT = new QAction(this);
  actionAboutQT->setMenuRole(QAction::AboutQtRole);
  actionAboutHardware = new QAction(this);
  actionAboutHardware->setMenuRole(QAction::ApplicationSpecificRole);
  actionAboutVersion = new QAction(this);
  actionAboutVersion->setMenuRole(QAction::ApplicationSpecificRole);
  actionAboutVersion->setText("Check for updates");

  actionAboutPlaybackFile = new QAction(this);
  actionAboutPlaybackFile->setMenuRole(QAction::AboutRole);
  actionAboutPlaybackFile->setText("About This Recording");

  menuHelp = new QMenu;
  menuBar()->addAction(menuHelp->menuAction());
  //mainMenuBar->addAction(menuHelp->menuAction());
  menuHelp->addAction(actionAboutPlaybackFile);
  menuHelp->addAction(actionAboutTrodes);
  menuHelp->addAction(actionAboutHardware);
  menuHelp->addAction(actionAboutVersion);
  menuHelp->addAction(actionAboutQT);

  QObject::connect(actionAboutTrodes, SIGNAL(triggered()), this, SLOT(about()));
  QObject::connect(actionAboutHardware, SIGNAL(triggered()), this, SLOT(aboutHardware()));
  QObject::connect(actionAboutQT, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
  QObject::connect(actionAboutVersion, SIGNAL(triggered()), this, SLOT(aboutVersion()));
  connect(actionAboutPlaybackFile, &QAction::triggered, this, &MainWindow::aboutPlayback);

  actionAboutPlaybackFile->setVisible(false); //visible if playback mode
  //----------------------------------------------------



  //Layouts and tabs-----------------------------------------
  mainLayout =  new QGridLayout();
  mainLayout->setContentsMargins(QMargins(10,0,10,0));
  mainLayout->setVerticalSpacing(3);
  TrodesFont dispFont;
  tabs = new QTabWidget(this);
  tabs->setAttribute(Qt::WA_NoSystemBackground);
  lastTabIndex = 0;
  //tabs->addTab(eventTabs,tr("nTrodes"));
  //tabs->addTab(eegDisp,tr("Streaming"));
  tabs->setStyleSheet("QTabWidget::pane {margin: 0px}") ;
  tabs->setTabPosition(QTabWidget::West);
  tabs->setFont(dispFont);
  connect(tabs, &QTabWidget::currentChanged, this, &MainWindow::streamTabChanged);

  mainLayout->addWidget(tabs,3,0);

//  tabs->addTab(splashScreen, "Intro");
//  tabs->setTab



  //---------------------------------------------------------

  //Top control panel setup----------------------------------
  headerLayout = new QGridLayout(); //contains the buttons and clock at the top of the screen
  headerLayout->setContentsMargins(QMargins(1,1,1,1));
  headerLayout->setHorizontalSpacing(3);



  int totalRightItems = 10;
  int totalLeftItems = 4;
  //Sound settings button
  soundSettingsButton = new TrodesButton;
  soundSettingsButton->setText(tr("Audio"));
  //soundSettingsButton->setFont(buttonFont);
  soundSettingsButton->setCheckable(false);
  if (isAudioOn)
    connect(soundSettingsButton,SIGNAL(pressed()),this,SLOT(soundButtonPressed()));
  soundSettingsButton->setFixedSize(70,20);
  headerLayout->addWidget(soundSettingsButton,0,totalLeftItems+9);

  trodeSettingsButton = new TrodesButton;
  trodeSettingsButton->setText(tr("nTrode"));
  //trodeSettingsButton->setFont(buttonFont);
  trodeSettingsButton->setCheckable(true);
  connect(trodeSettingsButton,SIGNAL(toggled(bool)),this,SLOT(trodesButtonToggled(bool)));
  trodeSettingsButton->setFixedSize(70,20);
  headerLayout->addWidget(trodeSettingsButton,0,totalLeftItems+8);

  spikesButton = new TrodesButton;
  spikesButton->setText(tr("Spikes"));
  //spikesButton->setFont(buttonFont);
  spikesButton->setCheckable(true);
  spikesButton->setChecked(false);
  connect(spikesButton,SIGNAL(toggled(bool)),this,SLOT(spikesButtonToggled(bool)));
  spikesButton->setFixedSize(70,20);
  headerLayout->addWidget(spikesButton,0,totalLeftItems+7);

  videoButton = new TrodesButton;
  videoButton->setText(tr("Video"));
  //videoButton->setFont(buttonFont);
  videoButton->setCheckable(false);
  connect(videoButton,SIGNAL(clicked()),this,SLOT(videoButtonPressed()));
  videoButton->setFixedSize(70,20);
  headerLayout->addWidget(videoButton,0,totalLeftItems+6);

  statescriptButton = new TrodesButton;
  statescriptButton->setText(tr("StateScript"));
  //statescriptButton->setFont(buttonFont);
  statescriptButton->setCheckable(false);
  connect(statescriptButton,SIGNAL(clicked()),this,SLOT(statescriptButtonPressed()));
  statescriptButton->setFixedSize(80,20);
  headerLayout->addWidget(statescriptButton,0,totalLeftItems+5);


  commentButton = new TrodesButton;
  commentButton->setText(tr("Annotate"));
  //commentButton->setFont(buttonFont);
  commentButton->setCheckable(false);
  commentButton->setEnabled(false);
  connect(commentButton,SIGNAL(pressed()),this,SLOT(commentButtonPressed()));
  commentButton->setFixedSize(70,20);
  headerLayout->addWidget(commentButton,0,totalLeftItems+4);


//  linkChangesButton = new TrodesButton;
//  linkChangesButton->setText(tr("Link nTrodes"));
//  //linkChangesButton->setFont(buttonFont);
//  linkChangesButton->setCheckable(true);
//  connect(linkChangesButton,SIGNAL(toggled(bool)),this,SLOT(linkChanges(bool)));
//  linkChangesButton->setFixedSize(90,20);
//  headerLayout->addWidget(linkChangesButton,0,totalLeftItems+3);

  uncoupleDisplayButton = new TrodesButton;
  uncoupleDisplayButton->setText(tr("Freeze display"));
  //linkChangesButton->setFont(buttonFont);
  uncoupleDisplayButton->setCheckable(true);
  connect(uncoupleDisplayButton,SIGNAL(toggled(bool)),this,SLOT(uncoupleDisplay(bool)));
  uncoupleDisplayButton->setFixedSize(90,20);
  headerLayout->addWidget(uncoupleDisplayButton,0,totalLeftItems+2);

  streamDisplayButton = new TrodesButton;
  streamDisplayButton->setText(tr("Filter Band"));
  char tTip[] = "<html><head/><body><p>" \
                "Choose which filter band to display in the main streaming page." \
                "</p></body></html>";
  streamDisplayButton->setToolTip(tTip);
  connect(streamDisplayButton, &TrodesButton::pressed, this, &MainWindow::streamdisplaybuttonPressed);
  streamDisplayButton->setFixedSize(90,20);
  headerLayout->addWidget(streamDisplayButton, 0, totalLeftItems+1);

  //Time display
  QTime mainClock(0,0,0,0);
  QFont labelFont;
  labelFont.setPixelSize(20);
  //labelFont.setFamily("Console");
  labelFont.setFamily("Arial");
  labelFont.setStyleStrategy(QFont::PreferAntialias);
  timeLabel  = new QLabel;
  timeLabel->setText(mainClock.toString(timeFormatString));
  timeLabel->setFont(labelFont);
  timeLabel->setMinimumWidth(100);
  timeLabel->setAlignment(Qt::AlignLeft);
  pullTimer = new QTimer(this);
  connect(pullTimer, SIGNAL(timeout()), this, SLOT(updateTime()));
  pullTimer->start(100); //update timer every 100 ms
  headerLayout->addWidget(timeLabel,0,totalRightItems+totalLeftItems);

    //Record, pause, and play, analyze buttons
    recordButton = new TrodesButton;
    pauseButton = new TrodesButton;
    playButton = new TrodesButton;
    playbackInfoButton = new TrodesButton;

    SourceButton *srcbtn = new SourceButton;
    srcbtn->addAction(actionSourceNone);
    srcbtn->addAction(actionSourceUSB);
    srcbtn->addAction(actionSourceEthernet);
    srcbtn->addAction(actionSourceFile);
    srcbtn->addAction(actionSourceFake);
    srcbtn->addAction(actionSourceFakeSpikes);

//    RecordFileButton *openrecbtn = new RecordFileButton;
//    openrecbtn->setOpenRecordAction(actionOpenRecordDialog);
//    openrecbtn->setCloseFileAction(actionCloseFile);
//    addToolBar(toolbar);


    setDockOptions(QMainWindow::AnimatedDocks);
    QDockWidget *dockwidget = new QDockWidget("RF dock widget");
    rfwidget = new RFWidget;
    dockwidget->setWidget(rfwidget);
    dockwidget->hide();
    rfwidget->hide();
    connect(actionDisplayRFWidget, &QAction::triggered, this, [dockwidget](bool toggled){
        dockwidget->setVisible(toggled);
    });
    addDockWidget(Qt::DockWidgetArea::BottomDockWidgetArea, dockwidget);
//    actionDisplayRFWidget->trigger();

    //Using QT Resource System
    QPixmap playPixmap(":/buttons/playImage.png");
    QPixmap pausePixmap(":/buttons/pauseImage.png");
    QPixmap recordPixmap(":/buttons/recordImage.png");

    QIcon recordButtonIcon(recordPixmap);
    QIcon pauseButtonIcon(pausePixmap);
    QIcon playButtonIcon(playPixmap);
    recordButton->setIcon(recordButtonIcon);
    recordButton->setRedDown(true);
    pauseButton->setIcon(pauseButtonIcon);
    playButton->setIcon(playButtonIcon);
    recordButton->setIconSize(QSize(15, 15));
    pauseButton->setIconSize(QSize(10, 10));
    playButton->setIconSize(QSize(15, 15));
    recordButton->setFixedSize(50, 20);
    pauseButton->setFixedSize(50, 20);
    playButton->setFixedSize(50, 20);
    recordButton->setToolTip(tr("Record"));
    pauseButton->setToolTip(tr("Pause"));
    playButton->setToolTip(tr("Play file"));
    recordButton->setEnabled(false);
    pauseButton->setEnabled(false);
    playButton->setEnabled(false);

    playbackInfoButton->setFixedSize(50, 20);
    playbackInfoButton->setText("i");
    QFont infoLabelFont;
    //infoLabelFont.setPixelSize(12);
    infoLabelFont.setItalic(true);
    infoLabelFont.setBold(true);
    infoLabelFont.setFamily("Times");
    //playbackInfoButton->setStyleSheet("QLabel { color : blue; }");
    playbackInfoButton->setFont(infoLabelFont);

    char myToolTip[] = "<html><head/><body><p>Display information about the recording file" \
                "</p></body></html>";

    playbackInfoButton->setToolTip(myToolTip);



    //recordButton->setCheckable(true);
    //pauseButton->setCheckable(true);
    //playButton->setCheckable(true);
    headerLayout->addWidget(recordButton, 0, 0);
    headerLayout->addWidget(pauseButton, 0, 1);
    headerLayout->addWidget(playButton, 0, 2);
    headerLayout->addWidget(playbackInfoButton, 0, 3);

    connect(recordButton, SIGNAL(pressed()), this, SLOT(recordButtonPressed()));
    connect(pauseButton, SIGNAL(pressed()), this, SLOT(pauseButtonPressed()));
    connect(playButton, SIGNAL(pressed()), this, SLOT(playButtonPressed()));
    connect(recordButton, SIGNAL(released()), this, SLOT(recordButtonReleased()));
    connect(pauseButton, SIGNAL(released()), this, SLOT(pauseButtonReleased()));
    connect(playButton, SIGNAL(released()), this, SLOT(playButtonReleased()));
    connect(playbackInfoButton, SIGNAL(released()), this, SLOT(aboutPlayback()));



    recordButton->setVisible(false);
    playButton->setVisible(false);
    pauseButton->setVisible(false);
    playbackInfoButton->setVisible(false);
    playbackInfoButton->setEnabled(false);

    totalTimeRecorded = 0;
    lastPlayPauseTime = 0;

    //--------------------------------------------------------------------
    playbackStartTime = 0;
    playbackEndTime = 0;
    playbackStartTimeLabel = new QLabel("--:--:--.---");
    playbackEndTimeLabel = new QLabel("--:--:--.---");
    playbackSlider = new QSlider(Qt::Horizontal);
    playbackLayout = new QGridLayout();

    labelFont.setPixelSize(15);

    playbackStartTimeLabel->setFont(labelFont);
    playbackEndTimeLabel->setFont(labelFont);
    playbackStartTimeLabel->setMinimumWidth(100);
    playbackEndTimeLabel->setMinimumWidth(100);

    playbackSlider->setMinimum(0);
    playbackSlider->setMaximum(1000);
    playbackSlider->setTracking(false);
    playbackSlider->setTickInterval(1);

    playbackLayout->setContentsMargins(QMargins(1,1,1,1));
    playbackLayout->setHorizontalSpacing(3);
    playbackLayout->addWidget(playbackStartTimeLabel, 0, 0);
    playbackLayout->addWidget(playbackSlider, 0, 1);
    playbackLayout->addWidget(playbackEndTimeLabel, 0, 2);
    playbackLayout->setColumnStretch(1, 1);

    playbackSlider->setEnabled(false);
    playbackStartTimeLabel->setEnabled(false);
    playbackEndTimeLabel->setEnabled(false);
    playbackSlider->setVisible(false);
    playbackStartTimeLabel->setVisible(false);
    playbackEndTimeLabel->setVisible(false);

    mainLayout->addLayout(playbackLayout, 2,0);

//    connect(pullTimer, SIGNAL(timeout()), this, SLOT(updateSlider()));
    connect(playbackSlider, SIGNAL(sliderPressed()), this, SLOT(sliderisPressed()));
    connect(playbackSlider, SIGNAL(sliderReleased()),this, SLOT(sliderIsReleased()));
    connect(playbackSlider, SIGNAL(actionTriggered(int)), this, SLOT(movingSlider(int)));
    connect(playbackSlider, SIGNAL(sliderMoved(int)), this, SLOT(updateTimeFromSlider(int)));
//    connect(playbackSlider, SIGNAL(valueChanged(int)), this, SLOT(jumpFileTo(int)));
//    connect()

    //--------------------------------------------------------------------

    //File name label

    QGridLayout* headerLayout2 = new QGridLayout(); //contains the buttons and clock at the top of the screen
    headerLayout2->setContentsMargins(QMargins(1,1,1,1));
    headerLayout2->setHorizontalSpacing(3);
    fileLabel = new QLabel;
    fileLabel->setMinimumWidth(200);
    QString FileLabelColor("gray");
    QString FileLabelText("No recording file open");
    fileLabel->setFont(dispFont);
    QString fileLabelTextTemplate = tr("<font color='%1'>%2</font>");
    fileString = fileLabelTextTemplate.arg(FileLabelColor, FileLabelText);
    fileLabel->setText(fileString);
    //fileLabel->setText(fileLabelTextTemplate.arg(FileLabelColor, FileLabelText));
    //headerLayout->addWidget(fileLabel,0,3);
    headerLayout2->addWidget(fileLabel, 0, 0);

    fileStatusColorIndicator = new QLabel;
    fileStatusColorIndicator->setStyleSheet("QLabel { background-color : yellow; color : black; border-radius: 5px}");
    //setStyleSheet("border:2px solid grey; border-radius: 5px;background-color: transparent;");
    //fileStatusColorIndicator->setFrameStyle(QFrame::Panel | QFrame::);
    //fileStatusColorIndicator->setLineWidth(2);
    fileStatusColorIndicator->setFont(dispFont);
    fileStatusColorIndicator->setAlignment(Qt::AlignCenter);
    fileStatusColorIndicator->setVisible(false);
    //QPalette palette;
    //palette.setColor( backgroundRole(), QColor( 0, 0, 255 ) );
    //palette.setColor( foregroundRole(), QColor( 0, 0, 255 ) );
    //palette.setColor(QPalette::Background,Qt::red);
    //fileStatusColorIndicator->setPalette( palette );
    //fileStatusColorIndicator->setAutoFillBackground( true );
    headerLayout2->addWidget(fileStatusColorIndicator, 0, 1);

    streamStatusColorIndicator = new QLabel;
    streamStatusColorIndicator->setStyleSheet("QLabel { background-color : white; color : black; border-radius: 5px}");
    streamStatusColorIndicator->setFixedWidth(200);
    streamStatusColorIndicator->setFont(dispFont);
    streamStatusColorIndicator->setText("STATUS: No source");
    streamStatusColorIndicator->setAlignment(Qt::AlignCenter);
//    streamStatusColorIndicator->setVisible(true);
    statusErrorBlinkTimer = new QTimer();
    connect(statusErrorBlinkTimer,SIGNAL(timeout()),this,SLOT(errorBlinkTmrFunc()));
    headerLayout2->addWidget(streamStatusColorIndicator, 0, 2);

    headerLayout->setColumnStretch(totalLeftItems, 1);
    headerLayout2->setColumnStretch(1, 1);
    mainLayout->addLayout(headerLayout, 0, 0);
    mainLayout->addLayout(headerLayout2,1,0);
    //---------------------------------------------

    //when the state of the source stream changes, the menus need to reflect that
    sourceControl = new SourceController(nullptr);
    connect(sourceControl, SIGNAL(stateChanged(int)), this, SLOT(setSourceMenuState(int)));
    connect(sourceControl, SIGNAL(stateChanged(int)), this, SLOT(sourceStateChanged(int)));
    connect(sourceControl,SIGNAL(setTimeStamps(uint32_t,uint32_t)), this, SLOT(setTimeStampLabels(uint32_t,uint32_t)));
    connect(sourceControl, SIGNAL(updateSlider(qreal)), this, SLOT(updateSlider(qreal)));
    connect(sourceControl,SIGNAL(packetSizeError(bool)),this,SLOT(sourcePacketSizeError(bool)));
    if (isAudioOn) {
        soundOut = new AudioController();
        if (!soundOut->setupOk()) {
            isAudioOn = false;
            QMessageBox msgBox;
            msgBox.setText("Either no audio device was found, or the Qt Multimedia plugin was not found. Audio functionality turned off.");
            msgBox.exec();
        }
    }
    if (isAudioOn) {
        soundOut->setChannel(-1); //set the audio channel to listen to

        connect(this, SIGNAL(setAudioChannel(int)), soundOut, SLOT(setChannel(int)));
        connect(this, SIGNAL(setAudioDevice(QString)), soundOut, SLOT(setDevice(QString)));
        connect(this, SIGNAL(updateAudio()), soundOut, SLOT(updateAudio()));
        connect(soundOut, SIGNAL(deviceChanged(QAudioSink*)),this,SLOT(audioDeviceChanged(QAudioSink*)));
        connect(this, SIGNAL(endAudioThread()),soundOut,SLOT(endAudio()));
        connect(sourceControl, SIGNAL(acquisitionStarted()), soundOut, SLOT(startAudio()));
        connect(sourceControl, SIGNAL(acquisitionStopped()), soundOut, SLOT(stopAudio()));
        connect(sourceControl, SIGNAL(acquisitionPaused()), soundOut, SLOT(stopAudio()));

        newSoundDialog = new soundDialog(0, 0);
        //Remembered settings...
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
        settings.beginGroup(QLatin1String("sound"));
        int a_thresh = settings.value(QLatin1String("thresh"),soundOut->getThresh()).toInt();
        int a_gain = settings.value(QLatin1String("gain"), soundOut->getGain()).toInt();
        QString a_device = settings.value(QLatin1String("device")).toString();

        soundOut->setThresh(a_thresh);
        soundOut->setGain(a_gain);
//        soundOut->setDevice(a_device);
        emit setAudioDevice(a_device);
        newSoundDialog->threshSlider->setValue(soundOut->getThresh());
        newSoundDialog->gainSlider->setValue(soundOut->getGain());
        settings.endGroup();

        QStringList devs = soundOut->getAvailableDevices();
        for (int i=0; i<devs.length(); i++) {
            newSoundDialog->deviceCombo->addItem(devs.at(i));
            if (soundOut->getCurrentDevice().compare(devs.at(i)) == 0) {
                newSoundDialog->deviceCombo->setCurrentIndex(i);
            }
        }
        newSoundDialog->setWindowFlags(Qt::Popup);
        newSoundDialog->setGeometry(QRect(this->geometry().x()+soundSettingsButton->x(),this->geometry().y()+soundSettingsButton->y()+soundSettingsButton->height()+this->menuBar()->height(),40,200));

        connect(newSoundDialog->gainSlider, &QSlider::valueChanged, soundOut, &AudioController::setGain);
        connect(newSoundDialog->threshSlider, &QSlider::valueChanged, soundOut, &AudioController::setThresh);
        connect(this, &MainWindow::closeAllWindows, newSoundDialog, &soundDialog::closeDialog);
        connect(this, &MainWindow::closeSoundDialog, newSoundDialog, &soundDialog::closeDialog);
        connect(newSoundDialog->deviceCombo, &QComboBox::currentTextChanged,this,&MainWindow::changeAudioDevice);
        //connect(newSoundDialog->deviceCombo, static_cast<void(QComboBox::*)(const QString &)>(&QComboBox::currentIndexChanged),this, &MainWindow::changeAudioDevice);
    } else {
        soundSettingsButton->setEnabled(false);
        actionSound->setEnabled(false);
    }
//    connect(playbackSlider, SIGNAL(sliderPressed()),sourceControl, SIGNAL(acquisitionPaused()));
//    connect(playbackSlider, SIGNAL(sliderReleased()), sourceControl, SIGNAL(acquisitionStarted()));
    connect(this, SIGNAL(jumpFileTo(qreal)), sourceControl, SIGNAL(jumpFileTo(qreal)));
    connect(sourceControl, SIGNAL(headstageSettingsReturned(HeadstageSettings)),this, SLOT(headstageSettingsChanged(HeadstageSettings)));
    connect(sourceControl, SIGNAL(controllerSettingsReturned(HardwareControllerSettings)),this, SLOT(controllerSettingsChanged(HardwareControllerSettings)));



    tabsBackground = new BackgroundFrame(tabs);
    tabsBackground->setVisible(true);
    tabsBackground->raise();



    //splash intro screen
    animateSplash = true;
    splashScreen = new TrodesSplashScreen(this);
    connect(splashScreen,SIGNAL(sig_createNewWorkspaceButtonPressed()),this,SLOT(loadFileInWorkspaceGui()));
    connect(splashScreen,SIGNAL(sig_loadTemplateWorkspace(QString)),this,SLOT(loadFileInWorkspaceGui(QString)));
    connect(splashScreen,SIGNAL(sig_loadWorkspaceButtonPressed()),this,SLOT(openWorkspaceFileForAcquisition()));
    connect(splashScreen,SIGNAL(sig_loadWorkspace(QString)),this,SLOT(openWorkspaceFileForAcquisition(QString)));
    connect(splashScreen,SIGNAL(sig_loadWorkspace(QString)),this,SLOT(saveWorkspacePathToSystem(QString)));
    connect(splashScreen,SIGNAL(sig_loadFileForPlaybackButtonPressed()),this,SLOT(openPlaybackFile()));
    connect(splashScreen,SIGNAL(sig_initializeFilePlayback(QString)),this,SLOT(openPlaybackFile(QString)));
    connect(splashScreen,SIGNAL(sig_initializeFilePlayback(QString)),this,SLOT(savePlaybackFilePathToSystem(QString)));
    connect(splashScreen,SIGNAL(sig_fadeAnimationCompleted()),this,SLOT(setBackgroundFrameVisibility()));
    //connect(splashScreen, &TrodesSplashScreen::quickstartEthernet, this, &MainWindow::quickstartEthernet);
    //connect(splashScreen, &TrodesSplashScreen::quickstartUSB, this, &MainWindow::quickstartUSB);
    connect(splashScreen, &TrodesSplashScreen::launchUpdater, this, &MainWindow::launchUpdater);
    connect(this, &MainWindow::gotversion, splashScreen, &TrodesSplashScreen::versionChecker);
    splashScreen->initializeAnimations();
    splashScreen->setVisible(true);
    splashScreen->raise();
    isSplashScreenVisible = true;


    workspaceEditor = new WorkspaceEditorDialog(this);

    connect(workspaceEditor,SIGNAL(sig_openTempWorkspace(QString,QString)),this,SLOT(openTempWorkspace(QString,QString)));
    //connect(workspaceEditor, SIGNAL(sig_openWorkspace(const TrodesConfiguration &)),this,SLOT(openWorkspaceFileForAcquisition(const TrodesConfiguration &)));

    //connect(workspaceEditor,&WorkspaceEditorDialog::openWorkspace,this,&MainWindow::reConfig_live);

    //connect(this, SIGNAL(closeAllWindows()), workspaceEditor, SLOT(deleteLater()));


    QWidget *window = new QWidget();
    window->setLayout(mainLayout);


    setCentralWidget(window);
    QMetaObject::connectSlotsByName(this);
    retranslateUi();


    //check if multiple instances of trodes are open
    checkMultipleInstances("Warning: An active Trodes server host was detected.  You will not be able to open a workspace file until closing all other instances of Trodes.");


    //create the benchmarking control panel
    benchmarkingControlPanel = new BenchmarkWidget();

    //Create preferences panel
    preferencesPanel = new PreferencesPanel();
    connect(menuPreferences, &QAction::triggered, preferencesPanel, &PreferencesPanel::show);
    connect(menuPreferences, &QAction::triggered, preferencesPanel, &PreferencesPanel::raise);

    networkPanel = new NetworkPanel({});
    connect(menuNetworkPanel, &QAction::triggered, networkPanel, &NetworkPanel::show);
    connect(menuNetworkPanel, &QAction::triggered, networkPanel, &NetworkPanel::raise);

    checkForUpdate();

    //Remembered settings...
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    //Place the window where it was the last session
    settings.beginGroup(QLatin1String("position"));
    //QRect tempPosition = settings.value(QLatin1String("position")).toRect();

    QByteArray geo = settings.value(QLatin1String("geometry")).toByteArray();
    if (!geo.isEmpty()) {
        restoreGeometry(geo);
    }

    /*if (tempPosition.height() > 0) {
        setGeometry(tempPosition);
        resize(tempPosition.width(),tempPosition.height());

    }*/
    settings.endGroup();


}

MainWindow::~MainWindow()
{
    if (triggerSettings != nullptr)
        delete triggerSettings;

    quitModules();
}

void MainWindow::retranslateUi()
{
    setWindowTitle(QApplication::translate("Main", "Trodes"));
    //menuFile->setTitle(QApplication::translate("Main", "File", 0));
    menuConfig->setTitle(QApplication::translate("Main", "Workspace", 0));
    actionLoadConfig->setText(QApplication::translate("Main", "Open...", 0));
    actionCloseConfig->setText(QApplication::translate("Main", "Close", 0));
    actionSaveConfig->setText(QApplication::translate("Main", "Save workspace as...", 0));
    actionReConfig->setText(QApplication::translate("Main", "Reconfigure...", 0));
    actionOpenRecordDialog->setText(QApplication::translate("Main", "New recording...", 0));
    actionPlaybackOpen->setText(QApplication::translate("Main", "Playback last recording", 0));
    actionCloseFile->setText(QApplication::translate("Main", "Close file", 0));
    menuExport->setText(QApplication::translate("Main", "Extract...", 0));
    actionSaveClusters->setText(QApplication::translate("Main", "Save clusters", 0));
    actionLoadClusters->setText(QApplication::translate("Main", "Load clusters", 0));
    actionRecord->setText(QApplication::translate("Main", "Record", 0));
    actionPause->setText(QApplication::translate("Main", "Pause", 0));
    actionPlay->setText(QApplication::translate("Main", "Play file", 0));
    actionMerge->setText(QApplication::translate("Main", "Merge with logger data", 0));
    menuSimulationSource->setTitle(QApplication::translate("Main", "Simulation", 0));
    menuSpikeGadgetsSource->setTitle(QApplication::translate("Main", "SpikeGadgets", 0));
    actionSourceNone->setText(QApplication::translate("Main", "None", 0));
    actionSourceUSB->setText(QApplication::translate("Main", "USB", 0));
    actionSourceUSB3->setText(QApplication::translate("Main", "USB3", 0));
    actionSourceDockUSB->setText(QApplication::translate("Main", "Dock (USB)"));
    actionSourceRhythm->setText(QApplication::translate("Main", "Rhythm", 0));
    actionSourceFake->setText(QApplication::translate("Main", "Signal generator", 0));
    actionSourceFakeSpikes->setText(QApplication::translate("Main", "Signal generator (w/spikes)", 0));
    actionSourceFile->setText(QApplication::translate("Main", "&File", 0));
    actionSourceEthernet->setText(QApplication::translate("Main", "Ethernet", 0));
    //actionReconfigure->setText(QApplication::translate("Main", "Reconfigure...", 0));
    actionConnect->setText(QApplication::translate("Main", "Stream from source"));
    actionDisconnect->setText(QApplication::translate("Main", "Disconnect"));
    actionClearBuffers->setText(QApplication::translate("Main", "Clear buffers"));
    actionSendSettle->setText(QApplication::translate("Main", "Settle amplifiers"));

    actionOpenGeneratorDialog->setText(QApplication::translate("Main", "Generator controls..."));
    actionSound->setText(QApplication::translate("Main", "Audio window"));
    actionRestartModules->setText(QApplication::translate("Main", "Restart modules"));

    actionQuit->setText(QApplication::translate("Main", "Exit"));

    actionAboutTrodes->setText(QApplication::translate("Main", "About Trodes", 0));
    actionAboutHardware->setText(QApplication::translate("Main", "About Hardware", 0));
    actionAboutQT->setText(QApplication::translate("Main", "About QT", 0));

    //menuSystem->setTitle(QApplication::translate("Main", "Connection", 0));
    sourceMenu->setTitle(QApplication::translate("Main", "Source"));
    sourceOptionsMenu->setTitle(QApplication::translate("Main", "Specific source control"));
    menuHelp->setTitle(QApplication::translate("Main", "Help"));
    //menuDisplay->setTitle(QApplication::translate("Main", "Display", 0));
    menuEEGDisplay->setTitle(QApplication::translate("Main", "Streaming"));
    menuSetTLength->setTitle(QApplication::translate("Main", "Trace length"));
    actionSetTLength0_2->setText(QApplication::translate("Main", "0.2 Seconds"));
    actionSetTLength0_5->setText(QApplication::translate("Main", "0.5 Seconds"));
    actionSetTLength1_0->setText(QApplication::translate("Main", "1.0 Seconds"));
    actionSetTLength2_0->setText(QApplication::translate("Main", "2.0 Seconds"));
    actionSetTLength5_0->setText(QApplication::translate("Main", "5.0 Seconds"));
    streamFilterMenu->setTitle(QApplication::translate("Main", "Data band"));
    streamLFPFiltersOn->setText(QApplication::translate("Main", "LFP filters"));
    streamSpikeFiltersOn->setText(QApplication::translate("Main", "Spike filters"));
    streamStimViewOn->setText(QApplication::translate("Main", "Stimulation output view"));
    streamNoFiltersOn->setText(QApplication::translate("Main", "No filters (raw)"));


    menuSettings->setTitle(QApplication::translate("Main", "Settings"));
    menuNTrode->setTitle(QApplication::translate("Main", "nTrodes"));
    actionShowCurrentTrode->setText(QApplication::translate("Main", "nTrode trigger window"));
    actionRealtimeDisplay->setText(QApplication::translate("Main", "Real-time processing mode"));
//    menuLinkChanges->setTitle(QApplication::translate("Main", "Link changes", 0));
//    actionLinkChanges->setText(QApplication::translate("Main", "Link changes across nTrodes", 0));
//    actionUnLinkChanges->setText(QApplication::translate("Main", "Change settings independently", 0));
    actionUncoupleDisplay->setText(QApplication::translate("Main", "Freeze display"));

    clearAllNTrodes->setText(QApplication::translate("Main", "Clear all scatterplots", nullptr));
    menuHeadstage->setTitle(QApplication::translate("Main", "Headstage", nullptr));
    actionHeadstageSettings->setText(QApplication::translate("Main", "Headstage settings...", nullptr));
    actionControllerSettings->setText(QApplication::translate("Main", "MCU settings...", nullptr));
    actionSettleDelay->setText(QApplication::translate("Main", "Settle delay...", nullptr));


}

void MainWindow::fillSourceOptionsMenu()
{
    if (channelsConfigured) {
        QString deviceType = activeWorkspacePointers.spikeConf->deviceType;

        if (deviceType.contains("NeuroPixels1",Qt::CaseInsensitive)) {
            QAction *tmpAction = new QAction("NeuroPixels1");
            connect(tmpAction, &QAction::triggered, this, &MainWindow::openCustomSourceOptionsGUI);
            sourceOptionsMenu->addAction(tmpAction);
        }
    }


}

void MainWindow::clearSourceOptionsMenu()
{
    sourceOptionsMenu->clear();
}

void MainWindow::enableSourceMenu(QString menuName)
{
    for (int i=0; i<sourceOptionsMenu->actions().length();i++) {
        if (sourceOptionsMenu->actions().at(i)->text() == menuName) {
            sourceOptionsMenu->actions()[i]->setEnabled(true);
            break;
        }
    }
}

//These two functions are used to display lots of debug statements for some functions if turned on
void MainWindow::setDebugCheckpoint(bool on, QString name)
{
    debugCheckpointsOn = on;
    debugCheckpointStatement = name;
    debugCheckpointNumber = 0;
}
void MainWindow::printDebugCheckpoint(QString customText) {
    if (debugCheckpointsOn) {
        qDebug() << "Checkpoint for"<<debugCheckpointStatement<<"number:" << debugCheckpointNumber << "Custom text:" << customText;
        debugCheckpointNumber++;
    }
}

void MainWindow::openCustomSourceOptionsGUI()
{
    /*QObject* obj = sender();
    QAction* action = qobject_cast<QAction*>(sender());
    if(action != nullptr) {

        qDebug() << "Open custom source GUI for" << action->text();
        if (action->text() == "NeuroPixels1") {

            ProbeLayoutWidget *newProbeLayoutWidget = new ProbeLayoutWidget(acquisitionWorkspace);

            newProbeLayoutWidget->setMinimumHeight(500);
            newProbeLayoutWidget->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);

            newProbeLayoutWidget->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed

            connect(this, SIGNAL(closeAllWindows()), newProbeLayoutWidget, SLOT(close()));
            connect(newProbeLayoutWidget, &ProbeLayoutWidget::newWorkspace,this,&MainWindow::reConfig_live_reopen);
            connect(newProbeLayoutWidget, &ProbeLayoutWidget::closeWorkspace,this,&MainWindow::reConfig_live_close);
            connect(newProbeLayoutWidget, &ProbeLayoutWidget::newNeuroPixelsSettings, sourceControl, &SourceController::setNeuroPixelsSettings);           
            connect(newProbeLayoutWidget, &ProbeLayoutWidget::closing, this, &MainWindow::enableSourceMenu);
            connect(newProbeLayoutWidget, &ProbeLayoutWidget::requestStopAcquire, sourceControl, &SourceController::disconnectFromSource);

            connect(eegDisp, &StreamDisplayManager::streamChannelClicked,newProbeLayoutWidget,&ProbeLayoutWidget::setFocusChannel);
            connect(spikeDisp, &MultiNtrodeDisplayWidget::channelClicked, newProbeLayoutWidget,&ProbeLayoutWidget::setFocusChannel);

            action->setEnabled(false);
            newProbeLayoutWidget->show();
            newProbeLayoutWidget->applySettings();
        }

    }*/




        if (acquisitionWorkspace.spikeConf.deviceType == "neuropixels1") {
            qDebug() << "Open custom source GUI for neuropixels";
            ProbeLayoutWidget *newProbeLayoutWidget = new ProbeLayoutWidget(acquisitionWorkspace);

            newProbeLayoutWidget->setMinimumHeight(500);
            newProbeLayoutWidget->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum);
            newProbeLayoutWidget->setWindowFlags(Qt::WindowStaysOnTopHint);

            newProbeLayoutWidget->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed

            connect(this, SIGNAL(closeAllWindows()), newProbeLayoutWidget, SLOT(close()));
            connect(newProbeLayoutWidget, &ProbeLayoutWidget::newWorkspace,this,&MainWindow::reConfig_live_reopen);
            connect(newProbeLayoutWidget, &ProbeLayoutWidget::closeWorkspace,this,&MainWindow::reConfig_live_close);
            connect(newProbeLayoutWidget, &ProbeLayoutWidget::newNeuroPixelsSettings, sourceControl, &SourceController::setNeuroPixelsSettings);
            connect(newProbeLayoutWidget, &ProbeLayoutWidget::closing, this, &MainWindow::enableSourceMenu);
            connect(newProbeLayoutWidget, &ProbeLayoutWidget::requestStopAcquire, sourceControl, &SourceController::disconnectFromSource);

            connect(eegDisp, &StreamDisplayManager::streamChannelClicked,newProbeLayoutWidget,&ProbeLayoutWidget::setFocusChannel);
            connect(spikeDisp, &MultiNtrodeDisplayWidget::channelClicked, newProbeLayoutWidget,&ProbeLayoutWidget::setFocusChannel);


            newProbeLayoutWidget->show();
            newProbeLayoutWidget->raise();
            if (tempWorkspaceLoading) {
                newProbeLayoutWidget->applySettings();
            }
        }

        QObject* obj = sender();
        QAction* action = qobject_cast<QAction*>(sender());
        if(action != nullptr) {
            action->setEnabled(false);
        }


}

void MainWindow::parseReceivedVersion(QString version, bool maintenancetool){
    QString versioncpy(version);
    //If version is empty, something went wrong with getting the info. No warning will be made to the user
    if(version.replace(".", "").toInt() > TRODES_VERSION && !version.isEmpty()){
        isLatestVersion = false;
    }
    else{
        isLatestVersion = true;
    }
    //emit gotversion(isLatestVersion, versioncpy, maintenancetool);
}

void MainWindow::checkForUpdate(){
    //To run when Installer is released:
#ifdef _WIN32
    const QString MTOOLNAME = QApplication::applicationDirPath() + "/maintenancetool.exe";
#else
    const QString MTOOLNAME = QApplication::applicationDirPath() + "/maintenancetool";
#endif
    QFileInfo mtool(MTOOLNAME);
//    QString version;
    if(mtool.exists() && mtool.isFile() && mtool.isExecutable()){
        QProcess *mtoolproc = new QProcess;
        mtoolproc->setProcessChannelMode(QProcess::MergedChannels);
        connect(mtoolproc, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
              [=](int exitCode, QProcess::ExitStatus exitStatus){
            if(exitCode == 0 && exitStatus == QProcess::NormalExit){
                QString output = mtoolproc->readAllStandardOutput();
                QDomDocument updates;
                if(updates.setContent(output)){
                    auto nodes = updates.elementsByTagName("update");
                    for(int i = 0; i < nodes.length(); ++i){
                        if(nodes.at(i).toElement().attribute("name") == "Trodes"){
                            QString version = nodes.at(i).toElement().attribute("version");
                            this->parseReceivedVersion(version, true);
                            break;
                        }
                    }
                }
            }
        });
        mtoolproc->start(MTOOLNAME, {"--checkupdates"});
    }
    else{
        //No maintenance tool. Check for latest version. If no internet access or network reply, dont continue.
        //https://forum.qt.io/topic/50200/solved-qnetworkaccessmanager-crash-related-to-ssl/7
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

        settings.beginGroup(QLatin1String("MainWindow"));
        int updatechecksuccess = settings.value(QLatin1String("updatecheck"), -1).toInt();
        if(updatechecksuccess == 0){
            //Trodes didn't check successfully last time, skip.
            qDebug() << "Trodes could not check for an update!";
            return;
        }

        //Set update check success to be false first, so that if trodes crashes then it knows next time to skip
        settings.setValue(QLatin1String("updatecheck"), 0);

        //Check for update
        /*QLoggingCategory::setFilterRules("qt.network.ssl.warning=false");
        QNetworkAccessManager *manager = new QNetworkAccessManager;
        if(manager->networkAccessible()==1){
            QNetworkRequest request = QNetworkRequest(QUrl("http://www.spikegadgets.com/software/latest_trodes_version.xml"));
            request.setRawHeader( "User-Agent" , "Mozilla Firefox" );
            QNetworkReply *response = manager->get(request);
            connect(response, &QNetworkReply::readyRead, [=](){
                if(manager->networkAccessible()==QNetworkAccessManager::Accessible && response->error() == QNetworkReply::NoError){
                    QString str = QString(response->readAll());
                    QDomDocument xml;
                    xml.setContent(str);
                    QString version = xml.elementsByTagName("VersionInfo").at(0).toElement().attribute("version");
                    this->parseReceivedVersion(version, false);
                    response->deleteLater();
                    manager->deleteLater();
                }
            });
        }*/
        //Set value back to 1
        settings.setValue(QLatin1String("updatecheck"), 1);
    }
}

void MainWindow::launchUpdater(){
    //To run when Installer is released:
#ifdef _WIN32
    const QString MTOOLNAME = QApplication::applicationDirPath() + "/maintenancetool.exe";
#else
    const QString MTOOLNAME = QApplication::applicationDirPath() + "/maintenancetool";
#endif
    QFileInfo mtool(MTOOLNAME);
    if(mtool.exists() && mtool.isFile() && mtool.isExecutable()){
        QProcess *mtoolproc = new QProcess;
        mtoolproc->startDetached(MTOOLNAME, {"--updater"});
    }
    else{
        QMessageBox::warning(this, "No updater found", "No updater found! Please go to <a href=\"https://bitbucket.org/mkarlsso/trodes/downloads/\">https://bitbucket.org/mkarlsso/trodes/downloads/</a> for the official installer or a zipped software package.");
    }
}

void MainWindow::startThreads()
{
    //Threads are started after the main window is created and showing

    //Audio thread
    if (isAudioOn) {
        QThread* audioThread = new QThread();
        audioThread->setObjectName("AudioThread");

        connect(audioThread, SIGNAL(started()), soundOut, SLOT(startAudio()));
        connect(soundOut, SIGNAL(finished()), audioThread, SLOT(quit()));
        connect(soundOut, SIGNAL(finished()), soundOut, SLOT(deleteLater()));
        connect(audioThread, SIGNAL(finished()), audioThread, SLOT(deleteLater()));
        soundOut->moveToThread(audioThread);
        audioThread->start();
    }
}

void MainWindow::settingsWarning(){
    if(configSettingsChanged){
        QMessageBox *questionbox = new QMessageBox(QMessageBox::Question, "Settings were changed",
                                                   "You have unsaved changes to your nTrode settings. Would you like to save them in a Trodes config file (.trodesconf)? ",
                                                   QMessageBox::Yes|QMessageBox::No);
        questionbox->setWindowFlag(Qt::WindowStaysOnTopHint);
        auto answer = questionbox->exec();
        //messageBox.setFixedSize(500,200);
        if (answer == QMessageBox::Yes) {
            saveConfig();
        }
        else {
            //"No" or messagebox closed
            //Do nothing extra
        }
        configSettingsChanged = false;
    }
}

void MainWindow::saveConfig()
{
    QString fname = "";

    if(recordFileOpen){
        fname = recordFileName;
    }
    else if(playbackFileOpen){
        fname = playbackFile;
    }
    else{
        fname = currentConfigFileName;
    }


    /*if(configSettingsChanged){
        if(recordFileOpen){
            fname = recordFileName;
        }
        else if(playbackFileOpen){
            fname = playbackFile;
        }
        else{
            fname = currentConfigFileName;
        }
    }
    else{
        fname = currentConfigFileName;
    }*/;
    saveConfig(fname);
}

void MainWindow::saveConfig(QString fname){
    //qDebug() << QDir::currentPath();
    //qDebug() << QCoreApplication::applicationDirPath();
    //qDebug() << currentConfigFileName;

    QFileInfo fI = QFileInfo(fname);
    QString configPath = fI.absolutePath();
    QString configDefaultName = fI.completeBaseName()+".trodesconf";


    QStringList filenames;
    QString filename;
    QFileDialog dialog(this, "Save configuration as");


    //dialog.selectFile(QString("test.xml"));
    dialog.setDirectory(configPath);
    dialog.setDefaultSuffix("trodesconf");
    dialog.selectFile(configDefaultName);
    //dialog.selectFile(configDefaultName);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setOption(QFileDialog::DontConfirmOverwrite, false);
    //dialog.setOption(QFileDialog::DontUseNativeDialog);
    if (dialog.exec()) {
        filenames = dialog.selectedFiles();
    }
    if (filenames.size() == 1) {
        filename = filenames.first();
    }
    if (!filename.isEmpty()) {

        bool success;
        if (playbackFileOpen) {
            if (usingExternalWorkspace) {
                success = playbackExternalWorkspace.writeTrodesConfig(filename);
            } else {
                success = playbackEmbeddedWorkspace.writeTrodesConfig(filename);
            }
        } else {
            success = acquisitionWorkspace.writeTrodesConfig(filename);

            loadedConfigFile = filename;
            currentConfigFileName = filename;

        }

        if (!success) {
            QMessageBox messageBox;
            messageBox.critical(nullptr, "Error", "Unable to save file.");
            messageBox.setFixedSize(500, 200);
        } else {
            configSettingsChanged = false;
        }

        /*if (!activeWorkspace->writeTrodesConfig(filename)) {
            QMessageBox messageBox;
            messageBox.critical(nullptr, "Error", "Unable to save file.");
            messageBox.setFixedSize(500, 200);
        }
        else {
            configSettingsChanged = false;
        }*/
    }
}

void MainWindow::closeWorkspace() {
    //this function is a wrapper for closeConfig(), used to allow us to customize certain behavior
    //In this case, we want the closeWorkspace command to close a playbackFile and the workspace config if a .rec file is open in Trodes
    if (playbackFileOpen) {
        closeFile();
        return;
    }
    closeConfig();
}

void MainWindow::closeConfig(bool openSplashScreen)
{

    //setDebugCheckpoint(true,"close workspace"); //turn on debug checkpoints (comment out if not debuging a crash or problem)
    printDebugCheckpoint("start");

    if (channelsConfigured) {//only save this setting if a config had been open when closeConfig is called
        saveTrodeSettingPanelVisible(trodeSettingsButton->isChecked());
        saveSpikesPanelVisible(spikesButton->isChecked());
    }

    dataWasStreamingBeforeLiveReconfig = false;
    liveReconfigHappening = false;
    trodeSettingsButton->setChecked(false);
    spikesButton->setChecked(false);
    //If ntrode Settings have been changed since opening the file, ask if they want to save the trodes config
    settingsWarning();
    if (openSplashScreen) {
        swapSplashScreenAndTabView(true);
    }

    workspaceEditor->clearGUI();
    printDebugCheckpoint("workspace editor cleared");

    if (channelsConfigured) {

        quitModules();
        printDebugCheckpoint("modules quit");

        disconnectFromSource();
        printDebugCheckpoint("source disconnected");


        if (isAudioOn)
            soundOut->setChannel(-1); //set the audio channel to -1 (off)

        //this if statement prevents an infinite loop from occuring if switching from playback source
        if (!playbackFileOpen) {
            setSource(SourceNone);
        }
        printDebugCheckpoint("source set to none");


        //delete the trodesNet->tcpServer if it was started
        emit endAllThreads();
        //recordOut->deleteLater(); //We need to add the proper thread shutdown to the record thread (currently
                                    //thread quitting in endAllThreads()
        printDebugCheckpoint("server and record threads ended");

        if (triggerSettings != nullptr) {
            delete triggerSettings;
            triggerSettings = nullptr;
        }
        printDebugCheckpoint("trigger settings deleted");

        //remove each display tab
        while (tabs->count() > 0) {
            tabs->removeTab(0);
        }


        delete eegDisp;

        printDebugCheckpoint("Stream display manager deleted");

//        if (sdDisp != nullptr) {
//            delete sdDisp;
//            sdDisp = nullptr;
//        }

        //Mark: to do - this is a direct call to end separate threads, causing race conditions
        streamManager->removeAllProcessors();
        printDebugCheckpoint("Processors removed");

        delete streamManager;
        delete spikeDisp;
        spikeDisp = nullptr;

        printDebugCheckpoint("stream mananger and NtrodeDisplayWidget deleted");

        //this should always come last b/c of the zsys_shutdown()
        if (activeWorkspacePointers.networkConf->networkType == NetworkConfiguration::zmq_based) {
            endTrodesNetwork();//Deletes zmq server stuff
        }


        playbackEmbeddedWorkspace.clear();
        playbackExternalWorkspace.clear();
        acquisitionWorkspace.clear();


        activeWorkspacePointers.networkConf=nullptr;
        activeWorkspacePointers.moduleConf=nullptr;
        activeWorkspacePointers.streamConf=nullptr;
        activeWorkspacePointers.spikeConf=nullptr;
        activeWorkspacePointers.headerConf=nullptr;
        activeWorkspacePointers.hardwareConf=nullptr;
        activeWorkspacePointers.globalConf=nullptr;

        networkConf=nullptr;
        moduleConf=nullptr;
        streamConf=nullptr;
        spikeConf=nullptr;
        headerConf=nullptr;
        //hardwareConf=NULL;
        //globalConf=NULL;

        //delete hardwareConf;
        printDebugCheckpoint("workspace cleared");




        //sourceControl->clearBuffers();

        channelsConfigured = false;

        actionCloseConfig->setEnabled(false);
        actionSaveConfig->setEnabled(false);
        actionSaveClusters->setEnabled(false);
        actionLoadClusters->setEnabled(false);
        actionReConfig->setEnabled(false);
        actionLoadConfig->setEnabled(true);
        actionAboutConfig->setEnabled(false);
        actionHeadstageSettings->setEnabled(false);
        actionControllerSettings->setEnabled(false);
        actionConnect->setEnabled(false);
        actionDisconnect->setEnabled(false);
        actionSendSettle->setEnabled(false);
        actionOpenRecordDialog->setEnabled(false);
        actionShowCurrentTrode->setEnabled(false);
        currentConfigFileName = "";

        commentButton->setEnabled(false);

        playbackInfoButton->setEnabled(false);
        playbackInfoButton->setVisible(false);
        clearSourceOptionsMenu();

        closeAllWindows();
    }

}

void MainWindow::reConfig_live_close()
{
    //setDebugCheckpoint(true,"close workspace live"); //turn on debug checkpoints (comment out if not debuging a crash or problem)

    printDebugCheckpoint("start");
    if (playbackFileOpen) return; //live reconfig is not available during playback
    if (recordFileOpen) return; //live reconfig is not available when a recording file is open

    oldConfigFile = loadedConfigFile;
    liveReconfigHappening = true;
    if (dataStreaming) {
        dataWasStreamingBeforeLiveReconfig = true;
    } else {
        dataWasStreamingBeforeLiveReconfig = false;
    }


    if (channelsConfigured) {//only save this setting if a config had been open when closeConfig is called
        saveTrodeSettingPanelVisible(trodeSettingsButton->isChecked());
        saveSpikesPanelVisible(spikesButton->isChecked());
    }

    trodeSettingsButton->setChecked(false);
    spikesButton->setChecked(false);


    if (channelsConfigured) {

        disconnect(tabs, &QTabWidget::currentChanged, this, &MainWindow::streamTabChanged);

        quitModules(); //TODO: We may want to reopen whatever modules were open.
        printDebugCheckpoint("modules quit");

        disconnectFromSource(); //TODO: ideally we do not fully disconnect, but rather pause processing of the input.
        printDebugCheckpoint("source disconnected");

        if (isAudioOn)
            soundOut->setChannel(-1); //set the audio channel to -1 (off)


        //We want to stay connected to the source
        //setSource(SourceNone);



        //delete the trodesNet->tcpServer if it was started

        emit endAllThreads(); //this will end the network server thread and the record thread
        printDebugCheckpoint("Network and record threads ended");

        if (triggerSettings != nullptr) {
            delete triggerSettings;
            triggerSettings = nullptr;
        }
        printDebugCheckpoint("trigger settings deleted");

        /*
        //remove each display tab
        while (tabs->count() > 0) {
            tabs->removeTab(0);
        }

        delete eegDisp;*/

        eegDisp->stopAllUpdates(); //The updating depends on the workspace, so we need to stop it
        printDebugCheckpoint("EEG updates stopped");

        //Mark: to do - this is a direct call to end separate threads, causing race conditions
        streamManager->stopAcquisition();
        //streamManager->removeAllProcessors();
        printDebugCheckpoint("Stream manager processors stopped");

        /*delete streamManager;
        delete spikeDisp;
        spikeDisp = nullptr;
        printDebugCheckpoint("StreamManager and spikeDisp deleted");*/

        //this should always come last b/c of the zsys_shutdown()
        if (activeWorkspacePointers.networkConf->networkType == NetworkConfiguration::zmq_based) {
            endTrodesNetwork();//Deletes zmq server stuff
        }

    }
    return;

}

void MainWindow::reConfig_live_reopen(const TrodesConfiguration &newConfig) {
    TrodesConfiguration confCopy;
    confCopy = newConfig;
    loadWorkspaceToDisplayWhenReady(confCopy);
}

void MainWindow::reConfig()
{
    //reConfig_live(acquisitionWorkspace);



    //workspaceEditor->loadFileIntoWorkspaceGui(currentConfigFileName);
    workspaceEditor->loadFileIntoWorkspaceGui(acquisitionWorkspace);
    if (!currentConfigFileName.isEmpty()) {
        workspaceEditor->setCurrentFileName(currentConfigFileName);
    }
    int ret = workspaceEditor->openReconfigEditor();
    if(ret == QDialog::Accepted){
        //changes made to current workspace
//        configSettingsChanged = true;
    }
//    int returnVal = -1;
//    //Save the current workspace to tempWorkspace
//    qDebug() << "=================" << currentConfigFileName << loadedConfigFile;
//    QString tempFilePath = QString("%1/tempWorkspace.trodesconf").arg(QCoreApplication::applicationDirPath());
//    if (writeTrodesConfig(tempFilePath)) { //save current settings to the tempFile
//        configSettingsChanged = false; // the save was successful, so no unsaved changes
//        if (currentConfigFileName != "") {
//            closeConfig(false);
//        }
//        workspaceEditor->loadFileIntoWorkspaceGui(tempFilePath);
//        returnVal = workspaceEditor->openReconfigEditor();
//    }
//    else {
//        qDebug() << "Error: saving tempFile failed. (MainWindow::reConfig)";
//    }

//    if (returnVal != QDialog::Accepted) { //if the user pressed cancel, open the old workspace
//        loadConfig(tempFilePath);
//    }


}

void MainWindow::loadWorkspaceToDisplayWhenReady(const TrodesConfiguration &tc)
{
    stagedWorkspaceForLoad = tc;
    workspaceShutdownCheckTimer = new QTimer(this);
    connect(workspaceShutdownCheckTimer, &QTimer::timeout, this, &MainWindow::checkReadyForWorkspaceLoad);
    workspaceShutdownCheckTimer->start(1000); //We need to allow enough time for the source controller and streamProcessor threads to end
    printDebugCheckpoint("Timer started");
}

void MainWindow::checkReadyForWorkspaceLoad()
{
    printDebugCheckpoint("Timer period elapsed");
    workspaceShutdownCheckTimer->stop();

    //TODO-- we need to check to make sure the source controller and streamProcessor threads have actually ended.
    //If they have not, we will get a crash as soon as we mess with the workspace
    /*if (sourceControl->currentSourceObj != nullptr) {
        if (sourceControl->currentSourceObj->isThreadRunning()) {
            workspaceShutdownCheckTimer->start(500);
            printDebugCheckpoint("Source not finished. Restaring timer.");
            return;
        }
    }*/

    workspaceShutdownCheckTimer->deleteLater();
    printDebugCheckpoint("Delete timer");

    streamManager->removeAllProcessors();

    delete streamManager;
    delete spikeDisp;
    spikeDisp = nullptr;
    printDebugCheckpoint("StreamManager and spikeDisp deleted");


    //playbackEmbeddedWorkspace.clear();
    //playbackExternalWorkspace.clear();
    acquisitionWorkspace.clear();
    printDebugCheckpoint("Workspace cleared");


    configSettingsChanged = true; //this will prompt user to save the new workspace when they close
    acquisitionWorkspace = stagedWorkspaceForLoad; //copy settings
    loadedConfigFile = oldConfigFile;
    currentConfigFileName = loadedConfigFile;


    //reset all of the pointers
    activeWorkspacePointers.globalConf = &acquisitionWorkspace.globalConf;
    activeWorkspacePointers.streamConf = &acquisitionWorkspace.streamConf;
    activeWorkspacePointers.headerConf = &acquisitionWorkspace.headerConf;
    activeWorkspacePointers.hardwareConf = &acquisitionWorkspace.hardwareConf;
    activeWorkspacePointers.moduleConf = &acquisitionWorkspace.moduleConf;
    activeWorkspacePointers.spikeConf = &acquisitionWorkspace.spikeConf;
    activeWorkspacePointers.networkConf = &acquisitionWorkspace.networkConf;
    activeWorkspacePointers.benchConfig = &acquisitionWorkspace.benchConfig;
    //activeWorkspace = &acquisitionWorkspace;

    globalConf = &acquisitionWorkspace.globalConf;
    streamConf = &acquisitionWorkspace.streamConf;
    headerConf = &acquisitionWorkspace.headerConf;
    hardwareConf = &acquisitionWorkspace.hardwareConf;
    moduleConf = &acquisitionWorkspace.moduleConf;
    spikeConf = &acquisitionWorkspace.spikeConf;
    networkConf = &acquisitionWorkspace.networkConf;
    benchConfig = &acquisitionWorkspace.benchConfig;

    printDebugCheckpoint("Workspace transferred");

    channelsConfigured = false;  //check if we need this



    //We remove the previous stream display now to prevent a long time with an empty screen
    //remove each display tab
    while (tabs->count() > 0) {
        tabs->removeTab(0);
    }
    if (eegDisp != nullptr) delete eegDisp;
    printDebugCheckpoint("Deleted eegDisp");



    loadWorkspaceToDisplay();
    if (tabs->count()>lastTabIndex) {
        tabs->setCurrentIndex(lastTabIndex);
    }

    connect(tabs, &QTabWidget::currentChanged, this, &MainWindow::streamTabChanged);
    printDebugCheckpoint("New workspace load complete");
}


bool MainWindow::loadWorkspaceToDisplay()
{

    //setDebugCheckpoint(true,"load workspace"); //turn on debug checkpoints (comment out if not debuging a crash or problem)
    printDebugCheckpoint("start");
    //closeConfig(false); //automatically close configuration files if they are open, also don't bring up splashScreen

    //reset the selected ntrode to the first in the list
    if (activeWorkspacePointers.spikeConf->ntrodes.length() > 0) {
        int clickedID = activeWorkspacePointers.spikeConf->ntrodes[0]->nTrodeId;
        selectedNTrodes.clear();
        selectedNTrodes.insert(0, clickedID);
        currentTrodeSelected = 0;
    }

    // This is used to load a configuration and create all the control/display widgets
    // according to the settings in the config file
    QString multInstError = "Error: An active Trodes server host was detected. Please close any other currently running instances of Trodes before continuing.";
    if (!unitTestFlag && checkMultipleInstances(multInstError)) {
        swapSplashScreenAndTabView(true); //return to the splash screen if load failed
        return false;
    }
//    splashScreen->hide();

    if (!channelsConfigured) {
        //Read the config .xml file; the second argument specifies that this is being called within trodes
        /*int parseCode = nsParseTrodesConfig(fileName);
        if (parseCode < 0) {
            //Show error dialog and abort load
            showErrorMessage(lastDebugMsg);
            //Make sure the user can still try to load another file
            splashScreen->setCanLoadFile(true);
            return parseCode;
        } else if (parseCode == 100) {
            //Show a warning but don't abort load.
            showWarningMessage(lastDebugMsg);
        }*/

        qDebug() << "Creating progress dialog";
        QProgressDialog progress("Setting up workspace",QString(), 0,100);
        progress.setWindowModality(Qt::WindowModal);
        progress.setWindowFlags(Qt::WindowStaysOnTopHint);
        progress.setAttribute(Qt::WA_DeleteOnClose, true);
        progress.setValue(0);
        progress.show();
        progress.raise();

        if (unitTestFlag) {
            unitTestMode = true;
        } else {
            unitTestMode = false;
        }

        if (!liveReconfigHappening) {
            configSettingsChanged = false;
        }
        printDebugCheckpoint();

        //Stream controller setup---------------------------
        SetUpStreamManager();
        printDebugCheckpoint("Stream manager done");
        progress.setValue(10);
        QCoreApplication::processEvents();

        //Stream display setup---------------------------
        SetUpStreamDisplay(streamManager);
        printDebugCheckpoint("Stream dislay done");
        progress.setValue(20);
        QCoreApplication::processEvents();


        //RFDisplay not being used right now...
        /*
        //RFDisplay
        bool rfConfigured = false;
        if (rfConfigured) {
            //sdDisp = new SDDisplay();
            sdDisp = new SDDisplayPanel();
            connect(sdDisp,SIGNAL(connectionRequest()),sourceControl,SLOT(connectToSDCard()));
            connect(sdDisp,SIGNAL(cardEnableRequest()),sourceControl,SLOT(enableSDCard()));
            connect(sdDisp,SIGNAL(reconfigureRequest(int)),sourceControl,SLOT(reconfigureSDCard(int)));
            connect(sourceControl,SIGNAL(SDCardStatus(bool,int,bool,bool)),sdDisp,SLOT(updateSDCardStatus(bool,int,bool,bool)));
            tabs->addTab(sdDisp,"SD card");
            //sdDisp->addPanel();
            //rfDisp->addPanel();

            qDebug() << "[MainWindow::loadConfig] Set up RF display";
        }

        */

        //Spike display setup---------------------------
        if (activeWorkspacePointers.hardwareConf->NCHAN > 0 && activeWorkspacePointers.spikeConf->ntrodes.length() > 0) {
            SetUpSpikeDisplay(streamManager);            
        }
        printDebugCheckpoint("Spike display done");
        progress.setValue(50);
        QCoreApplication::processEvents();


        if (streamManager->avxsupported && spikeDisp) {
            for (int i=0; i < streamManager->streamProcessors.length(); i++) {
                connect(streamManager->streamProcessors[i],&StreamProcessor::newSpikes,spikeDisp,&MultiNtrodeDisplayWidget::receiveNewSpikes,Qt::DirectConnection);
            }
        } else {

            // Set up connections for spikeDetectors (non-avx2 processing mode)

            for (int i = 0; i < streamManager->spikeDetectors.length(); i++) {
                connect(streamManager->spikeDetectors[i],
                        SIGNAL(spikeDetectionEvent(int, const QVector<int2d>*, const int*, uint32_t)),
                        spikeDisp, SLOT(receiveNewEvent(int,const QVector<int2d>*,const int*,uint32_t)), Qt::DirectConnection); //This might not be thread-safe. Needs to be double checked

                connect(streamManager->spikeDetectors[i],SIGNAL(spikeDetectionEvent_TimeOnly(int,uint32_t)),streamManager,SLOT(receiveSpikeEvent(int,uint32_t)));
            }
        }

        printDebugCheckpoint();
        progress.setValue(60);
        QCoreApplication::processEvents();


        //Audio thread setup--------------------------
        if (activeWorkspacePointers.hardwareConf->NCHAN > 0 && activeWorkspacePointers.spikeConf->ntrodes.length() > 0 && isAudioOn) {
            soundOut->setConfiguration(activeWorkspacePointers);
            soundOut->setChannel(activeWorkspacePointers.spikeConf->ntrodes[0]->hw_chan[0]); //set the audio channel to listen to
            soundOut->updateAudio();
        }
        printDebugCheckpoint("Audio done");
        progress.setValue(70);
        //---------------------------------------------

        //Record thread setup---------------------------
        if (!playbackFileOpen) {
            recordOut = new RecordThread(&acquisitionWorkspace, activeWorkspacePointers.globalConf->saveDisplayedChanOnly, activeWorkspacePointers.globalConf->MBPerFileChunk);
            QThread* workerThread = new QThread();
            recordOut->moveToThread(workerThread);
            connect(workerThread, SIGNAL(started()), recordOut, SLOT(setUp()));
            connect(recordOut, SIGNAL(finished()), workerThread, SLOT(quit()));
            connect(recordOut,SIGNAL(finished()), recordOut, SLOT(deleteLater()));
            connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));
            workerThread->setObjectName("Recorder");
            workerThread->start(QThread::HighPriority); //if the priority is too low, then it is possible for the buffer to fill up before a file write.
            connect(this, SIGNAL(endAllThreads()), recordOut, SLOT(endRecordThread()));
            // update the list of channels to save once a configuration file has been loaded
            connect(this, SIGNAL(configFileLoaded()), recordOut, SLOT(setupSaveDisplayedChan()));
            connect(recordOut, &RecordThread::writeError, this, &MainWindow::errorSaving);
            connect(recordOut, &RecordThread::noSpaceLeftError, this, &MainWindow::noSpaceLeftWhileSaving);
            connect(recordOut, &RecordThread::lowDiskSpaceWarning, this, &MainWindow::lowDiskSpaceWarning);
            connect(recordOut, &RecordThread::triggerNextFilePart, this, &MainWindow::createNextFilePart);
            printDebugCheckpoint("Record thread done");
        }
        progress.setValue(80);
        QCoreApplication::processEvents();

        //----------------------------------------------


        channelsConfigured = true;
        actionLoadConfig->setEnabled(true);
        actionCloseConfig->setEnabled(true);
        actionSaveConfig->setEnabled(true);
        actionReConfig->setEnabled(true);
        actionSaveClusters->setEnabled(true);
        actionLoadClusters->setEnabled(true);
        actionAboutConfig->setEnabled(true);
        if (sourceControl->currentSource > 0) {
            actionConnect->setEnabled(true);
            actionHeadstageSettings->setEnabled(true);
            actionControllerSettings->setEnabled(true);
        }
        actionDisconnect->setEnabled(false);
        actionSendSettle->setEnabled(false);
        //actionOpenRecordDialog->setEnabled(true);
        actionShowCurrentTrode->setEnabled(true);

        commentButton->setEnabled(true);

        //We don't fill the source options if we are doing a live reconfig, because it should already be filled
        if (!liveReconfigHappening) {
            fillSourceOptionsMenu();
        }

        // start the local TCP server


        //-----------------------------------------------------
        //Create comment dialog
        if(!newCommentDialog){
            QString commentFileName;
            if (recordFileOpen) {
                QFileInfo fileInfo(recordFileName);
                commentFileName = fileInfo.absolutePath() + "/" + fileInfo.completeBaseName() + ".trodesComments";
            }
            else {
                commentFileName = "";
            }

            newCommentDialog = new CommentDialog(commentFileName, this);

            newCommentDialog->setWindowFlags(Qt::Popup);
            newCommentDialog->setGeometry(QRect(this->geometry().x()+commentButton->x(),this->geometry().y()+commentButton->y()+commentButton->height()+this->menuBar()->height(),80,200));

            connect(this, SIGNAL(closeAllWindows()), newCommentDialog, SLOT(close()));
            if (isAudioOn)
                connect(this, SIGNAL(closeSoundDialog()), newCommentDialog, SLOT(close()));
        }

        startMainNetworkMessaging();
        printDebugCheckpoint("Network start done");
        progress.setValue(90);
        QCoreApplication::processEvents();


        // set up the network connections with the modules if modules are defined
        // set myID to -1 to indicate that this is trodes
        activeWorkspacePointers.moduleConf->myID = TRODES_ID;

        startModules(activeWorkspacePointers.globalConf->configfilepath);
        printDebugCheckpoint("Modules done");

        //mark: restartMod

        //MARK: Event
        if (activeWorkspacePointers.networkConf->networkType == NetworkConfiguration::qsocket_based) {


            connect(this, SIGNAL(sendEvent(uint32_t,int,QString)),trodesNet->tcpServer, SLOT(eventOccurred(uint32_t,int,QString)));
            if (spikeDisp) {
                connect(spikeDisp, SIGNAL(broadcastEvent(TrodesEventMessage)),this,SLOT(broadcastEvent(TrodesEventMessage)));
                connect(spikeDisp, SIGNAL(broadcastNewEventReq(QString)), trodesNet->tcpServer, SLOT(addEventTypeToList(QString)));//
                connect(spikeDisp, SIGNAL(broadcastEventRemoveReq(QString)), trodesNet->tcpServer, SLOT(removeEventTypeFromList(QString)));//
            }
            connect(sourceControl, SIGNAL(newTimestamp(uint32_t)), trodesNet->tcpServer, SLOT(sendCurTimeToModules(uint32_t)));
        }

        connect(sourceControl, &SourceController::newTimestamp, this, &MainWindow::seekPlaybackSignal, Qt::UniqueConnection);
        // update the list of channels to save for the record thread
        progress.setValue(100);
        QCoreApplication::processEvents();
        emit configFileLoaded();
        printDebugCheckpoint();

        swapSplashScreenAndTabView(false);
        printDebugCheckpoint();

        uncoupleDisplay(false); //Make sure display is not in frozen mode
        printDebugCheckpoint();

        //only read benchmarking settings from the config file if the user hasn't edited benchConf via either the GUI or cmdLine yet
        if (activeWorkspacePointers.benchConfig != nullptr && !activeWorkspacePointers.benchConfig->wasEditedByUser() && !activeWorkspacePointers.benchConfig->wasInitiatedFromCommandLine())
            benchmarkingControlPanel->getSettingsFromConfig(); //TODO: use the new pointers instead of globals

        printDebugCheckpoint();
        //open triggerSettings panel if it had previously been open
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
        settings.beginGroup(QLatin1String("trodeSettingsPanel"));
        bool wasOpen = settings.value(QLatin1String("wasOpen")).toBool();
        settings.endGroup();
        if (wasOpen)
            trodeSettingsButton->setChecked(true);
        printDebugCheckpoint("Done opening trodes settings");

        settings.beginGroup(QLatin1String("spikesPanel"));
        wasOpen = settings.value(QLatin1String("wasOpen")).toBool();
        settings.endGroup();
        if (wasOpen) {
            spikesButton->setChecked(true);
            spikesButtonToggled(true);
        } else {
            spikesButton->setChecked(false);
            spikesButtonToggled(false);
        }

        printDebugCheckpoint("Done opening spike trigger window");

        if (activeWorkspacePointers.hardwareConf->NCHAN > 0)
            nTrodeClicked(0, Qt::NoModifier);
        printDebugCheckpoint("Ntrode Clicked");

        emit activeWorkspacePointers.spikeConf->updatedAllModes(); //make sure all stream displays are in the right display mode (LFP, spike, or RAW)

        printDebugCheckpoint("End of load");


        if (dataWasStreamingBeforeLiveReconfig) {
            //We probably need to put a delay on this.  There is a good chance the above threads have not finished being set up yet.
            connectToSource();
            dataWasStreamingBeforeLiveReconfig = false;
        }

        mixedVisualFiltersOn();

        liveReconfigHappening = false;


        setRealTimeDisplayMode(actionRealtimeDisplay->isChecked());
        //connect(actionRealtimeDisplay,SIGNAL(toggled(bool)),this,SLOT(setRealTimeDisplayMode(bool)));
        progress.close();

        return true;
    }
    else {
        //mark: Load Screen fade out
//        emit sig_fadeOutLoadscreen();
//        loadingScreen->fadeOut();
        return false;
    }
}

void MainWindow::SetUpStreamManager(){
    streamManager = new StreamProcessorManager(nullptr,activeWorkspacePointers, enableAVXFlag);
    connect(streamManager, SIGNAL(bufferOverrun()), this, SLOT(bufferOverrunHandler())); //In case data rates are too fast, use an emergency stop signal
    connect(streamManager, SIGNAL(sourceFail_Sig(bool)), sourceControl, SLOT(noDataComing(bool)));
    connect(streamManager,SIGNAL(sourceFail_Sig(bool)),this,SLOT(sourceNoDataError(bool)));

    connect(streamManager,SIGNAL(headstageFail_Sig(bool)),this,SLOT(sourceNoHeadstageError(bool)));
    connect(this, SIGNAL(newTraceLength(double)), streamManager, SLOT(updateDataLength(double)));
    //All connections between streamconf/spikeconf and streammanager is done in streammanager's constructor
    connect(activeWorkspacePointers.spikeConf, SIGNAL(changeAllMaxDisp(int)), this, SLOT(setAllMaxDisp(int)));
    connect(activeWorkspacePointers.spikeConf, SIGNAL(changeAllThresh(int)), this, SLOT(setAllThresh(int)));
    connect(sourceControl, SIGNAL(acquisitionStarted()), streamManager, SLOT(startAcquisition()));
    connect(sourceControl, SIGNAL(acquisitionStopped()), streamManager, SLOT(stopAcquisition()));
    connect(sourceControl,SIGNAL(signal_UnresponsiveHeadstage(bool)), this, SLOT(sourceUnresponsiveHeadstage(bool)));
    connect(sourceControl,&SourceController::newGlobalStimSettings,streamManager,&StreamProcessorManager::updateGlobalStimSettings);
    connect(streamManager, SIGNAL(functionTriggerRequest(int)), sourceControl, SLOT(sendFunctionTriggerCommand(int)));
    qDebug() << "[MainWindow::loadConfig] Set up stream manager";
}

void MainWindow::SetUpStreamDisplay(StreamProcessorManager* streamManager){
    printDebugCheckpoint("Stream display begin");
    eegDisp = new StreamDisplayManager(nullptr, streamManager);
    printDebugCheckpoint("Stream display mangager object created");
    //spikeFiltersOn();
    eegDisp->setInvertSpikes(preferencesPanel->invertSpikes());

    int columnCount = 0;
    for (int tnum = 0; tnum < eegDisp->eegDisplayWidgets.length(); tnum++) {

        int columnsOnPage = eegDisp->columnsPerPage[tnum];
        if (!eegDisp->isHeaderDisplayPage[tnum]) {
            tabs->addTab(eegDisp->eegDisplayWidgets[tnum], QString("Tr ") + QString("%1").arg(eegDisp->nTrodeIDs[columnCount].first()) + "-" + QString("%1").arg(eegDisp->nTrodeIDs[columnCount + columnsOnPage - 1].last()));
        } else {
            if (eegDisp->isHeaderDisplayPage[tnum]) {
                tabs->addTab(eegDisp->eegDisplayWidgets[tnum], QString("Aux"));

                //tabs->setTabText(tnum, QString("Aux  ") + QString("%1").arg(eegDisp->streamDisplayChannels[columnCount].first() + 1) + "-" + QString("%1").arg(eegDisp->streamDisplayChannels[columnCount + columnsOnPage - 1].last() + 1));
                //tabs->setTabText(tnum, QString("Aux"));
            }
        }
        columnCount += columnsOnPage;
    }
    printDebugCheckpoint("Stream display added tabs.");

    //As of qt5.1.1, you have to switch between the tabs in order for the mousePressEvent callback in the glWidgets to work.
    tabs->setCurrentIndex(eegDisp->eegDisplayWidgets.length() - 1);
    tabs->setCurrentIndex(0);

    //connect(this, SIGNAL(setAudioChannel(int)), eegDisp, SLOT(updateAudioHighlightChannel(int)));
    connect(eegDisp, SIGNAL(trodeSelected(int)), this, SLOT(selectTrode(int)));
    connect(eegDisp, SIGNAL(nTrodeClicked(int,Qt::KeyboardModifiers)), this, SLOT(nTrodeClicked(int,Qt::KeyboardModifiers)));
    connect(eegDisp, SIGNAL(sig_rightClickAction(QString)), this, SLOT(processNTrodeRightClickAction(QString)));
    if (isAudioOn) {
        connect(eegDisp, SIGNAL(streamChannelClicked(int)),this,SLOT(audioChannelChanged(int)));
    }
    connect(eegDisp, SIGNAL(newPSTHTrigger(int,bool)),streamManager,SLOT(setPSTHTrigger(int,bool)));
    connect(eegDisp, SIGNAL(newSettleControlChannel(int,quint8,quint8)),this,SLOT(setSettleControlChannel(int,quint8,quint8)));
    connect(eegDisp, SIGNAL(setNewMaxDisp(int)), this, SLOT(setMaxDisp(int)));
    connect(preferencesPanel, &PreferencesPanel::invertSpikesSet, eegDisp, &StreamDisplayManager::setInvertSpikes);

    qDebug() << "[MainWindow::loadConfig] Set up stream display";
}

void MainWindow::SetUpSpikeDisplay(StreamProcessorManager* streamManager){
    spikeDisp = new MultiNtrodeDisplayWidget(nullptr);
    spikeDisp->setStreamManagerPtr(streamManager);

    //spikeDisp->setWindowFlags(Qt::WindowStaysOnTopHint);
    //spikeDisp->show();
    singleTriggerWindowOpen = true;
    if (isAudioOn) {
        connect(eegDisp, SIGNAL(streamChannelClicked(int)),spikeDisp,SLOT(changeAudioChannel(int)));
        //connect(spikeDisp, SIGNAL(resetAudioButtons()), this, SLOT(resetAllAudioButtons()));
        connect(spikeDisp, SIGNAL(channelClicked(int)), this, SLOT(audioChannelChanged(int)));
        connect(spikeDisp,SIGNAL(channelClicked(int)),eegDisp,SLOT(updateAudioHighlightChannel(int)));
    }
    connect(activeWorkspacePointers.spikeConf,SIGNAL(newMaxDisplay(int,int)),spikeDisp,SLOT(setMaxDisplay(int,int)));
    connect(spikeDisp, SIGNAL(sig_newMaxDisp(int)), this, SLOT(setMaxDisp(int)));
    connect(spikeDisp, SIGNAL(windowClosed()), this, SLOT(spikesPanelClosed()));
    connect(this, &MainWindow::openallpeths, spikeDisp, &MultiNtrodeDisplayWidget::openAllNTrodePETHs);
    connect(this,SIGNAL(signalSaveClusters(QString)),spikeDisp, SLOT(saveCurrentClusters(QString)));
    connect(this,SIGNAL(signalLoadClusters(QString)),spikeDisp, SLOT(loadClusterFile(QString)));
    qDebug() << "[MainWindow::loadConfig] Set up spike trigger display";
}

void MainWindow::startModules(QString configFileName)
{
    if (activeWorkspacePointers.moduleConf->singleModuleConf.length() && !playbackFileOpen) {

        SingleModuleConf s;
        // launch the modules and start the TCPIP server to each one as we do so. This is slower than launching
        // all the modules at once, but allows us to keep their connections in order

        for (int i = 0; i < activeWorkspacePointers.moduleConf->singleModuleConf.length(); i++) {
            s = activeWorkspacePointers.moduleConf->singleModuleConf[i];

            // Set up stderr/stdout forwarding (will be rerouted to qDebug and then logged)
            QProcess *moduleProcess = new QProcess(this);
            connect(moduleProcess,SIGNAL(readyReadStandardError()),this,SLOT(forwardProcessOutput()));
            connect(moduleProcess,SIGNAL(readyReadStandardOutput()),this,SLOT(forwardProcessOutput()));


            startSingleModule(s, moduleProcess);

        }
    }
    // enable the Restart Modules menu item
    actionRestartModules->setEnabled(true);
}

//*****************************************
//Copied from old trodesNet code. Did not use any of the old network member variables besides the
//address and port, which was easily converted
QProcess* MainWindow::startSingleModule(SingleModuleConf s, QProcess *moduleProcess){
    // QProcess is optional -- this way caller can set up things like stderr/stdout forwarding, if needed

    // TODO:
    // -Try to call it from FSGui, stateScript?) Paths are relative to calling App,
    // not Trodes
    // -How to handle failure? Return same process, unstarted, or destroy the process and return NULL

    // N.B. relative paths are treated as relative to the directory containing
    // the executable (or, on a Mac, the app bundle) that launched the module
    // (this is usually Trodes). This is different than the current working
    // directory of the app.

    // If the filename component of the path is a bare module name (e.g.
    // "cameraModule"), then the behavior is platform-dependent. On a Mac, if
    // the requested file does not exist we look for an app bundle by that
    // name. On Windows, we look for an executable named moduleName.exe. On
    // Linux we just use the moduleName as-is

    QString moduleAbsName; // Absolute path to use
    QFileInfo moduleAbsFileInfo;

    // Absolute path of directory containing the module, or module app bundle
    // (potentially after relative path moves)
    QString moduleWorkingDir;

    QString moduleBaseName = QFileInfo(s.moduleName).completeBaseName();

    // Parent doesn't much matter (only used for QObject tree), process will still be killed when calling program exits, e.g.
    if (!moduleProcess)
      moduleProcess = new QProcess();

    //Get the directory containing the calling executable (usu. Trodes)
    QString callingAppDir = QCoreApplication::applicationDirPath();

#ifdef __APPLE__
    //If calling app is an app bundle, use its parent as the base directory
    if (callingAppDir.endsWith(QString(".app/Contents/MacOS")))
        callingAppDir = QDir::cleanPath(callingAppDir + "/../../../");
#endif

    // Convert moduleName to absolute path
    if (QFileInfo(s.moduleName).isRelative()){
        moduleAbsName= QDir::cleanPath(callingAppDir + "/" + s.moduleName);
        qDebug() << "[MainWindow::startModule] 'moduleName' string (" << s.moduleName << ") converted to absolute path: " << moduleAbsName;
    } else {

        // Use absolute path provided
        moduleAbsName = s.moduleName;
        qDebug() << "[MainWindow::startModule] 'moduleName' string (" << s.moduleName << ") is an absolute path (NOT RECOMMENDED); used as-is.)";

        if (!activeWorkspacePointers.globalConf->suppressModuleAbsPathWarning && !unitTestMode){

            QMessageBox::warning(
                        0, "error", QString(
                            "Configuration file contains a hard-coded, absolute path to "
                            "a module executable: \n\nmoduleName=\""
                            + s.moduleName +
                            "\" \n\nThis is strongly discouraged since it can lead to "
                            "running old versions of modules. Instead, use a "
                            "relative path to locate modules. (Path is relative "
                            "to directory containing the launching application, in this case: '"
                            + callingAppDir  + QDir::separator() + "'). \n\n "
                            "When in doubt, use the bare moduleName in your config file: \n\n moduleName=\""
                            + moduleBaseName + "\"\n\n "
                            "(Suppress this warning with suppressModuleAbsPathWarning=\"1\" "
                            "in GlobalConfiguration)."));

        }
    }

    // Use *module's* parent directory as moduleWorkingDir
    moduleAbsFileInfo = QFileInfo(moduleAbsName);
    moduleWorkingDir = moduleAbsFileInfo.path();

#ifdef __APPLE__
    // ***************************
    // Handle Mac app bundle structure.
    //
    // The following moduleName strings:
    // 1: "/Path/to/moduleName" (where file doesn't exist, but /Path/to/moduleName.app does)
    // 2: "/Path/to/moduleName.app"
    // 3: "/Path/to/moduleName.app/Contents/MacOS/moduleName"
    // ... should launch-> "/Path/to/moduleName.app/Contents/MacOS/moduleName",
    //
    // While:
    // 4: "/Path/to/moduleName" (exists, not a bundle)
    // ... should launch -> "/Path/to/moduleName"
    //
    // In all these cases, moduleWorkingDir should be set to -> "/Path/to"

    // Case 1: User provides path to a module's base name, and an app bundle exists
    if (!moduleAbsFileInfo.exists() && QFileInfo::exists(moduleAbsFileInfo.filePath() + ".app"))
        moduleAbsName = moduleAbsName + ".app/Contents/MacOS/" + moduleBaseName;

    // Case 2: User provides absolute path to module app bundle
    else if (moduleAbsFileInfo.exists() && (moduleAbsFileInfo.suffix() == "app"))
        moduleAbsName = moduleAbsName + "/Contents/MacOS/" + moduleBaseName;

    // Case 3/4: Path to executable
    else if (moduleAbsFileInfo.exists() && !moduleAbsFileInfo.isDir()){
        // Is it part of a bundle?
        if (moduleWorkingDir.endsWith(QString(".app/Contents/MacOS"))) {
            // Case 3: executable part of a bundle, update working dir only
            moduleWorkingDir = QDir::cleanPath(moduleWorkingDir + "/../../../");
        } else {
            // Case 4: bare executable, no need to update moduleAbsName or moduleWorkingDir
        }
    }

    // ***************************


#endif
#ifdef WIN32

    // Set Windows executable paths (QProcess will launch without the '.exe'
    // suffix, but exists() and isExecutable() need the full file name.")
    if (!(moduleAbsName).endsWith(".exe")){
        moduleAbsName = moduleAbsName + ".exe";
    }

#endif
    // Update FileInfo
    moduleAbsFileInfo = QFileInfo(moduleAbsName);

    if (!moduleAbsFileInfo.exists()){
        if (!unitTestMode) {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", QString("Module \"%1\" could not be started at path: \"%2\".\n\nFile does not exist.").arg(moduleBaseName).arg(moduleAbsName));
            qDebug() << "[MainWindow::startModule] MODULE LOADING ERROR! Config file moduleName string: " << s.moduleName \
                 << ". Launching application directory: " << callingAppDir << ". Module search location (file doesn't exist): " << moduleAbsName;
        }
        return moduleProcess;
    }
    if (!moduleAbsFileInfo.isExecutable()){
        if (!unitTestMode) {
            QMessageBox messageBox;
            messageBox.critical(0, "Error", QString("Module \"%1\" could not be started at path: \"%2\".\n\nFile is not an executable.").arg(moduleBaseName).arg(moduleAbsName));
            qDebug() << "[MainWindow::startModule] MODULE LOADING ERROR! Config file moduleName string: " << s.moduleName \
                 << ". Launching application directory: " << callingAppDir << ". Module search location (not an executable file): " << moduleAbsName;
        }
        return moduleProcess;
    }

    // launch the module with the main configuration file and the specified configuration file
    qDebug() << "[MainWindow::startModule] Launching: " << moduleAbsName;
    qDebug() << "[MainWindow::startModule] Working directory for module: " << moduleWorkingDir;

    moduleProcess->setWorkingDirectory(moduleWorkingDir);

    QStringList arglist;

    if (s.sendTrodesConfig == 1) {       
        QStringList configInfo;
        // Need to pass absolute path to config file as module's working
        // directory may be different (and config file location may be relative)
        configInfo << "-trodesConfig" << QFileInfo(moduleConf->trodesConfigFileName).absoluteFilePath();       
        arglist << configInfo;
    }

    if (s.sendNetworkInfo) {

        QStringList netInfo;
        if (activeWorkspacePointers.networkConf->networkType == NetworkConfiguration::qsocket_based) {
             netInfo << "-serverAddress" << trodesNet->tcpServer->getCurrentAddress() << "-serverPort" << QString("%1").arg(trodesNet->tcpServer->getCurrentPort());
        } else {
            netInfo << "-serverAddress" << QString::fromStdString(trodesModule->getAddress()) << "-serverPort" << QString::number(trodesModule->getPort());
        }
        arglist << netInfo;
    }
    arglist << s.moduleArguments;
    qDebug().noquote() << "[MainWindow::startModule] arguments: " << arglist;
    moduleProcess->start(moduleAbsName, arglist);
    if (moduleProcess->waitForStarted(10000) == false) {
        QMessageBox messageBox;
        messageBox.critical(0, "Error", QString("%1 could not be started").arg(s.moduleName));
    }
    return moduleProcess;
}

void MainWindow::restartCrashedModule(QString modName) {
    if (modName == "Camera") {
        videoButtonPressed();
    }
}

void MainWindow::forwardProcessOutput() {
    QProcess* senderProcess = (QProcess*)sender();

    qDebug().noquote() << senderProcess->readAllStandardError();
}

void MainWindow::openSettleChannelDelayDialog() {

    bool ok;
    int newDelay = QInputDialog::getInt(nullptr, "Enter settle delay after trigger",
                "Delay (samples):", settleChannelDelay, 0, 30000,1,&ok);
    if (ok) {
        setSettleChannelDelay(newDelay);
    }
}

void MainWindow::setSettleChannelDelay(int delay) {
    settleChannelDelay = delay;
    sourceControl->sendSettleChannel(settleChannelByteInPacket,settleChannelBit,settleChannelDelay,settleChannelTriggerState);
}

void MainWindow::setSettleControlChannel(int byteInPacket, quint8 bit, quint8 triggerState) {

    settleChannelByteInPacket = byteInPacket;
    settleChannelBit = bit;
    settleChannelTriggerState = triggerState;
    //settleChannelDelay = 0;
    sourceControl->sendSettleChannel(byteInPacket,bit,settleChannelDelay,triggerState);
}

void MainWindow::startTrodesNetwork() {
    //mark: network
    try{
        QString qs_address = DEFAULT_SERVER_ADDRESS;
        int port = DEFAULT_SERVER_PORT;
        if ((networkConf->networkConfigFound) && (networkConf->trodesHost != "")) {
            qs_address = networkConf->trodesHost;
            if(!qs_address.startsWith("tcp://")){
                qs_address.insert(0, "tcp://");
            }
        }
        if ((networkConf->networkConfigFound) && (networkConf->trodesPort != 0)) {
            port = networkConf->trodesPort;
        }
        std::string address = qs_address.toStdString();
        // address is a std::string with the network address
        // port is an int with the network port
        qDebug() << "[MainWindow::startTrodesNetwork] Starting Trodes network at address" << qs_address << "port" << port;
        
        trodesModule = std::move(std::unique_ptr<trodes::TrodesModule>(new trodes::TrodesModule(address, port)));
        networkConf->trodesHost = QString::fromStdString(trodesModule->getAddress());
        networkConf->trodesPort = trodesModule->getPort();

        trodesModule->setModuleTimePtr(&currentTimeStamp);
        trodesModule->setModuleTimeRate(hardwareConf->sourceSamplingRate);

        // keep this regardless because it's signalling itself
        connect(this, &MainWindow::signal_sendPlaybackCommand, this, &MainWindow::sendAquisitionCommand);

        // may require unique_ptr's get to obtain raw pointer
        // all of these connects are cause and effect
        connect(this, &MainWindow::recordFileOpened, trodesModule.get(), &trodes::TrodesModule::sendFileOpened, Qt::QueuedConnection);
        connect(this, &MainWindow::recordFileClosed, trodesModule.get(), &trodes::TrodesModule::sendFileClose, Qt::QueuedConnection);
        connect(this, &MainWindow::sourceConnected, trodesModule.get(), &trodes::TrodesModule::sendSourceConnect, Qt::QueuedConnection);
        connect(this, &MainWindow::recordingStarted, trodesModule.get(), &trodes::TrodesModule::sendRecordingStarted, Qt::QueuedConnection);
        connect(this, &MainWindow::recordingStopped, trodesModule.get(), &trodes::TrodesModule::sendRecordingStopped, Qt::QueuedConnection);
        connect(sourceControl, &SourceController::acquisitionStarted, trodesModule.get(), &trodes::TrodesModule::sendAcquisitionStarted, Qt::QueuedConnection);
        connect(sourceControl, &SourceController::acquisitionStopped, trodesModule.get(), &trodes::TrodesModule::sendAcquisitionStopped, Qt::QueuedConnection);
        connect(trodesModule.get(), &trodes::TrodesModule::moduleCrashed, this, &MainWindow::restartCrashedModule, Qt::QueuedConnection);

        connect(trodesModule.get(), &trodes::TrodesModule::settleCommandTriggered, this, &MainWindow::sendSettleCommand);
        connect(this, &MainWindow::sig_sendPlaybackCommand, trodesModule.get(), &trodes::TrodesModule::sendPlaybackCommand);
        connect(trodesModule.get(), &trodes::TrodesModule::sig_acquisitionReceived, this, &MainWindow::processAquisitionCommand);

        // these can be removed because they're related to the previous system

        connect(sourceControl, &SourceController::newContinuousData, trodesModule.get(), &trodes::TrodesModule::registerNewContData, Qt::QueuedConnection);
        connect(streamManager, &StreamProcessorManager::newContinuousData, trodesModule.get(), &trodes::TrodesModule::registerNewContData, Qt::QueuedConnection);
//        connect(sourceControl, &SourceController::headstageSettingsReturned, mainServer, &TrodesCentralServer::newHeadstageSettings, Qt::QueuedConnection);
        connect(sourceControl, &SourceController::controllerSettingsReturned, trodesModule.get(), &trodes::TrodesModule::newControllerSettings, Qt::QueuedConnection);

        if (spikeDisp != nullptr) {
            connect(spikeDisp, &MultiNtrodeDisplayWidget::newContinuousData, trodesModule.get(), &trodes::TrodesModule::registerNewContData, Qt::QueuedConnection);
            connect(spikeDisp, &MultiNtrodeDisplayWidget::broadcastNewEventReq, trodesModule.get(), &trodes::TrodesModule::provideEventSlot, Qt::QueuedConnection);
            connect(spikeDisp, &MultiNtrodeDisplayWidget::broadcastEventRemoveReq, trodesModule.get(), &trodes::TrodesModule::removeEventSlot, Qt::QueuedConnection);
        }
        connect(sourceControl, &SourceController::deregisterHighFreqData, trodesModule.get(), &trodes::TrodesModule::deregisterContData, Qt::QueuedConnection);

        // keep these because they're necessary to generate the connects
        qRegisterMetaType<uint16_t>("uint16_t");
        qRegisterMetaType<StimulationCommand>("StimulationCommand");
        qRegisterMetaType<GlobalStimulationSettings>("GlobalStimulationSettings");
        qRegisterMetaType<GlobalStimulationCommand>("GlobalStimulationCommand");

        // all of these are related to receiving commands over the network and
        // making them happen on the hardware because the current code is
        // directly connected to the hardware
        connect(trodesModule.get(), &trodes::TrodesModule::sendSetStimulationParams, sourceControl, &SourceController::SetStimulationParams);
        connect(trodesModule.get(), &trodes::TrodesModule::sendClearStimulationParams, sourceControl, &SourceController::ClearStimulationParams);
        connect(trodesModule.get(), &trodes::TrodesModule::sendStimulationStartSlot, sourceControl, &SourceController::SendStimulationStart);
        connect(trodesModule.get(), &trodes::TrodesModule::sendStimulationStartGroup, sourceControl, &SourceController::SendStimulationStartGroup);
        connect(trodesModule.get(), &trodes::TrodesModule::sendStimulationStopSlot, sourceControl, &SourceController::SendStimulationStop);
        connect(trodesModule.get(), &trodes::TrodesModule::sendStimulationStopGroup, sourceControl, &SourceController::SendStimulationStopGroup);
        connect(trodesModule.get(), &trodes::TrodesModule::sendGlobalStimulationSettings, sourceControl, &SourceController::SendGlobalStimulationSettings);
        connect(trodesModule.get(), &trodes::TrodesModule::sendGlobalStimulationCommand, sourceControl, &SourceController::SendGlobalStimulationCommand);
        connect(trodesModule.get(), &trodes::TrodesModule::sendECUShortcutMessage, sourceControl, &SourceController::SendECUShortcutMessage);
        connect(trodesModule.get(), &trodes::TrodesModule::annotationRequest, newCommentDialog, &CommentDialog::saveLine);

        // open .rec file, start recording, pause recording, close file
        connect(trodesModule.get(), &trodes::TrodesModule::sig_rec_open, this, &MainWindow::openRecordFile);
        connect(trodesModule.get(), &trodes::TrodesModule::sig_rec_start, this, &MainWindow::recordButtonPressed);
        connect(trodesModule.get(), &trodes::TrodesModule::sig_rec_pause, this, &MainWindow::pauseButtonPressed);
        connect(trodesModule.get(), &trodes::TrodesModule::sig_rec_close, this, &MainWindow::closeFile);

        trodesModule->provideEvent("NtrodeChanSelect");
        connect(eegDisp, &StreamDisplayManager::nTrode_chan_selected, trodesModule.get(), &trodes::TrodesModule::ntrodeChanSelected);

        networkPanel->setAddressAndPort(trodesModule->getAddress().c_str(), trodesModule->getPort());

        networkPanel->updateClients(trodesModule->getClientList());
        connect(trodesModule.get(), &trodes::TrodesModule::moduleClosed, networkPanel, &NetworkPanel::clientClosed);
        connect(trodesModule.get(), &trodes::TrodesModule::moduleConnected, networkPanel, &NetworkPanel::clientConnected);
        connect(trodesModule.get(), &trodes::TrodesModule::moduleCrashed, networkPanel, &NetworkPanel::clientCrashed);
        connect(networkPanel, &NetworkPanel::sendQuit, trodesModule.get(), &trodes::TrodesModule::sendQuitToModule);

        if(spikeDisp) spikeDisp->startDedicatedSpikePub();
        streamManager->startLFPPubThread();
        loadConfigIntoNetworkObject();

    } catch(std::string &e){
        qDebug() << "Could not start the network. Please close the config and try again. ";
        qDebug() << e.c_str();
        QMessageBox::warning(this, "Could not start Trodes Network",
                              QString("Could not start the network. Please close the config and try again. ") + e.c_str());
    }
}

void MainWindow::endTrodesNetwork() {
    //shutdown the trodes network
    qDebug() << "Shutting down ZMQ-based network...";

    trodesModule.reset();
    qDebug() << "ZMQ-based network down.";
}

void MainWindow::loadConfigIntoNetworkObject() {
//    qDebug() << "******** LoadConfigIntoNetworkObject() ********";
    TrodesConfig config;

//    qDebug() << "Add the Devices";
    //Add Devices
    for (int i = 0; i < hardwareConf->devices.length(); i++) {
        DeviceInfo curDev = hardwareConf->devices.at(i);
        NDevice newDev;
        newDev.setName(curDev.name.toStdString());
        newDev.setNumBytes(curDev.numBytes);
        newDev.setByteOffset(curDev.byteOffset);
//        qDebug() << "For Device [" << newDev->getName().c_str() << "] add channels";
        for (int j = 0; j < curDev.channels.length(); j++) {
            DeviceChannel curChan = curDev.channels.at(j);
            NDeviceChannel newChan;

            newChan.setId(curChan.idString.toStdString());
            newChan.setStartByte(curChan.startByte);
            newChan.setStartBit(curChan.digitalBit);
            newChan.setType((int)curChan.dataType);
            newChan.setInterleavedDataByte(curChan.interleavedDataIDByte);
            newChan.setInterleavedDataBit(curChan.interleavedDataIDBit);

            newDev.addChannel(newChan);
        }

        config.addDevice(newDev);
    }

//    qDebug() << "Add the nTrodes";
    for (int i = 0; i < spikeConf->ntrodes.length(); i++) {
        SingleSpikeTrodeConf *curNTrode = spikeConf->ntrodes.at(i);
        NTrodeObj newNTrode;
        QString nTrodeID = QString("%1").arg(curNTrode->nTrodeId);
        newNTrode.setId(nTrodeID.toStdString());
        newNTrode.setIndex(curNTrode->nTrodeIndex);
//        qDebug() << "Add list of hw_chans to the NTrode [" << i <<"]";
        for (int j = 0; j < curNTrode->hw_chan.length(); j++) {
            newNTrode.addHWChan(curNTrode->hw_chan.at(j));
        }

        config.addNTrode(newNTrode);
    }

    trodesModule->setConfig(config);
    trodesModule->sendConfigToModules();
}

void MainWindow::processAquisitionCommand(QString cmd, qint32 time) {
//processPlaybackCommand();
    if (cmd == acq_PLAY) {
        qDebug() << "Got start command";
        processPlaybackCommand(PC_PLAY, time);
//        startRecording();
    }
    else if (cmd == acq_PAUSE) {
        qDebug() << "Got pause command";
        processPlaybackCommand(PC_PAUSE, time);
    }
    else if (cmd == acq_STOP) {
        qDebug() << "Got stop command";
        processPlaybackCommand(PC_STOP, time);
    }
    else if (cmd == acq_SEEK) {
        qDebug() << "Got seek command";
        processPlaybackCommand(PC_SEEK, time);
    }
    else if (cmd == acq_RECORD) {
        qDebug() << "Got record command";
        //Do nothing for now.
    }
    else if (cmd == acq_STOPRECORD) {
        qDebug() << "Got stop record command";
        //Do nothing for now
    }
    else
        qDebug() << "ERROR: aquisition command not recognized. (MainWindow::processAquisitionCommand)";

}

void MainWindow::sendAquisitionCommand(qint8 cmd, qint32 time) {
//    qDebug() << "TRODES Sending Acqusition command! [" << cmd << "]";
    QString strCmd;
    if (cmd == PC_PLAY) {
        strCmd = acq_PLAY;
    }
    else if (cmd == PC_PAUSE) {
        strCmd = acq_PAUSE;
    }
    else if (cmd == PC_STOP) {
        strCmd = acq_STOP;
    }
    else if (cmd == PC_SEEK) {
        strCmd = acq_SEEK;
    }
    else {
        qDebug() << "HUGE ERROR!!!";
        return;
    }

    emit sig_sendPlaybackCommand(strCmd, time);
}

void MainWindow::playbackAcquisitionStopped(){
    emit sig_sendPlaybackCommand(acq_STOP, 0);
}

void MainWindow::startMainNetworkMessaging(void)
{

    if (activeWorkspacePointers.networkConf->networkType == NetworkConfiguration::zmq_based) {
        startTrodesNetwork();
    } else if (activeWorkspacePointers.networkConf->networkType == NetworkConfiguration::qsocket_based){

        // Create the global trodes module network object
        trodesNet = new TrodesModuleNetwork();


        // Set the ID to the specified ID for the main program.
        trodesNet->moduleID = TRODES_ID;
        trodesNet->tcpServer = new TrodesServer();

        //The server is able to respond to small data requests from the modules.
        //Here is where we give the server access to the possible data types requested.
        //So far just time.
        trodesNet->tcpServer->setModuleTimePtr(&currentTimeStamp);
        connect(this, SIGNAL(recordFileOpened(QString)), trodesNet->tcpServer, SLOT(sendFileOpened(QString)));
        connect(this, SIGNAL(sourceConnected(int)), trodesNet->tcpServer, SLOT(sendSourceConnect(int)));
        connect(this, SIGNAL(recordFileClosed()), trodesNet->tcpServer, SLOT(sendFileClose()));
        connect(this, SIGNAL(recordingStarted()), trodesNet->tcpServer, SLOT(sendStartRecord()));
        connect(this, SIGNAL(recordingStopped()), trodesNet->tcpServer, SLOT(sendStopRecord()));
        connect(this, SIGNAL(endAllThreads()), trodesNet->tcpServer, SLOT(deleteServer()));
        connect(trodesNet->tcpServer, SIGNAL(trodesServerError(QString)), this, SLOT(threadError(QString)));
        connect(trodesNet->tcpServer,SIGNAL(settleCommandTriggered()),this,SLOT(sendSettleCommand()));



        //Connection to update all modules whenever the user edits the benchmark settings
        connect(this->benchmarkingControlPanel, SIGNAL(signal_benchConfigUpdated()), trodesNet->tcpServer, SLOT(sendBenchConfigToModules()));

        //Connections for file playback, communicate with other modules playing back files
        connect(trodesNet->tcpServer,SIGNAL(playbackCommandReceived(qint8,qint32)),this,SLOT(processPlaybackCommand(qint8,qint32)));
        connect(this, SIGNAL(signal_sendPlaybackCommand(qint8,qint32)), trodesNet->tcpServer, SLOT(sendPlaybackCommand(qint8,qint32)));


        // connect slots related to data exchange among modules.  These just pass signals from a message handler through
        // tcpServer and on to trodesNet
        connect(trodesNet->tcpServer, SIGNAL(doSendAllDataAvailable(TrodesSocketMessageHandler*)), trodesNet, SLOT(sendAllDataAvailableToModule(TrodesSocketMessageHandler*)));
        connect(trodesNet->tcpServer, SIGNAL(doAddDataAvailable(DataTypeSpec*)), trodesNet, SLOT(addDataAvailable(DataTypeSpec*)));
        connect(trodesNet->tcpServer, SIGNAL(doRemoveDataAvailable(qint8)), trodesNet, SLOT(removeDataAvailable(qint8)));
        connect(trodesNet->tcpServer, SIGNAL(nameReceived(TrodesSocketMessageHandler*,QString)), trodesNet->tcpServer,
                SLOT(setNamedModuleMessageHandler(TrodesSocketMessageHandler*,QString)));


        connect(trodesNet->tcpServer, SIGNAL(moduleDataStreamOn(bool)), this, SLOT(setModuleDataStreaming(bool)));
        connect(this, SIGNAL(messageForModules(TrodesMessage*)), trodesNet->tcpServer, SLOT(sendMessageToModules(TrodesMessage*)));
        //connect(this, SIGNAL(clearMessageHandler()), trodesNet->tcpServer, SLOT(clearModuleMessageHandlers()));
        connect(this, SIGNAL(clearDataAvailable()), trodesNet, SLOT(clearDataAvailable()));

        connect(trodesNet, SIGNAL(messageForModule(TrodesSocketMessageHandler*, TrodesMessage*)),
                trodesNet->tcpServer, SLOT(sendMessageToModule(TrodesSocketMessageHandler*, TrodesMessage*)));

        connect(trodesNet->tcpServer, SIGNAL(restartCrashedModule(QString)), this, SLOT(restartCrashedModule(QString)));


        // add a signal for internal DataAvailable information to be added to the main list
        //connect(streamManager, SIGNAL(addDataProvided(DataTypeSpec*)), trodesNet, SLOT(addDataAvailable(DataTypeSpec*)));


        //check Connection status


        // set the port for communication to the hardware

        if ((activeWorkspacePointers.networkConf->networkConfigFound) && (activeWorkspacePointers.networkConf->trodesHost != "")) {
            trodesNet->tcpServer->setAddress(activeWorkspacePointers.networkConf->trodesHost);
        }
        if ((activeWorkspacePointers.networkConf->networkConfigFound) && (activeWorkspacePointers.networkConf->trodesPort != 0)) {
            trodesNet->tcpServer->startServer("Trodes main", activeWorkspacePointers.networkConf->trodesPort);
        }
        else {

            //otherwise just find an available address and port
            //localhost is the last option if nothing else is available
            trodesNet->tcpServer->startServer("Trodes main");
            //        // set the host name and the port

            activeWorkspacePointers.networkConf->trodesHost = QHostInfo::localHostName();
            activeWorkspacePointers.networkConf->trodesPort = trodesNet->tcpServer->serverPort();


        }


        qDebug() << "[MainWindow::startMainNetworkMessaging] trodesHost =" << activeWorkspacePointers.networkConf->trodesHost << "port =" << activeWorkspacePointers.networkConf->trodesPort;


        //Move the trodesNet object to a separate thread so that networking can go on outside the GUI thread

        trodesNetThread = new QThread;
        //connect(trodesNetThread,SIGNAL(started()),trodesNet->tcpServer,SLOT());
        connect(trodesNet->tcpServer, SIGNAL(finished()), trodesNetThread, SLOT(quit()));
        connect(trodesNet->tcpServer, SIGNAL(finished()), trodesNet->tcpServer, SLOT(deleteLater()));
        trodesNet->tcpServer->moveToThread(trodesNetThread);
        connect(trodesNetThread, SIGNAL(finished()), trodesNetThread, SLOT(deleteLater()));
        trodesNetThread->start();
    }
}

void MainWindow::sourceNoDataError(bool on) {
    //Error when no data is coming from hardware
    noDataErrFlag = on;
    updateErrorBlink();
}

void MainWindow::sourceNoHeadstageError(bool on) {
    //Error when no data is coming from hardware
    noHeadstageErrFlag = on;
    updateErrorBlink();
}

void MainWindow::sourceUnresponsiveHeadstage(bool u) {
    //Error when the incoming packet has a flag indicating that the headstage became unresponsive or became responsive again
    unresponsiveHeadstageFlag = u;
    updateErrorBlink();
}

void MainWindow::sourcePacketSizeError(bool on) {
    //Error when the incoming packet size does not match what is expected from the workspace
    packetErrFlag = on;
    updateErrorBlink();
}

void MainWindow::updateErrorBlink() {
    //Used to update the STATUS indicator text and red blinking state

    if (!playbackFileOpen) {
        if (!packetErrFlag && !noDataErrFlag && !noHeadstageErrFlag && !unresponsiveHeadstageFlag) {
            toggleErrorBlink(false);
            if (sourceMenuState == SOURCE_STATE_RUNNING) {
                streamStatusColorIndicator->setText("STATUS: Receiving stream ok");
            } else {
                 streamStatusColorIndicator->setText("STATUS: Connected to source");
            }
        } else if (packetErrFlag) {
            streamStatusColorIndicator->setText("ERROR: Wrong incoming packet size");
            toggleErrorBlink(true);
        } else if (noDataErrFlag) {
            streamStatusColorIndicator->setText("ERROR: No data being received");
            toggleErrorBlink(true);

        } else if (noHeadstageErrFlag) {
            streamStatusColorIndicator->setText("ERROR: Headstage data empty");
            toggleErrorBlink(true);

        } else if (unresponsiveHeadstageFlag) {
            streamStatusColorIndicator->setText("ERROR: Headstage unresponsive");
            toggleErrorBlink(true);
        }
    }
}

void MainWindow::toggleErrorBlink(bool blink) {
    //Used to toggle the STATUS indicator red blinking state

    statusBlinkRed = false;
    if (blink) {
        statusErrorBlinkTimer->start(250);
    } else {
        statusErrorBlinkTimer->stop();
        streamStatusColorIndicator->setStyleSheet("QLabel { background-color : white; color : black; border-radius: 5px}");
    }

}

void MainWindow::errorBlinkTmrFunc() {
    if (statusBlinkRed) {
        //change to white
        streamStatusColorIndicator->setStyleSheet("QLabel { background-color : white; color : black; border-radius: 5px}");
        statusBlinkRed = false;
    } else {
        streamStatusColorIndicator->setStyleSheet("QLabel { background-color : red; color : black; border-radius: 5px}");
        statusBlinkRed = true;
    }
}



void MainWindow::audioChannelChanged(int hwchannel)
{
    if (isAudioOn) {
        soundOut->setChannel(hwchannel);
        emit updateAudio();
    }
    //emit setAudioChannel(hwchannel);
}

void MainWindow::nTrodeClicked(int nTrodeInd, Qt::KeyboardModifiers mods) {
    printDebugCheckpoint("Click start");
    int clickedID = activeWorkspacePointers.spikeConf->ntrodes[nTrodeInd]->nTrodeId;
    int clickedIndex = nTrodeInd;
    bool deselected = false; //this bool tracks whether the clicked nTrode was deselected or selected

    QHashIterator<int, int> prevIter(selectedNTrodes);
    while (prevIter.hasNext()) { //set the labels of the previously selected nTrodes to black
        prevIter.next();
        eegDisp->setNTrodeSelected(prevIter.key(), false); //deselect all previous nTrodes
    }
    printDebugCheckpoint("Click set eeg setntrode done");

    if (!(mods & Qt::ControlModifier) && !(mods & Qt::ShiftModifier)) {
        selectedNTrodes.clear(); //if neither control or shift were pressed, overwrite nTrodes
        selectedNTrodes.insert(clickedIndex, clickedID);
    }


    if (mods & Qt::ControlModifier) { //if control clicked
        if (!selectedNTrodes.contains(clickedIndex)) {//if not selected, select it
            selectedNTrodes.insert(clickedIndex, clickedID);
        }
        else {//if previously selected, deselect it
            selectedNTrodes.remove(clickedIndex);
            deselected = true;
        }
    }
    else {//if control was not clicked, overwrite all selected nTrodes
        selectedNTrodes.clear();
    }
    printDebugCheckpoint("Click selected ntrodes done");

    if (mods & Qt::ShiftModifier) { //if shift clicked
//        qDebug() << "Select all between " << spikeConf->ntrodes[prevSelectedNTrodeIndex]->nTrodeId << " and " << clickedID;
        int highInd, lowInd;
        if (prevSelectedNTrodeIndex > clickedIndex) {
            highInd = prevSelectedNTrodeIndex;
            lowInd = clickedIndex;
        }
        else {
            highInd = clickedIndex;
            lowInd = prevSelectedNTrodeIndex;
        }

        //select all nTrodes in between the selected range
        for (int i = lowInd; i <= highInd; i++) {
            int curId = activeWorkspacePointers.spikeConf->ntrodes[i]->nTrodeId;
            if (!selectedNTrodes.contains(i)) //don't select the same nTrode twice
                selectedNTrodes.insert(i,curId);
        }

    }
    else //only overwrite prev selected nTrode if shift was not clicked
        prevSelectedNTrodeIndex = clickedIndex;


    if (!selectedNTrodes.contains(clickedIndex) && !deselected)
        selectedNTrodes.insert(clickedIndex,clickedID);

    QHashIterator<int, int> iter(selectedNTrodes); //the key is the index and the value is the nTrodeID
    while (iter.hasNext()) { //set the labels of the selected nTrodes to red
        iter.next();
        eegDisp->setNTrodeSelected(iter.key(), true);
    }
    selectedNTrodesUpdated(); //update the ntrode settings panel
    printDebugCheckpoint("Click done");
}

//This function processes the actions occuring when right clicking one of the StreamWidgetGL
void MainWindow::processNTrodeRightClickAction(QString actionStr) {
    QString lfpAction = "Toggle LFP display for this nTrode";
    QString spikeAction = "Toggle Spike display for this nTrode";
    QString rawAction = "Toggle raw data display for this nTrode";
    QString stimAction = "Toggle stimulation display for this nTrode";
    QString ntrodeColorAction = "Change nTrode color...";
    QString backgroundColorAction = "Change background color...";

    if (actionStr == "nTrode settings...") {

    } else if (actionStr == ntrodeColorAction) {

        QHashIterator<int, int> iter(selectedNTrodes);
        iter.next();
        int trodeNum = iter.key();
        QColor iniColor = activeWorkspacePointers.spikeConf->ntrodes.at(trodeNum)->color;
        QColor newColor = QColorDialog::getColor(iniColor, this, "Select New Channel Color");
        if (newColor.isValid()) {
            activeWorkspacePointers.spikeConf->setColor(trodeNum, newColor);
            emit sig_channelColorChanged(newColor);
            while (iter.hasNext()) {
                iter.next();
                trodeNum = iter.key();
                activeWorkspacePointers.spikeConf->setColor(trodeNum, newColor);
            }

        }

//        showNtrodeColorSelector(channel);
    } else if (actionStr == backgroundColorAction) {
        QColor newColor = QColorDialog::getColor(activeWorkspacePointers.streamConf->backgroundColor, this, "Select New Background Color");
        if (newColor.isValid())
            activeWorkspacePointers.streamConf->setBackgroundColor(newColor);

    } else if(actionStr == lfpAction){
        QHashIterator<int, int> iter(selectedNTrodes);
        while (iter.hasNext()) {
            iter.next();
            int trodeNum = iter.key();
            activeWorkspacePointers.spikeConf->setLFPModeOn(trodeNum, true);
            activeWorkspacePointers.spikeConf->setSpikeModeOn(trodeNum, false);
            activeWorkspacePointers.spikeConf->setStimViewModeOn(trodeNum, false);
        }
        mixedVisualFiltersOn();

    } else if(actionStr == spikeAction){
        QHashIterator<int, int> iter(selectedNTrodes);
        while (iter.hasNext()) {
            iter.next();
            int trodeNum = iter.key();
            activeWorkspacePointers.spikeConf->setLFPModeOn(trodeNum, false);
            activeWorkspacePointers.spikeConf->setSpikeModeOn(trodeNum, true);
            activeWorkspacePointers.spikeConf->setStimViewModeOn(trodeNum, false);
        }
        mixedVisualFiltersOn();
    } else if(actionStr == rawAction){
        QHashIterator<int, int> iter(selectedNTrodes);
        while (iter.hasNext()) {
            iter.next();
            int trodeNum = iter.key();
            activeWorkspacePointers.spikeConf->setLFPModeOn(trodeNum, false);
            activeWorkspacePointers.spikeConf->setSpikeModeOn(trodeNum, false);
            activeWorkspacePointers.spikeConf->setStimViewModeOn(trodeNum, false);
        }
        mixedVisualFiltersOn();
    } else if(actionStr == stimAction){
        QHashIterator<int, int> iter(selectedNTrodes);
        while (iter.hasNext()) {
            iter.next();
            int trodeNum = iter.key();
            activeWorkspacePointers.spikeConf->setLFPModeOn(trodeNum, false);
            activeWorkspacePointers.spikeConf->setSpikeModeOn(trodeNum, false);
            activeWorkspacePointers.spikeConf->setStimViewModeOn(trodeNum, true);
        }
        mixedVisualFiltersOn();
    }
}

void MainWindow::selectTrode(int nTrode)
{
    if (nTrode != currentTrodeSelected) {
        currentTrodeSelected = nTrode;
        if (singleTriggerWindowOpen) {
            spikeDisp->setShownNtrode(currentTrodeSelected);
        }
    }
}

void MainWindow::streamTabChanged(int tabIndex)
{
    //qDebug() << "Tab changed" << tabIndex;
    lastTabIndex = tabIndex;
}

int MainWindow::findEndOfConfigSection(QString configFileName) {


    QFile file;
    int filePos = -1;

    Q_ASSERT(!configFileName.isEmpty());

    file.setFileName(configFileName);
    if (!file.open(QIODevice::ReadOnly)) {
        return -1;
    }

    QFileInfo fi(configFileName);
    QString ext = fi.suffix();
    if (ext.compare("rec") == 0) {
        //this is a rec file with a configuration in the header

        QString configContent;
        QString configLine;
        bool foundEndOfConfig = false;

        while (file.pos() < 1000000) {
            configLine += file.readLine();
            configContent += configLine;
            if (configLine.contains("</Configuration>") ) {
                foundEndOfConfig = true;
                break;
            }
            configLine = "";
        }

        if (foundEndOfConfig) {
            filePos = file.pos();
        }
    }
    file.close();
    return filePos;
}

void MainWindow::toggleImpedanceTool(bool on) {
    if (on && !impedanceToolOpen) {



        ImpedanceDialog *newAnalysisDialog = new ImpedanceDialog(acquisitionWorkspace);
        newAnalysisDialog->setWindowFlags(Qt::WindowStaysOnTopHint);
        connect(this, SIGNAL(closeAllWindows()), newAnalysisDialog, SLOT(close()));
        connect(this, SIGNAL(closeAnalysisDialog()), newAnalysisDialog, SLOT(close()));
        //connect(soundOut,&AudioController::newSpectrum,newAnalysisDialog,&SignalAnalysisDialog::plot);
        connect(newAnalysisDialog, &ImpedanceDialog::windowClosed,this,&MainWindow::impedanceToolClosed);
        connect(newAnalysisDialog, &ImpedanceDialog::impedanceMeasureRequested,sourceControl,&SourceController::measureImpedance);
        connect(sourceControl,&SourceController::impedanceValueReturned,newAnalysisDialog,&ImpedanceDialog::newImpedanceValue);
        newAnalysisDialog->show();

        impedanceToolOpen = true;

    } else if (on && impedanceToolOpen) {
        //Do nothing
    } else {

        actionDisplayImpedanceTool->setChecked(false);

        emit closeAnalysisDialog();

    }
}

void MainWindow::impedanceToolClosed() {

    impedanceToolOpen = false;
    actionDisplayImpedanceTool->setChecked(false);
}

void MainWindow::toggleSpectralAnalysis(bool on) {
    if (on && !spectrumAnalysisWindowOpen) {



        SignalAnalysisDialog *newAnalysisDialog = new SignalAnalysisDialog();
        newAnalysisDialog->setWindowFlags(Qt::WindowStaysOnTopHint);
        connect(this, SIGNAL(closeAllWindows()), newAnalysisDialog, SLOT(close()));
        connect(this, SIGNAL(closeAnalysisDialog()), newAnalysisDialog, SLOT(close()));
        connect(soundOut,&AudioController::newSpectrum,newAnalysisDialog,&SignalAnalysisDialog::plot);
        connect(newAnalysisDialog, &SignalAnalysisDialog::windowClosed,this,&MainWindow::spectrumAnalysisWindowClosed);
        newAnalysisDialog->show();

        spectrumAnalysisWindowOpen = true;
        soundOut->toggleSpectralAnalysis(true);

    } else if (on && spectrumAnalysisWindowOpen) {
        //Do nothing
    } else {

        actionDisplaySpectrumAnalysis->setChecked(false);

        emit closeAnalysisDialog();

    }
}

void MainWindow::spectrumAnalysisWindowClosed() {

    spectrumAnalysisWindowOpen = false;
    soundOut->toggleSpectralAnalysis(false);
    actionDisplaySpectrumAnalysis->setChecked(false);
}

void MainWindow::toggleRMSPlot(bool on){
    if(on && !RMSPlotOpen){

        RMSPlot *plot;
        if (playbackFileOpen && usingExternalWorkspace) {
            plot = new RMSPlot(playbackExternalWorkspace);
        } else if (playbackFileOpen && !usingExternalWorkspace) {
            plot = new RMSPlot(playbackEmbeddedWorkspace);
        } else {
            plot = new RMSPlot(acquisitionWorkspace);
        }

        plot->setWindowFlag(Qt::WindowStaysOnTopHint);
        connect(this, &MainWindow::closeAllWindows, plot, &RMSPlot::close);
        connect(this, &MainWindow::closeRMSDialog, plot, &RMSPlot::close);
        connect(streamManager, &StreamProcessorManager::sendRMSValues, plot, &RMSPlot::plot);
        connect(plot, &RMSPlot::windowClosed, this, &MainWindow::RMSPlotClosed);
        connect(plot, &RMSPlot::oneSecBin, streamManager, &StreamProcessorManager::setOneSecBin);
        connect(plot, &RMSPlot::tenSecBin, streamManager, &StreamProcessorManager::setTenSecBin);
        plot->show();
        RMSPlotOpen = true;
        streamManager->enableRMSCalculations(true);
    } else if(on && RMSPlotOpen){
    } else{
        emit closeRMSDialog();
    }
}

void MainWindow::RMSPlotClosed(){
    RMSPlotOpen = false;
    actionDisplayRMSPlot->setChecked(false);
    streamManager->enableRMSCalculations(false);
}

bool MainWindow::openPlaybackFile() {
    //Used the saved system settings from the last session as the default folder
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("paths"));
    QStringList tempPathList = settings.value(QLatin1String("prevPlaybackPaths")).toStringList();
    QString tempPath;
    if(tempPathList.isEmpty())
        tempPath = QDir::currentPath();
    else
        tempPath = tempPathList.first();
    settings.endGroup();

#if defined (__linux__)
    QString pFileName = QFileDialog::getOpenFileName(this, tr("Open file for playback"), tempPath, tr("Rec files (*.rec)"),nullptr, QFileDialog::DontUseNativeDialog);
#else
    QString pFileName = QFileDialog::getOpenFileName(this, tr("Open file for playback"), tempPath, tr("Rec files (*.rec)"));
#endif


    if (!pFileName.isEmpty()) {
        //Save the folder in system setting for the next session
        QFileInfo fi(pFileName);
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
        settings.beginGroup(QLatin1String("paths"));
        settings.setValue(QLatin1String("playbackPath"), fi.absoluteFilePath());
        settings.endGroup();
        savePlaybackFilePathToSystem(fi.absoluteFilePath());

        //Load the config file
        return(openPlaybackFile(pFileName));
    }
    return(false);
}

void MainWindow::openPlaybackFileLater()
{


    workspaceShutdownCheckTimer->stop();

    QString fileName = stagedPlaybackFileForLoad;
    int filePos = findEndOfConfigSection(fileName);
    usingExternalWorkspace = false;

    QFileInfo fI(fileName);

    QString baseName = fI.completeBaseName();
    QString workspaceCheckName = fI.absolutePath() + "/"+ baseName + ".trodesconf";

    QFileInfo workspaceFile(workspaceCheckName);
    if (workspaceFile.exists()) {
        qDebug() << "[MainWindow::openPlaybackFile] Using the following workspace file: " << workspaceFile.fileName();
        usingExternalWorkspace = true;

    }





    //TODO-- we need to check to make sure the source controller and streamProcessor threads have actually ended.
    //If they have not, we will get a crash as soon as we mess with the workspace
    /*if (sourceControl->currentSourceObj != nullptr) {
        if (sourceControl->currentSourceObj->isThreadRunning()) {
            workspaceShutdownCheckTimer->start(500);
            printDebugCheckpoint("Source not finished. Restaring timer.");
            return;
        }
    }*/

    workspaceShutdownCheckTimer->deleteLater();




    if (liveReconfigHappening) {

        streamManager->removeAllProcessors();

        delete streamManager;
        delete spikeDisp;
        spikeDisp = nullptr;
    }

    //We remove the previous stream display now to prevent a long time with an empty screen
    //remove each display tab
    while (tabs->count() > 0) {
        tabs->removeTab(0);
    }

    if (liveReconfigHappening) {
        if (eegDisp != nullptr) delete eegDisp;
    }


    //Open up the configuration settings used when the
    //data were recorded. x current workspace.
    /*if (channelsConfigured) {
        closeConfig();
    }*/


    playbackEmbeddedWorkspace.clear();
    playbackExternalWorkspace.clear();

//        int loadReturn;
    playbackFileOpen = true; //we set this global flag before opening the config file, because that way we can use it to stop opening modules
    if (usingExternalWorkspace) {
        QString errString = playbackExternalWorkspace.readTrodesConfig(workspaceFile.absoluteFilePath());
        if (!errString.isEmpty()) {
            qDebug() << "There was an error parsing" << workspaceCheckName << "." << errString;
            showErrorMessage(errString);
            actionSourceNone->setChecked(true);
            sourceControl->setSource(SourceNone);
            playbackFileOpen = false;
            splashScreen->setCanLoadFile(true);
            return;
        }


    } else {
       //No external workspace with the same name found, so try to load the settings
       //embedded in the recording file.  NOTE: this will also be removed soon
       //--Cut here--
       //filePos = loadConfig(fileName);
       //-----------
    }
    QString errString = playbackEmbeddedWorkspace.readTrodesConfig(fileName);
    if (!errString.isEmpty()) {
        qDebug() << "There was an error parsing" << fileName << "." << errString;
        showErrorMessage(errString);
        actionSourceNone->setChecked(true);
        sourceControl->setSource(SourceNone);
        playbackFileOpen = false;
        splashScreen->setCanLoadFile(true);
        return;
    }



    //The global configuration contains session-specific information, so it must be loaded from the embedded workspace in the recording file
    activeWorkspacePointers.globalConf = &playbackEmbeddedWorkspace.globalConf;
    globalConf = &playbackEmbeddedWorkspace.globalConf;

    if (usingExternalWorkspace) {
        activeWorkspacePointers.streamConf = &playbackExternalWorkspace.streamConf;
        activeWorkspacePointers.headerConf = &playbackExternalWorkspace.headerConf;
        activeWorkspacePointers.hardwareConf = &playbackExternalWorkspace.hardwareConf;
        activeWorkspacePointers.moduleConf = &playbackExternalWorkspace.moduleConf;
        activeWorkspacePointers.spikeConf = &playbackExternalWorkspace.spikeConf;
        activeWorkspacePointers.networkConf = &playbackExternalWorkspace.networkConf;
        activeWorkspacePointers.benchConfig = &playbackExternalWorkspace.benchConfig;
        //activeWorkspace = &playbackExternalWorkspace;
        //...except for the realtimeMode in the globalconf. that needs to be changed.
        globalConf->realTimeMode = playbackExternalWorkspace.globalConf.realTimeMode;

        streamConf = &playbackExternalWorkspace.streamConf;
        headerConf = &playbackExternalWorkspace.headerConf;
        hardwareConf = &playbackExternalWorkspace.hardwareConf;
        moduleConf = &playbackExternalWorkspace.moduleConf;
        spikeConf = &playbackExternalWorkspace.spikeConf;
        networkConf = &playbackExternalWorkspace.networkConf;
        benchConfig = &playbackExternalWorkspace.benchConfig;
    } else {
        activeWorkspacePointers.streamConf = &playbackEmbeddedWorkspace.streamConf;
        activeWorkspacePointers.headerConf = &playbackEmbeddedWorkspace.headerConf;
        activeWorkspacePointers.hardwareConf = &playbackEmbeddedWorkspace.hardwareConf;
        activeWorkspacePointers.moduleConf = &playbackEmbeddedWorkspace.moduleConf;
        activeWorkspacePointers.spikeConf = &playbackEmbeddedWorkspace.spikeConf;
        activeWorkspacePointers.networkConf = &playbackEmbeddedWorkspace.networkConf;
        activeWorkspacePointers.benchConfig = &playbackEmbeddedWorkspace.benchConfig;
        //activeWorkspace = &playbackEmbeddedWorkspace;

        streamConf = &playbackEmbeddedWorkspace.streamConf;
        headerConf = &playbackEmbeddedWorkspace.headerConf;
        hardwareConf = &playbackEmbeddedWorkspace.hardwareConf;
        moduleConf = &playbackEmbeddedWorkspace.moduleConf;
        spikeConf = &playbackEmbeddedWorkspace.spikeConf;
        networkConf = &playbackEmbeddedWorkspace.networkConf;
        benchConfig = &playbackEmbeddedWorkspace.benchConfig;
    }




    bool displayLoadSuccess = loadWorkspaceToDisplay();



    actionCloseFile->setEnabled(true);
    playbackFile = fileName;
    fileString = fI.fileName();

    fileLabel->setText(fileString);

    fileString = fI.absoluteFilePath();

    /*
    //Automatically open video file in camera module
    if(playbackFileOpen && !unitTestFlag){
        QFileInfo file(fileName);
        QDir dir = file.absoluteDir();
        QStringList filters;
        filters << "*.h264";

        QFileInfoList files = dir.entryInfoList(filters);
        for (int i = 0; i < files.size(); ++i) {
            QFileInfo fileInfo = files.at(i);
            if(QString::compare(fileInfo.absoluteFilePath().replace(QRegExp("(\\.\\d)*\\.h264"), ".rec"), file.absoluteFilePath()) == 0){
                videoButtonPressed(fileInfo.absoluteFilePath());
//                    break;
            }
        }
    }*/

    fileDataPos = playbackEmbeddedWorkspace.dataStartLoc;
    actionSourceFile->setChecked(true);
    sourceControl->setSource(SourceFile);
    actionPlay->setEnabled(true);
    if (playbackEmbeddedWorkspace.hardwareConf.NCHAN == 0) {
        //We enable the merge menu if this playback workspace was for a recording
        //with no headstage attached (0 channels). The neural channels were probably recorded
        //on a logger, and the two files need to be merged.
        actionMerge->setEnabled(true);
    }

    playButton->setEnabled(true);
    pauseButton->setEnabled(true);
    playbackInfoButton->setEnabled(true);
    menuExport->setEnabled(true);
    actionSourceNone->setChecked(false);
    actionSourceFake->setChecked(false);
    actionSourceFakeSpikes->setChecked(false);
    actionSourceUSB->setChecked(false);
    actionSourceUSB3->setChecked(false);
    actionSourceDockUSB->setChecked(false);
    actionSourceRhythm->setChecked(false);
    actionSourceEthernet->setChecked(false);
    actionOpenGeneratorDialog->setEnabled(false);


    recordButton->setVisible(false);
    playButton->setVisible(true);
    pauseButton->setVisible(true);
    playbackInfoButton->setVisible(true);
    actionAboutPlaybackFile->setVisible(true);

    connect(tabs, &QTabWidget::currentChanged, this, &MainWindow::streamTabChanged);

    //we could in priciple record data from the playback, but this seems like an odd thing
    //to do.  If we don't allow this, we can use the pause button for both playback and record.
    actionOpenRecordDialog->setEnabled(false);
    swapSplashScreenAndTabView(false);

}

bool MainWindow::openPlaybackFile(const QString fileName)
{
    qDebug() << "[MainWindow::openPlaybackFile] Opening file" << fileName;

    if ((!playbackFileOpen) && (!recordFileOpen)) {

        if (channelsConfigured) {

            reConfig_live_close();
            channelsConfigured = false;
        }
        stagedPlaybackFileForLoad = fileName;
        workspaceShutdownCheckTimer = new QTimer(this);
        connect(workspaceShutdownCheckTimer, &QTimer::timeout, this, &MainWindow::openPlaybackFileLater);
        workspaceShutdownCheckTimer->start(1000); //We need to allow enough time for the source controller and streamProcessor threads to end

    }

    return true;
}

bool MainWindow::openWorkspaceFileForAcquisition() {
    //    if (currentConfigFileName != "") {
    //        closeConfig(false); //automatically close configuration files if they are open, also don't bring up splashScreen
    //    }
        QString multInstError = "Error: An active Trodes server host was detected. Please close any other currently running instances of Trodes before continuing.";
    //    if (!unitTestFlag && checkMultipleInstances(multInstError))
    //        return;

        //Used the saved system settings from the last session as the default folder
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
        settings.beginGroup(QLatin1String("paths"));
        QStringList tempPathList = settings.value(QLatin1String("prevWorkspacePaths")).toStringList();
        QString tempPath;
        if(tempPathList.isEmpty())
            tempPath = QDir::currentPath();
        else
            tempPath = tempPathList.first();
        settings.endGroup();

#if defined (__linux__)
        QString fileName = QFileDialog::getOpenFileName(this, QString("Open configuration file"), tempPath, "Trodes config files (*.trodesconf *.xml)",nullptr, QFileDialog::DontUseNativeDialog);
#else
        QString fileName = QFileDialog::getOpenFileName(this, QString("Open configuration file"), tempPath, "Trodes config files (*.trodesconf *.xml)");
#endif
        if (!fileName.isEmpty()) {
            //Save the folder in system setting for the next session
            QFileInfo fi(fileName);
            QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
            settings.beginGroup(QLatin1String("paths"));
            settings.setValue(QLatin1String("configPath"), fi.absoluteFilePath());
            settings.endGroup();
            //save the configuration path the previously used workspaces list
            saveWorkspacePathToSystem(fi.absoluteFilePath());
            //Load the config file
            return openWorkspaceFileForAcquisition(fileName);

        }
        return false;
}

bool MainWindow::openWorkspaceFileForAcquisition(const TrodesConfiguration &c)
{



    closeConfig(false); //automatically close configuration files if they are open, also don't bring up splashScreen
    // This is used to load a configuration and create all the control/display widgets
    // according to the settings in the config file
    QString multInstError = "Error: An active Trodes server host was detected. Please close any other currently running instances of Trodes before continuing.";
    if (!unitTestFlag && checkMultipleInstances(multInstError)) {
        swapSplashScreenAndTabView(true); //return to the splash screen if load failed
        return false;
    }
//    splashScreen->hide();
    //loadedConfigFile = fileName;

    //Read the config .xml file; the second argument specifies that this is being called within trodes
    acquisitionWorkspace.clear();
    acquisitionWorkspace = c;

    //QString errString = acquisitionWorkspace.readTrodesConfig(fileName);

    /*if (!errString.isEmpty()) {
        qDebug() << "There was an error parsing" << fileName << "." << errString;
        showErrorMessage(errString);
        splashScreen->setCanLoadFile(true);
        return false;
    }*/


    //currentConfigFileName = fileName;
    configSettingsChanged = true;

    activeWorkspacePointers.globalConf = &acquisitionWorkspace.globalConf;
    activeWorkspacePointers.streamConf = &acquisitionWorkspace.streamConf;
    activeWorkspacePointers.headerConf = &acquisitionWorkspace.headerConf;
    activeWorkspacePointers.hardwareConf = &acquisitionWorkspace.hardwareConf;
    activeWorkspacePointers.moduleConf = &acquisitionWorkspace.moduleConf;
    activeWorkspacePointers.spikeConf = &acquisitionWorkspace.spikeConf;
    activeWorkspacePointers.networkConf = &acquisitionWorkspace.networkConf;
    activeWorkspacePointers.benchConfig = &acquisitionWorkspace.benchConfig;
    //activeWorkspace = &acquisitionWorkspace;

    globalConf = &acquisitionWorkspace.globalConf;
    streamConf = &acquisitionWorkspace.streamConf;
    headerConf = &acquisitionWorkspace.headerConf;
    hardwareConf = &acquisitionWorkspace.hardwareConf;
    moduleConf = &acquisitionWorkspace.moduleConf;
    spikeConf = &acquisitionWorkspace.spikeConf;
    networkConf = &acquisitionWorkspace.networkConf;
    benchConfig = &acquisitionWorkspace.benchConfig;

    bool displayLoadSuccess = loadWorkspaceToDisplay();

    return displayLoadSuccess;

}

bool MainWindow::openWorkspaceFileForAcquisition(const QString fileName)
{

    qDebug() << "Loading workspace for acquisition.";
    closeConfig(false); //automatically close configuration files if they are open, also don't bring up splashScreen
    // This is used to load a configuration and create all the control/display widgets
    // according to the settings in the config file
    QString multInstError = "Error: An active Trodes server host was detected. Please close any other currently running instances of Trodes before continuing.";
    if (!unitTestFlag && checkMultipleInstances(multInstError)) {
        swapSplashScreenAndTabView(true); //return to the splash screen if load failed
        return false;
    }
//    splashScreen->hide();
    loadedConfigFile = fileName;

    //Read the config .xml file; the second argument specifies that this is being called within trodes
    acquisitionWorkspace.clear();
    QString errString = acquisitionWorkspace.readTrodesConfig(fileName);

    if (!errString.isEmpty()) {
        qDebug() << "There was an error parsing" << fileName << "." << errString;
        showErrorMessage(errString);
        splashScreen->setCanLoadFile(true);
        return false;
    }


    currentConfigFileName = fileName;
    configSettingsChanged = false;

    activeWorkspacePointers.globalConf = &acquisitionWorkspace.globalConf;
    activeWorkspacePointers.streamConf = &acquisitionWorkspace.streamConf;
    activeWorkspacePointers.headerConf = &acquisitionWorkspace.headerConf;
    activeWorkspacePointers.hardwareConf = &acquisitionWorkspace.hardwareConf;
    activeWorkspacePointers.moduleConf = &acquisitionWorkspace.moduleConf;
    activeWorkspacePointers.spikeConf = &acquisitionWorkspace.spikeConf;
    activeWorkspacePointers.networkConf = &acquisitionWorkspace.networkConf;
    activeWorkspacePointers.benchConfig = &acquisitionWorkspace.benchConfig;

    globalConf = &acquisitionWorkspace.globalConf;
    streamConf = &acquisitionWorkspace.streamConf;
    headerConf = &acquisitionWorkspace.headerConf;
    hardwareConf = &acquisitionWorkspace.hardwareConf;
    moduleConf = &acquisitionWorkspace.moduleConf;
    spikeConf = &acquisitionWorkspace.spikeConf;
    networkConf = &acquisitionWorkspace.networkConf;
    benchConfig = &acquisitionWorkspace.benchConfig;

    //acquisitionWorkspace.printChannelOrderInPacket(); //Used to list the expected order of the nTrodes in the packet.

    bool displayLoadSuccess = loadWorkspaceToDisplay();

    return displayLoadSuccess;



}

void MainWindow::threadError(QString errorString) {
    // show a box with the error message
    if (!unitTestFlag) {
        QMessageBox::warning(this, "Error", errorString);
    }
}

void MainWindow::openRecordDialog()
{
    //dialog to create a new record file
    QString dataDir = activeWorkspacePointers.globalConf->filePath;
    QString fileName;
    QString defaultFileName;
    QDateTime fileCreateTime = QDateTime::currentDateTime();



    if (dataDir.isEmpty()) {

        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
        settings.beginGroup(QLatin1String("paths"));
        dataDir = settings.value(QLatin1String("prevRecordPath")).toString();

        if(dataDir.isEmpty()) {
            dataDir = QDir::homePath();
        }
        settings.endGroup();

    }
    else {
        dataDir = QDir(dataDir).absolutePath();
    }


    //defaultFileName = globalConf->filePrefix + fileCreateTime.toString("yyyy.MM.dd").replace(".", "") +
    //                  tr("_") + fileCreateTime.toString("hh.mm.ss").replace(".", "") + tr(".rec");

    defaultFileName = activeWorkspacePointers.globalConf->filePrefix + fileCreateTime.toString("yyyy.MM.dd").replace(".", "") +
                      tr("_") + fileCreateTime.toString("hh.mm.ss").replace(".", "");
    dataDir = QDir().toNativeSeparators(dataDir + "/" + defaultFileName);
    qDebug() << "[MainWindow::openRecordDialog] Default file " << dataDir;


    //This is the name of the folder to be created

#if defined (__linux__)
    fileName = QFileDialog::getSaveFileName(this, tr("Create File"), dataDir,"Trodes recording (*.rec)",nullptr, QFileDialog::DontUseNativeDialog);
#else
    fileName = QFileDialog::getSaveFileName(this, tr("Create File"), dataDir,"Trodes recording (*.rec)");
#endif



    if (!(fileName.isEmpty())) {

        //Store the path for the next Trodes session
        QString chosenDir = QFileInfo(fileName).dir().absolutePath();
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
        settings.beginGroup(QLatin1String("paths"));
        settings.setValue(QLatin1String("prevRecordPath"), chosenDir);
        settings.endGroup();

        if(preferencesPanel->createSubDirectory()){
            //Find the base name of the folder
            QFileInfo filenameInfo(fileName);
            QString basename = filenameInfo.baseName();

            //Create the folder
            QDir dir;
            dir.mkpath(fileName);

            //The filename will be the same as the folder name
            fileName = fileName + "/" + basename;
        }


        if (!fileName.endsWith(".rec")) {
            fileName += ".rec";
        }

        QFileInfo fInfo(fileName);
        recordFileBaseName = fInfo.absolutePath() + "/" + fInfo.completeBaseName();
        currentFilePart = 1; //This is the first part, if we are breaking up into multiple files.
        openRecordFile(fileName);

        //HeadstageSettings hsSettings = sourceControl->getHeadstageSettings();
        //HardwareControllerSettings hcSettings = sourceControl->getControllerSettings();

        /*int fileOpenStatus = recordOut->openFile(fileName, controllerSettings, headstageSettings); //creates the file and writes the current config info
        if (fileOpenStatus == -1) {
            QMessageBox::information(nullptr, "error", tr("File already exists. Please rename existing file first."));
            return;
        }
        if (fileOpenStatus == -2) {
            QMessageBox::information(nullptr, "error", tr("Error: File could not be created."));
            return;
        }

        recordFileOpen = true;
        recordFileName = fileName;
        lastRecordFileName = fileName;
        currentFilePart = 1; //This is the first part, if we are breaking up into multiple files.
        //Comment dialog
        QFileInfo fileInfo(recordFileName);
        QString commentFileName = fileInfo.absolutePath() + "/" + fileInfo.completeBaseName() + ".trodesComments";
        newCommentDialog->enableCommenting(commentFileName);

        QFileInfo fI(fileName);
        fileString = fI.fileName();
        fileLabel->setText(fileString + tr("   (0 MB)")); //append the current size of the file in the display

        fileStatusColorIndicator->setStyleSheet("QLabel { background-color : yellow; color : black; border-radius: 5px}");
        fileStatusColorIndicator->setText("Recording Paused");
        fileStatusColorIndicator->setVisible(true);
        actionOpenRecordDialog->setEnabled(false);
        actionCloseFile->setEnabled(true);
        actionCloseConfig->setEnabled(false);
        actionLoadConfig->setEnabled(false);
        actionReConfig->setEnabled(false);
        actionRestartModules->setEnabled(false);
        statescriptButton->setEnabled(false);

        recordButton->setVisible(true);
        playButton->setVisible(false);
        playbackInfoButton->setVisible(false);
        pauseButton->setVisible(true);


        if (dataStreaming) {
            actionRecord->setEnabled(true);
            recordButton->setEnabled(true);
            pauseButton->setEnabled(true);
            pauseButton->setDown(true);
        }
        recordTimer = new QTime;
        msecRecorded = 0;
        emit recordFileOpened(fileName);
        */

    }
}

void MainWindow::openRecordFile(QString fileName)
{
    int fileOpenStatus = recordOut->openFile(fileName, controllerSettings, headstageSettings); //creates the file and writes the current config info
    if (fileOpenStatus == -1) {
        QMessageBox::information(nullptr, "error", tr("File already exists. Please rename existing file first."));
        return;
    }
    if (fileOpenStatus == -2) {
        QMessageBox::information(nullptr, "error", tr("Error: File could not be created."));
        return;
    }

    recordFileOpen = true;
    recordFileName = fileName;
    lastRecordFileName = fileName;
    //Comment dialog
    QFileInfo fileInfo(recordFileName);
    QString commentFileName = fileInfo.absolutePath() + "/" + fileInfo.completeBaseName() + ".trodesComments";
    newCommentDialog->enableCommenting(commentFileName);

    QFileInfo fI(fileName);
    fileString = fI.fileName();
    fileLabel->setText(fileString + tr("   (0 MB)")); //append the current size of the file in the display

    fileStatusColorIndicator->setStyleSheet("QLabel { background-color : yellow; color : black; border-radius: 5px}");
    fileStatusColorIndicator->setText("Recording Paused");
    fileStatusColorIndicator->setVisible(true);
    actionOpenRecordDialog->setEnabled(false);
    actionCloseFile->setEnabled(true);
    actionCloseConfig->setEnabled(false);
    actionLoadConfig->setEnabled(false);
    actionReConfig->setEnabled(false);
    actionRestartModules->setEnabled(false);
    statescriptButton->setEnabled(false);

    recordButton->setVisible(true);
    playButton->setVisible(false);
    playbackInfoButton->setVisible(false);
    pauseButton->setVisible(true);


    if (dataStreaming) {
        actionRecord->setEnabled(true);
        recordButton->setEnabled(true);
        pauseButton->setEnabled(true);
        pauseButton->setDown(true);
    }
    recordTimer = new QElapsedTimer;
    msecRecorded = 0;
    emit recordFileOpened(fileName);

}

void MainWindow::openQueuedRecordFile()
{
    openRecordFile(queuedRecordFileName);
}

void MainWindow::createNextFilePart()
{
//This is used to break the recording up into multiple parts of equal sizes.
//Signals are sent out to modules to create new files as well.


    if (recordFileOpen) {

        queuedRecordFileName = recordFileBaseName+QString(".part%1").arg(currentFilePart+1)+".rec";
        closeFile();


        //We create the next file and start recording a bit later to give modules time to stop recording, etc.
        QTimer::singleShot(1000, this, &MainWindow::openQueuedRecordFile);
        QTimer::singleShot(1500, this, &MainWindow::actionRecordSelected);

        currentFilePart++;

        //actionRecordSelected();

    }



}

void MainWindow::exportData(bool spikesOn, bool ModuleDataon, int triggerSetting, int noiseSetting, int ModuleDataChannelSetting, int ModuleDataFilterSetting)
{

    //TODO: many of the inputs are not yet used.  Don't delete!!

    if (playbackFileOpen && !dataStreaming) {
        qDebug() << "[MainWindow::exportData] Exporting: " << "Spikes: " << spikesOn << " ModuleData: " << ModuleDataon;
        QFileInfo fileInfo;
        fileInfo.setFile(playbackFile);
        QString baseName = fileInfo.completeBaseName();
        QDir fileDir = fileInfo.absoluteDir();
        if (spikesOn) {
            QString spikeDirName = baseName + "_Spikes";
            fileDir.mkdir(spikeDirName);
            if (!fileDir.cd(spikeDirName)) {
                qDebug() << "[MainWindow::exportData] Error making spike directory";
                return;
            }
            streamManager->createSpikeLogs(fileDir.absolutePath());
        }
        qDebug() << "[MainWindow::exportData] Path: " << fileDir.path() << " Name: " << fileInfo.completeBaseName();

        disconnectFromSource();
        exportMode = true;
        connectToSource();
    }
}

void MainWindow::cancelExport()
{

    disconnectFromSource();
    exportMode = false;
}

void MainWindow::bufferOverrunHandler()
{
    if (!exportMode) {
        //disconnectFromSource();
        //qDebug() << "Data streaming stopped-- data rate is too fast";
        qDebug() << "[MainWindow::bufferOverrunHandler] Buffer overrun!";
    }
    else {
        sourceControl->waitForThreads();
    }
}

void MainWindow::showWarningMessage(QString msg) {
    //Multi-purpose error dialog
    if (!unitTestFlag) {
        QMessageBox messageBox;
        messageBox.warning(nullptr,"Warning",msg);
        messageBox.setFixedSize(500,200);
    }
}

void MainWindow::showErrorMessage(QString msg) {
    //Multi-purpose error dialog
    if (!unitTestFlag) {
        QMessageBox messageBox;
        messageBox.critical(nullptr,"Error",msg);
        messageBox.setFixedSize(500,200);
    }
}

void MainWindow::errorSaving() {
    //pauseButtonPressed();
    showErrorMessage(QString("Serious error writing to disk.  A brief period of data was lost because the hard drive became unresponive. See debug log for more information. Please report to SpikeGadgets support team."));
}

void MainWindow::noSpaceLeftWhileSaving() {
    pauseButtonPressed();
    showErrorMessage(QString("There is no space left on the drive. The recording has been paused.  Please free up space before continuing the recording."));
}

void MainWindow::lowDiskSpaceWarning() {
    showErrorMessage(QString("You are running out is disk space! Please free up space now."));
}

void MainWindow::closeFile()
{
    if (playbackFileOpen) {
        setSource(SourceNone);
    } else {
        pauseButtonPressed();
        settingsWarning();
        recordOut->closeFile();
        recordFileOpen = false;
        QString FileLabelColor("gray");
        QString FileLabelText("No recording file open");
        QString fileLabelTextTemplate = tr("<font color='%1'>%2</font>");
        fileStatusColorIndicator->setStyleSheet("QLabel { background-color : yellow; color : black; border-radius: 5px}");

        fileStatusColorIndicator->setText("Recording Paused");
        fileStatusColorIndicator->setVisible(false);
        fileLabel->setText(fileLabelTextTemplate.arg(FileLabelColor, FileLabelText));
        actionOpenRecordDialog->setEnabled(true);
        actionCloseFile->setEnabled(false);
        actionRecord->setEnabled(false);
        recordButton->setEnabled(false);
        actionPause->setEnabled(false);
        pauseButton->setEnabled(false);
        playbackInfoButton->setEnabled(false);
        actionRestartModules->setEnabled(true);
        statescriptButton->setEnabled(true);


        recordButton->setVisible(false);
        playButton->setVisible(false);
        pauseButton->setVisible(false);
        playbackInfoButton->setVisible(false);



        totalTimeRecorded = 0;

        if (channelsConfigured) {
            actionCloseConfig->setEnabled(true);
            actionLoadConfig->setEnabled(true);
        }

        newCommentDialog->enableCommenting("");
        emit recordFileClosed();
    }
}

void MainWindow::recordButtonPressed()
{
    pauseButton->setDown(false);
    actionPause->setEnabled(true);
    actionRecord->setEnabled(false);
    lastPlayPauseTime = currentTimeStamp;
    recordOut->startRecord();
    actionCloseFile->setEnabled(false);
    recording = true;
    actionDisconnect->setEnabled(false);
    statusbar->showMessage(tr("Started recording at ") + calcTimeString(currentTimeStamp));
    qDebug() << statusbar->currentMessage();
    fileStatusColorIndicator->setStyleSheet("QLabel { background-color : lightgreen; color : black; border-radius: 5px}");

    fileStatusColorIndicator->setText("Recording Active");
    emit recordingStarted();
    recordTimer->restart();
    videoButton->setEnabled(false);//disable video button when recording

}
void MainWindow::recordButtonReleased()
{
    recordButton->setDown(true);
    recordButton->setEnabled(false);
}
void MainWindow::actionRecordSelected()
{
    recordButton->setDown(true);
    recordButtonPressed();
}


void MainWindow::pauseButtonPressed()
{
    if (recordFileOpen) {
        recordButton->setDown(false);
        actionPause->setEnabled(false);
        actionDisconnect->setEnabled(true);
        actionCloseFile->setEnabled(true);
        totalTimeRecorded += (currentTimeStamp-lastPlayPauseTime);
        lastPlayPauseTime = currentTimeStamp;
        recordOut->pauseRecord();
        recording = false;
        actionRecord->setEnabled(true);
        recordButton->setEnabled(true);
        statusbar->showMessage(tr("Paused recording at ") + calcTimeString(currentTimeStamp));
        fileStatusColorIndicator->setStyleSheet("QLabel { background-color : yellow; color : black; border-radius: 5px}");
        qDebug() << statusbar->currentMessage();
        fileStatusColorIndicator->setText("Recording Paused");
        emit recordingStopped();
        msecRecorded += recordTimer->elapsed();
        recordTimer->restart();
        videoButton->setEnabled(true);
    }
    else if (playbackFileOpen) {
        actionPause->setEnabled(false);
        actionPlay->setEnabled(true);
        if (playbackEmbeddedWorkspace.hardwareConf.NCHAN == 0) {
            //We enable the merge menu if this playback workspace was for a recording
            //with no headstage attached (0 channels). The neural channels were probably recorded
            //on a logger, and the two files need to be merged.
            actionMerge->setEnabled(true);
        }
        playButton->setDown(false);
        sourceControl->pauseSource();
        menuExport->setEnabled(true);
        emit signal_sendPlaybackCommand(PC_PAUSE, currentTimeStamp);
    }
}
void MainWindow::pauseButtonReleased()
{
    pauseButton->setDown(true);
}
void MainWindow::actionPauseSelected()
{
    pauseButton->setDown(true);
    pauseButtonPressed();
}

void MainWindow::playButtonPressed()
{

    if (!dataStreaming) {
        pauseButton->setDown(false);
        actionPause->setEnabled(true);
        actionPlay->setEnabled(false);
        actionMerge->setEnabled(false);
        menuExport->setEnabled(false); //We don't allow exporting when the file is being played back
        filePlaybackSpeed = 1; //Normal speed
        exportMode = false;

        connectToSource();
        emit signal_sendPlaybackCommand(PC_PLAY, currentTimeStamp);
    }
}
void MainWindow::playButtonReleased()
{
    playButton->setDown(true);
}
void MainWindow::actionPlaySelected()
{
    playButton->setDown(true);
    playButtonPressed();
}

void MainWindow::actionMergeSelected()
{
    //The user wants to merge the current playback file with neural data collected from a data logger
    //This function starts a separate process running the DataLoggerGUI program
    qDebug() << "Opening data logger GUI for merge.";

    //Get the directory containing the calling executable (usu. Trodes)
    QString callingAppDir = QCoreApplication::applicationDirPath();
    QString absProgramName;
#ifdef __APPLE__
    //If calling app is an app bundle, we have more folders to navigate through
    absProgramName = QDir::cleanPath(callingAppDir + "/../../../DataLoggerGUI.app/Contents/MacOS/DataLoggerGUI");
#else
   absProgramName= QDir::cleanPath(callingAppDir + "/" + "DataLoggerGUI");
#endif

    QStringList processArguments;
    processArguments << playbackFile; //The first argument should be the path to the playback file

    //Exporting is done by starting a standalone program
    mergeProcess = new QProcess(this);

    connect(mergeProcess,SIGNAL(readyReadStandardOutput()),this,SLOT(readDataFromMergeProcess()));
    connect(mergeProcess,SIGNAL(readyReadStandardError()),this,SLOT(readDataFromMergeProcess()));
    //connect(mergeProcess,SIGNAL(finished(int)),this,SLOT(mergeProgramFinished(int)));
    connect(mergeProcess,SIGNAL(finished(int)),mergeProcess,SLOT(deleteLater()));
    connect(mergeProcess,SIGNAL(finished(int)),this,SLOT(openMergedFile()));

    connect(this, SIGNAL(closeAllWindows()), mergeProcess, SLOT(kill()));
    //connect(mergeProcess,SIGNAL(started()),this,SLOT(mergeProgramStarted()));

    mergeSucessful = false;
    mergeProcess->start(absProgramName,processArguments);
}

void MainWindow::openMergedFile()
{
    if (mergeSucessful) {
        qDebug() << "Merge successful. Switching playback to merged file:" << lastRecordFileName;
        setSource(SourceNone);
        setSource(SourceFile);
    }
}

void MainWindow::readDataFromMergeProcess()
{
    //This function monitors the text output of the DataLoggerGUI program
    QString output(mergeProcess->readAllStandardOutput());
    QString outputErr(mergeProcess->readAllStandardError());

    if (outputErr.contains("Extract finished successfully:")) {

        //The merge was successful, so we open the merged file for playback.
        QStringList splitOutput = outputErr.split("\"");
        if (splitOutput.length() > 1) {

            lastRecordFileName = splitOutput.at(1).trimmed();
            mergeSucessful = true;


        }

    }



}

void MainWindow::sendSettleCommand() {
    sourceControl->sendSettleCommand();
}

bool MainWindow::disconnectFromSource()
{
    if (recordFileOpen) {
        if (recordOut->getBytesWritten() > 0) {
            //There is a record file open with data in it
            //This needs to be closed
            //If No pressed, then return and ignore closeEvent event

//            QMessageBox messageBox;
//            int answer = messageBox.question(0, "File is open", "The record file will be closed. Proceed?");
            QMessageBox::StandardButton answer = QMessageBox::question(nullptr, "File is open", "This action will also close the recording file that currently open. Proceed?",  QMessageBox::Yes|QMessageBox::No);
            //messageBox.setFixedSize(500,200);
            if (answer == QMessageBox::Yes) {
                closeFile();
            }
            else {
                //"No" or messagebox closed
                return false;
            }
        }
    }

    if (playbackFileOpen) {
        actionPlay->setEnabled(true);
        if (playbackEmbeddedWorkspace.hardwareConf.NCHAN == 0) {
            //We enable the merge menu if this playback workspace was for a recording
            //with no headstage attached (0 channels). The neural channels were probably recorded
            //on a logger, and the two files need to be merged.
            actionMerge->setEnabled(true);
        }
        actionPause->setEnabled(false);
        pauseButton->setDown(true);
        playButton->setDown(false);
        menuExport->setEnabled(false);
    }

    if(triggerSettings) triggerSettings->streamingStopped();
    //disconnect from source

    qDebug() << "Disconnecting from source.";
    sourceControl->disconnectFromSource();
    return true;
}

void MainWindow::connectToSource()
{
    if(triggerSettings) triggerSettings->streamingStarted();
    sourceControl->connectToSource();

}

void MainWindow::setSource()
{
    //source selected with menu ( wrapper for setSource(int) )
    QAction* action = (QAction*)sender();

    setSource(action->data().value<DataSource>());
}

void MainWindow::setSource(DataSource source)
{
    //changes the source of the data stream

    if (source != sourceControl->currentSource) {
        //set the menu state
        actionSourceNone->setChecked(false);
        actionSourceFake->setChecked(false);
        actionSourceFakeSpikes->setChecked(false);
        actionSourceFile->setChecked(false);
        actionSourceUSB->setChecked(false);
        actionSourceUSB3->setChecked(false);
        actionSourceDockUSB->setChecked(false);
        actionSourceRhythm->setChecked(false);
        actionSourceEthernet->setChecked(false);
        actionOpenGeneratorDialog->setEnabled(false);


        bool needToClosePlaybackConfig = false;
        if (playbackFileOpen && (source != SourceFile)) {
            needToClosePlaybackConfig = true;

        }

        //Used the saved system settings from the last session as the default folder
        QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
        settings.beginGroup(QLatin1String("paths"));
        QString tempPath = settings.value(QLatin1String("playbackPath")).toString();
        settings.endGroup();
        QString pFileName;

        switch (source) {
        case SourceNone:
            if(sourceControl->currentSource == SourceDockUSB){
                //rfwidget->dockDisconnected(sourceControl->dockUSBSource->dockSettings.serial, "USB");
                //rfwidget->hide();
                //actionDisplayRFWidget->trigger();
                if(recordFileOpen){
                    closeFile();
                }
            }

            headstageSettings.valid = false;
            controllerSettings.valid = false;
            actionSourceNone->setChecked(true);
            sourceControl->setSource(source);
            actionSourceNone->setEnabled(true);
            actionSourceFake->setEnabled(true);
            actionSourceFile->setEnabled(true);
            actionPlaybackOpen->setEnabled(true);
            actionSourceFakeSpikes->setEnabled(true);
            actionSourceEthernet->setEnabled(true);
            actionSourceUSB->setEnabled(true);
            actionSourceUSB3->setEnabled(true);
            actionSourceDockUSB->setEnabled(true);
            actionSourceRhythm->setEnabled(true);
            if (!recordFileOpen) {
                actionCloseFile->setEnabled(false);
            }
            playbackSlider->setVisible(false);
            playbackSlider->setEnabled(false);
            playbackSlider->setValue(0);
            playbackStartTimeLabel->setVisible(false);
            playbackEndTimeLabel->setVisible(false);

            actionAboutPlaybackFile->setVisible(false);

            break;

        case SourceFake:
            actionSourceFake->setChecked(true);
            actionOpenGeneratorDialog->setEnabled(true);
            sourceControl->setSource(source);

            break;

        case SourceFakeSpikes:
            actionSourceFakeSpikes->setChecked(true);
            //actionOpenGeneratorDialog->setEnabled(true);
            sourceControl->setSource(source);

            break;

        case SourceFile:


            //pFileName = QFileDialog::getOpenFileName(this, tr("Open file for playback"), tempPath, tr("Rec files (*.rec)"));
            pFileName = lastRecordFileName;
            if (!pFileName.isEmpty()) {
                //Save the folder in system setting for the next session
                QFileInfo fi(pFileName);
                QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
                settings.beginGroup(QLatin1String("paths"));
                settings.setValue(QLatin1String("playbackPath"), fi.absoluteFilePath());
                settings.endGroup();
                savePlaybackFilePathToSystem(fi.absoluteFilePath());

                //Load the config file
                openPlaybackFile(pFileName);


//                playButtonPressed();
//                playButtonReleased();
//                playButton->setDown(false);
//                connectToSource();
            }



            //openPlaybackFile(QFileDialog::getOpenFileName(this, tr("Open file for playback"), "", tr("Rec files (*.rec)")));

            break;

        case SourceEthernet:
            actionSourceEthernet->setChecked(true);
            sourceControl->setSource(source);

            break;

        case SourceUSBDAQ:
            actionSourceUSB->setChecked(true);
            sourceControl->setSource(source);

            break;
        case SourceUSB3:
            actionSourceUSB3->setChecked(true);
            sourceControl->setSource(source);

            break;
        case SourceRhythm:
            actionSourceRhythm->setChecked(true);
            sourceControl->setSource(source);

            break;

        case SourceDockUSB:
            actionSourceDockUSB->setChecked(true);
            sourceControl->setSource(source);

            /*if(sourceControl->dockUSBSource->state == SOURCE_STATE_INITIALIZED){

                actionOpenRecordDialog->setEnabled(true);

                //This feature is not ready yet.
                //actionDisplayRFWidget->trigger();
                //rfwidget->dockConnected("DockingStation", "USB", sourceControl->dockUSBSource->dockSettings);
                //rfwidget->show();
                //connect(sourceControl->dockUSBSource, &DockUSBInterface::idlePayloadForwarding, rfwidget, &RFWidget::idlePayloadReceived);
                //connect(sourceControl->dockUSBSource, &DockUSBInterface::streamPayloadForwarding, rfwidget, &RFWidget::streamPayloadReceived);
                connect(this, &MainWindow::recordFileOpened, this, [this](){
                    actionConnect->setText("Stream and Start Recording");
                });
                connect(sourceControl, &SourceController::acquisitionStarted, this, [this](){
                    if(recordFileOpen){
                        actionRecord->trigger();
                        recordButton->setDown(true);
                        pauseButton->setEnabled(true);
                        pauseButton->setDown(false);
                        actionOpenRecordDialog->setEnabled(false);
                    }
                });
                connect(sourceControl, &SourceController::acquisitionPaused, this, [this](){
                    if(recordFileOpen){
                        actionPause->trigger();
                        actionRecord->setEnabled(true);
                        recordButton->setEnabled(true);
                        recordButton->setDown(false);
                        pauseButton->setEnabled(false);
                        pauseButton->setDown(true);
                    }
                });
            }*/
            break;
        }


        if (needToClosePlaybackConfig) {

            qDebug() << "[MainWindow::setSource] Closing playback config.";
            closeConfig(); //if the source was a file, we close the associated configuration

            QString FileLabelColor("gray");
            QString FileLabelText("No recording file open");
            QString fileLabelTextTemplate = tr("<font color='%1'>%2</font>");
            fileLabel->setText(fileLabelTextTemplate.arg(FileLabelColor, FileLabelText));

            actionPlay->setEnabled(false);
            actionMerge->setEnabled(false);
            actionPause->setEnabled(false);
            playButton->setEnabled(false);
            pauseButton->setEnabled(false);
            menuExport->setEnabled(false);

            recordButton->setVisible(false);
            playButton->setVisible(false);
            pauseButton->setVisible(false);

        }
    }
}

void MainWindow::setTLength()
{
    QAction* action = (QAction*)sender();

    actionSetTLength0_2->setChecked(false);
    actionSetTLength0_5->setChecked(false);
    actionSetTLength1_0->setChecked(false);
    actionSetTLength2_0->setChecked(false);
    actionSetTLength5_0->setChecked(false);
    action->setChecked(true);
    double value = action->data().toDouble();

    emit newTraceLength(value);
}

void MainWindow::soundButtonPressed()
{
    soundSettingsButton->setDown(false);
    newSoundDialog->setGeometry(QRect(this->geometry().x()+soundSettingsButton->x(),this->geometry().y()+soundSettingsButton->y()+soundSettingsButton->height()+this->menuBar()->height(),40,200));

//    soundDialog *newSoundDialog = new soundDialog(0, 0);
//    //if (channelsConfigured) {
//    newSoundDialog->threshSlider->setValue(soundOut->getThresh());
//    newSoundDialog->gainSlider->setValue(soundOut->getGain());
//    QStringList devs = soundOut->getAvailableDevices();
//    for (int i=0; i<devs.length(); i++) {
//        newSoundDialog->deviceCombo->addItem(devs.at(i));
//        if (soundOut->getCurrentDevice().compare(devs.at(i)) == 0) {
//            newSoundDialog->deviceCombo->setCurrentIndex(i);
//        }
//    }
//    //soundDialog *newSoundDialog = new soundDialog(soundOut->getGain(),soundOut->getThresh());
//    //}


///*
//    //For some reason, windows and Mac give different geometry info for the sound button. Will probably need to
//    //add something for Linux here too.
//#if defined (__WIN32__)
//    newSoundDialog->setGeometry(QRect(this->x() + soundSettingsButton->x() + 8, this->y() + soundSettingsButton->y() + 70, 40, 200));
//#else
//    newSoundDialog->setGeometry(QRect(this->x() + soundSettingsButton->x(), this->y() + soundSettingsButton->y() + 43, 40, 200));
//#endif

//*/
//    //if (channelsConfigured) {
////    connect(newSoundDialog->gainSlider, SIGNAL(valueChanged(int)), soundOut, SLOT(setGain(int)));
////    connect(newSoundDialog->threshSlider, SIGNAL(valueChanged(int)), soundOut, SLOT(setThresh(int)));
//    connect(newSoundDialog->gainSlider, &QSlider::sliderMoved, soundOut, &AudioController::setGain);
//    connect(newSoundDialog->threshSlider, &QSlider::sliderMoved, soundOut, &AudioController::setThresh);
//    //}
//    connect(this, &MainWindow::closeAllWindows, newSoundDialog, &soundDialog::closeDialog);
//    connect(this, &MainWindow::closeSoundDialog, newSoundDialog, &soundDialog::closeDialog);
//    //connect(newSoundDialog->deviceCombo,SIGNAL(currentIndexChanged(QString)),this,SLOT(changeAudioDevice(QString)));
//    connect(newSoundDialog->deviceCombo, static_cast<void(QComboBox::*)(const QString &)>(&QComboBox::currentIndexChanged),this, &MainWindow::changeAudioDevice);




    newSoundDialog->show();
}

void MainWindow::changeAudioDevice(QString device) {
    emit setAudioDevice(device);
//    bool success = soundOut->setDevice(device);
//    if (!success) {
//        //Request to change dev was rejected likely because of threading state. Try one more time before giving up.
//        soundOut->setDevice(device);
//    }
}

void MainWindow::audioDeviceChanged(QAudioSink *oldDevice) {
    //The previous QAudioOutput device can only be deleted in the main UI thread, so we do it here.
    if (oldDevice != NULL) {
        oldDevice->deleteLater();
    }
}


void MainWindow::selectedNTrodesUpdated() {
//    qDebug() << "nTrode either selected or deslected";
    if (triggerSettings != nullptr) {
        triggerSettings->setSelectedNTrodes(selectedNTrodes);
    }
}

void MainWindow::trodesButtonToggled(bool on) {
    //if (on) setDebugCheckpoint(true,"Trodes settings open");
//    qDebug() << "Trodes button turned to " << on;
    printDebugCheckpoint("Entering trodesButtonToggled");
    if (channelsConfigured && activeWorkspacePointers.hardwareConf->NCHAN > 0) { //only execute if there are nTrodes to edit
        trodeSettingsButton->setDown(on);

        if (triggerSettings == nullptr) { //create the trigger settings panel if it doesn't already exist
//            qDebug() << "Creating Trigger Settings Panel";
            printDebugCheckpoint("Creating Trigger Settings Panel");
            triggerSettings = new NTrodeSettingsWidget(nullptr, currentTrodeSelected);
            connect(triggerSettings, SIGNAL(configChanged()), this, SLOT(settingsChanged()));
            if (isAudioOn) {
                connect(triggerSettings, SIGNAL(updateAudioSettings()), this, SIGNAL(updateAudio()));
            }
            connect(triggerSettings, SIGNAL(changeAllRefs(int, int)), this, SLOT(setAllRefs(int, int)));
            connect(triggerSettings, SIGNAL(changeAllFilters(int, int)), this, SLOT(setAllFilters(int, int)));
            connect(triggerSettings, SIGNAL(toggleAllFilters(bool)), this, SLOT(toggleAllFilters(bool)));
            connect(triggerSettings, SIGNAL(toggleAllRefs(bool)), this, SLOT(toggleAllRefs(bool)));
//            connect(triggerSettings, SIGNAL(toggleLinkChanges(bool)), this, SLOT(linkChanges(bool)));
//            connect(triggerSettings, SIGNAL(moduleDataChannelChanged(int, int)), this, SLOT(sendModuleDataChanToModules(int, int)));

            connect(triggerSettings, SIGNAL(toggleAllTriggers(bool)), this, SLOT(toggleAllTriggers(bool)));
            connect(triggerSettings, SIGNAL(changeAllThresholds(int)), this, SLOT(setAllThresh(int)));
            connect(triggerSettings, SIGNAL(changeAllMaxDisp(int)), this, SLOT(setAllMaxDisp(int)));
            connect(triggerSettings, SIGNAL(openSelectByDialog()), this, SLOT(openSelectionDialog()));
            connect(triggerSettings, &NTrodeSettingsWidget::selectAllNtrodes, this, &MainWindow::selectAllNTrodes);

            connect(triggerSettings, SIGNAL(sig_threshUpdated(int)), spikeDisp, SIGNAL(sig_newThreshRecv(int)));
            connect(triggerSettings, SIGNAL(sig_maxDisplayUpdated(int)), spikeDisp, SIGNAL(sig_newMaxDispRecv(int)));

            connect(spikeDisp, SIGNAL(sig_newThresh(int)), triggerSettings, SLOT(setThresh(int)));
            connect(this, SIGNAL(sig_channelColorChanged(QColor)), triggerSettings, SLOT(setChanColorBox(QColor)));

            if (activeWorkspacePointers.networkConf->networkType == NetworkConfiguration::qsocket_based) {
                connect(trodesNet->tcpServer, SIGNAL(moduleDataStreamOn(bool)), triggerSettings, SLOT(setEnabledForStreaming(bool)));
            }
            connect(triggerSettings, SIGNAL(closing()), this, SLOT(trodeSettingsPanelClosed()));
            connect(triggerSettings, SIGNAL(saveGeo(QRect)), this, SLOT(saveTrodeSettingPanelPos(QRect)));
            connect(this, SIGNAL(closeAllWindows()), triggerSettings, SLOT(close()));
            printDebugCheckpoint("Done connecting Trigger Settings Panel");


            //load previous position
            QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

            settings.beginGroup(QLatin1String("trodeSettingsPanel"));
            QRect panelGeo = settings.value(QLatin1String("geometry")).toRect();

            if (panelGeo.height() > 0) { //if the geometry was previously saved, load it
                triggerSettings->setPanelDim(panelGeo.width(), panelGeo.height());
                triggerSettings->setPosition(panelGeo.x(), panelGeo.y());
            }
            else { //otherwise, load default
                triggerSettings->setPanelDim(225, 485);
                triggerSettings->setPositionOffset((this->geometry().width()), (tabs->geometry().y()+fileLabel->geometry().height()));
                triggerSettings->setPosition(this->geometry().x()+this->geometry().width()-triggerSettings->geometry().width(), this->geometry().y() + (tabs->geometry().y()+fileLabel->geometry().height()));
            }
            triggerSettings->attachToWidget(this);
            settings.endGroup();

            if (selectedNTrodes.count() <= 0)
                triggerSettings->loadNTrodeIntoPanel(activeWorkspacePointers.spikeConf->ntrodes[0]->nTrodeId); //if none selected, default load the first nTrode on startup
            printDebugCheckpoint("Done creating Trigger Settings Panel");
        }


        triggerSettings->setSelectedNTrodes(selectedNTrodes);
        printDebugCheckpoint("trodesButtonToggled: setSelectedNtrodes done");
        triggerSettings->setEnabledForStreaming(isModuleDataStreaming());
        printDebugCheckpoint("trodesButtonToggled: setEnabledForStreaming done");
        triggerSettings->setVisible(on);
        printDebugCheckpoint("Exiting trodesButtonToggled");


    }

}

void MainWindow::trodeSettingsPanelClosed() {
    trodeSettingsButton->setChecked(false);
}

void MainWindow::saveTrodeSettingPanelVisible(bool wasOpen) {
//    qDebug() << "Saving the Trodes Settings Panel's visibility panel, was it open when the config was closed?";
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("trodeSettingsPanel"));
    settings.setValue(QLatin1String("wasOpen"), wasOpen);
    settings.endGroup();
}

void MainWindow::saveSpikesPanelVisible(bool wasOpen) {
//    qDebug() << "Saving the Trodes Settings Panel's visibility panel, was it open when the config was closed?";
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("spikesPanel"));
    settings.setValue(QLatin1String("wasOpen"), wasOpen);
    settings.endGroup();
}

void MainWindow::saveTrodeSettingPanelPos(QRect geo) {
//    qDebug() << "Saving the Trode Settings Panel's last location";
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("trodeSettingsPanel"));
    settings.setValue(QLatin1String("geometry"), geo);
    settings.endGroup();
}

void MainWindow::settingsChanged(){
    configSettingsChanged = true;
}

void MainWindow::statescriptButtonPressed() {
    //open video window

    if (channelsConfigured) {
        QStringList arglist;
        QString modName;

        int modNum = activeWorkspacePointers.moduleConf->findModule("stateScript");

        SingleModuleConf s;
        if (modNum > -1) {
            s = activeWorkspacePointers.moduleConf->singleModuleConf[modNum];
            // launch the module with the main configuration file and the specified configuration file
            qDebug() << "[MainWindow::statescriptButtonPressed] Launching module: " << s.moduleName;
        } else {
            // Try the default location
            s.moduleName = "stateScript";
            s.sendTrodesConfig = 1;
            s.sendNetworkInfo = 1;
            qDebug() << "[MainWindow::statescriptButtonPressed] No stateScript entry in config file, launching: " << s.moduleName;
        }

        // Set up stderr/stdout forwarding (will be rerouted to qDebug and then logged)
        QProcess *moduleProcess = new QProcess(this);
        connect(moduleProcess,SIGNAL(readyReadStandardError()),this,SLOT(forwardProcessOutput()));
        connect(moduleProcess,SIGNAL(readyReadStandardOutput()),this,SLOT(forwardProcessOutput()));

        startSingleModule(s, moduleProcess);
    }
}

void MainWindow::videoButtonPressed(QString videoString) {
    //open video window
    //videoButton->setDown(false);
    if (channelsConfigured && !recording) {
        QStringList arglist;
        QString modName;

        int camModNum = activeWorkspacePointers.moduleConf->findModule("cameraModule");

        SingleModuleConf s;
        if (camModNum > -1) {
            s = activeWorkspacePointers.moduleConf->singleModuleConf[camModNum];
            // launch the module with the main configuration file and the specified configuration file
            qDebug() << "[MainWindow::videoButtonPressed] Launching module: " << s.moduleName;
        } else {
            // Try the default cameraModule location
            s.moduleName = "cameraModule";
            s.sendTrodesConfig = 1;
            s.sendNetworkInfo = 1;
            qDebug() << "[MainWindow::videoButtonPressed] No cameraModule entry in config file, launching: " << s.moduleName;
        }

        if(playbackFileOpen){
            s.moduleArguments.append("-playback");
            if(videoString.isEmpty()){
                QFileInfo file(playbackFile);
                QDir dir = file.absoluteDir();
                QStringList filters;
                filters << "*.h264";

                QFileInfoList files = dir.entryInfoList(filters);
                for (int i = 0; i < files.size(); ++i) {
                    QFileInfo fileInfo = files.at(i);
                    if(QString::compare(fileInfo.absoluteFilePath().replace(QRegularExpression("(\\.\\d)*\\.h264"), ".rec"), file.absoluteFilePath()) == 0){
                        videoString = fileInfo.absoluteFilePath();
                        break;
                    }
                }
            }
            s.moduleArguments.append(videoString);
        }

        // Set up stderr/stdout forwarding (will be rerouted to qDebug and then logged)
        QProcess *moduleProcess = new QProcess(this);
        connect(moduleProcess,SIGNAL(readyReadStandardError()),this,SLOT(forwardProcessOutput()));
        connect(moduleProcess,SIGNAL(readyReadStandardOutput()),this,SLOT(forwardProcessOutput()));

        startSingleModule(s, moduleProcess);
    }
}

void MainWindow::spikesButtonToggled(bool on)
{
    //open spike trigger window
    //spikesButton->setDown(false);

    if (channelsConfigured && activeWorkspacePointers.hardwareConf->NCHAN > 0) {
        spikesButton->setDown(on);


        if (on) {
            spikeDisp->show();
            spikeDisp->raise();
        } else {
            spikeDisp->setVisible(false);
        }

    }

}

void MainWindow::spikesPanelClosed() {
    spikeDisp->setVisible(false);
    spikesButton->setChecked(false);
}


void MainWindow::commentButtonPressed()
{
    commentButton->setDown(false);

    QString commentFileName;
    if (recordFileOpen) {
        QFileInfo fileInfo(recordFileName);
        commentFileName = fileInfo.absolutePath() + "/" + fileInfo.completeBaseName() + ".trodesComments";
    } else if (playbackFileOpen) {
        QFileInfo fileInfo(playbackFile);
        commentFileName = fileInfo.absolutePath() + "/" + fileInfo.completeBaseName() + ".trodesComments";
    }
    else {
        commentFileName = "";
    }

    CommentDialog *newCommentDialog = new CommentDialog(commentFileName, this);
    if (recordFileOpen) {
        newCommentDialog->setLiveMode(true);
    } else if (playbackFileOpen) {
        newCommentDialog->setLiveMode(false);
    } else {
        newCommentDialog->setLiveMode(false);
    }

    newCommentDialog->setSamplingRate(activeWorkspacePointers.hardwareConf->sourceSamplingRate);

    newCommentDialog->setWindowFlags(Qt::Popup);
    newCommentDialog->setGeometry(QRect(this->geometry().x()+commentButton->x(),this->geometry().y()+commentButton->y()+commentButton->height()+this->menuBar()->height(),80,400));

/*
    //For some reason, windows and Mac give different geometry info for the sound button. Will probably need to
    //add something for Linux here too.
#if defined (__WIN32__)
    newCommentDialog->setGeometry(QRect(this->x() + commentButton->x() + 8, this->y() + commentButton->y() + 70, 40, 200));
#else
    newCommentDialog->setGeometry(QRect(this->x() + commentButton->x(), this->y() + commentButton->y() + 43, 40, 200));
#endif
*/

    connect(this, SIGNAL(closeAllWindows()), newCommentDialog, SLOT(close()));
    if (isAudioOn)
        connect(this, SIGNAL(closeSoundDialog()), newCommentDialog, SLOT(close()));
    newCommentDialog->show();
}

void MainWindow::openHeadstageDialog() {
    //opens the dialog used to control the signal generator for debugging without hardware connected

    headstageSettings = sourceControl->getHeadstageSettings();
    HeadstageSettingsDialog *newDialog = new HeadstageSettingsDialog(headstageSettings);

    newDialog->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed
    //newDialog->setWindowFlags(Qt::WindowStaysOnTopHint);
    connect(this, SIGNAL(closeAllWindows()), newDialog, SLOT(close()));
    connect(newDialog, SIGNAL(windowClosed()), this, SLOT(enableHeadstageDialogMenu()));
    connect(newDialog,SIGNAL(newSettings(HeadstageSettings)),sourceControl,SLOT(setHeadstageSettings(HeadstageSettings)));
    //connect(newDialog,SIGNAL(newSettings(HeadstageSettings)),this,SLOT(headstageSettingsChanged(HeadstageSettings)));
    newDialog->show();
    actionHeadstageSettings->setEnabled(false);
}

void MainWindow::openControllerSettingsDialog() {
    //opens the dialog used to control the signal generator for debugging without hardware connected

    if(sourceControl->currentSource != SourceUSBDAQ){
        QMessageBox::warning(this, "Only available over USB!", "Changing MCU settings may only work over a USB connection! Ethernet is coming soon");
    }
    controllerSettings = sourceControl->getControllerSettings();
    ControllerSettingsDialog *newDialog = new ControllerSettingsDialog(controllerSettings);

    newDialog->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed
    //newDialog->setWindowFlags(Qt::WindowStaysOnTopHint);
    connect(this, SIGNAL(closeAllWindows()), newDialog, SLOT(close()));
    connect(newDialog, SIGNAL(windowClosed()), this, SLOT(enableControllerDialogMenu()));
    connect(newDialog,SIGNAL(newSettings(HardwareControllerSettings)),sourceControl,SLOT(setControllerSettings(HardwareControllerSettings)));

    newDialog->show();
    actionControllerSettings->setEnabled(false);
}

void MainWindow::headstageSettingsChanged(HeadstageSettings s) {
    qDebug() << "New headstage settings";
    headstageSettings = s;
    if(activeWorkspacePointers.globalConf){
        activeWorkspacePointers.globalConf->headstageSerialNumber = QString("%1 %2").arg(headstageSettings.hsTypeCode,5,10,QChar('0')).arg(headstageSettings.hsSerialNumber,5,10,QChar('0'));
        activeWorkspacePointers.globalConf->autoSettleOn = (int)headstageSettings.autoSettleOn;
        activeWorkspacePointers.globalConf->smartRefOn = (int)headstageSettings.smartRefOn;
        activeWorkspacePointers.globalConf->gyroSensorOn =(int)(headstageSettings.gyroSensorAvailable && headstageSettings.gyroSensorOn);
        activeWorkspacePointers.globalConf->accelSensorOn = (int)(headstageSettings.accelSensorAvailable && headstageSettings.accelSensorOn);
        activeWorkspacePointers.globalConf->magSensorOn = (int)(headstageSettings.magSensorAvailable && headstageSettings.magSensorOn);
        activeWorkspacePointers.globalConf->headstageFirmwareVersion = QString("%1.%2").arg(headstageSettings.majorVersion).arg(headstageSettings.minorVersion);
    }


    if (!playbackFileOpen) {
        //Connected to live source (not playback file)
            if (channelsConfigured) {
                if (acquisitionWorkspace.spikeConf.deviceType == "neuropixels1") {
                    //Neuropixels headstage needs to know which channels should be turned on
                    NeuroPixelsSettings s;
                    acquisitionWorkspace.getNeuropixelsHardwareCommandData(s);
                    sourceControl->setNeuroPixelsSettings(s);

                }

            }
    }
}

void MainWindow::controllerSettingsChanged(HardwareControllerSettings s) {
    qDebug() << "New controller settings";
    controllerSettings = s;
    if(activeWorkspacePointers.globalConf){
        activeWorkspacePointers.globalConf->controllerSerialNumber =  QString("%1 %2").arg(controllerSettings.modelNumber,5,10,QChar('0')).arg(controllerSettings.serialNumber,5,10,QChar('0'));
        activeWorkspacePointers.globalConf->controllerFirmwareVersion = QString("%1.%2").arg(controllerSettings.majorVersion).arg(controllerSettings.minorVersion);
    }

    if(channelsConfigured && controllerSettings.valid && controllerSettings.samplingRateKhz && activeWorkspacePointers.hardwareConf && controllerSettings.samplingRateKhz*1000 != activeWorkspacePointers.hardwareConf->sourceSamplingRate){

        //warn user of sampling rate mismatch
        //  if workspace loaded (channelsconfigured), valid mcu settings, samplingrate!=0, hardwareConf is set
        //TODO: dialog that allows user to change config sampling rate or set MCU sampling rate
        QMessageBox::warning(this, "Sampling rate mismatch",
                             QString("The sampling rate of the MCU is set to %1hz but your .trodesconf file is set to %2hz. Please change them to match.")
                             .arg(controllerSettings.samplingRateKhz*1000).arg(activeWorkspacePointers.hardwareConf->sourceSamplingRate));
    }
}

void MainWindow::openGeneratorDialog()
{
    //opens the dialog used to control the signal generator for debugging without hardware connected
    waveformGeneratorDialog *newDialog = new waveformGeneratorDialog(sourceControl->waveGeneratorSource->getModulatorFrequency(),
                                                                     sourceControl->waveGeneratorSource->getFrequency(),
                                                                     sourceControl->waveGeneratorSource->getAmplitude(),
                                                                     sourceControl->waveGeneratorSource->getThreshold());

    newDialog->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed
    newDialog->setWindowFlags(Qt::WindowStaysOnTopHint);
    connect(newDialog->modulatorFreqSpinBox, SIGNAL(valueChanged(double)), sourceControl->waveGeneratorSource, SLOT(setModulatorFrequency(double)));
    connect(newDialog->freqSlider, SIGNAL(valueChanged(int)), sourceControl->waveGeneratorSource, SLOT(setFrequency(int)));
    connect(newDialog->ampSlider, SIGNAL(valueChanged(int)), sourceControl->waveGeneratorSource, SLOT(setAmplitude(int)));
    connect(newDialog->threshSlider, SIGNAL(valueChanged(int)), sourceControl->waveGeneratorSource, SLOT(setThreshold(int)));
    connect(this, SIGNAL(closeAllWindows()), newDialog, SLOT(close()));
    connect(this, SIGNAL(closeWaveformDialog()), newDialog, SLOT(close()));
    connect(newDialog, SIGNAL(windowClosed()), this, SLOT(enableGeneratorDialogMenu()));
    newDialog->show();

    actionOpenGeneratorDialog->setEnabled(false);
}

void MainWindow::openTrodeSettingsWindow()
{
    //opens the settings dialog for a selected nTrode
    NTrodeSettingsWidget* triggerSettings = new NTrodeSettingsWidget(nullptr, currentTrodeSelected);
    //mark: TODO maybe delete this after finished
    triggerSettings->setSelectedNTrodes(selectedNTrodes);
    triggerSettings->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed
    if (isAudioOn)
        connect(triggerSettings, SIGNAL(updateAudioSettings()), this, SIGNAL(updateAudio()));
    connect(triggerSettings, SIGNAL(changeAllRefs(int, int)), this, SLOT(setAllRefs(int, int)));
    connect(triggerSettings, SIGNAL(changeAllFilters(int, int)), this, SLOT(setAllFilters(int, int)));
    connect(triggerSettings, SIGNAL(toggleAllFilters(bool)), this, SLOT(toggleAllFilters(bool)));
    connect(triggerSettings, SIGNAL(toggleAllRefs(bool)), this, SLOT(toggleAllRefs(bool)));
    connect(triggerSettings, SIGNAL(moduleDataChannelChanged(int, int)), this, SLOT(sendModuleDataChanToModules(int, int)));
    if (activeWorkspacePointers.networkConf->networkType == NetworkConfiguration::qsocket_based) {
        connect(trodesNet->tcpServer, SIGNAL(moduleDataStreamOn(bool)), triggerSettings, SLOT(setEnabledForStreaming(bool)));
    }

    triggerSettings->setEnabledForStreaming(isModuleDataStreaming());
    triggerSettings->show();
}

void MainWindow::openTrodeWindow()
{
    spikeDisp->setShownNtrode(currentTrodeSelected);
    singleTriggerWindowOpen = true;
    //spikeDisp[0]->ntrodeWidgets[currentTrodeSelected]->setFocusPolicy(Qt::FocusPolicy(0));
}

void MainWindow::removeFromOpenNtrodeList(int nTrodeNum)
{
    //TODO:  if multiple nTrode windows are open, we will use the nTrodeNum input

    if (singleTriggerWindowOpen) {
        singleTriggerWindowOpen = false;
    }
}

void MainWindow::enableHeadstageDialogMenu()
{
    actionHeadstageSettings->setEnabled(true);
}

void MainWindow::enableControllerDialogMenu()
{
    actionControllerSettings->setEnabled(true);
}

void MainWindow::enableGeneratorDialogMenu()
{
    actionOpenGeneratorDialog->setEnabled(true);
}

void MainWindow::loadClusterFile() {
    //Used the saved system settings from the last session as the default folder
    /*QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("paths"));
    QStringList tempPathList = settings.value(QLatin1String("prevWorkspacePaths")).toStringList();
    QString tempPath;
    if(tempPathList.isEmpty())
        tempPath = QDir::currentPath();
    else
        tempPath = tempPathList.first();
    settings.endGroup();*/
    //dialog to create a new record file


    QString dataDir;
    if (playbackFileOpen) {
        dataDir = QFileInfo(fileString).absolutePath();
    } else if (recordFileOpen) {
        dataDir = QFileInfo(recordFileName).absolutePath();
    } else {
        dataDir = activeWorkspacePointers.globalConf->filePath;
    }

    qDebug() << dataDir;

    if (!QDir(dataDir).exists()) {
        dataDir = QDir::homePath();
    }


#if defined (__linux__)
    QString fileName = QFileDialog::getOpenFileName(this, QString("Open cluster file"), dataDir, "Trodes cluster files (*.trodesClusters *.xml)",nullptr, QFileDialog::DontUseNativeDialog);
#else
    QString fileName = QFileDialog::getOpenFileName(this, QString("Open cluster file"), dataDir, "Trodes cluster files (*.trodesClusters *.xml)");
#endif

    if (!fileName.isEmpty()) {

        //Load the file
        loadClusterFile(fileName);

    }
}

void MainWindow::loadClusterFile(QString filename) {
    emit signalLoadClusters(filename);
}

void MainWindow::saveCurrentClusters() {
    QString saveClusterName;
    if (recordFileOpen) {
        QFileInfo fileInfo(recordFileName);
        saveClusterName = fileInfo.absolutePath() + "/" + fileInfo.completeBaseName() + "_time_" + QString("%1").arg(currentTimeStamp) + ".trodesClusters";
    } else if (playbackFileOpen) {
        QFileInfo fileInfo(playbackFile);
        saveClusterName = fileInfo.absolutePath() + "/" + fileInfo.completeBaseName() + "_time_" + QString("%1").arg(currentTimeStamp) + ".trodesClusters";
    }

    //Open a dialog to verify that the default name is ok
#if defined (__linux__)
    saveClusterName = QFileDialog::getSaveFileName(this, tr("Save cluster file"), saveClusterName, tr("Trodes cluster files (*.trodesClusters)"),nullptr, QFileDialog::DontUseNativeDialog);
#else
    saveClusterName = QFileDialog::getSaveFileName(this, tr("Save cluster file"), saveClusterName, tr("Trodes cluster files (*.trodesClusters)"));
#endif

    if (!saveClusterName.isEmpty()) {
        emit signalSaveClusters(saveClusterName);
    }
}

void MainWindow::openExportMenu()
{
    //If settings have been modified, prompt user to save them
    settingsWarning();

    //QAction *actionPtr = (QAction*)sender();
    QString programToExecute = "trodesexport";

    //Get the directory containing the calling executable (usu. Trodes)
    QString callingAppDir = QCoreApplication::applicationDirPath();
#ifdef __APPLE__
    //If calling app is an app bundle, use its parent as the base directory
    if (callingAppDir.endsWith(QString(".app/Contents/MacOS")))
        callingAppDir = QDir::cleanPath(callingAppDir + "/../../../"+programToExecute+".app/Contents/MacOS/");
#endif

    QString absProgramName= QDir::cleanPath(callingAppDir + "/" + programToExecute);
    QStringList processArguments;
    processArguments << "-rec" << playbackFile;

    if (playbackEmbeddedWorkspace.spikeConf.deviceType == "neuropixels1") {
        processArguments << "-interp" << "0"; //No interpolation allowed with neuropixels data
    }

    ExportProgressDialog *newExportDialog = new ExportProgressDialog(absProgramName, processArguments, this);

    newExportDialog->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed


    connect(this, SIGNAL(closeAllWindows()), newExportDialog, SLOT(close()));

    newExportDialog->show();
}

void MainWindow::exportActionSelected()
{
    //If settings have been modified, prompt user to save them
    settingsWarning();

    QAction *actionPtr = (QAction*)sender();
    QString programToExecute = actionPtr->data().toString();

    //Get the directory containing the calling executable (usu. Trodes)
    QString callingAppDir = QCoreApplication::applicationDirPath();
#ifdef __APPLE__
    //If calling app is an app bundle, use its parent as the base directory
    if (callingAppDir.endsWith(QString(".app/Contents/MacOS")))
        callingAppDir = QDir::cleanPath(callingAppDir + "/../../../");
#endif

    QString absProgramName= QDir::cleanPath(callingAppDir + "/" + programToExecute);
    QStringList processArguments;
    processArguments << "-rec" << playbackFile;


    ExportProgressDialog *newExportDialog = new ExportProgressDialog(absProgramName, processArguments, this);

    newExportDialog->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed
    //newExportDialog->setWindowModality(Qt::WindowModal);


    connect(this, SIGNAL(closeAllWindows()), newExportDialog, SLOT(close()));
    //connect(newExportDialog, SIGNAL(startExport(bool, bool, int, int, int, int)), this, SLOT(exportData(bool, bool, int, int, int, int)));

    newExportDialog->show();


}



void MainWindow::openSoundDialog()
{
    //opens the dialog used to control the sound output.
    if (channelsConfigured) {
        soundDialog *newSoundDialog = new soundDialog(soundOut->getGain(), soundOut->getThresh());
        newSoundDialog->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed
        connect(newSoundDialog->gainSlider, SIGNAL(valueChanged(int)), soundOut, SLOT(setGain(int)));
        connect(newSoundDialog->threshSlider, SIGNAL(valueChanged(int)), soundOut, SLOT(setThresh(int)));
        connect(this, SIGNAL(closeAllWindows()), newSoundDialog, SLOT(close()));
        newSoundDialog->show();
    }
    else { //No config file loaded, so make a dummy sound controller
        soundDialog *newSoundDialog = new soundDialog(30, 30);
        newSoundDialog->setAttribute(Qt::WA_DeleteOnClose); //deletes the object when the window is closed
        connect(this, SIGNAL(closeAllWindows()), newSoundDialog, SLOT(close()));
        newSoundDialog->show();
    }
}

void MainWindow::sourceStateChanged(int state)
{
    //Here we perform any actions (besides menu-related ones) that need to occur when the source state changes


}

void MainWindow::setSourceMenuState(int state)
{
    //When the state of the source changes, the state is emitted and this function is called
    //to set the menus
    sourceMenuState = state;
    if (state == SOURCE_STATE_NOT_CONNECTED) {
        if (channelsConfigured && (sourceControl->currentSource > 0)) {
            actionConnect->setEnabled(true);
            actionHeadstageSettings->setEnabled(true);
            actionControllerSettings->setEnabled(true);
            actionCloseConfig->setEnabled(true);
            actionLoadConfig->setEnabled(true);
            actionReConfig->setEnabled(true);
        }
        else {
            actionHeadstageSettings->setEnabled(false);
            actionControllerSettings->setEnabled(false);
            actionConnect->setEnabled(false);
        }
        actionDisconnect->setEnabled(false);
        actionSendSettle->setEnabled(false);
        //reset error flags
        sourcePacketSizeError(false);
        sourceNoDataError(false);
        sourceNoHeadstageError(false);
        sourceUnresponsiveHeadstage(false);
        statusbar->showMessage(tr("Not connected to device"));
        streamStatusColorIndicator->setText("STATUS: No source");
        dataStreaming = false;
        actionOpenRecordDialog->setEnabled(false);
        emit sourceConnected((TrodesSource)SourceNone);
    }
    else if (state == SOURCE_STATE_CONNECTERROR) {
        //setSource(1);
        qDebug() << "[MainWindow::setSourceMenuState] Connection to source failed.";
        //QMessageBox messageBox;
        //messageBox.critical(0, "Error", "Connection to source failed.");
        //messageBox.setFixedSize(500, 200);

        showErrorMessage("Connection to source failed.");
        streamStatusColorIndicator->setText("STATUS: No source");
        setSource(SourceNone);
    }
    else if (state == SOURCE_STATE_INITIALIZED) {
        if (channelsConfigured) {
            actionConnect->setEnabled(true);
            actionHeadstageSettings->setEnabled(true);
            actionControllerSettings->setEnabled(true);
            actionCloseConfig->setEnabled(true);
            actionLoadConfig->setEnabled(true);
            actionReConfig->setEnabled(false);

        }
        else {
            actionConnect->setEnabled(false);
        }
        actionOpenRecordDialog->setEnabled(false);
        if (playbackFileOpen) {
            actionPause->setEnabled(false);
            actionPlay->setEnabled(true);
            if (playbackEmbeddedWorkspace.hardwareConf.NCHAN == 0) {
                //We enable the merge menu if this playback workspace was for a recording
                //with no headstage attached (0 channels). The neural channels were probably recorded
                //on a logger, and the two files need to be merged.
                actionMerge->setEnabled(true);
            }
            playButton->setDown(false);
            pauseButton->setDown(true);
            menuExport->setEnabled(true);
            actionSourceFile->setEnabled(false);
            actionPlaybackOpen->setEnabled(false);
        } else {
            actionSourceFile->setEnabled(true);
            actionPlaybackOpen->setEnabled(true);
        }
        actionDisconnect->setEnabled(false);
        actionSendSettle->setEnabled(false);
        statusbar->showMessage(tr("Connected to device. Currently not streaming."));

        //reset error flags
        sourcePacketSizeError(false);
        sourceNoDataError(false);
        sourceNoHeadstageError(false);
        sourceUnresponsiveHeadstage(false);

        streamStatusColorIndicator->setText("STATUS: Connected to source");

        dataStreaming = false;

        actionSourceNone->setEnabled(true);
        actionSourceFake->setEnabled(true);
        //actionSourceFile->setEnabled(true);
        //actionPlaybackOpen->setEnabled(true);
        actionSourceFakeSpikes->setEnabled(true);
        actionSourceEthernet->setEnabled(true);
        actionSourceUSB->setEnabled(true);
        actionSourceUSB3->setEnabled(true);
        actionSourceDockUSB->setEnabled(true);
        actionSourceRhythm->setEnabled(true);


        actionRecord->setEnabled(false);
        recordButton->setEnabled(false);

        //Emit connection signal
        //Listeners:
        //TrodesServer -> modules
        emit sourceConnected((TrodesSource)sourceControl->currentSource);

        emit closeWaveformDialog();
//        streamManager->clearAllDigitalStateChanges(); //clears remembered DIO state changes
//        clearAll(); // clears skipe scatter plots.
        if (quitting) {
            closeEvent(new QCloseEvent());
            return;
        }
    }
    else if (state == SOURCE_STATE_RUNNING) {
        actionLoadConfig->setEnabled(false);
        actionCloseConfig->setEnabled(false);
        actionReConfig->setEnabled(false);
        actionConnect->setEnabled(false);
        actionDisconnect->setEnabled(true);
        actionSendSettle->setEnabled(true);
        actionSourceNone->setEnabled(false);
        actionSourceFake->setEnabled(false);
        actionSourceFakeSpikes->setEnabled(false);
        actionSourceFile->setEnabled(false);
        actionPlaybackOpen->setEnabled(false);
        actionSourceEthernet->setEnabled(false);
        actionSourceUSB->setEnabled(false);
        actionSourceUSB3->setEnabled(false);
        actionSourceDockUSB->setEnabled(false);
        actionSourceRhythm->setEnabled(false);
        actionHeadstageSettings->setEnabled(false);
        actionControllerSettings->setEnabled(false);
        statusbar->showMessage(tr("Data currently streaming from device"));
        streamStatusColorIndicator->setText("STATUS: Receiving stream ok");


        dataStreaming = true;
        //Moved these two to the block above, where state == SOURCE_STATE_INITIALIZED
//        streamManager->clearAllDigitalStateChanges(); //clears remembered DIO state changes
//        clearAll(); // clears skipe scatter plots.

        if (sourceControl->currentSource != SourceFile) {
            actionOpenRecordDialog->setEnabled(true);
        }

        if (recordFileOpen) {
            actionRecord->setEnabled(true);
            recordButton->setEnabled(true);
            pauseButton->setEnabled(true);
            pauseButton->setDown(true);
        }
        if (playbackFileOpen) {
            actionPause->setEnabled(true);
            actionPlay->setEnabled(false);
            actionMerge->setEnabled(false);
            menuExport->setEnabled(false);
            playButton->setDown(true);
            pauseButton->setDown(false);
        }
    }
    else if (state == SOURCE_STATE_PAUSED) {
        dataStreaming = false;
    }
}

void MainWindow::about()
{
    QString abouttext("<h4>Trodes is a free, open-source neuroscience data collection and experimental control toolbox.</h4>"
                      "<p><i>If there is a feature or integration not available, please feel free to contact us!</i></p>"
                      "<hr/>");
    QString versioninfo = qPrintable(GlobalConfiguration::getVersionInfo(false, true));
    QMessageBox::about(this, tr("About Trodes"), abouttext + versioninfo);
}

void MainWindow::aboutHardware()
{
    if(!controllerSettings.valid){
        controllerSettings = sourceControl->getControllerSettings();
    }
    if(!headstageSettings.valid){
        headstageSettings = sourceControl->getHeadstageSettings();
    }

    HardwareSettingsDisplay *settingsdisplay = new HardwareSettingsDisplay(controllerSettings, headstageSettings);
    settingsdisplay->setAttribute(Qt::WA_DeleteOnClose);
    settingsdisplay->show();
    settingsdisplay->move(pos().x()+size().width()/2-settingsdisplay->size().width()/2,
                          pos().y()+size().height()/2-settingsdisplay->size().height()/2);
}

void MainWindow::aboutCurConfig()
{
    QString configInfo;
    if (strcmp(qPrintable(activeWorkspacePointers.globalConf->trodesVersion),"0") != 0) {
        QString tv = (activeWorkspacePointers.globalConf->trodesVersion=="-1") ? "<Not Available>" : activeWorkspacePointers.globalConf->trodesVersion;
        QString cd = (activeWorkspacePointers.globalConf->compileDate=="-1") ? "<Not Available>" : activeWorkspacePointers.globalConf->compileDate;
        QString ct = (activeWorkspacePointers.globalConf->compileTime=="-1") ? "<Not Available>" : activeWorkspacePointers.globalConf->compileTime;
        QString qv = (activeWorkspacePointers.globalConf->qtVersion=="-1") ? "<Not Available>" : activeWorkspacePointers.globalConf->qtVersion;
        QString ch = (activeWorkspacePointers.globalConf->commitHeadStr=="-1") ? "<Not Available>" : activeWorkspacePointers.globalConf->commitHeadStr;
        configInfo = QString("Workspace Info:\n"
                        "-Trodes Version:\t%1\n"
                        "-Compiled on:\t%2 %3\n"
                        "-Qt version:\t\t%4\n"
                        "-Git commit:\t'%5'").
                arg(tv).arg(cd).arg(ct).arg(qv).arg(ch);
    }
    else
        configInfo = "No workspace information available.";

    QMessageBox::about(this, tr("About Configuration"), tr(qPrintable(configInfo)));
}

void MainWindow::aboutVersion(){
//    checkForUpdate();
    if(isLatestVersion){
        QMessageBox::about(this, tr("Version"), tr("Your version is up to date."));
    }
    else{
        int ret = QMessageBox::warning(this, tr("Version"), tr("There is a more recent version available. Launch updater? "),
                             QMessageBox::Yes, QMessageBox::No);
        if(ret == QMessageBox::Yes){
            launchUpdater();
        }
    }
}

void MainWindow::aboutPlayback(){
    QMessageBox::about(this, "About this recording",
                       QString("<h4>%1</h4>"
                               "<p>Full path: %2</p>"
                               "<p>System time at creation: %3</p>"
                               "<p>Date/Time: %4 UTC</p>"
                               "<p>Trodes version: %5</p>"
                               "<p>Qt version: %6</p>"
                               "<p>Headstage serial #: %7</p>"
                               "<p>MCU serial #: %8</p>"
                               "<p>MCU firmware version: %9</p>"
                               "<p>Headstage firmware version: %10</p>"
                               "<p>Sampling rate: %11</p>"
                       )
                       .arg(QFileInfo(playbackFile).fileName())
                       .arg(playbackFile)
                       .arg(activeWorkspacePointers.globalConf->systemTimeAtCreation/1000)
                       .arg(QDateTime::fromSecsSinceEpoch(activeWorkspacePointers.globalConf->systemTimeAtCreation/1000).toString())
                       .arg(activeWorkspacePointers.globalConf->trodesVersion)
                       .arg(activeWorkspacePointers.globalConf->qtVersion)
                       .arg(activeWorkspacePointers.globalConf->headstageSerialNumber)
                       .arg(activeWorkspacePointers.globalConf->controllerSerialNumber)
                       .arg(activeWorkspacePointers.globalConf->controllerFirmwareVersion)
                       .arg(activeWorkspacePointers.globalConf->headstageFirmwareVersion)
                       .arg(activeWorkspacePointers.hardwareConf->sourceSamplingRate)
                       );
}

bool MainWindow::checkMultipleInstances(QString errorMessage) {
//    //qDebug() << "Checking if Trodes is already open...";
//    TrodesClient dummyClient;
//    QStringList activeTrodesHosts = dummyClient.findLocalTrodesServers();
//    if (activeTrodesHosts.length() > 0) {
//        qDebug() << "Error: Multiple instances of Trodes detected [" << activeTrodesHosts.length() << "]";
//        QMessageBox::critical(this, tr("Multiple Instance Error"),tr(qPrintable(errorMessage)));
//        return(true);
//    }

    return(false);
}


void MainWindow::saveWorkspacePathToSystem(QString workspacePath) {
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("paths"));

    QStringList prevWorkspaces;
    if (settings.contains("prevWorkspacePaths"))
        prevWorkspaces = settings.value("prevWorkspacePaths").toStringList();
    int alreadyPresentIndex = -1;
    for (int i = 0; i < prevWorkspaces.length(); i++) {
        if (prevWorkspaces.at(i) == workspacePath) {
            alreadyPresentIndex = i;
        }
    }
    if (alreadyPresentIndex > -1) {
        prevWorkspaces.removeAt(alreadyPresentIndex);
    }
    prevWorkspaces.push_front(workspacePath);
    if (prevWorkspaces.length() > 5) {
        prevWorkspaces.pop_back();
    }
    settings.setValue(QLatin1String("prevWorkspacePaths"), prevWorkspaces);

    settings.endGroup();
}

void MainWindow::savePlaybackFilePathToSystem(QString playbackFilePath) {
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));
    settings.beginGroup(QLatin1String("paths"));

    QStringList prevPlaybackFiles;
    if (settings.contains("prevPlaybackPaths"))
        prevPlaybackFiles = settings.value("prevPlaybackPaths").toStringList();

    int alreadyPresentIndex = -1;
    for (int i = 0; i < prevPlaybackFiles.length(); i++) {
        if (prevPlaybackFiles.at(i) == playbackFilePath) {
            alreadyPresentIndex = i;
        }
    }
    if (alreadyPresentIndex > -1) {
        prevPlaybackFiles.removeAt(alreadyPresentIndex);
    }
    prevPlaybackFiles.push_front(playbackFilePath);
    if (prevPlaybackFiles.length() > 5) {
        prevPlaybackFiles.pop_back();
    }
    settings.setValue(QLatin1String("prevPlaybackPaths"), prevPlaybackFiles);

    settings.endGroup();
}

void MainWindow::setSplashScreenAnimation(bool v){
    animateSplash = v;
}

void MainWindow::swapSplashScreenAndTabView(bool loadSplash) {
    isSplashScreenVisible = loadSplash;
    if (loadSplash) {
        tabsBackground->setVisible(true);
        splashScreen->setVisible(true);
        if(animateSplash)
            splashScreen->runAnimation_windowFade(150, true);
        else
            splashScreen->runAnimation_windowFade(0, true);
    }
    else {
        if(animateSplash)
            splashScreen->runAnimation_windowFade(150);
        else
            splashScreen->runAnimation_windowFade(0);
    }
}

void MainWindow::setBackgroundFrameVisibility() {
    tabsBackground->setVisible(isSplashScreenVisible);
}



QString MainWindow::timeStampToString(uint32_t tmpTimeStamp){
    QString currentTimeString("");
//    uint32_t tmpTimeStamp = currentTimeStamp;
    int hoursPassed = floor(tmpTimeStamp / (activeWorkspacePointers.hardwareConf->sourceSamplingRate * 60 * 60));

    tmpTimeStamp = tmpTimeStamp - (hoursPassed * 60 * 60 * activeWorkspacePointers.hardwareConf->sourceSamplingRate);
    int minutesPassed = floor(tmpTimeStamp / (activeWorkspacePointers.hardwareConf->sourceSamplingRate * 60));
    tmpTimeStamp = tmpTimeStamp - (minutesPassed * 60 * activeWorkspacePointers.hardwareConf->sourceSamplingRate);
    int secondsPassed = floor(tmpTimeStamp / (activeWorkspacePointers.hardwareConf->sourceSamplingRate));
    tmpTimeStamp = tmpTimeStamp - (secondsPassed * activeWorkspacePointers.hardwareConf->sourceSamplingRate);
    int tenthsPassed = floor(((tmpTimeStamp * 10) / activeWorkspacePointers.hardwareConf->sourceSamplingRate));

    if (hoursPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(hoursPassed));
    currentTimeString.append(":");
    if (minutesPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(minutesPassed));
    currentTimeString.append(":");
    if (secondsPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(secondsPassed));
    currentTimeString.append(".");
    currentTimeString.append(QString::number(tenthsPassed));

    return currentTimeString;
}

void MainWindow::setTimeStampLabels(uint32_t playbackStartTimeStamp, uint32_t playbackEndTimeStamp){

//    qDebug() << "playbackstarttimestamp" << playbackStartTimeStamp;
//    qDebug() << "playbackendtimeStamp" << playbackEndTimeStamp;
    playbackSlider->setEnabled(true);
    playbackStartTimeLabel->setEnabled(true);
    playbackEndTimeLabel->setEnabled(true);
    playbackSlider->setVisible(true);
    playbackStartTimeLabel->setVisible(true);
    playbackEndTimeLabel->setVisible(true);


//    playbackStartClock = new QTime(timeStampToString(playbackStartTimeStamp));
//    playbackEndClock = new QTime(timeStampToString(playbackEndTimeStamp));

    playbackStartTimeLabel->setText(timeStampToString(playbackStartTimeStamp));
    playbackEndTimeLabel->setText(timeStampToString(playbackEndTimeStamp));

    playbackStartTime = playbackStartTimeStamp;
    playbackEndTime = playbackEndTimeStamp;
}



void MainWindow::updateTime()
{
    visibleTime++;
    if (visibleTime > 5) {
        if (eventTabWasChanged) {
            eventTabWasChanged = false;
            update();
        }
    }

    if (channelsConfigured && !exportMode && !playbackSlider->isSliderDown()) { //if slider is down, user is dragging it. so it needs to stop self-correcting
        //QTime currentTime;
        QString currentTimeString("");
        uint32_t tmpTimeStamp = currentTimeStamp;

        int hoursPassed = floor(tmpTimeStamp / (activeWorkspacePointers.hardwareConf->sourceSamplingRate * 60 * 60));
        tmpTimeStamp = tmpTimeStamp - (hoursPassed * 60 * 60 * activeWorkspacePointers.hardwareConf->sourceSamplingRate);
        int minutesPassed = floor(tmpTimeStamp / (activeWorkspacePointers.hardwareConf->sourceSamplingRate * 60));
        tmpTimeStamp = tmpTimeStamp - (minutesPassed * 60 * activeWorkspacePointers.hardwareConf->sourceSamplingRate);
        int secondsPassed = floor(tmpTimeStamp / (activeWorkspacePointers.hardwareConf->sourceSamplingRate));
        tmpTimeStamp = tmpTimeStamp - (secondsPassed * activeWorkspacePointers.hardwareConf->sourceSamplingRate);
        //int tenthsPassed = floor(((tmpTimeStamp*10)/hardwareConf->sourceSamplingRate));
        //int32_t currentTimeStamp = rawData.timestamps[rawData.writeIdx];

        if (hoursPassed < 10)
            currentTimeString.append("0");
        currentTimeString.append(QString::number(hoursPassed));
        currentTimeString.append(":");
        if (minutesPassed < 10)
            currentTimeString.append("0");
        currentTimeString.append(QString::number(minutesPassed));
        currentTimeString.append(":");
        if (secondsPassed < 10)
            currentTimeString.append("0");
        currentTimeString.append(QString::number(secondsPassed));
        //currentTimeString.append(".");
        //currentTimeString.append(QString::number(tenthsPassed));


//        currentTimeString = timeStampToString(tmpTimeStamp).toString(timeFormatString);
        timeLabel->setText(currentTimeString);

        timerTick = (timerTick + 1) % 5;
        if ((recordFileOpen) && (timerTick == 0)) {
            uint32_t timerecorded;
            if(recording){
                timerecorded = totalTimeRecorded + (currentTimeStamp-lastPlayPauseTime);
            }
            else{
                timerecorded = totalTimeRecorded + 0;
            }
//            fileLabel->setText(fileString + QString("   (%1 MB    Available: %2 MB)").arg(recordOut->getBytesWritten() / 1000000).arg(recordOut->getBytesFree() / 1000000));
            fileLabel->setText(fileString + QString("   (Recorded: %1 MB  |  Available: %2 MB  |  %3m %4sec recorded)")
                               .arg(recordOut->getBytesWritten() / 1024/1024)
                               .arg(recordOut->getBytesFree() / 1024/1024)
                               .arg(timerecorded/activeWorkspacePointers.hardwareConf->sourceSamplingRate/60, 2)
                               .arg(timerecorded/activeWorkspacePointers.hardwareConf->sourceSamplingRate % 60, 2));
//                               .arg(recordOut->getPacketsWritten() / hardwareConf->sourceSamplingRate / 60)
//                               .arg(recordOut->getPacketsWritten() / hardwareConf->sourceSamplingRate % 60));

                               /*.arg((msecRecorded+recordTimer->elapsed()*recordButton->isDown()) / 1000 / 60)
                               .arg((msecRecorded+recordTimer->elapsed()*recordButton->isDown()) / 1000 % 60));*/
        }
    }
}

void MainWindow::lfpFiltersOn()
{
    activeWorkspacePointers.spikeConf->setAllModesOn(false, true, false);
    streamLFPFiltersOn->setChecked(true);
    streamSpikeFiltersOn->setChecked(false);
    streamNoFiltersOn->setChecked(false);
    streamStimViewOn->setChecked(false);
}

void MainWindow::mixedVisualFiltersOn() {
    streamLFPFiltersOn->setChecked(false);
    streamSpikeFiltersOn->setChecked(false);
    streamNoFiltersOn->setChecked(false);
    streamStimViewOn->setChecked(false);
}

void MainWindow::spikeFiltersOn()
{
    activeWorkspacePointers.spikeConf->setAllModesOn(true, false, false);
    streamSpikeFiltersOn->setChecked(true);
    streamLFPFiltersOn->setChecked(false);
    streamNoFiltersOn->setChecked(false);
    streamStimViewOn->setChecked(false);
}

void MainWindow::stimViewOn()
{
    activeWorkspacePointers.spikeConf->setAllModesOn(false, false, true);
    streamSpikeFiltersOn->setChecked(false);
    streamLFPFiltersOn->setChecked(false);
    streamNoFiltersOn->setChecked(false);
    streamStimViewOn->setChecked(true);
}

void MainWindow::noFiltersOn(){
    activeWorkspacePointers.spikeConf->setAllModesOn(false, false, false);
    streamSpikeFiltersOn->setChecked(false);
    streamLFPFiltersOn->setChecked(false);
    streamNoFiltersOn->setChecked(true);
    streamStimViewOn->setChecked(false);
}
//void MainWindow::linkChanges(bool link)
//{
//    linkChangesBool = link;
////    if (link) {
////        actionLinkChanges->setChecked(true);
////        actionUnLinkChanges->setChecked(false);
////        linkChangesButton->setChecked(true);
////    }
////    else {
////        actionLinkChanges->setChecked(false);
////        actionUnLinkChanges->setChecked(true);
////        linkChangesButton->setChecked(false);
////    }
//}

////link methods also separated by the true and false components
//void MainWindow::linkChanges()
//{
//    linkChangesBool = true;
////    actionLinkChanges->setChecked(true);
////    actionUnLinkChanges->setChecked(false);
////    linkChangesButton->setChecked(true);
//}

void MainWindow::uncoupleDisplay(bool uncouple)
{
    if (channelsConfigured) {
        uncoupleDisplayButton->setChecked(uncouple);
//        uncoupleDisplayButton->setRedDown(uncouple);
        eegDisp->freezeDisplay(uncouple);
        uncoupleDisplayButton->setblink(uncouple);
    }

}

void MainWindow::setRealTimeDisplayMode(bool realtime)
{
    if (spikeDisp) {
        spikeDisp->setRealTimeMode(realtime);
    }
    if (realtime) {
        spikesButton->setChecked(false);
        spikesButtonToggled(false);
        spikesButton->setEnabled(false);
        eegDisp->setUpdateInterval(500);
    } else {
        spikesButton->setEnabled(true);
        eegDisp->setUpdateInterval(100);
    }


}

void MainWindow::streamdisplaybuttonPressed(){
    streamDisplayButton->setDown(false);
    StreamDisplayOptionsDialog *dialog =
            new StreamDisplayOptionsDialog(streamLFPFiltersOn->isChecked(), streamSpikeFiltersOn->isChecked(), streamNoFiltersOn->isChecked(), streamStimViewOn->isChecked(), this);
    connect(dialog, &StreamDisplayOptionsDialog::lfpChosen, this, &MainWindow::lfpFiltersOn);
    connect(dialog, &StreamDisplayOptionsDialog::spikeChosen, this, &MainWindow::spikeFiltersOn);
    connect(dialog, &StreamDisplayOptionsDialog::rawChosen, this, &MainWindow::noFiltersOn);
    connect(dialog, &StreamDisplayOptionsDialog::stimChosen, this, &MainWindow::stimViewOn);
    connect(dialog, &StreamDisplayOptionsDialog::ticksToggled, streamManager, &StreamProcessorManager::setDisplaySpikeTicks);
    connect(this, &MainWindow::closeAllWindows, dialog, &StreamDisplayOptionsDialog::close);

    dialog->setWindowFlags(Qt::Popup);
    dialog->setGeometry(QRect(this->geometry().x()+streamDisplayButton->x(),this->geometry().y()+streamDisplayButton->y()+streamDisplayButton->height()+this->menuBar()->height(),75,100));

    dialog->show();
}

//void MainWindow::unLinkChanges()
//{
//    linkChangesBool = false;
////    actionLinkChanges->setChecked(false);
////    actionUnLinkChanges->setChecked(true);
//}

void MainWindow::toggleAllFilters(bool on) {

  linkChangesBool = false;

  for (int i = 0; i < activeWorkspacePointers.spikeConf->ntrodes.length();i++) {
    activeWorkspacePointers.spikeConf->setFilterSwitch(i,on);

  }
  if (isAudioOn)
      emit updateAudio();
  linkChangesBool = true;
}

void MainWindow::toggleAllRefs(bool on)
{
    linkChangesBool = false;
    for (int i = 0; i < activeWorkspacePointers.spikeConf->ntrodes.length(); i++) {
        activeWorkspacePointers.spikeConf->setRefSwitch(i, on);
    }
    if (isAudioOn)
        updateAudio();

    linkChangesBool = true;
}

void MainWindow::toggleAllTriggers(bool on) {
    for (int i = 0; i < activeWorkspacePointers.spikeConf->ntrodes.length(); i++) {
        activeWorkspacePointers.spikeConf->setTriggerMode(i,on);
    }
    if (isAudioOn)
        updateAudio();
}

void MainWindow::setAllMaxDisp(int newMaxDisp)
{
    linkChangesBool = false;
    for (int i = 0; i < activeWorkspacePointers.spikeConf->ntrodes.length(); i++) {
        activeWorkspacePointers.spikeConf->setMaxDisp(i, newMaxDisp);
        /*
        for (int j = 0; j < spikeConf->ntrodes[i]->maxDisp.length(); j++) {
            spikeConf->setMaxDisp(i, j, newMaxDisp);
        }*/
    }


    linkChangesBool = true;
}

void MainWindow::setMaxDisp(int newMaxDisp) {

    //We need to disconnect these signals to prevent an infinite loop:
    disconnect(eegDisp, SIGNAL(setNewMaxDisp(int)), this, SLOT(setMaxDisp(int)));
    disconnect(spikeDisp, SIGNAL(sig_newMaxDisp(int)), this, SLOT(setMaxDisp(int)));

    QHashIterator<int, int> iter(selectedNTrodes);
    while (iter.hasNext()) {
        iter.next();
        if (iter.key() >= activeWorkspacePointers.spikeConf->ntrodes.length() || iter.key() < 0) {
            qDebug() << "Error: Selected Index out of array range. (MainWindow::setMaxDisp)";
            continue;
        }
        activeWorkspacePointers.spikeConf->setMaxDisp(iter.key(), newMaxDisp);
//        SingleSpikeTrodeConf *curNTrode = spikeConf->ntrodes[iter.key()];
//        for (int i = 0; i < curNTrode->maxDisp.length(); i++) {
//            spikeConf->setMaxDisp(iter.key(), i, newMaxDisp);
//        }
    }
    if (triggerSettings != nullptr) {
        triggerSettings->setMaxDisplay(newMaxDisp);
    }

    //reconnect the signals
    connect(eegDisp, SIGNAL(setNewMaxDisp(int)), this, SLOT(setMaxDisp(int)));
    connect(spikeDisp, SIGNAL(sig_newMaxDisp(int)), this, SLOT(setMaxDisp(int)));

}

void MainWindow::setAllRefs(int nTrode, int channel)
{
    linkChangesBool = false;
    for (int i = 0; i < activeWorkspacePointers.spikeConf->ntrodes.length(); i++) {
        activeWorkspacePointers.spikeConf->setReference(i, nTrode, channel);
    }
    if (isAudioOn)
        updateAudio();

    linkChangesBool = true;
}

void MainWindow::setAllFilters(int low, int high)
{
    linkChangesBool = false;

    for (int i = 0; i < activeWorkspacePointers.spikeConf->ntrodes.length(); i++) {
        activeWorkspacePointers.spikeConf->setLowFilter(i, low);
        activeWorkspacePointers.spikeConf->setHighFilter(i, high);
    }
    if (isAudioOn)
        emit updateAudio();

    linkChangesBool = true;
}

void MainWindow::setAllThresh(int newThresh)
{
    linkChangesBool = false;

    for (int i = 0; i < activeWorkspacePointers.spikeConf->ntrodes.length(); i++) {
        activeWorkspacePointers.spikeConf->setThresh(i, newThresh);
        /*
        for (int j = 0; j < spikeConf->ntrodes[i]->thresh.length(); j++) {
            spikeConf->setThresh(i, j, newThresh);
        }*/
    }

    linkChangesBool = true;
}

void MainWindow::checkRestartModules(void)
{
    int ret = QMessageBox::warning(this, tr("Restart Modules?"),
                                   tr("Are you sure you want to quit and restart the modules?"),
                                   QMessageBox::No | QMessageBox::Yes, QMessageBox::Yes);

    if (ret == QMessageBox::Yes) {
        quitModules();
        QThread::sleep(2);
        emit clearDataAvailable();
        QThread::sleep(2); //MARK: todo change this to not be a simple sleep, perhaps a threadlock until clear is finished would be best OR add clearDataAvailable to the startModule routine
        startModules(loadedConfigFile);
    }
}

void MainWindow::quitModules(void)
{

    if (!channelsConfigured) {
        return;
    }
    if (activeWorkspacePointers.networkConf->networkType == NetworkConfiguration::zmq_based) {
        if(trodesModule) {
            trodesModule->sendQuit();
        }
    } else if (activeWorkspacePointers.networkConf->networkType == NetworkConfiguration::qsocket_based) {

        //Send out a quit signal to all modules
        if ((channelsConfigured) && (trodesNet->tcpServer->nConnections())) {
            TrodesMessage *trodesMessage = new TrodesMessage;
            trodesMessage->messageType = TRODESMESSAGE_QUIT;
            emit messageForModules(trodesMessage);
        }
    }
    QThread::msleep(250); //Give the modules some time to quit
}

void MainWindow::clearAll()
{

    if (spikeDisp != nullptr) {
        spikeDisp->clearAllButtonPressed();
    }
    //for (int i = 0; i < ntrodeDisplayWidgetPtrs.length(); i++) {
    //    ntrodeDisplayWidgetPtrs[i]->clearButtonPressed();
    //}
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    if (event->key() == Qt::Key_P) {

//        loadingScreen->debug();
    }

    QMainWindow::keyPressEvent(event);
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    if (closeInitiated) {
        event->accept();
        return;
    }

    closeInitiated = true;
    if (channelsConfigured) {
        if (playbackFileOpen) {
            pauseButtonPressed();
            setSource(SourceNone);

        } else {
            if (recordFileOpen) {
                closeFile();
            }

            reConfig_live_close();
            channelsConfigured = false;
        }
    }

    workspaceShutdownCheckTimer = new QTimer(this);
    connect(workspaceShutdownCheckTimer, &QTimer::timeout, this, &MainWindow::exitProgramLater);
    workspaceShutdownCheckTimer->start(1000); //We need to allow enough time for the source controller and streamProcessor threads to end

    event->ignore();

    /*
    //Quit all threads and close all windows before accepting the close event
    qDebug() << "[MainWindow::closeEvent] Closing Trodes";
    if (dataStreaming || playbackFileOpen) {
        //stop streaming data
        //quitting = true;
        if(!disconnectFromSource()){
            event->ignore();
            return;
        }
        //event->ignore();

        //return;
        QThread::msleep(250);
    }

    sourceControl->setSource(SourceNone); //closes and deletes all source threads
    QThread::msleep(250);

    //quitModules();
    if (channelsConfigured) {
        closeConfig();
    }
    if (isAudioOn) {
        soundOut->endAudio();
        emit endAudioThread();
    }
    QThread::msleep(250);
    emit endAllThreads();
     QThread::msleep(250);

    benchmarkingControlPanel->close();


//    trodesButtonToggled(false);
    trodeSettingsButton->setChecked(false);

    if (preferencesPanel != nullptr){
        preferencesPanel->saveSettings();
        delete preferencesPanel;
    }

    if (networkPanel != nullptr){
        delete networkPanel;
    }
    emit closeAllWindows();
    qApp->closeAllWindows();
    event->accept();
    */
}

void MainWindow::exitProgramLater()
{

    workspaceShutdownCheckTimer->stop();

    if (liveReconfigHappening) {

        streamManager->removeAllProcessors();

        delete streamManager;
        delete spikeDisp;
        spikeDisp = nullptr;
        if (eegDisp != nullptr) delete eegDisp;
    }


    if (isAudioOn) {
        soundOut->endAudio();
        emit endAudioThread();
    }


    benchmarkingControlPanel->close();

    trodeSettingsButton->setChecked(false);

    if (preferencesPanel != nullptr){
        preferencesPanel->saveSettings();
        delete preferencesPanel;
    }

    if (networkPanel != nullptr){
        delete networkPanel;
    }

    quitModules();

    emit closeAllWindows();
    qApp->closeAllWindows();
    qApp->quit();
}

void MainWindow::resizeEvent(QResizeEvent *)
{
    if (isAudioOn)
        emit closeSoundDialog();
    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("position"));

    settings.setValue(QLatin1String("geometry"), saveGeometry());

    settings.setValue(QLatin1String("position"), this->geometry());
    settings.endGroup();
}

void MainWindow::moveEvent(QMoveEvent *)
{
    if (isAudioOn)
        emit closeSoundDialog();
    //Remember the new size for the next session
    QSettings settings(QLatin1String("SpikeGadgets"), QLatin1String("Trodes"));

    settings.beginGroup(QLatin1String("position"));
    settings.setValue(QLatin1String("geometry"), saveGeometry());

    settings.setValue(QLatin1String("position"), this->geometry());
    settings.endGroup();
}


void MainWindow::paintEvent(QPaintEvent *event)
{
    if (!eventTabWasChanged) {
        event->accept();
    }
    else {
        //QPixmap pixmap(size());
        QPainter painter;

        //painter.begin(&pixmap);
        //render(&painter);
        //painter.end();

        // Do processing on pixmap here

        painter.begin(this);
        //painter.drawPixmap(0, 0, pixmap);
        render(&painter);
        painter.end();
    }
}


void MainWindow::eventTabChanged(int newTab)
{
    if (eventTabsInitialized[newTab] == false) {
        eventTabWasChanged = true;
        visibleTime = 0;
        eventTabsInitialized[newTab] = true;
    }
}

void MainWindow::resetAllAudioButtons()
{
    //for (int i = 0; i < spikeDisp->ntrodeWidgets.length(); i++) {
    //    spikeDisp->ntrodeWidgets[i]->triggerScope->turnOffAudio();
    //}
}

QString MainWindow::calcTimeString(uint32_t tmpTimeStamp)
{
    QString currentTimeString("");
//    uint32_t tmpTimeStamp = currentTimeStamp;
    int hoursPassed = floor(tmpTimeStamp / (activeWorkspacePointers.hardwareConf->sourceSamplingRate * 60 * 60));

    tmpTimeStamp = tmpTimeStamp - (hoursPassed * 60 * 60 * activeWorkspacePointers.hardwareConf->sourceSamplingRate);
    int minutesPassed = floor(tmpTimeStamp / (activeWorkspacePointers.hardwareConf->sourceSamplingRate * 60));
    tmpTimeStamp = tmpTimeStamp - (minutesPassed * 60 * activeWorkspacePointers.hardwareConf->sourceSamplingRate);
    int secondsPassed = floor(tmpTimeStamp / (activeWorkspacePointers.hardwareConf->sourceSamplingRate));
    tmpTimeStamp = tmpTimeStamp - (secondsPassed * activeWorkspacePointers.hardwareConf->sourceSamplingRate);
//    int tenthsPassed = floor(((tmpTimeStamp * 10) / hardwareConf->sourceSamplingRate));

    if (hoursPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(hoursPassed));
    currentTimeString.append(":");
    if (minutesPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(minutesPassed));
    currentTimeString.append(":");
    if (secondsPassed < 10)
        currentTimeString.append("0");
    currentTimeString.append(QString::number(secondsPassed));
//    currentTimeString.append(".");
//    currentTimeString.append(QString::number(tenthsPassed));

    return currentTimeString;
}

void MainWindow::setEnableAVXFlag(bool value){
    enableAVXFlag = value;
}

void MainWindow::sendModuleDataChanToModules(int nTrode, int chan)
{
//    spikeConf->ntrodes[nTrode]->moduleDataChan = chan;

    QByteArray msg;
    TrodesDataStream tmpStream(&msg, QIODevice::ReadWrite);

    tmpStream << nTrode << chan;
    TrodesMessage *tm = new TrodesMessage(TRODESMESSAGE_NTRODEMODULEDATACHAN, msg, this);
    qDebug() << "[MainWindow::sendModuleDataChanToModules]: sending new moduleData chan to modules";
    emit messageForModules(tm);
    //trodesNet->tcpServer->sendMessageToModules(tm);
}

void MainWindow::setModuleDataStreaming(bool streamOn)
{
    moduleDataStreaming = streamOn;
}

bool MainWindow::isModuleDataStreaming(void)
{
    return moduleDataStreaming;
}


void MainWindow::broadcastEvent(TrodesEventMessage ev) {

    emit sendEvent(currentTimeStamp, ev.getTime(), ev.getEventMessage());

}

void MainWindow::raiseWorkspaceGui() {
//    qDebug() << "Lets create some shit, YEEEEE HAWWWW";
//    workspaceEditor->exec();
//    workspaceEditor->raise();
    //workspaceEditor->openEditor();
    workspaceEditor->openFileEditor();
}

void MainWindow::loadFileInWorkspaceGui(QString filePath) {
    workspaceEditor->loadFileIntoWorkspaceGui(filePath);
    raiseWorkspaceGui();
}

void MainWindow::loadFileInWorkspaceGui() {
    loadFileInWorkspaceGui(""); //Load default unconfigured workspace
}

void MainWindow::openTempWorkspace(QString tempPath, QString fileName) {
    //qDebug() << "Temp Path workspace -" << tempPath << fileName;


    openWorkspaceFileForAcquisition(tempPath);
    if (!fileName.isEmpty()) {
        loadedConfigFile = fileName;
        currentConfigFileName = fileName;
    } else {
        loadedConfigFile = "";
        currentConfigFileName = "";
    }

    tempWorkspaceLoading = true;
    openCustomSourceOptionsGUI();
    tempWorkspaceLoading = false;
//    QFile tmpFile(path);
//    tmpFile.remove(); //don't remove temp file because if you do that will crash modules set up to 'send config info'
}

//MARK: select by dialog
void MainWindow::openSelectionDialog(void) {
//    qDebug() << "Open select by dialog";
    if (nTrodeSelectWindow == nullptr) {
        nTrodeSelectWindow = new NTrodeSelectionDialog();
        connect(nTrodeSelectWindow, SIGNAL(sig_selectTags(int,QHash<GroupingTag,int>)), this, SLOT(receivedSelectByTagCommand(int,QHash<GroupingTag,int>)));
        connect(nTrodeSelectWindow, SIGNAL(sig_clearSelection()), this, SLOT(clearAllSelected()));
        connect(this, SIGNAL(closeAllWindows()), nTrodeSelectWindow, SLOT(close()));
    }

    if (nTrodeSelectWindow->isVisible()) {
        nTrodeSelectWindow->setVisible(false);
    }
    else {
        nTrodeSelectWindow->update();
        nTrodeSelectWindow->show();
        nTrodeSelectWindow->raise();
    }
}

void MainWindow::receivedSelectByTagCommand(int operation, QHash<GroupingTag, int> selectedTags) {
//    qDebug() << "receivedSelectByTagCommand";

    //clear previously selected
    QHashIterator<int, int> prevIter(selectedNTrodes);
    while (prevIter.hasNext()) { //set the labels of the previously selected nTrodes to black
        prevIter.next();
        eegDisp->setNTrodeSelected(prevIter.key(), false); //deselect all previous nTrodes
    }
    selectedNTrodes.clear();
    if (selectedTags.isEmpty()) //if no tags were selected, then return
        return;
    //possibly add in the currentTrodeSelected here to preserve the highlighted selection

    if (operation == FO_OR) {
//        qDebug() << "   performing OR";
        for (int i = 0; i < activeWorkspacePointers.spikeConf->ntrodes.length(); i++) {
            SingleSpikeTrodeConf *curNTrode = activeWorkspacePointers.spikeConf->ntrodes[i];

            QHashIterator<GroupingTag, int> trodeIter(curNTrode->gTags);
            while (trodeIter.hasNext()) {
                trodeIter.next();
                if (selectedTags.contains(trodeIter.key())) {
                    eegDisp->setNTrodeSelected(i, true);
                    //MAYBE check selectedNTrodes for identical entries...
                    selectedNTrodes.insert(i, curNTrode->nTrodeId);
                    break;
                }
            }
        }
    }
    else if (operation == FO_AND) {
//        qDebug() << "   performing And";
        for (int i = 0; i < activeWorkspacePointers.spikeConf->ntrodes.length(); i++) {
            SingleSpikeTrodeConf *curNTrode = activeWorkspacePointers.spikeConf->ntrodes[i];

            QHashIterator<GroupingTag, int> tagsIter(selectedTags);
            //assume that the curNTrode at index i will have all tags
            eegDisp->setNTrodeSelected(i, true);
            selectedNTrodes.insert(i, curNTrode->nTrodeId);

            while (tagsIter.hasNext()) {
                tagsIter.next();
                if (!curNTrode->gTags.contains(tagsIter.key())) { //if the curNTrode doesn't have a single one of the selected tags, deselect it
                    eegDisp->setNTrodeSelected(i, false);
                    //MAYBE check selectedNTrodes for identical entries...
                    selectedNTrodes.remove(i);
                    break;
                }
            }
        }
    }

    selectedNTrodesUpdated(); //update the ntrode settings panel

    currentTrodeSelected = selectedNTrodes.begin().key();
    spikeDisp->setShownNtrode(currentTrodeSelected);
    eegDisp->updateAudioHighlightChannel(activeWorkspacePointers.spikeConf->ntrodes[currentTrodeSelected]->hw_chan.first());

}

void MainWindow::clearAllSelected() {
//    qDebug() << "clear all selected";
    //clear previously selected
    QHashIterator<int, int> prevIter(selectedNTrodes);
    while (prevIter.hasNext()) { //set the labels of the previously selected nTrodes to black
        prevIter.next();
        eegDisp->setNTrodeSelected(prevIter.key(), false); //deselect all previous nTrodes
    }
    selectedNTrodes.clear();

    selectedNTrodesUpdated(); //update the ntrode settings panel
}

void MainWindow::selectAllNTrodes() {
    clearAllSelected();
    for (int i = 0; i < activeWorkspacePointers.spikeConf->ntrodes.length(); i++) {
        SingleSpikeTrodeConf *curNTrode = activeWorkspacePointers.spikeConf->ntrodes[i];
        eegDisp->setNTrodeSelected(i, true);
        selectedNTrodes.insert(i, curNTrode->nTrodeId);
    }
    selectedNTrodesUpdated(); //update the ntrode settings panel

    currentTrodeSelected = selectedNTrodes.begin().key();
    spikeDisp->setShownNtrode(currentTrodeSelected);
    eegDisp->updateAudioHighlightChannel(activeWorkspacePointers.spikeConf->ntrodes[currentTrodeSelected]->hw_chan.first());
}

//MARK: benchmarking dialog
void MainWindow::openBenchmarkingDialog() {
    //qDebug() << "open benchmarking control pannel!";
    benchmarkingControlPanel->show();
    benchmarkingControlPanel->raise();
}

void MainWindow::processPlaybackCommand(qint8 flg, qint32 timestamp) {
//    qDebug() << "playback command received: " << flg << " -- " << timestamp;
    switch(flg) {
    case PC_PAUSE: {
        if (dataStreaming) {
            pauseButtonPressed();
            pauseButtonReleased();
        }
        break;
    }
    case PC_PLAY: {
        playButtonPressed();
        playButtonReleased();
        break;
    }
    case PC_STOP: {
        break;
    }
    case PC_SEEK:{
        approxSliderMove(timestamp);
        break;
    }
    case PC_NULL: {
        //null command
        break;
    }
    default:
        qDebug() << "Error: Invalid playback command flag [" << flg << "] received. (MainWindow::processPlaybackCommand)";
        break;
    }
}


//--------------------------------------------------------------------------------
//File Playback Slider functions

void MainWindow::movingSlider(int action){
    //action is enum value, 0 is NoAction and 7 is SliderMove. QAbstractSlider enums
    if(action > 0){
        emit jumpFileTo((qreal)playbackSlider->sliderPosition()/playbackSlider->maximum());
    }
}

void MainWindow::updateTimeFromSlider(int value){
    if(playbackSlider->isEnabled()){
        qreal pct = (qreal)value / (qreal)(playbackSlider->maximum() - playbackSlider->minimum());
        uint32_t time = (playbackEndTime - playbackStartTime)*pct + playbackStartTime;
        timeLabel->setText(calcTimeString(time));
    }
}
void MainWindow::updateSlider(qreal pct){
    if(playbackSlider->isEnabled() && !playbackSlider->isSliderDown()){
        playbackSlider->setValue(pct*playbackSlider->maximum());
    }
}

void MainWindow::sliderisPressed(){
    if(!pauseButton->isDown())
        sourceControl->pauseSource();

}

void MainWindow::sliderIsReleased(){
    if(!pauseButton->isDown())
        connectToSource();
    streamManager->clearAllDigitalStateChanges(); //clears remembered DIO state changes
    clearAll(); // clears skipe scatter plots.
}

void MainWindow::pausePlaybackSignal(){
    emit signal_sendPlaybackCommand(PC_PAUSE, currentTimeStamp);
}

void MainWindow::playPlaybackSignal(){
    emit signal_sendPlaybackCommand(PC_PLAY, currentTimeStamp);
}

void MainWindow::seekPlaybackSignal(uint32_t t){
    emit signal_sendPlaybackCommand(PC_SEEK, t);
}

void MainWindow::approxSliderMove(uint32_t timestamp){
    qreal pct = (qreal)(timestamp-playbackStartTime)/(playbackEndTime - playbackStartTime);
    emit jumpFileTo(pct);


}

/*void MainWindow::quickstartEthernet(){
    //STUB:  DELETE THIS FUNCTION

    //    qDebug() << "-------quickstart ethernet";
    QMessageBox *box = new QMessageBox(nullptr);//("Connecting", "Connecting to ethernet...");
    box->setAttribute(Qt::WA_DeleteOnClose);
    box->setWindowTitle("Connecting...");
    box->setText(tr("Connecting to ethernet..."));
    box->setModal(false);
    box->open();
    actionSourceEthernet->trigger();
    box->close();
    if(sourceControl->ethernetSource && sourceControl->ethernetSource->state == SOURCE_STATE_INITIALIZED){
        int psize = sourceControl->ethernetSource->MeasurePacketLength(headstageSettings);
        QuickSetup *setup = new QuickSetup(controllerSettings, headstageSettings, psize);
        connect(setup, &QuickSetup::CreateWorkspace, this, &MainWindow::generateWorkspace);
        connect(setup, &QuickSetup::OpenWorkspaceEditor, this, &MainWindow::launchWorkspaceEditorFromQuickstart);
        connect(setup, &QuickSetup::CancelPressed, actionSourceNone, &QAction::trigger);
        connect(setup, &QuickSetup::rejected, actionSourceNone, &QAction::trigger);
        connect(this, &MainWindow::closeAllWindows, setup, &QuickSetup::close);
        setup->show();
        setup->raise();
        setup->activateWindow();
    }
}

void MainWindow::quickstartUSB(){
    //STUB:  DELETE THIS FUNCTION
    //    qDebug() << "-------quickstart usb";
    QMessageBox *box = new QMessageBox(nullptr);//("Connecting", "Connecting to ethernet...");
    box->setAttribute(Qt::WA_DeleteOnClose);
    box->setWindowTitle("Connecting...");
    box->setText(tr("Connecting to USB..."));
    box->setModal(false);
    box->open();
    actionSourceUSB->trigger();
    box->close();
    if(sourceControl->USBSource && sourceControl->USBSource->state == SOURCE_STATE_INITIALIZED){
        int psize = sourceControl->USBSource->MeasurePacketLength(headstageSettings);
        QuickSetup *setup = new QuickSetup(controllerSettings, headstageSettings, psize);
        connect(setup, &QuickSetup::CreateWorkspace, this, &MainWindow::generateWorkspace);
        connect(setup, &QuickSetup::CancelPressed, actionSourceNone, &QAction::trigger);
        connect(setup, &QuickSetup::rejected, actionSourceNone, &QAction::trigger);
        connect(this, &MainWindow::closeAllWindows, setup, &QuickSetup::close);
        connect(setup, &QuickSetup::OpenWorkspaceEditor, this, [this, setup](bool ecuconnected, bool rfconnected, int chansperntrode, int psize, int samplingRate){
            disconnect(setup, &QuickSetup::CancelPressed, actionSourceNone, &QAction::trigger);
            disconnect(setup, &QuickSetup::rejected, actionSourceNone, &QAction::trigger);
            launchWorkspaceEditorFromQuickstart(ecuconnected, rfconnected, chansperntrode, psize, samplingRate);
        });
        setup->show();
        setup->raise();
        setup->activateWindow();
    }
}*/

void MainWindow::generateWorkspace(bool ecuconnected, bool rfconnected, int chansperntrode, int psize, int samplingRate){
    //STUB:  DELETE THIS FUNCTION
    //    qDebug() << "generating workspace with ecu: " << ecuconnected << "rf: " << rfconnected << "chanspernt" << chansperntrode << "samplingrate:" << samplingRate;
    WorkspaceEditorDialog *workspaceeditor = fillInWorkspaceEditor(ecuconnected, rfconnected, chansperntrode, psize, samplingRate);
    if(workspaceeditor->workspaceGui->saveToXML("temp.trodesconf", true)){
        openTempWorkspace("temp.trodesconf","");
    }

    finalizeFromQuickstart();
}

void MainWindow::launchWorkspaceEditorFromQuickstart(bool ecuconnected, bool rfconnected, int chansperntrode, int psize, int samplingRate){
    WorkspaceEditorDialog *workspaceeditor = fillInWorkspaceEditor(ecuconnected, rfconnected, chansperntrode, psize, samplingRate);
    connect(workspaceEditor,SIGNAL(sig_openTempWorkspace(QString,QString)),this,SLOT(openTempWorkspace(QString,QString)));

    //connect(workspaceeditor, &WorkspaceEditorDialog::sig_openTempWorkspace, this, &MainWindow::openTempWorkspace);
//    workspaceeditor->enableOpenButton();
    workspaceeditor->show();
    //int ret = workspaceeditor->openEditor();
    int ret = workspaceeditor->openFileEditor();
    if(ret){
        finalizeFromQuickstart();
    }
}

WorkspaceEditorDialog *MainWindow::fillInWorkspaceEditor(bool ecuconnected, bool rfconnected, int chansperntrode, int psize, int samplingRate){

    //STUB:  DELETE THIS FUNCTION

    qDebug() << "Filling workspace editor from MainWindow";


    WorkspaceEditorDialog *workspaceeditor = new WorkspaceEditorDialog;
//    WorkspaceEditor *workspaceeditor = new WorkspaceEditor(M_EMBEDDED);
    workspaceeditor->hide();
    workspaceeditor->loadFileIntoWorkspaceGui("");

    if(ecuconnected){
        workspaceeditor->workspaceGui->addECU();
    }

    if(rfconnected){
        workspaceeditor->workspaceGui->addRF();
    }

//    if(headstageSettings.accelSensorAvailable
//            || headstageSettings.gyroSensorAvailable
//            || headstageSettings.magSensorAvailable)
//    {
    if(headstageSettings.valid && headstageSettings.numberOfChannels){
        int workspacepsize= headstageSettings.numberOfChannels*sizeof(int16_t) + sizeof(uint32_t) + 2;
        if(psize-workspacepsize == 8){
            workspaceeditor->workspaceGui->addHSSensors();//Add in sensors
        }
    }

    workspaceeditor->workspaceGui->displayAllAux();

    int numchans = headstageSettings.valid ? headstageSettings.numberOfChannels : 0;
    int ntrodes = numchans/chansperntrode;
    workspaceeditor->workspaceGui->setNumChannels(numchans);

    QList<int> channelmapping;
    for(int i = 0; i < numchans/chansperntrode; ++i){
        for(int j = 0; j < chansperntrode; ++j){
            channelmapping.append(i+1);
        }
    }
    workspaceeditor->workspaceGui->setChannelMap(channelmapping);

    workspaceeditor->workspaceGui->setSamplingRate(samplingRate);

    int cols, pages;
    if(ntrodes == 2){
        cols = 1;
        pages = 1;
    }
    else if(numchans < 256){
        cols = 2;
        pages = 2;
    }
    else{
        cols = 4;
        pages = 4;
    }
    workspaceeditor->workspaceGui->configureDisplay(cols, pages);

    return workspaceeditor;
}

void MainWindow::finalizeFromQuickstart(){
    for(int i = 0; i < activeWorkspacePointers.spikeConf->ntrodes.length(); ++i){
        if(i % 8 == 0){
            activeWorkspacePointers.spikeConf->setColor(i, QColor("#fafafa"));
        }
        if(i % 8 == 1){
            activeWorkspacePointers.spikeConf->setColor(i, QColor("#fa3232"));
        }
        if(i % 8 == 2){
            activeWorkspacePointers.spikeConf->setColor(i, QColor("#14fa14"));
        }
        if(i % 8 == 3){
            activeWorkspacePointers.spikeConf->setColor(i, QColor("#1414fa"));
        }
        if(i % 8 == 4){
            activeWorkspacePointers.spikeConf->setColor(i, QColor("#ff00ff"));
        }
        if(i % 8 == 5){
            activeWorkspacePointers.spikeConf->setColor(i, QColor("#e9b96e"));
        }
        if(i % 8 == 6){
            activeWorkspacePointers.spikeConf->setColor(i, QColor("#729fcf"));
        }
        if(i % 8 == 7){
            activeWorkspacePointers.spikeConf->setColor(i, QColor("#2eff94"));
        }

    }
    activeWorkspacePointers.streamConf->setBackgroundColor(QColor("#202020"));
    settingsChanged();

    DataSource currsrc = sourceControl->currentSource;
    actionSourceNone->trigger();
    if(currsrc == SourceEthernet){
        actionSourceEthernet->trigger();
    }
    else if(currsrc == SourceUSBDAQ){
        actionSourceUSB->trigger();
    }
}
