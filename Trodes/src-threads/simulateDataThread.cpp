/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "simulateDataThread.h"
#include "globalObjects.h"
#include "CZHelp.h"

simulateDataRuntime::simulateDataRuntime(QObject *parent):
    waveModulatorFrequency(0.0)
    , waveFrequency(400.0)
    , waveAmplitude(200.0)
    , threshold(0)
    , nSecElapsed(0)
    , currentSampleNum(0)
    , waveRes(5000)
    , currentCyclePosition(0)
    , cyclePositionCarryOver(0)
    , currentModulatorCyclePosition(0) {

    loopFinished = true;
    generateData();

}

simulateDataRuntime::~simulateDataRuntime() {

}

void simulateDataRuntime::Run() {

    emit startHelperThread();

    //int PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + 1 + 1;
//    PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + (2*hardwareConf->headerSize); //Packet size in bytes
    calculatePacketSize();

    quitNow = false;
    acquiring = true;
    runLoopActive = true;
    //timeKeeper->start();
    stopWatch.start();


    //Some variables for implementing a 'brake' for the loop
    QElapsedTimer loopTimer;
    qint64 optimalLoopTime_nS = (qint64)(1/((double) hardwareConf->sourceSamplingRate)*1000000000);
    qint64 loopTimeSurplus_nS;

    qDebug() << "Signal generator loop running....";
    loopFinished = false;
    qint64 samplesWritten = 0;
    int16_t currentValue;
    // float cycleTime; // UNUSED
    float cycleStepsPerSample;
    float nSecPerSample;

    double modulatorFreqStepsDue = 0.0;
    modulatorFreqStepSize = waveModulatorFrequency *((double) waveRes / (double) hardwareConf->sourceSamplingRate);

    calculateHeaderSize();
    double convertFactor = (double) 65536/ (double) AD_CONVERSION_FACTOR; //assumes Intan-based source

    while (!quitNow) {

        if (!acquiring) {
            stopWatch.restart();
            samplesWritten = 0;
            //currentTimeStamp = 0;
            QThread::msleep(100);
        } else {
            loopTimer.restart();
            samplesPerCycle = (hardwareConf->sourceSamplingRate/waveFrequency);
            cycleStepsPerSample = waveRes/samplesPerCycle;

            //float nSecPerSample = 1000000000.0/hardwareConf->sourceSamplingRate;
            nSecPerSample = 1000.0/hardwareConf->sourceSamplingRate;
            //nSecElapsed = nSecElapsed + stopWatch.nsecsElapsed();
            //nSecElapsed = stopWatch.nsecsElapsed();
            nSecElapsed = stopWatch.elapsed();

            //stopWatch.restart();
            int samplesDue = floor(nSecElapsed/nSecPerSample)-samplesWritten;
            //nSecElapsed = nSecElapsed - (nSecPerSample*samplesDue);


            while (samplesDue > 0) {

                if (waveModulatorFrequency == 0) {
                    //No modulator wave
                    currentValue = (int16_t)(sourceData[currentCyclePosition]*waveAmplitude*convertFactor);
                } else {

                    //Calulate the current position in the modulator frequency
                    modulatorFreqStepsDue = modulatorFreqStepsDue+modulatorFreqStepSize;
                    currentModulatorCyclePosition = (currentModulatorCyclePosition+ (int)modulatorFreqStepsDue)%waveRes;
                    //the current output value is multiplied by the modulator vaue
                    currentValue = (int16_t)(sourceData[currentCyclePosition]*waveAmplitude*((sourceData[currentModulatorCyclePosition]+1.0)/2)*convertFactor);
                    modulatorFreqStepsDue = modulatorFreqStepsDue - floor(modulatorFreqStepsDue);

                }

                /*
                cycleTime =  (float) currentTimeStamp/ (float) hardwareConf->sourceSamplingRate; // UNUSED
                currentValue =  (int16_t) ((double) waveAmplitude *
                                           qSin(2.0 * M_PI * ((double) waveFrequency * cycleTime)) *
                                           qCos(2.0 * M_PI * (((double) waveModulatorFrequency * cycleTime))));
                */
                if (qAbs(currentValue) < threshold*convertFactor) {
                    currentValue = 0;
                }

                for (int h = 0; h < headerConf->headerChannels.length();h++) {
                    if (headerConf->headerChannels[h].dataType == DeviceChannel::DIGITALTYPE) {
                        //qDebug() << headerConf->headerChannels[h].startByte;
                        //rawData.digitalInfo[(rawData.writeIdx*hardwareConf->headerSize)+(headerConf->headerChannels[h].startByte/2)] = currentValue; //Decomposes the waveform value into bits
                        rawData.digitalInfo[(rawData.writeIdx*headerSize)+(headerConf->headerChannels[h].startByte/2)] = currentValue > 0; // lower the frequency of digital changes
                    }
                    else if (headerConf->headerChannels[h].dataType == DeviceChannel::INT16TYPE) {
                        // TO DO: add analogIO output
                        if (headerConf->headerChannels[h].interleavedDataIDByte != -1) {
                            //interleavedDataIDBytePtr = ((char*)(rawData.digitalInfo + (rawIdx * hardwareConf->headerSize))) + headerConf->headerChannels[h].interleavedDataIDByte;
                            //if (*interleavedDataIDBytePtr & (1 << headerConf->headerChannels[h].interleavedDataIDBit)) {
                            //    //This interleaved data point belongs to this channel, so update the channel. Otherwise, no update occurs.
                            //    rawDataSample.headerData[h] = *((int16_t*)(startBytePtr)); //change to 16-bit pointer, then dereference
                            //}
                        } else {
                            rawData.digitalInfo[(rawData.writeIdx*headerSize)+(headerConf->headerChannels[h].startByte/2)] = currentValue; //Write waveform value for the other slots
                        }
                    }
                    else if (headerConf->headerChannels[h].dataType == DeviceChannel::UINT32TYPE) {
                        // TO DO: add analogIO output
                        if (headerConf->headerChannels[h].interleavedDataIDByte != -1) {
                            //interleavedDataIDBytePtr = ((char*)(rawData.digitalInfo + (rawIdx * hardwareConf->headerSize))) + headerConf->headerChannels[h].interleavedDataIDByte;
                            //if (*interleavedDataIDBytePtr & (1 << headerConf->headerChannels[h].interleavedDataIDBit)) {
                            //    //This interleaved data point belongs to this channel, so update the channel. Otherwise, no update occurs.
                            //    rawDataSample.headerData[h] = *((int16_t*)(startBytePtr)); //change to 16-bit pointer, then dereference
                            //}
                        } else {
                            //rawData.digitalInfo[(rawData.writeIdx*hardwareConf->headerSize)+(headerConf->headerChannels[h].startByte/2)] = currentValue; //Write waveform value for the other slots
                        }
                    }
                }
                //header
                /*
                for (int headerNum = 0; headerNum < hardwareConf->headerSize; headerNum++) {


                    if (headerNum==0) {
                        rawData.digitalInfo[(rawData.writeIdx*hardwareConf->headerSize)+headerNum] = currentValue; //Decomposes the waveform value into bits
                    } else {
                        rawData.digitalInfo[(rawData.writeIdx*hardwareConf->headerSize)+headerNum] = currentValue; //Write waveform value for the other slots
                    }
                }*/
                //rawData.digitalInfo[rawData.writeIdx] = 0;

                //time stamp
                //if (currentTimeStamp % 30000 == 0)
                //    qDebug() << "simulate: ts, value, index" << currentTimeStamp << currentValue << rawData.writeIdx;

                rawData.timestamps[rawData.writeIdx] = currentTimeStamp;

                // system time in nanoseconds
//                rawData.sysClock[rawData.writeIdx] = QDateTime::currentMSecsSinceEpoch() * 1000000;
                rawData.sysClock[rawData.writeIdx] = AbstractSourceRuntime::getSystemTimeNanoseconds();

                currentTimeStamp++;
                //rawData.sysTimestamps[rawData.writeIdx] = CZHelp::systemTimeMSecs();

                rawData.sysTimestamps[rawData.writeIdx] = QTime::currentTime().msecsSinceStartOfDay();

                //uchar statusByte = 0x40;
                uchar statusByte = 0;
                checkHardwareStatus(&statusByte);


                //Parse the stimulation info if it exists
                /*if (stimPacketSize > 0) {
                    const int stimStatusSize = stimPacketSize-2;
                    rawData.stimCommands[rawData.writeIdx*2] = 0;
                    rawData.stimCommands[(rawData.writeIdx*2)+1] = 0;
                    int stimStatusWriteByte = 0;
                    for (int statusByte = 2; statusByte < stimPacketSize; statusByte++) {
                        rawData.stimStatus[(rawData.writeIdx*stimStatusSize)+stimStatusWriteByte] = 0;
                        stimStatusWriteByte++;
                    }
                }*/


                for (int sampNum = 0; sampNum < hardwareConf->NCHAN; sampNum++) {
                    rawData.data[(rawData.writeIdx*hardwareConf->NCHAN)+sampNum] = currentValue;

                }

                //publishDataPacket();
                calculateReferences();

                samplesDue--;
                samplesWritten++;

                //Advance the write markers and release a semaphore
                writeMarkerMutex.lock();
                rawData.writeIdx = (rawData.writeIdx + 1) % EEG_BUFFER_SIZE;
                rawDataWritten++;
                writeMarkerMutex.unlock();



                for (int a = 0; a < rawDataAvailable.length(); a++) {
                    rawDataAvailable[a]->release(1);
                }
                sourceDataAvailable->release(1);




                //Calculate the next position in the sine wave
                cyclePositionCarryOver = cyclePositionCarryOver + cycleStepsPerSample;
                currentCyclePosition = (currentCyclePosition+(int)cyclePositionCarryOver) % waveRes;
                cyclePositionCarryOver = cyclePositionCarryOver - floor(cyclePositionCarryOver);


            }


            //A brake to make sure that the loop doesn't run faster than is has to
            loopTimeSurplus_nS = optimalLoopTime_nS-loopTimer.nsecsElapsed();


                //QThread::usleep((nSecPerSample/1000000)*5);





            if (loopTimeSurplus_nS > 1000) {
                QThread::usleep(5*loopTimeSurplus_nS/1000);
            }



        }

    }

    runtimeHelper->quitNow = true;

    loopFinished = true;
    runLoopActive = false;
    qDebug() << "Sim Data Runtime loop finished";

    emit finished();

}


/*
void simulateDataRuntime::endThread() {
    quit();
}*/


void simulateDataRuntime::generateData() {

   //int waveResolution = 5000;

    for (int sampleIndex = 0; sampleIndex < waveRes; sampleIndex++) {
        //sourceData[sampleIndex] = (int16_t) (qSin(2 * M_PI * (sampleIndex/waveRes)) * (65536/12500) );
        sourceData[sampleIndex] = (double) ((qSin(2 * M_PI * ((float) sampleIndex/ (float) waveRes)) ));


    }

}

void simulateDataRuntime::setModulatorFrequency(double cf) {
    waveModulatorFrequency = cf;
    modulatorFreqStepSize = waveModulatorFrequency *((double) waveRes / (double) hardwareConf->sourceSamplingRate);

}

void simulateDataRuntime::pullTimerExpired() {

/*
    if (aquiring) {
        nSecElapsed = nSecElapsed + stopWatch.nsecsElapsed(); //get the number of nanoseconds since the last timer call
        stopWatch.restart(); //reset the stopWatch to time the next cycle

        if (nSecElapsed > round(1000000000/hardwareConf->sourceSamplingRate)) {
            int
        }

    }
*/

}





simulateDataInterface::simulateDataInterface(QObject *) {
  state = SOURCE_STATE_NOT_CONNECTED;
  acquisitionThread = NULL;

}

simulateDataInterface::~simulateDataInterface() {


    /*if (acquisitionThread != NULL) {
        delete(acquisitionThread);
    }*/
}

void simulateDataInterface::SendSettleCommand() {
    qDebug() << "Sending fake settle command.";
}

void simulateDataInterface::SetStimulationParams(StimulationCommand s){
    qDebug() << "got stimulation command. Is valid? " << s.isValid();
    QByteArray data;
    qDebug() << "convert to statemachine: " << convertToStateMachineCommand(s, &data);
}

void simulateDataInterface::ClearStimulationParams(uint16_t slot){
    qDebug() << "got clear params command for slot " << slot;
}

void simulateDataInterface::SendStimulationStartSlot(uint16_t slot){
    qDebug() << "got send stim start slot " << slot;
}

void simulateDataInterface::SendStimulationStartGroup(uint16_t group){
    qDebug() << "got send stim start group " << group;
}

void simulateDataInterface::SendStimulationStopSlot(uint16_t slot){
    qDebug() << "got send stim stop slot " << slot;
}

void simulateDataInterface::SendStimulationStopGroup(uint16_t group){
    qDebug() << "got send stim stop group" << group;
}

void simulateDataInterface::SendImpendaceMeasureCommand(HardwareImpedanceMeasureCommand s) {
    //qDebug() << "Returning fake impedance value";
    emit impedanceValueReturned(s.channel,rand() % 1000);
}

bool simulateDataInterface::SendGlobalStimulationSettings(GlobalStimulationSettings s){
    qDebug() << "got send global stim settings";
    QByteArray data;
    if (!convertToStateMachineCommand(s, &data)) {
        return false;
    }
    return true;
}

void simulateDataInterface::SendGlobalStimulationAction(GlobalStimulationCommand s){
    qDebug() << "got send global stim action command";
    QByteArray data;
    qDebug() << "convert to statemachine: " << convertToStateMachineCommand(s, &data);
}
void simulateDataInterface::InitInterface() {

  //acquisitionThread->generateData(signalFrequency,signalAmplitude);

  //initialization went ok, so start the runtime thread
  acquisitionThread = new simulateDataRuntime(NULL);
  setUpThread(acquisitionThread);

  //acquisitionThread->connect(this, SIGNAL(startRuntime()), SLOT(Run()));

  state = SOURCE_STATE_INITIALIZED;
  emit stateChanged(SOURCE_STATE_INITIALIZED);

  HeadstageSettings headstageSettings;
  headstageSettings.autoSettleOn = false;
  headstageSettings.percentChannelsForSettle = 0;
  headstageSettings.threshForSettle = 0;
  headstageSettings.smartRefOn = false;
  headstageSettings.accelSensorOn = false;
  headstageSettings.gyroSensorOn = false;
  headstageSettings.magSensorOn = false;
  headstageSettings.rfChannel = 0;

  headstageSettings.smartRefAvailable = false;
  headstageSettings.autosettleAvailable = false;
  headstageSettings.accelSensorAvailable = false;
  headstageSettings.gyroSensorAvailable = false;
  headstageSettings.magSensorAvailable = false;
  headstageSettings.rfAvailable = false;

  emit headstageSettingsReturned(headstageSettings);

}

void simulateDataInterface::StartAcquisition(void) {

  rawData.writeIdx = 0; // location where we're currently writing

  acquisitionThread->acquiring = true;

  if (acquisitionThread->loopFinished) {
    emit startRuntime();
    qDebug() << "Told runtime to start";
  }

  emit acquisitionStarted();
  state = SOURCE_STATE_RUNNING;
  emit stateChanged(SOURCE_STATE_RUNNING);
}


void simulateDataInterface::StopAcquisition(void) {


  emit acquisitionStopped();
  state = SOURCE_STATE_INITIALIZED;
  emit stateChanged(SOURCE_STATE_INITIALIZED);
  acquisitionThread->acquiring = false;
}

void simulateDataInterface::CloseInterface(void) {
  if (state == SOURCE_STATE_RUNNING)
    StopAcquisition();

  if (state == SOURCE_STATE_INITIALIZED) {
    acquisitionThread->deleteDedicatedDataPub();
    //if the runtime thread is running, kill it
    acquisitionThread->quitNow = true;

    /*
    if (acquisitionThread != NULL) {
        acquisitionThread->quitNow = true;
        acquisitionThread->endThread();
        acquisitionThread->wait(); //block until the thread has fully terminated
    }*/

    //close device here


    qDebug() << "Closed device.";
    emit stateChanged(SOURCE_STATE_NOT_CONNECTED);

  }
}


double simulateDataInterface::getModulatorFrequency() {
    return (double) acquisitionThread->waveModulatorFrequency;
}

int simulateDataInterface::getFrequency() {
    return (int) acquisitionThread->waveFrequency;
}

int simulateDataInterface::getAmplitude() {
    return (int) acquisitionThread->waveAmplitude;
}

int simulateDataInterface::getThreshold() {
    return acquisitionThread->threshold;
}

void simulateDataInterface::setModulatorFrequency(double freqIn) {
    acquisitionThread->setModulatorFrequency(freqIn);
    //acquisitionThread->waveModulatorFrequency = freqIn;

}

void simulateDataInterface::setFrequency(int freqIn) {
    acquisitionThread->waveFrequency = (double) freqIn;
}

void simulateDataInterface::setAmplitude(int ampIn) {

    acquisitionThread->waveAmplitude = (double) ampIn;

}

void simulateDataInterface::setThreshold(int threshIn) {
    acquisitionThread->threshold = threshIn;
}
/*
void simulateDataRuntime::StopAcquisition() {
    pullTimer->stop();
    quit();
}*/

