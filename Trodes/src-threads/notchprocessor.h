#ifndef NOTCHPROCESSOR_H
#define NOTCHPROCESSOR_H

class NotchProcessor
{
public:
    NotchProcessor();
    ~NotchProcessor();

    void newSamples(const float *data);
    const float* getLowpassOutput() const {return n_y0;}

    int setNotchFreq(int ind, int nfreq);
    int setInputFreq(int ifreq);
    int setBandwidth(int ind, int bandwidth);

private:
    float *n_y0;
    float *n_y1;
    float *n_y2;

    float *n_x0;
    float *n_x1;
    float *n_x2;

    float *n_a1;
    float *n_a2;

    float *n_b0;
    float *n_b1;
    float *n_b2;

    //filter settings
    void calcCoef(int ind);
    int n_nfreq[16];
    int inputFreqHz;
    int n_bandwidth[16];

    char *full_buffer;
};

#endif // NOTCHPROCESSOR_H
