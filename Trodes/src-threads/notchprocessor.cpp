#include "notchprocessor.h"
#include "avxutils.h"
#include <immintrin.h>
#include <string.h>
#define _USE_MATH_DEFINES
#include <math.h>
NotchProcessor::NotchProcessor()
{
    size_t N = 16*11*sizeof(float) + 31;
    full_buffer = new char[N];
    memset(full_buffer, 0, N);

    Aligned32Allocator a(full_buffer, N);

    n_y0 = a.aligned_alloc<float>(16);
    n_y1 = a.aligned_alloc<float>(16);
    n_y2 = a.aligned_alloc<float>(16);

    n_x0 = a.aligned_alloc<float>(16);
    n_x1 = a.aligned_alloc<float>(16);
    n_x2 = a.aligned_alloc<float>(16);

    n_a1 = a.aligned_alloc<float>(16);
    n_a2 = a.aligned_alloc<float>(16);

    n_b0 = a.aligned_alloc<float>(16);
    n_b1 = a.aligned_alloc<float>(16);
    n_b2 = a.aligned_alloc<float>(16);
}

NotchProcessor::~NotchProcessor(){
    delete full_buffer;
}

void NotchProcessor::newSamples(const float *data){

    //shift data over
    //x2=x1; x1=x0; x0=data; y2=y1; y1=y0;
    std::copy(n_x1, n_x1+16, n_x2);
    std::copy(n_x0, n_x0+16, n_x1);
    std::copy(data, data+16, n_x0);
    std::copy(n_y1, n_y1+16, n_y2);
    std::copy(n_y0, n_y0+16, n_y1);

    for(int i = 0; i < 2; ++i){
        //=============================================================
        // notch filter
        //  y0 = b0*x0 + b1*x1 + b2*x2 - (a1*y1 + a2*y2)
        //=============================================================

        //load data b and x into registers
        __m256 b0 = _mm256_load_ps( &n_b0[i*8]);
        __m256 b1 = _mm256_load_ps( &n_b1[i*8]);
        __m256 b2 = _mm256_load_ps( &n_b2[i*8]);
        __m256 x0 = _mm256_load_ps( &n_x0[i*8]);
        __m256 x1 = _mm256_load_ps( &n_x1[i*8]);
        __m256 x2 = _mm256_load_ps( &n_x2[i*8]);

        //multiply step
        __m256 bx0 = _mm256_mul_ps(b0, x0);
        __m256 bx1 = _mm256_mul_ps(b1, x1);
        __m256 bx2 = _mm256_mul_ps(b2, x2);

        //load data a and y
        __m256 a1 = _mm256_load_ps( &n_a1[i*8]);
        __m256 a2 = _mm256_load_ps( &n_a2[i*8]);
        __m256 y1 = _mm256_load_ps( &n_y1[i*8]);
        __m256 y2 = _mm256_load_ps( &n_y2[i*8]);

        //multiply step
        __m256 ay1 = _mm256_mul_ps(a1, y1);
        __m256 ay2 = _mm256_mul_ps(a2, y2);

        //combine
        __m256 temp0 = _mm256_add_ps(bx0, bx1);
        __m256 temp1 = _mm256_add_ps(ay1, ay2);
        temp0 = _mm256_add_ps(temp0, bx2);
        temp0 = _mm256_sub_ps(temp0, temp1);

        //store value to y0
        _mm256_store_ps( &n_y0[i*8], temp0);
    }
}

int NotchProcessor::setNotchFreq(int ind, int nfreq)
{
    if(ind < 0 || ind > 15){
        return -1;
    }

    n_nfreq[ind]=nfreq;
    return 0;
}

int NotchProcessor::setInputFreq(int ifreq)
{
    inputFreqHz=ifreq;
    return 0;
}

int NotchProcessor::setBandwidth(int ind, int bandwidth)
{
    if(ind < 0 || ind > 15){
        return -1;
    }

    n_bandwidth[ind]=bandwidth;
    return 0;
}

void NotchProcessor::calcCoef(int ind)
{
    float d = exp(-M_PI * (float)n_bandwidth[ind] / (float)inputFreqHz);
//    n_a0[ind] = 1.0;
    n_a1[ind] = -(1.0 + d * d) * cos(2.0 * M_PI * (float)n_nfreq[ind] / (float)inputFreqHz);
    n_a2[ind] = d*d;
    n_b0[ind] = (1.0 + d * d) / 2.0;
    n_b1[ind] = n_a1[ind];
    n_b2[ind] = n_b0[ind];
}

