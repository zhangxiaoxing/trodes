﻿/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DOCKUSBTHREAD_H
#define DOCKUSBTHREAD_H



#include <QThread>
#include <QVector>
#include "abstractTrodesSource.h"

#ifdef WIN32
    #include <windows.h>
    #include "ftd2xx.h"
#endif
#ifdef __APPLE__
    #include "WinTypes.h"
    #include "ftd2xx.h"
#endif
#ifdef linux
    #include "WinTypes.h"
    #include "ftd2xx.h"
#endif


#define VENDOR 0x0403
#define DEVICE 0x6010

struct DockingStation_t
{
    DockingStation_t() {
        this->description = "";
        this->serial = 0;
        this->model = 0;
        this->rfchan = 0;
        this->samplingRateKhz = 0;
        valid = true;
    }
    QString description;
    uint16_t serial;
    uint16_t model;
    int rfchan;
    int samplingRateKhz;
    bool valid;
};


class DockUSBRuntime : public AbstractSourceRuntime {
  Q_OBJECT
public:
  DockUSBRuntime(bool ecu, QObject *parent);
  QVector<unsigned char> buffer;
  QByteArray datagram;

private:
  bool ecuConnected;
public slots:
  void Run(void);

//  void ft_write_slot(QByteArray datagram);
signals:
  void payloadReceived(int dockid, QByteArray);
//  void ft_write_status_return(FT_STATUS, DWORD);
};

class DockUSBInterface : public AbstractTrodesSource {
  Q_OBJECT

public:
  DockUSBInterface(QObject *parent);
  ~DockUSBInterface(void);
  int state;
  quint64 getTotalDroppedPacketEvents();
  quint64 getTotalUnresponsiveHeadstagePackets();

  //HardwareControllerSettings lastControllerSettings;
  //HeadstageSettings          lastHeadstageSettings;
//  uint16_t serial;
//  uint16_t model;
//  int rfChannel;
//  QString dockDescription;
//  uint8_t samplingRateKhz;
  DockingStation_t dockSettings;

  bool hasAck;

private:
  DockUSBRuntime *usbDataProcessor;
  QThread       *workerThread;

  QTimer        payloadTimer;
  //  //A function that waits before calling FT_Write, making sure waitMSecs ms has passed since last message
//  FT_STATUS FT_WRITE_wrapper(QByteArray &data);
  FT_STATUS retval;
  DWORD byteswritten;
  QTimer writeSignalTimer;
  int waittime;
  QElapsedTimer timer;

signals:
//  void ft_write_signal(QByteArray);
  void idlePayloadForwarding(int dockid, QByteArray);
  void streamPayloadForwarding(int dockid, QByteArray);
private slots:
  void restartThread();
  void getLastPayload();
//  void ft_write_slot(FT_STATUS status, DWORD bytes);

public slots:
  void InitInterface(void);
  void StartAcquisition(void);
//  void StartSimulation(void);
  void StopAcquisition(void);
  void CloseInterface(void);
//  void SendSettleCommand(void);
//  void SendHeadstageSettings(HeadstageSettings s);
////  void SetStimulationParams(StimulationCommand s);
////  void SendGlobalStimulationSettings(GlobalStimulationSettings s);
////  void SendGlobalStimulationAction(GlobalStimulationCommand s);
////  void ClearStimulationParams(uint16_t slot);
////  void SendStimulationStartSlot(uint16_t slot);
////  void SendStimulationStartGroup(uint16_t group);
////  void SendStimulationStopSlot(uint16_t slot);
////  void SendStimulationStopGroup(uint16_t group);
//  void SendControllerSettings(HardwareControllerSettings s);
//  HeadstageSettings GetHeadstageSettings();
//  HardwareControllerSettings GetControllerSettings();
//  void SendSettleChannel(int byteInPacket, quint8 bit, int delay, quint8 triggerState);
//  int MeasurePacketLength(HeadstageSettings settings);
//  static bool isSourceAvailable();
};

#endif // DOCKUSBTHREAD_H
