#ifndef STREAMPROCESSHANDLERS_H
#define STREAMPROCESSHANDLERS_H

#include <stdint.h>
#include <QtGlobal>
#include "configuration.h"
#include "iirFilter.h"
#include "globalObjects.h"
#include "trodesglobaltypes.h"

class SpikeWaveform
{
public:
    SpikeWaveform();
    SpikeWaveform(const int16_t* const* waveform, const int &ntrodeInd, const uint32_t &pTime, const int &numChan, const int &numSamp);
    QVector<int16_t> getPeaks() const;
    void clearMemory() const;


    const int16_t* const* waveData;
    uint32_t peakTime;
    int ntrodeIndex;
    int numChannels;
    int numSamples;



};

class AbstractNeuralDataHandler
{
public:
    //Extra stuff that the processor may need to do,
    //depending on the hardware
    enum ExtraProcessorType {
      None              = 0,
      IntanStim         = 0x01,
      NeuroPixels1    = 0x02,
      Export            = 0x04
      //NOT USED        = 0x08,
      //NOT USED        = 0x10,
      //NOT USED        = 0x20,
      //NOT USED        = 0x40
    };

    AbstractNeuralDataHandler(TrodesConfigurationPointers conf, QList<int> nTrodeList_in, uint32_t extraSteps = 0);
    virtual ~AbstractNeuralDataHandler();
    enum DataBand {RAW_UNREFERENCED,RAW_REFERENCED, LFP, SPIKE, STIMULATION};




    //Virtual public methods (implemeted in abstract class, but can be reimplemented)
    virtual void updateChannels();
    virtual void updateChannelsRef();
    virtual void updateFilters();


    //Must be implemented in inheriting class!

    //virtual void processNextSamples(uint32_t timestamp,int16_t* neuralDataBlock, int16_t* CARDataBlock) = 0;
    
    //Given the last processed sample, for each channel, process the bin (min/max), and display trace (raw/spike/lfp)
    virtual void processDisplaySamples(bool newbin) = 0;
    virtual const float* getMinDisplaySamples(int nTrodeIndex) = 0;
    virtual const float* getMaxDisplaySamples(int nTrodeIndex) = 0;


    //virtual void processNextSamples(uint32_t timestamp,int16_t* neuralDataBlock, int16_t* CARDataBlock) = 0;
    virtual void processNextSamples(uint32_t timestamp,const int16_t* neuralDataBlock, const int16_t* CARDataBlock, const char* auxDataBlock) = 0;


    //Public methods
    virtual const int16_t* getNTrodeSamples(int nTrodeIndex,DataBand d) = 0;
    virtual const int16_t* getEntirePacket(DataBand d, int& numSamples) = 0;
    virtual void stepPreviousSample() = 0;

    //
    void setDataLength(double dataLength, int traceLength);
    void setSpikeInvert(bool invert);
    void setInterpMode(bool on, uint32_t maxGapSize);
    int getSamplesLeftToInterpolate();
    bool gapOccured();
    uint32_t getSamplesProcessed();
    void getAvailableWaveforms(QVector<SpikeWaveform> &waveformCopies);
    void clearAvailableWaveforms();

    void updateGlobalStimSettings(GlobalStimulationSettings globalStim);
    float getStimScale_nA();

    //Error handling access
    int numLoopsWithNoHeadstageData;
    bool latestDataHasNan;

    //Stim settings
    GlobalStimulationSettings globalStimSettings;

 protected:
    virtual void resetFilters() = 0;

    //Assorted protected variables
    uint32_t extraProcessingModes;
    int NCHAN;
    TrodesConfigurationPointers conf_ptrs;
    QList<int> nTrodeList;
    bool spikeInvert;

    bool interpMode;
    uint32_t maxInterpGap;
    int numSamplesLeftToInterp;
    bool interpValuesReadyToFetch;
    bool gapInDataOccured;
    uint32_t samplesProcessed;
    QVector<SpikeWaveform> availableSpikes;

    //Stim variables
    float stimCurrentScale_nA;

    //sensor section info
    int sensorSectionStartByte;
    int sensorSectionSize;

    //Settings
    QVector<bool> notchFiltersOn;
    QVector<bool> spikeFiltersOn; //Renamed from filtersOn
    QVector<bool> lfpFiltersOn;
    QVector<bool> lfpRefOn;
    QVector<bool> refOn;
    QVector<bool> rawRefOn;
    QVector<int>  moduleDataChan;
    QVector<int>  refChan;
    QVector<bool> groupRefOn;
    QVector<int>  refGroup;
    QVector<bool> lfpModeOn;
    QVector<bool> spikeModeOn;
    QVector<bool> stimViewModeOn;



//    double dataLength;
//    int traceLength;
//    QVector<int> raw_increment;
//    int stream_inc;
//    int dataIdx;

    //Timestamp
    uint32_t timestamp_latest;
    uint32_t timestamp_previous;




private:



};

class NeuralDataHandler : public AbstractNeuralDataHandler
{
public:
    NeuralDataHandler(TrodesConfigurationPointers conf, QList<int> nTrodeList_in, uint32_t extraSteps = 0);
    ~NeuralDataHandler();
    //Public methods

    virtual void processNextSamples(uint32_t timestamp,const int16_t* neuralDataBlock, const int16_t* CARDataBlock, const char* auxDataBlock);

    //virtual void processNextSamples(uint32_t timestamp,int16_t* neuralDataBlock, int16_t* CARDataBlock);
    virtual const int16_t* getNTrodeSamples(int nTrodeIndex,DataBand d);
    virtual const int16_t* getEntirePacket(DataBand d, int &numSamples);
    virtual void processDisplaySamples(bool newbin);
    virtual const float* getMinDisplaySamples(int nTrodeIndex);
    virtual const float* getMaxDisplaySamples(int nTrodeIndex);
    virtual void stepPreviousSample();
    virtual void updateFilters();



private:
    void resetFilters();
    void switchDataPointers();


    //Filters
    QVector<QVector<BesselFilter*> > spikeFilters;
    QVector<BesselFilter*> lfpFilters;
//    QVector<QVector<NotchFilter*> > notchFilters;

    //Pointers to data for the latest and the previous packets
    int16_t** spike_band_dataPoints_latest;
    int16_t* lfp_band_dataPoints_latest;
    int16_t** raw_band_dataPoints_latest;
    int16_t** raw_unreferenced_band_dataPoints_latest;
    int16_t** spike_band_dataPoints_previous;
    int16_t* lfp_band_dataPoints_previous;
    int16_t** raw_band_dataPoints_previous;
    int16_t** raw_unreferenced_band_dataPoints_previous;
    int16_t** spike_band_dataPoints_interpolated;
    int16_t* lfp_band_dataPoints_interpolated;
    int16_t** raw_band_dataPoints_interpolated;
    int16_t** raw_unreferenced_band_dataPoints_interpolated;
    bool storageGate; //directs which container to point to (latest vs previous packet)

    //data containers for latest and previous packets
    int16_t** spike_band_dataPoints;
    int16_t* lfp_band_dataPoints;
    int16_t** raw_band_dataPoints;
    int16_t** raw_unreferenced_band_dataPoints;
    int16_t** spike_band_dataPoints2;
    int16_t* lfp_band_dataPoints2;
    int16_t** raw_band_dataPoints2;
    int16_t** raw_unreferenced_band_dataPoints2;


    //If any channels are stim capable, we keep the output current here
    int16_t** stim_band_dataPoints;
    bool stimHappeningNow;



    //data for display values
    float **minValues;
    float **maxValues;




};



/*class NeuroPixelsDataHandler : public AbstractNeuralDataHandler
{
public:
    NeuroPixelsDataHandler(TrodesConfigurationPointers conf, QList<int> nTrodeList_in);
    //Public methods

    virtual void processNextSamples(uint32_t timestamp,const int16_t* neuralDataBlock, const int16_t* CARDataBlock, const char* auxDataBlock);

};*/


class AuxDataHandler
{
public:
    AuxDataHandler(TrodesConfigurationPointers conf, QVector<int> auxChannelList_in);
    ~AuxDataHandler();
    void processNextSamples(uint32_t timestamp,int16_t* auxDataBlock);

private:
    TrodesConfigurationPointers conf_ptrs;
    QVector<int> auxChannelList;
    bool hasDigIO;
    bool hasAuxAnalog;
    QHash<QString, int> interleavedAuxChannelStates;
    QVector<bool> digStates;
    QVector<bool> triggered;

    //Timestamp
    uint32_t timestamp_latest;
    uint32_t timestamp_previous;
};

#endif // STREAMPROCESSHANDLERS_H
