/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "audioThread.h"
#include "globalObjects.h"

#include <QtCore/qmath.h>
#include <QtCore/qendian.h>
#include <QDebug>


const int DataFrequencyHz = 44100;
const int BufferSize      = 32768;


//quint64 rawDataWritten = 0; //global variable in globalObjects.h

Generator::Generator(QAudioDevice dev, const QAudioFormat &format, QObject *parent)
    :   QIODevice(parent)
    ,   listenHWChannel(-1)
    ,   listenRefHWChan(0)
    ,   listenRefCARGroup(0)
    ,   soundDataRead(0)
    ,   totalReadHead(0)
    ,   totalWriteHead(0)
    ,   rawReadIdx(0)
    ,   writeHead(0)
    ,   readHead(0)
    ,   bufferLength(88200*1)
    ,   interpRatio(0.0)
    ,   nextValue(0)


{
    m_buffer.resize(bufferLength);
    specAnalysis = new SpectralAnalysis(this);
//    inputFreqHz = (int) hardwareConf->sourceSamplingRate;
    masterGain = 2;
    threshMicroVolts = 15;
    thresh = round((threshMicroVolts*65536)/AD_CONVERSION_FACTOR); //assumes Intan-based source as default
    scalingToUv = 0.195;
    inputFreqHz = 30000;
    filterObj.setSamplingRate(30000);
    notchFilter.setSamplingRate(30000);
    specAnalysis->setSamplingRate(30000);
    spectralAnalysisOn = false;
}


Generator::~Generator() {

}


void Generator::resetBuffer() {
    writeHead = 0;
    readHead = 0;
    totalReadHead = 0;
    totalWriteHead = 0;
    rawReadIdx = 0;
    soundDataRead = 0;
    interpRatio = 0.0;
    currentValue = 0;
    nextValue = 0;

    for (int i = 0; i < m_buffer.length(); i++) {
        m_buffer[i] = 0;
    }
}

void Generator::start() {
    open(QIODevice::ReadOnly | QIODevice::Unbuffered);
}

void Generator::stop() {
    close();
}

qint64 Generator::readData(char *data, qint64 len) {

    //Output written data to soundcard.
//    qDebug() << "reading data";
    qint64 total = 0;

    if (totalReadHead + len > totalWriteHead) {
        len = totalWriteHead-totalReadHead;
    }

    if (totalReadHead + len <= totalWriteHead) {
        while (len - total > 0) {
            qint64 chunk = 0;
            chunk = qMin((bufferLength - readHead), len - total);
            memcpy(data + total, m_buffer.constData() + readHead, chunk);
            readHead  = (readHead + chunk) % bufferLength;
            totalReadHead += chunk;
            total += chunk;
        }

    } else {
        qDebug() << "End of sound reached";
    }
    return total;
}


qint64 Generator::writeData(const char *data, qint64 len) {

    //Write available data to buffer which will later be read out to sound card.
    qint64 total = 0;
    qint64 chunk = 0;
    while (len - total > 0) {
        chunk = qMin((bufferLength - writeHead), len - total);
        memcpy(m_buffer.data() + writeHead, data + total, chunk);
        writeHead = (writeHead + chunk) % bufferLength;
        totalWriteHead += chunk;
        total += chunk;
    }

    return total;
}

qint64 Generator::bytesWaiting() {
    return totalWriteHead-totalReadHead;
}


qint64 Generator::bytesAvailable() const
{
    return m_buffer.size() + QIODevice::bytesAvailable();
}

int Generator::checkForSamples() {

    //This function takes in the raw data, filters it, interpolates it
    //to 44100Hz, and writes it to another buffer.

    int samplesToCopy = 0;
    int samplesCopied = 0;
    int soundSamplesCopied = 0;
    int inputFreqCorrection = 0;
    writeMarkerMutex.lock();
    samplesToCopy = rawDataWritten - soundDataRead;
    writeMarkerMutex.unlock();
    double dataRatio = (double)(inputFreqHz+inputFreqCorrection)/((double) DataFrequencyHz);
    int numConvertSamples = 0;
    //qDebug() << samplesToCopy;

    if (listenHWChannel == -1) {// sound off
      return 0;
    }

    if ((samplesToCopy > 10) && (samplesToCopy < 30000) ) {
        numConvertSamples = (int) round(((double) samplesToCopy)/dataRatio);
        char *tempdata = new char[numConvertSamples*2 + 2];
        unsigned char *ptr = reinterpret_cast<unsigned char *>(tempdata);

        while (samplesCopied < (samplesToCopy-1)) {
            currentValue = nextValue;
            if(listenRefCARGroup){
                //subtract off a reference group
                nextValue = static_cast<int16_t>(rawData.data[listenHWChannel + ((rawReadIdx+1) % EEG_BUFFER_SIZE)*currentConfiguration.hardwareConf->NCHAN] -
                                             rawData.carvals[(listenRefCARGroup-1) + ((rawReadIdx+1)%EEG_BUFFER_SIZE)*currentConfiguration.spikeConf->carGroups.length()]);
            }
            else if (listenRefHWChan >= 0) {
                //subtract off a reference signal
                 nextValue = static_cast<int16_t>(rawData.data[listenHWChannel + ((rawReadIdx+1) % EEG_BUFFER_SIZE)*currentConfiguration.hardwareConf->NCHAN] -
                                             rawData.data[listenRefHWChan + ((rawReadIdx+1) % EEG_BUFFER_SIZE)*currentConfiguration.hardwareConf->NCHAN]);

            }else {
                nextValue = static_cast<int16_t>(rawData.data[listenHWChannel + (((rawReadIdx+1) % EEG_BUFFER_SIZE)*currentConfiguration.hardwareConf->NCHAN)]);
            }         

            //filter the signal

            //Notch first
            if (notchFilterOn)
                nextValue = notchFilter.addValue(nextValue);

            //Then bandpass (using the spike filter)
            if (filterOn)
                nextValue = filterObj.addValue(nextValue);

            if (spectralAnalysisOn) {
                //Do power spectral density analysis if the plot is open
                specAnalysis->addValue(nextValue);
            }


            //implement the digital diode effect

            if (nextValue >= 0) {
                nextValue = qMax(nextValue-thresh, 0);
            } else {
                nextValue = qMin(nextValue+thresh, 0);
            }

            //The signal needs to be interpolated to 44100 Hz
            while (interpRatio < 1) {
                qToLittleEndian<int16_t>(masterGain*(currentValue + interpRatio*(nextValue-currentValue))*scalingToUv, ptr);

                ptr += 2;
                soundSamplesCopied++;
                interpRatio += dataRatio;
            }
            interpRatio = interpRatio - floor(interpRatio);
            rawReadIdx = (rawReadIdx + 1) % EEG_BUFFER_SIZE;
            samplesCopied = samplesCopied + 1;

        }

        soundDataRead += (samplesToCopy-1);
        qint64 templen = soundSamplesCopied;
        writeData(tempdata,templen*2);
        delete [] tempdata;
    }

    return soundSamplesCopied*2;
}

void Generator::setChannel(int hwchannel) {
    listenHWChannel = hwchannel;
    if (hwchannel != -1)
      inputFreqHz = (int) currentConfiguration.hardwareConf->sourceSamplingRate;

}

void Generator::setRefChannel(int hwchannel) {
    listenRefHWChan = hwchannel;
}

void Generator::setRefGroup(int group){
    listenRefCARGroup = group;
}

AudioController::AudioController(QObject *parent)
    :   QObject(parent)
    ,   m_device(QMediaDevices::defaultAudioOutput())
    ,   m_generator(NULL)
    ,   m_audioOutput(NULL)
    ,   m_output(NULL)
{

    foundSupportedAudioHardware = false;
    deviceChangeLock = false;
    initializeAudio();


}

bool AudioController::setupOk()
{
    return foundSupportedAudioHardware;
}


void AudioController::endAudio() {
    emit stopTimer();
}

AudioController::~AudioController() {

    if (m_audioOutput) {
        m_audioOutput->stop();
        delete(m_audioOutput);
    }

    if (m_generator) {
        delete(m_generator);
    }
}

QStringList AudioController::getAvailableDevices() {
    return availableDevices;
}

QString AudioController::getCurrentDevice() {
    return currentDevice;
}

void AudioController::setConfiguration(TrodesConfigurationPointers c) {
    currentConfiguration.streamConf = c.streamConf;
    currentConfiguration.spikeConf = c.spikeConf;
    currentConfiguration.networkConf = c.networkConf;
    currentConfiguration.moduleConf = c.moduleConf;
    currentConfiguration.headerConf = c.headerConf;
    currentConfiguration.hardwareConf = c.hardwareConf;
    currentConfiguration.globalConf = c.globalConf;
    currentConfiguration.benchConfig = c.benchConfig;

    m_generator->currentConfiguration.streamConf = c.streamConf;
    m_generator->currentConfiguration.spikeConf = c.spikeConf;
    m_generator->currentConfiguration.networkConf = c.networkConf;
    m_generator->currentConfiguration.moduleConf = c.moduleConf;
    m_generator->currentConfiguration.headerConf = c.headerConf;
    m_generator->currentConfiguration.hardwareConf = c.hardwareConf;
    m_generator->currentConfiguration.globalConf = c.globalConf;
    m_generator->currentConfiguration.benchConfig = c.benchConfig;

}


void AudioController::initializeAudio() {

    qDebug() << "[AudioController] Initializing audio";

    m_buffer.resize(BufferSize);
    hasSupportedAudio = true;

    //m_format.setFrequency(44100);
    m_format.setSampleRate(44100);
    //m_format.setChannels(1);
    m_format.setChannelCount(1);
    m_format.setSampleFormat(QAudioFormat::Int16);

    //m_format.setSampleSize(16);
    //m_format.setCodec("audio/pcm");
    //m_format.setByteOrder(QAudioFormat::LittleEndian);
    //m_format.setSampleType(QAudioFormat::SignedInt);

    availableDevices.clear();
    qDebug() << "[AudioController] Available audio devices:";



    QList<QAudioDevice>devices = QMediaDevices::audioOutputs();//QAudioDeviceInfo::availableDevices(QAudio::AudioOutput);
    for (const QAudioDevice &deviceInfo : devices) {
        if (deviceInfo.isFormatSupported(m_format)) {
 //       qDebug() << "    -- sample rates:   " << deviceInfo.supportedSampleRates();
 //       qDebug() << "    -- channel counts: " << deviceInfo.supportedChannelCounts();
 //       qDebug() << "    -- sample sizes:   " << deviceInfo.supportedSampleSizes();
 //       qDebug() << "    -- codecs:         " << deviceInfo.supportedCodecs();
 //       qDebug() << "    -- byte orders:    " << deviceInfo.supportedByteOrders();
 //       qDebug() << "    -- sample types:   " << deviceInfo.supportedSampleTypes();
#ifdef linux
            if(deviceInfo.description().contains("ALSA", Qt::CaseInsensitive) || deviceInfo.description().contains("pulse", Qt::CaseInsensitive)){
                qDebug() << " Supported audio device: " << deviceInfo.description();
                availableDevices.append(deviceInfo.description());
            }
#else
            qDebug() << " Supported audio device: " << deviceInfo.description();
            availableDevices.append(deviceInfo.description());
#endif
        } else {
//            qDebug() << " Unsupported audio device: " << deviceInfo.deviceName();
        }
    }


    if (!availableDevices.isEmpty()) {
        foundSupportedAudioHardware = true;
        QAudioDevice info(QMediaDevices::defaultAudioOutput());
        QString defaultDevice = info.description();

        foreach (const QAudioDevice &deviceInfo, QMediaDevices::audioOutputs()) {
            if (deviceInfo.description().compare(defaultDevice)==0) {

                qDebug() << "Using default audio device: " << deviceInfo.description();
                m_device = deviceInfo;

            }
        }
        currentDevice =  m_device.description();
        createAudioOutput();
    }



    /*if (!info.isFormatSupported(m_format)) {
        qWarning() << "[AudioController] Default format not supported - trying to use nearest";
        m_format = info.nearestFormat(m_format);
        hasSupportedAudio = false;
    }
    createAudioOutput();*/

}

void AudioController::createAudioOutput() {
    m_generator = new Generator(m_device, m_format, this);

    connect(m_generator->specAnalysis,&SpectralAnalysis::newSpectrum,this, &AudioController::newSpectrum);
    //connect(m_audioOutput, SIGNAL(stateChanged(QAudio::State)), SLOT(stateChanged(QAudio::State)));

    m_generator->start();
}


void AudioController::pullTimerExpired() {
    if (!deviceChangeLock) {
        deviceChangeLock = true;
        if (!exportMode && hasSupportedAudio) {
            int bytesTransfered = m_generator->checkForSamples();
            //qint64 bytesWaiting = m_generator->bytesWaiting();
            if (bytesTransfered && bytesTransfered <= m_buffer.size()) {
                const qint64 len = m_generator->read(m_buffer.data(), bytesTransfered);
                if (len < 0)
                    qDebug() << "[AudioController] Error in QIODevice audiocontroller";

                int ret = m_output->write(m_buffer.data(), bytesTransfered);
                if(ret < 0){
                    qDebug() << "[AudioController] Error writing to audio iodevice";
                }
                else if(ret < bytesTransfered){
                    m_output->write(m_buffer.data()+ret, bytesTransfered-ret);
                }
            }
        }
        deviceChangeLock = false;
    }

}

void AudioController::stateChanged(QAudio::State state)
{
//    qDebug() << "[AudioController::stateChanged] state = " << state;
}

void AudioController::setChannel(int hwchannel) {
    if (!foundSupportedAudioHardware) return;

    m_generator->setChannel(hwchannel);

    if (hwchannel >= 0) {
        int trodeIndexLookup = currentConfiguration.streamConf->trodeIndexLookupByHWChan[hwchannel];
        int currentThreshUv = m_generator->threshMicroVolts;
        m_generator->thresh = round(currentThreshUv*(1/currentConfiguration.spikeConf->ntrodes[trodeIndexLookup]->spike_scaling_to_uV));
        m_generator->scalingToUv = currentConfiguration.spikeConf->ntrodes[trodeIndexLookup]->spike_scaling_to_uV;

    }


}

void AudioController::setRefChannel(int hwchannel) {
    if (!foundSupportedAudioHardware) return;

    m_generator->setRefChannel(hwchannel);
    m_generator->setRefGroup(0);
}

void AudioController::setRefGroup(int group){
    if (!foundSupportedAudioHardware) return;

    m_generator->setRefGroup(group);
    m_generator->setRefChannel(-1);
}


bool AudioController::setDevice(QString device) {
    //Changes the output device
    bool success = false;
    QAudioSink *oldDevice = NULL;
    if (!availableDevices.isEmpty()) {

        foreach (const QAudioDevice &deviceInfo, QMediaDevices::audioOutputs()) {
            if (deviceInfo.description().compare(device)==0) {

                if (!deviceChangeLock) {
                    deviceChangeLock = true;
                    qDebug() << "Using audio device: " << deviceInfo.description();
                    m_device = deviceInfo;
                    currentDevice = device;

                    //m_audioOutput might not have been created yet, before audio is started
                    if(m_audioOutput){
                        m_audioOutput->stop();

                        oldDevice = m_audioOutput;
                        m_audioOutput = new QAudioSink(m_device,m_format, this);

                        m_output = m_audioOutput->start();
                    }

                    //The previous QAudioOutput device can only be deleted in the main UI thread,
                    //so we send a signal with a pointer to the old device that is processed in the main UI thread.
                    emit deviceChanged(oldDevice);
                    deviceChangeLock = false;
                    success = true;
                }
                break;


            }
        }


    }
    return success;
}

void AudioController::updateAudio() {
    if (!foundSupportedAudioHardware) return;

    //update the reference
    int trodeIndexLookup = currentConfiguration.streamConf->trodeIndexLookupByHWChan[m_generator->listenHWChannel];
    if(currentConfiguration.spikeConf->ntrodes[trodeIndexLookup]->refOn){
        if(currentConfiguration.spikeConf->ntrodes[trodeIndexLookup]->groupRefOn){
            setRefGroup(currentConfiguration.spikeConf->ntrodes[trodeIndexLookup]->refGroup);
        }
        else {
            int refTrodeChannel = currentConfiguration.spikeConf->ntrodes[trodeIndexLookup]->refChan;
            int refTrode = currentConfiguration.spikeConf->ntrodes[trodeIndexLookup]->refNTrode;
            int refHWChannel = currentConfiguration.spikeConf->ntrodes[refTrode]->hw_chan[refTrodeChannel];
            setRefChannel(refHWChannel);
        }
    }
    else {
        setRefChannel(-1);
    }

    //update the filters
    int lowCutoff = currentConfiguration.spikeConf->ntrodes[trodeIndexLookup]->lowFilter;
    int highCutoff = currentConfiguration.spikeConf->ntrodes[trodeIndexLookup]->highFilter;
    m_generator->filterObj.setSamplingRate(currentConfiguration.hardwareConf->sourceSamplingRate);
    m_generator->filterObj.setFilterRange(lowCutoff,highCutoff);
    m_generator->filterOn = currentConfiguration.spikeConf->ntrodes[trodeIndexLookup]->filterOn;

    m_generator->notchFilter.setSamplingRate(currentConfiguration.hardwareConf->sourceSamplingRate);
    m_generator->notchFilter.setNotchFreq(currentConfiguration.spikeConf->ntrodes[trodeIndexLookup]->notchFreq);
    m_generator->notchFilter.setBandwidth(currentConfiguration.spikeConf->ntrodes[trodeIndexLookup]->notchBW);

    m_generator->notchFilterOn = currentConfiguration.spikeConf->ntrodes[trodeIndexLookup]->notchFilterOn;

    m_generator->specAnalysis->setSamplingRate(currentConfiguration.hardwareConf->sourceSamplingRate);


}

void AudioController::setGain(int gain) {
    if (!foundSupportedAudioHardware) return;

    m_generator->masterGain = gain;
}

void AudioController::setThresh(int thresh) {

    if (!foundSupportedAudioHardware) return;
    m_generator->threshMicroVolts = thresh;

    if (m_generator->listenHWChannel >= 0) {
        int trodeIndexLookup = currentConfiguration.streamConf->trodeIndexLookupByHWChan[m_generator->listenHWChannel];
        m_generator->thresh = round(thresh*(1/currentConfiguration.spikeConf->ntrodes[trodeIndexLookup]->spike_scaling_to_uV));
    }
    //m_generator->thresh = round((thresh*65536)/AD_CONVERSION_FACTOR);
}

int AudioController::getThresh() {
    if (foundSupportedAudioHardware) {
        return m_generator->threshMicroVolts;
    } else {
      return 0;
    }
}

int AudioController::getGain() {
    if (foundSupportedAudioHardware) {
        return m_generator->masterGain;
    } else {
        return 0;
    }
}

void AudioController::startAudio() {
    if (!foundSupportedAudioHardware) {
        qDebug() << "Audio output not started becuase no supported audio hardware was found.";
        return;
    }

    if(!m_audioOutput){
        m_audioOutput = new QAudioSink(m_device, m_format, this);
        //m_audioOutput->setNotifyInterval(128);

        connect(m_audioOutput, SIGNAL(stateChanged(QAudio::State)), SLOT(stateChanged(QAudio::State)));

        //For some reason, a short buffer is good in Linux:  no crackles and little delay. A long buffer causes a very long audio delay.
        //In Windows, a longer buffer is required to reduce crackles, with no major delay.

    #ifdef linux
        m_audioOutput->setBufferSize(10000); //A longer buffer decreases the choppiness of the sound, but increases the delay
    #else
        m_audioOutput->setBufferSize(16384); //A longer buffer decreases the choppiness of the sound, but increases the delay
    #endif
        qDebug() << "[AudioController] Audio buffer size: " << m_audioOutput->bufferSize();
        m_output = m_audioOutput->start();

    }
    m_pullTimer = new QTimer();
    connect(m_pullTimer, SIGNAL(timeout()), SLOT(pullTimerExpired()));
    connect(this,SIGNAL(stopTimer()),m_pullTimer,SLOT(stop()));
    connect(this,SIGNAL(stopTimer()),m_pullTimer,SLOT(deleteLater()));
    m_pullTimer->start(30);


}

void AudioController::stopAudio() {
    m_generator->resetBuffer();
    m_generator->soundDataRead = 0;
    m_generator->rawReadIdx = 0;
}

void AudioController::toggleSpectralAnalysis(bool on) {
    m_generator->spectralAnalysisOn = on;
}

void AudioController::resetBuffer() {
    m_generator->resetBuffer();
}
