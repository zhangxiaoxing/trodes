/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "usbdaqThread.h"
#include "globalObjects.h"
#include "CZHelp.h"

FT_STATUS res;
FT_HANDLE ftdi;
char const *desc_old = "Rat Backpack 2 A";
char const *desc = "Spikegadgets MCU A";
char const *desc2 = " A";
//char const *desc2 = "";

USBDAQRuntime::USBDAQRuntime(QObject *parent) {
}


void USBDAQRuntime::Run() {

  emit startHelperThread();

  DWORD BytesReceived;
  //int PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + 1 + 1;

  calculateHeaderSize();
  PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + (2*headerSize); //Packet size in bytes
  //unsigned char buffer[PACKET_SIZE];
  //buffer = new unsigned char[PACKET_SIZE];
  buffer.resize(PACKET_SIZE);

  unsigned char *RxBuffer;
  int remainingSamples = 0;
  int leftInBuffer;
  double dTimestamp = 0.0;
  int tic = 0;
  int tempMax;
  int maxAvailable = 0;
  int numConsecErrors = 0;
  quitNow = false;
  acquiring = true;
  runLoopActive = true;
  lastTimeStamp = 0;

  packetSizeErrorThrown = false;
  qDebug() << "USB handle events loop running....";

  FT_Purge(ftdi, FT_PURGE_RX);
  while (quitNow != true) {

      if ((tic % 10000) == 0) {
          //This is what checks if there is anything to write to the device. We don't check every loop becuase it slows down everything.
          QApplication::processEvents();
      }


      if (acquiring) {

          if (badFrameAlignment && tempSyncByteLocation > 0) {
              //We have lost packet alignment, but we have a candidate sync byte location. We dump
              //everything up to that point in the current packet.
              res = FT_Read(ftdi,buffer.data(),tempSyncByteLocation,&BytesReceived);
          }

          //Read in data from FTDI chip.  This function does not return until PACKET_SIZE
          //number of bytes have been read into the buffer.
          res = FT_Read(ftdi,buffer.data(),PACKET_SIZE,&BytesReceived);
          if (BytesReceived != (DWORD) PACKET_SIZE) {
              //if (aquiring) {
              qDebug() << "Error in USB acquisition";

              numConsecErrors++;
              if (!packetSizeErrorThrown && numConsecErrors > 5 && BytesReceived > 0) {

                  qDebug() << "Wrong incoming packet size. Expected:" << PACKET_SIZE << "Receiving:" << BytesReceived;
                  packetSizeErrorThrown = true;
                  emit timeStampError(true);
                  emit failure();
              }
              //break;
              //}
          } else {
              //Process the received data packet.
              //The packet contains a 16-bit header for binary info,
              //a 32-bit time stamp, and hardwareConf->NCHAN 16-bit samples.

              leftInBuffer = PACKET_SIZE;
              RxBuffer = buffer.data();

              //The first byte is always 0x55.  Check to make sure we have
              //the correct frame alignment

              if (!checkFrameAlignment(RxBuffer)) {
                  //We don't have alignment!  So now we drop everything
                  //and try to find it.
                  numConsecErrors++;
                  if (!packetSizeErrorThrown && numConsecErrors > 5) {
                      //if we have had many bad alignments in a row,
                      //consider it a fatal error and stop

                      //quitNow = true;
                      //emit failure();
                      qDebug() << "Wrong incoming packet size";

                      packetSizeErrorThrown = true;
                      emit timeStampError(true);
                      emit failure();

                  }
                  //We dump the rest of the processing for this packet
                  continue;
              }



              numConsecErrors = 0; //Packet looks good
              checkHardwareStatus((const uchar*)RxBuffer+1);



              //Read header info
              memcpy(&(rawData.digitalInfo[rawData.writeIdx*headerSize]), \
                      RxBuffer, headerSize*sizeof(uint16_t));

              RxBuffer += (2*headerSize);
              leftInBuffer -= (2*headerSize);



              //Process time stamp
              uint32_t* dataPtr = (uint32_t *)(RxBuffer);
              currentTimeStamp = *dataPtr;


              if (checkForCorrectTimeSequence() && !packetSizeErrorThrown) {
                  rawData.timestamps[rawData.writeIdx] = *dataPtr;

                  rawData.sysTimestamps[rawData.writeIdx] = QTime::currentTime().msecsSinceStartOfDay();

                  RxBuffer += 4;
                  leftInBuffer -= 4;
                  dTimestamp += 1.0;
                  rawData.dTime[rawData.writeIdx] = dTimestamp;
                  // system time in nanoseconds
//                  rawData.sysClock[rawData.writeIdx] = QDateTime::currentMSecsSinceEpoch() * 1000000;
                  rawData.sysClock[rawData.writeIdx] = AbstractSourceRuntime::getSystemTimeNanoseconds();




                  //Process the channels in the packet
                  remainingSamples = hardwareConf->NCHAN;
                  memcpy(&(rawData.data[rawData.writeIdx*hardwareConf->NCHAN]), \
                          RxBuffer, remainingSamples*sizeof(uint16_t));
                  RxBuffer += remainingSamples * 2;
                  leftInBuffer -= remainingSamples * 2;
                  remainingSamples = 0;

                  //This should ideally go in a seperate thread
                  calculateReferences();


                  //Advance the write markers and release a semaphore
                  writeMarkerMutex.lock();
                  rawData.writeIdx = (rawData.writeIdx + 1) % EEG_BUFFER_SIZE;
                  rawDataWritten++;
                  writeMarkerMutex.unlock();



                  for (int a = 0; a < rawDataAvailable.length(); a++) {
                      rawDataAvailable[a]->release(1);
                  }
                  sourceDataAvailable->release(1);


                  if ((tempMax = rawDataAvailable[0]->available()) > maxAvailable)
                      maxAvailable = tempMax;

                  if ((++tic % 10000) == 0) {
                      maxAvailable = 0;
                  }
              }


          }
      } else {
          //Not acquiring, but connected to source
          if (packetSizeErrorThrown) {
            packetSizeErrorThrown = false;
            emit timeStampError(false);
          }
          QThread::msleep(50);
      }
  }

  runtimeHelper->quitNow = true;
  qDebug() << "USB source thread finished.";
  runLoopActive = false;

  emit timeStampError(false);
  emit finished();



}

void USBDAQRuntime::ft_write_slot(QByteArray datagram){
    DWORD BytesWritten;
    FT_STATUS r = FT_Write(ftdi, datagram.data(), datagram.size(), &BytesWritten);
    emit ft_write_status_return(r, BytesWritten);
}

USBDAQInterface::USBDAQInterface(QObject *) {
  state = SOURCE_STATE_NOT_CONNECTED;
  usbDataProcessor = NULL;


  //usbDataProcessor = new USBDAQRuntime(NULL);
  //usbDataProcessor->connect(this, SIGNAL(startRuntime()), SLOT(Run()));
  qRegisterMetaType<FT_STATUS>("FT_STATUS");
  qRegisterMetaType<DWORD>("DWORD");
  retval = 20;//Default value if usb runtime thread never signals back
  byteswritten = 0;
  writeSignalTimer.setSingleShot(true);
  waittime = 5;
}

USBDAQInterface::~USBDAQInterface() {

    /*
    if (usbDataProcessor != NULL) {
        delete(usbDataProcessor);
    }*/
}

bool USBDAQInterface::sendCommandToHardware(QByteArray &command) {
    res = FT_WRITE_wrapper(command);
    if (res != FT_OK) {
        qDebug() << "Error writing to USB source";
        return false;
    } else {
        return true;
    }
}

bool USBDAQInterface::getDataStreamFromHardware(QByteArray* data, int numBytes) {
    //This function should be redefined for each inheriting class
    //It sends the start command 'sCommand' to the hardware, then puts all of the streaming bytes that
    //come back into *data until numBytes have been collected.
    //Returns true if all went well and false if something went wrong.

    if(usbDataProcessor->acquiring){
        return false;
    }
    bool failed = false;

    FT_STATUS	ftStatus;
    FT_Purge(ftdi, FT_PURGE_RX);


    // Setup FTDI FT2232H interface

/*#if defined (__linux) || defined (__APPLE__)
  // need to manually set VID and PID on linux
  res = FT_SetVIDPID(VENDOR, DEVICE);
  if (res != FT_OK) {
    qDebug() << "SetVIDPID failed";
    return false;
  }
#endif




  //res = FT_Open(0, &ftdi);
  res = FT_OpenEx((void*)desc, FT_OPEN_BY_DESCRIPTION, &ftdi);
  if (res != FT_OK) {
    //try the old USB label
    res = FT_OpenEx((void*)desc_old, FT_OPEN_BY_DESCRIPTION, &ftdi);
    if (res != FT_OK) {
        res = FT_OpenEx((void*)desc2, FT_OPEN_BY_DESCRIPTION, &ftdi);
        if (res != FT_OK) {
            qDebug() << "Open FTDI failed (See Resources/SetupHelp/).";
            return false;
        }
    }
  }


  res = FT_ResetDevice(ftdi);
  res |= FT_SetUSBParameters(ftdi, 65536, 65536);	//Set USB request transfer size
  res |= FT_SetFlowControl(ftdi,FT_FLOW_RTS_CTS,0,0);
  res |= FT_SetChars(ftdi, false, 0, false, 0);	 //Disable event and error characters
  res |= FT_SetBitMode(ftdi, 0xff, 0x40);
  res |= FT_Purge(ftdi, FT_PURGE_RX | FT_PURGE_TX);
  res |= FT_SetLatencyTimer(ftdi, 64);
  res |= FT_SetTimeouts(ftdi, 1000, 1000);
  if (res != FT_OK) {
    qDebug() << "Error initializing device";

    return false;
  }
  */

  unsigned char TxBuffer[256]; // Contains data to write to device


  // Send "start data capture" command
  TxBuffer[0] = 0x62; // stop
  TxBuffer[1] = 0x63; // load channel configuration
  TxBuffer[2] = 0x01; // only card 0 enabled = 32 channels

  if (lastControllerSettings.ECUDetected) {
       TxBuffer[3] = command_startWithECU;
  } else {
      TxBuffer[3] = command_startNoECU;
  }



  // send start capture command

  QByteArray ar((char*)TxBuffer, 4);

  //  emit ft_write_signal(ar);
  //  DWORD BytesWritten;
  res = FT_WRITE_wrapper(ar);
  if (res != FT_OK) {
      qDebug() << "Error writing";
      return false;
  }


  DWORD BytesReceived;
  uint32_t numBytesRecieved = 0;
  QVector<char> buffer;


  int PACKET_SIZE =  lastControllerSettings.packetSize;//Packet size in bytes
  buffer.resize(PACKET_SIZE);





  while (numBytesRecieved < numBytes) {

      //Read in data from FTDI chip.  This function does not return until PACKET_SIZE
      //number of bytes have been read into the buffer.
      res = FT_Read(ftdi,buffer.data(),PACKET_SIZE,&BytesReceived);
      if (BytesReceived != (DWORD) PACKET_SIZE) {
          //if (aquiring) {

          failed = true;
          break;
      }

      data->append(buffer.constData(),PACKET_SIZE);
      numBytesRecieved += PACKET_SIZE;
  }


  TxBuffer[0] = 0x62; // 0x62 = stop data capture
  QByteArray sar((char*)TxBuffer, 1);

  res = FT_WRITE_wrapper(sar);

  if (res != FT_OK) {
      qDebug() << "Error stopping";
  }

  //CloseInterface();

  if (failed) {
      return false;
  } else {
      return true;
  }

}

bool USBDAQInterface::RunDiagnostic(QByteArray *data,HeadstageSettings &hsSettings, HardwareControllerSettings &cnSettings) {
    bool failed = false;



    FT_STATUS	ftStatus;


    // Setup FTDI FT2232H interface

#if defined (__linux) || defined (__APPLE__)
  // need to manually set VID and PID on linux
  res = FT_SetVIDPID(VENDOR, DEVICE);
  if (res != FT_OK) {
    qDebug() << "SetVIDPID failed";
    return false;
  }
#endif




  //res = FT_Open(0, &ftdi);
  res = FT_OpenEx((void*)desc, FT_OPEN_BY_DESCRIPTION, &ftdi);
  if (res != FT_OK) {
    //try the old USB label
    res = FT_OpenEx((void*)desc_old, FT_OPEN_BY_DESCRIPTION, &ftdi);
    if (res != FT_OK) {
        res = FT_OpenEx((void*)desc2, FT_OPEN_BY_DESCRIPTION, &ftdi);
        if (res != FT_OK) {
            qDebug() << "Open FTDI failed (See Resources/SetupHelp/).";
            return false;
        }
    }
  }


  res = FT_ResetDevice(ftdi);
  res |= FT_SetUSBParameters(ftdi, 65536, 65536);	//Set USB request transfer size
  res |= FT_SetFlowControl(ftdi,FT_FLOW_RTS_CTS,0,0);
  res |= FT_SetChars(ftdi, false, 0, false, 0);	 //Disable event and error characters
  res |= FT_SetBitMode(ftdi, 0xff, 0x40);
  res |= FT_Purge(ftdi, FT_PURGE_RX | FT_PURGE_TX);
  res |= FT_SetLatencyTimer(ftdi, 64);
  res |= FT_SetTimeouts(ftdi, 1000, 1000);
  if (res != FT_OK) {
    qDebug() << "Error initializing device";

    return false;
  }



    hsSettings = GetHeadstageSettings();

    cnSettings = GetControllerSettings();



    bool headstageDetected = false;
    bool ECUReported = false;

    if (cnSettings.valid) {
        if (cnSettings.ECUDetected) {
            ECUReported = true;
            setECUConnected(true);
        }
    }
    if (hsSettings.valid) {
       headstageDetected = true;
    }




    if (headstageDetected) {


        unsigned char TxBuffer[256]; // Contains data to write to device


        // Send "start data capture" command
        TxBuffer[0] = 0x62; // stop
        TxBuffer[1] = 0x63; // load channel configuration

        //TxBuffer[2] = 0x00; // only card 0 enabled = 32 channels

        TxBuffer[2] = 0x01; // only card 0 enabled = 32 channels

        //TxBuffer[3] = 0x61; // 0x61 = start data capture
        TxBuffer[3] = startCommandValue;


        // send start capture command

        QByteArray ar((char*)TxBuffer, 4);

      //  emit ft_write_signal(ar);
      //  DWORD BytesWritten;
        res = FT_WRITE_wrapper(ar);
        if (res != FT_OK) {
          qDebug() << "Error writing";
          return false;
        }


        DWORD BytesReceived;
        uint32_t numBytesRecieved = 0;
        QVector<char> buffer;

        buffer.resize(10000);



        FT_Purge(ftdi, FT_PURGE_RX);

         while (numBytesRecieved < 200000) {

                //Read in data from FTDI chip.  This function does not return until PACKET_SIZE
                //number of bytes have been read into the buffer.
                res = FT_Read(ftdi,buffer.data(),1000,&BytesReceived);
                if (BytesReceived != (DWORD) 1000) {
                    //if (aquiring) {
                    emit newDiagnosticMessage("Error: no data coming from hardware.");
                    failed = true;
                    break;
                }

                data->append(buffer.constData(),1000);
                numBytesRecieved += 1000;
         }


         TxBuffer[0] = 0x62; // 0x62 = stop data capture
         QByteArray sar((char*)TxBuffer, 1);

         res = FT_WRITE_wrapper(sar);

         if (res != FT_OK) {
           qDebug() << "Error stopping";
         }

    }

    CloseInterface();

    if (failed) {
        return false;
    } else {
        return true;
    }

}

FT_STATUS USBDAQInterface::FT_WRITE_wrapper(QByteArray &data){

    if (usbDataProcessor == nullptr || !usbDataProcessor->acquiring) {

        DWORD BytesWritten;
        FT_STATUS r = FT_Write(ftdi, data.data(), data.size(), &BytesWritten);
        return r;
    } else {

        QEventLoop loop;
        connect(usbDataProcessor, &USBDAQRuntime::ft_write_status_return, &loop, &QEventLoop::quit);

        if(waittime && timer.isValid() && timer.elapsed() < waittime){
            QThread::msleep(waittime - timer.elapsed());
        }
        timer.restart();

        emit ft_write_signal(data);
        //Loop waits for the signal ft_write_status_return from runtime thread
        loop.exec();
        return retval;
    }
}

void USBDAQInterface::ft_write_slot(FT_STATUS status, DWORD bytes){
    retval = status;
    byteswritten = bytes;
}
void USBDAQInterface::InitInterface() {

    restartThreadAfterShutdown = true;

    FT_STATUS	ftStatus;
    DWORD libraryVersion = 0;

    ftStatus = FT_GetLibraryVersion(&libraryVersion);
    if (ftStatus == FT_OK)
    {
        qDebug() << "Library version: " << (unsigned int)libraryVersion;
    }
    else
    {
        qDebug() << "Error reading library version.";

    }


    // Setup FTDI FT2232H interface
    connectErrorThrown = false;
#if defined (__linux) || defined (__APPLE__)
  // need to manually set VID and PID on linux
  res = FT_SetVIDPID(VENDOR, DEVICE);
  if (res != FT_OK) {
    qDebug() << "SetVIDPID failed";
    return;
  }
#endif


/*
  //Display all detected devices
  // int iNumDevices = 0;
  FT_DEVICE_LIST_INFO_NODE *devInfo;
  DWORD numDevs = 0;
  ftStatus = FT_CreateDeviceInfoList(&numDevs);
  qDebug() << "Number of FTDI devices: " << numDevs;


  if((ftStatus == FT_OK) && (numDevs > 0))
  {
      //devInfo = (FT_DEVICE_LIST_INFO_NODE*)malloc(sizeof(FT_DEVICE_LIST_INFO_NODE)*numDevs);
      devInfo = new FT_DEVICE_LIST_INFO_NODE;

      ftStatus = FT_GetDeviceInfoList(devInfo,&numDevs);
      if (ftStatus == FT_OK) {
          {
              for(long unsigned int i = 0; i < numDevs; i++) {
                  qDebug() << "Description: " << (const char*)devInfo[i].Description;
                  qDebug() << "Serial: " << devInfo[i].SerialNumber;
              }
              //delete devInfo;
          }
      }
  }
*/

  //res = FT_Open(0, &ftdi);
  res = FT_OpenEx((void*)desc, FT_OPEN_BY_DESCRIPTION, &ftdi);
  if (res != FT_OK) {
    //try the old USB label
    res = FT_OpenEx((void*)desc_old, FT_OPEN_BY_DESCRIPTION, &ftdi);
    if (res != FT_OK) {
        res = FT_OpenEx((void*)desc2, FT_OPEN_BY_DESCRIPTION, &ftdi);
        if (res != FT_OK) {
            qDebug() << "Open FTDI failed (See Resources/SetupHelp/).";
            connectErrorThrown = true;
            emit stateChanged(SOURCE_STATE_CONNECTERROR);
            return;
        }
    }
  }

  res = FT_ResetDevice(ftdi);
  res |= FT_SetUSBParameters(ftdi, 64000, 64000);	//Set USB request transfer size
  res |= FT_SetFlowControl(ftdi,FT_FLOW_RTS_CTS,0,0);
  res |= FT_SetChars(ftdi, false, 0, false, 0);	 //Disable event and error characters
  res |= FT_SetBitMode(ftdi, 0xff, 0x40);
  res |= FT_Purge(ftdi, FT_PURGE_RX | FT_PURGE_TX);
  res |= FT_SetLatencyTimer(ftdi, 64);
  res |= FT_SetTimeouts(ftdi, 1000, 10);
  if (res != FT_OK) {
    qDebug() << "Error initializing device";
    connectErrorThrown = true;
    emit stateChanged(SOURCE_STATE_CONNECTERROR);
    return;
  }

  //initialization went ok, so start the runtime thread
  usbDataProcessor = new USBDAQRuntime(NULL);
  usbDataProcessor->acquiring = false;
  setUpThread(usbDataProcessor);
  connect(this, &USBDAQInterface::ft_write_signal, usbDataProcessor, &USBDAQRuntime::ft_write_slot);
  connect(usbDataProcessor, &USBDAQRuntime::ft_write_status_return, this, &USBDAQInterface::ft_write_slot,Qt::UniqueConnection);

  /*
  workerThread = new QThread();
  usbDataProcessor->moveToThread(workerThread);

  //connect(workerThread, SIGNAL(started()), usbDataProcessor, SLOT(Run()));
  usbDataProcessor->connect(this, SIGNAL(startRuntime()), SLOT(Run()));
  connect(usbDataProcessor, SIGNAL(finished()), workerThread, SLOT(quit()));
  connect(usbDataProcessor, SIGNAL(finished()), usbDataProcessor, SLOT(deleteLater()));
  connect(workerThread, SIGNAL(finished()), workerThread, SLOT(deleteLater()));
  workerThread->start();
  */


  //get headstage and controller settings if this is the first init (and not a reinit)
  //We need to do this here so that the settings can be logged onto the headers of recording files.
  if (!reinitMode) {
      HeadstageSettings settingsRead = GetHeadstageSettings();
      if (settingsRead.valid) {
          emit headstageSettingsReturned(settingsRead);
      }

      HardwareControllerSettings hardwareRead = GetControllerSettings();
      if (hardwareRead.valid) {
          emit controllerSettingsReturned(hardwareRead);
      }
  }
  state = SOURCE_STATE_INITIALIZED;

  emit stateChanged(SOURCE_STATE_INITIALIZED);
}

void USBDAQInterface::StartSimulation(void) {
  static int runtimeStarted  = 0;

  qDebug() << "Sending simulation command. Aux bytes:" << (hardwareConf->headerSize*2)-2 << "Num channel banks:" << ((hardwareConf->NCHAN/32)-1);


  QByteArray datagram;
  datagram.resize(3);
  QDataStream msg(&datagram, QIODevice::ReadWrite);
  msg.setByteOrder(QDataStream::LittleEndian);
  msg << (quint8)command_startSimulation;
  msg << (quint8)((hardwareConf->headerSize*2)-2);  //Number of auxilliary bytes to add to the packet
  //msg << (quint8)(0);  //Number of auxilliary bytes to add to the packet

  msg << (quint8)((hardwareConf->NCHAN/32)-1);


  rawData.writeIdx = 0; // location where we're currently writing



  usbDataProcessor->acquiring = true;

  // send start capture command

//  emit ft_write_signal(datagram);
//  DWORD BytesWritten;
//  res = FT_Write(ftdi, datagram.data(), 3, &BytesWritten);
  res = FT_WRITE_wrapper(datagram);
  if (res != FT_OK) {
    qDebug() << "Error writing";
    return;
  }

  if (runtimeStarted == 0) {
    emit startRuntime();
    qDebug() << "Told runtime to start";
  }

  emit acquisitionStarted();
  state = SOURCE_STATE_RUNNING;
  emit stateChanged(SOURCE_STATE_RUNNING);
}

void USBDAQInterface::StartAcquisition(void) {

  if (!threadCreated) {
      restartThread();
  }

  static int runtimeStarted  = 0;
  unsigned char TxBuffer[256]; // Contains data to write to device

  rawData.writeIdx = 0; // location where we're currently writing

  // Send "start data capture" command
  TxBuffer[0] = 0x62; // stop
  TxBuffer[1] = 0x63; // load channel configuration
  switch (hardwareConf->NCHAN) {
    case 0:
      TxBuffer[2] = 0x00; // only card 0 enabled = 32 channels
      break;
    case 32:
      TxBuffer[2] = 0x01; // only card 0 enabled = 32 channels
      break;
    case 64:
      TxBuffer[2] = 0x03; // card 0 and 1 enabled = 64 channels
      break;
    case 96:
      TxBuffer[2] = 0x07; // card 0,1,and 2 enabled = 96 channels
      break;
    case 128:
      TxBuffer[2] = 0x0F; // card 0,1,2, and 3 enabled = 128 channels
      break;
    case 160:
      TxBuffer[2] = 0x1F; // card 0,1,2,3, and 4 enabled = 160 channels
      break;
    default:
      TxBuffer[2] = 0x01; // default mode: only card 0 enabled = 32 channels
      break;

  }

  //TxBuffer[3] = 0x61; // 0x61 = start data capture
  TxBuffer[3] = startCommandValue;
  //nsUSBRuntime->aquiring = true;
  usbDataProcessor->resetStreamingStats();
  usbDataProcessor->acquiring = true;

  // send start capture command

  QByteArray ar((char*)TxBuffer, 4);

//  emit ft_write_signal(ar);
//  DWORD BytesWritten;
  res = FT_WRITE_wrapper(ar);
  if (res != FT_OK) {
    qDebug() << "Error writing";
    return;
  }

  if (runtimeStarted == 0) {
    emit startRuntime();
    qDebug() << "Told runtime to start";
  }

  emit acquisitionStarted();
  state = SOURCE_STATE_RUNNING;
  emit stateChanged(SOURCE_STATE_RUNNING);
}


void USBDAQInterface::StopAcquisition(void) {

  if (!threadCreated) return;
  unsigned char TxBuffer[256]; // Contains data to write to device

  TxBuffer[0] = 0x62; // 0x62 = stop data capture
  QByteArray ar((char*)TxBuffer, 1);
//  emit ft_write_signal(ar);
  // send stop capture command
//  DWORD BytesWritten;
//  res = FT_Write(ftdi, TxBuffer, 1, &BytesWritten);

  res = FT_WRITE_wrapper(ar);

  if (res != FT_OK) {
    qDebug() << "Error stopping";
  } else {
    qDebug() << "Stopped acquisition. Timestamp:" << currentTimeStamp;
  }

  //The worker thread is stopped, and will restart again if restartThreadAfterShutdown is true



  usbDataProcessor->printStreamingStats();
  usbDataProcessor->quitNow = true;
  usbDataProcessor->acquiring = false;




  emit acquisitionStopped();
  state = SOURCE_STATE_INITIALIZED;
  emit stateChanged(SOURCE_STATE_INITIALIZED);

}

void USBDAQInterface::restartThread() {
    qDebug() << "Restarting USB source thread.";
    //start the runtime thread
    usbDataProcessor = new USBDAQRuntime(NULL);
    usbDataProcessor->acquiring = false;
    setUpThread(usbDataProcessor);
    connect(this, &USBDAQInterface::ft_write_signal, usbDataProcessor, &USBDAQRuntime::ft_write_slot);
    connect(usbDataProcessor, &USBDAQRuntime::ft_write_status_return, this, &USBDAQInterface::ft_write_slot,Qt::UniqueConnection);
    state = SOURCE_STATE_INITIALIZED;
    emit stateChanged(SOURCE_STATE_INITIALIZED);
}

void USBDAQInterface::SendImpendaceMeasureCommand(HardwareImpedanceMeasureCommand s) {
    //qDebug() << "USB interface got impedance measure request.";
    if (usbDataProcessor == nullptr || !usbDataProcessor->acquiring) {
        //QVector<unsigned char> buffer;
        QByteArray buffer;

        DWORD BytesReceived;
        DWORD RxBytesAvailable;
        DWORD TxBytesAvailable;
        DWORD Event;

        //First clear anything in the receive queue
        res = FT_GetStatus(ftdi, &RxBytesAvailable, &TxBytesAvailable, &Event);
        if (res == FT_OK && RxBytesAvailable > 0) {
            buffer.resize(RxBytesAvailable);
            res = FT_Read(ftdi,buffer.data(),RxBytesAvailable,&BytesReceived);
        }


        char inputarray[1000];
        if (s.numberOfCycles > 16) {
            return; //Not allowed
        }

        QByteArray datagram;
        datagram.resize(s.WRITESETTINGSBYTES);
        s.writeSettings(datagram.data());


        res = FT_WRITE_wrapper(datagram);
        if (res != FT_OK) {
            qDebug() << "Error sending impedance measure command.";
        } else {
            //qDebug() << "Requested impedance measure.";
        }

        bool messageAvail = false;
        for (int tryNum=0; tryNum < 20;tryNum++) {
            res = FT_GetStatus(ftdi, &RxBytesAvailable, &TxBytesAvailable, &Event);
            if (res == FT_OK && RxBytesAvailable >= 3) {
                messageAvail = true;
                //qDebug() << "Got response";
                break;
            }
            QThread::msleep(100);

        }

        if (messageAvail) {
            buffer.resize(RxBytesAvailable);
            res = FT_Read(ftdi,inputarray,RxBytesAvailable,&BytesReceived);

            int expectedBytes = 3;
            uint32_t zCheck_diff = 0;
            if ((BytesReceived >= expectedBytes) && inputarray[0] == (char)received_impedanceValues) {
                zCheck_diff = (uint32_t)((reinterpret_cast<unsigned char&>(inputarray[2])))*256;
                zCheck_diff = zCheck_diff + ((reinterpret_cast<unsigned char&>(inputarray[1])));
                //zCheck_diff = (zCheck_diff<<8) + inputarray[1];
                //printf("zCheck peak-to-peak amplitude: %u\n", zCheck_diff);
            }  else {
                qDebug() << "Error reading impedance value.";

                return;
            }



            double meanPeakVoltage_nV = 0.0;
            double impedanceCalc_Ohm = 0.0;
            if (spikeConf->deviceType == "intan") {
                meanPeakVoltage_nV = (zCheck_diff*195.0)/2;
                double peakCurrent_nA;

                if (s.notchHz == 0) {
                    //0.1 pF
                    peakCurrent_nA = (((double)(s.frequency))/1000.0)*.38; //nA

                } else if (s.notchHz == 1) {
                    //1.0 pF
                    peakCurrent_nA = (((double)(s.frequency))/1000.0)*3.8; //nA

                } else if (s.notchHz == 2) {
                    //10 pF
                    peakCurrent_nA = (((double)(s.frequency))/1000.0)*38; //nA

                } else {
                    return;
                }

                impedanceCalc_Ohm = meanPeakVoltage_nV/peakCurrent_nA;
            }



            emit impedanceValueReturned(s.channel,(int)impedanceCalc_Ohm);


        } else {
            qDebug() << "No return message found after request.";
        }

    }


}

quint64 USBDAQInterface::getTotalUnresponsiveHeadstagePackets() {
    quint64 rVal = 0;
    if (usbDataProcessor != NULL) {
        rVal = usbDataProcessor->totalUnresponsiveHeadstagePackets;
    }
    return rVal;
}

quint64 USBDAQInterface::getTotalDroppedPacketEvents() {

    quint64 rVal = 0;
    if (usbDataProcessor != NULL) {
        rVal = usbDataProcessor->totalDroppedPacketEvents;
    }
    return rVal;

}

void USBDAQInterface::SendControllerSettings(HardwareControllerSettings s) {
    //SET CONTROLLER SETTINGS (MESSAGE TO HARDWARE)-- 9 BYTES
    //<0x84><uint8 rfChannel><uint8 samplingRateKhz><uint8 s3><uint8 s4><uint8 s5><uint8 s6><uint8 s7><uint8 s8>

    QByteArray datagram;
    datagram.resize(9);
    s.writeSettings(datagram.data());

    DWORD BytesWritten;
    res = FT_Write(ftdi, datagram.data(), 9, &BytesWritten);
    if (res != FT_OK) {
      qDebug() << "Error sending controller settings.";
      QMessageBox messageBox;
      messageBox.setWindowFlags(Qt::WindowStaysOnTopHint);
      messageBox.critical(0,"Error","Settings could not be sent to hardware.");
      messageBox.setFixedSize(500,200);
      return;
    } else {
      qDebug() << "Controller settings sent.";
      //GetHeadstageSettings();
    }
}

HardwareControllerSettings USBDAQInterface::GetControllerSettings() {
    //Ask the hardware for info about the main controller
    HardwareControllerSettings s;

    if (usbDataProcessor == nullptr || !usbDataProcessor->acquiring) {
        //QVector<unsigned char> buffer;
        QByteArray buffer;

        DWORD BytesReceived;
        DWORD RxBytesAvailable;
        DWORD TxBytesAvailable;
        DWORD Event;

        //First clear anything in the receive queue
        res = FT_GetStatus(ftdi, &RxBytesAvailable, &TxBytesAvailable, &Event);
        if (res == FT_OK && RxBytesAvailable > 0) {
            buffer.resize(RxBytesAvailable);
            res = FT_Read(ftdi,buffer.data(),RxBytesAvailable,&BytesReceived);
        }

        QByteArray datagram;
        datagram.resize(1);
        datagram[0] = (quint8)command_getControllerSettings;

//        emit ft_write_signal(datagram);
//        DWORD BytesWritten;
//        res = FT_Write(ftdi, datagram.data(), 1, &BytesWritten);
        res = FT_WRITE_wrapper(datagram);
        if (res != FT_OK) {
            qDebug() << "Error requesting controller settings.";
        } else {
            qDebug() << "Requested controller settings.";
        }

        bool messageAvail = false;
        for (int tryNum=0; tryNum < 5;tryNum++) {
            res = FT_GetStatus(ftdi, &RxBytesAvailable, &TxBytesAvailable, &Event);
            if (res == FT_OK && RxBytesAvailable >= 17) {
                messageAvail = true;
                qDebug() << "got controller settings";
                break;
            }
            QThread::msleep(500);

        }

        if (messageAvail) {
            buffer.resize(RxBytesAvailable);
            res = FT_Read(ftdi,buffer.data(),RxBytesAvailable,&BytesReceived);
            if (buffer.at(0) == (char)received_controllerSettings) {
                qDebug() << "Controller settings returned from hardware.";
                s = AbstractTrodesSource::convertHardwareControllerSettings(buffer);
                emit controllerSettingsReturned(s);
                lastControllerSettings = s;


            } else {
                qDebug() << "Error reading controller settings message from hardware. Code: " << (quint8)buffer.at(0) << "bytes: " << RxBytesAvailable;
            }

        } else {
            qDebug() << "No return message found after request.";
        }

    }
    else if(lastControllerSettings.valid){
        //if is acquiring and previously stored hardware settings is valid
        s = lastControllerSettings;
    }

    return s;
}

HeadstageSettings USBDAQInterface::GetHeadstageSettings() {
    //Ask the hardware for info about any connected headstage
    HeadstageSettings s;

    if (usbDataProcessor == nullptr || !usbDataProcessor->acquiring) {
        //QVector<unsigned char> buffer;
        QByteArray buffer;

        DWORD BytesReceived;
        DWORD RxBytesAvailable;
        DWORD TxBytesAvailable;
        DWORD Event;

        //First clear anything in the receive queue
        res = FT_GetStatus(ftdi, &RxBytesAvailable, &TxBytesAvailable, &Event);
        if (res == FT_OK && RxBytesAvailable > 0) {
            buffer.resize(RxBytesAvailable);
            res = FT_Read(ftdi,buffer.data(),RxBytesAvailable,&BytesReceived);
        }



        QByteArray datagram;
        datagram.resize(1);
        datagram[0] = (quint8)command_getHeadstageSettings;

//        emit ft_write_signal(datagram);
//        DWORD BytesWritten;
//        res = FT_Write(ftdi, datagram.data(), 1, &BytesWritten);
        res = FT_WRITE_wrapper(datagram);
        if (res != FT_OK) {
            qDebug() << "Error requesting headstage settings.";
        } else {
            qDebug() << "Requested headstage settings.";
        }

        bool messageAvail = false;
        for (int tryNum=0; tryNum < 5;tryNum++) {
            res = FT_GetStatus(ftdi, &RxBytesAvailable, &TxBytesAvailable, &Event);
            if (res == FT_OK && RxBytesAvailable >= 27) {
                messageAvail = true;
                break;
            }
            QThread::msleep(500);

        }

        if (messageAvail) {
            buffer.resize(RxBytesAvailable);
            res = FT_Read(ftdi,buffer.data(),RxBytesAvailable,&BytesReceived);
            if (buffer.at(0) == (char)received_headstageSettings) {
                qDebug() << "Headstage settings returned from hardware.";
                s = AbstractTrodesSource::convertHeadstageSettings(buffer);
                emit headstageSettingsReturned(s);
                lastHeadstageSettings = s;
            } else {
                qDebug() << "Error reading headstage settings message from hardware. Code: " << (quint8)buffer.at(0);
            }

        } else {
            qDebug() << "No return message found after request.";
        }

    }
    else if(lastHeadstageSettings.valid){
        //if is acquiring and previously stored settings is valid
        s = lastHeadstageSettings;
    }

    return s;


}

bool USBDAQInterface::SendNeuroPixelsSettings(NeuroPixelsSettings s) {

    qDebug() << "Sending np settings";
    QByteArray datagram;
    datagram.resize(s.WRITESETTINGSBYTES);

    for (int p=0;p<s.numProbes();p++) {

        s.writeSettings(datagram.data(),p);

        res = FT_WRITE_wrapper(datagram);
        if (res != FT_OK) {
            qDebug() << "Error sending headstage settings.";
            QMessageBox messageBox;
            messageBox.setWindowFlags(Qt::WindowStaysOnTopHint);
            messageBox.critical(0,"Error","Settings could not be sent to hardware.");
            messageBox.setFixedSize(500,200);
            return false;
        } else {
            qDebug() << "NeuroPixels settings sent for probe" << p;
            QThread::msleep(1500); //We need to give the headstage enough time to program the probe.
            //GetHeadstageSettings();
        }
    }

    return true;

    //SendSaveHeadstageSettings();


//    emit ft_write_signal(datagram);



    return true;
}

void USBDAQInterface::SendSaveHeadstageSettings() {
    //We send the command for the headstage to save settings to flash memory
    QByteArray datagram2;
    datagram2.resize(1);
    QDataStream msg2(&datagram2, QIODevice::ReadWrite);
    msg2.setByteOrder(QDataStream::LittleEndian);
    msg2 << (quint8)command_writeHSSettingsToFlash;
//    res = FT_Write(ftdi, datagram2.data(), 1, &BytesWritten);
    res = FT_WRITE_wrapper(datagram2);
    if (res != FT_OK) {
      qDebug() << "Error sending command to save settings on headstage.";
      QMessageBox messageBox;
      messageBox.setWindowFlags(Qt::WindowStaysOnTopHint);
      messageBox.critical(0,"Error","Settings could not be saved to hardware.");
      messageBox.setFixedSize(500,200);
    } else {
      qDebug() << "Headstage save settings command sent.";
    }
}

void USBDAQInterface::SendHeadstageSettings(HeadstageSettings s) {


    //SET HEADSTAGE SETTINGS (MESSAGE TO HARDWARE)-- 16 BYTES
    //<0x82><uint8 autoSettleOn><uint16 autosettle number of channels><uint16 autosettle thresh><uint8 smartRefOn><uint8 sensors on><uint8 s1><uint8 s2><uint8 s3><uint8 s4><uint8 s5><uint8 s6><uint8 s7><uint8 s8>
    //sensors on is an 8-bit code, least significant bit last <unused unused unused unused unused mag gyro accel>
    QByteArray datagram;
    datagram.resize(s.WRITESETTINGSBYTES);

    s.writeSettings(datagram.data());
//    emit ft_write_signal(datagram);
//    DWORD BytesWritten;
//    res = FT_Write(ftdi, datagram.data(), 16, &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending headstage settings.";
      QMessageBox messageBox;
      messageBox.setWindowFlags(Qt::WindowStaysOnTopHint);
      messageBox.critical(0,"Error","Settings could not be sent to hardware.");
      messageBox.setFixedSize(500,200);
      return;
    } else {
      qDebug() << "Headstage settings sent.";
      //GetHeadstageSettings();
    }


//    emit ft_write_signal(datagram);

    //Next we send the command for the headstage to save settings to flash memory
    QByteArray datagram2;
    datagram2.resize(1);
    QDataStream msg2(&datagram2, QIODevice::ReadWrite);
    msg2.setByteOrder(QDataStream::LittleEndian);
    msg2 << (quint8)command_writeHSSettingsToFlash;
//    res = FT_Write(ftdi, datagram2.data(), 1, &BytesWritten);
    res = FT_WRITE_wrapper(datagram2);
    if (res != FT_OK) {
      qDebug() << "Error sending command to save settings on headstage.";
      QMessageBox messageBox;
      messageBox.setWindowFlags(Qt::WindowStaysOnTopHint);
      messageBox.critical(0,"Error","Settings could not be saved to hardware.");
      messageBox.setFixedSize(500,200);
    } else {
      qDebug() << "Headstage save settings command sent.";
    }

    QThread::msleep(5000);
    //emit headstageSettingsReturned(s);
    HeadstageSettings newRead = GetHeadstageSettings();

    if (newRead == s) {
        qDebug() << "Verified new headstage settings";
    } else {


        qDebug() << "Error while verifying new headstage settings.";
        QMessageBox messageBox;
        messageBox.setWindowFlags(Qt::WindowStaysOnTopHint);
        messageBox.critical(0,"Error","Settings change could not be verified from headstage. Please make sure headstage is properly connected.");
        messageBox.setFixedSize(500,200);
    }

}

void USBDAQInterface::SetStimulationParams(StimulationCommand s) {
    //SEND STIMULATION PATTERN DEFINITION (MESSAGE TO HARDWARE)-- 24 BYTES

    QByteArray datagram;

    //Serialize the information in the object into a byte array containing the critical state machine information
    if(!convertToStateMachineCommand(s, &datagram)){
        qDebug() << "Stimulation parameters are not valid!";
        return;
    }

//    emit ft_write_signal(datagram);
    //Send the packet
//    DWORD BytesWritten;
//    res = FT_WRITE_wrapper(ftdi, datagram.data(), datagram.size(), &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending stimulation parameters." << res;
    }
}

bool USBDAQInterface::SendGlobalStimulationSettings(GlobalStimulationSettings s) {
    //SEND GLOBAL STIMULATION SETTINGS (MESSAGE TO HARDWARE)

    QByteArray datagram;

    //Serialize the information in the object into a byte array containing the critical state machine information
    if(!convertToStateMachineCommand(s, &datagram)){
        qDebug() << "Stimulation parameters are not valid!";
        return false;
    }

    //Send the packet
//    DWORD BytesWritten;
//    res = FT_WRITE_wrapper(ftdi, datagram.data(), datagram.size(), &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending stimulation parameters." << res;
    }
//    emit ft_write_signal(datagram);
    return true;
}

void USBDAQInterface::SendGlobalStimulationAction(GlobalStimulationCommand s) {
    //SEND GLOBAL STIMULATION ACTION (MESSAGE TO HARDWARE)

    QByteArray datagram;

    //Serialize the information in the object into a byte array containing the critical state machine information
    if(!convertToStateMachineCommand(s, &datagram)){
        qDebug() << "Stimulation parameters are not valid!";
        return;
    }

//    //Send the packet
//    DWORD BytesWritten;
//    res = FT_WRITE_wrapper(ftdi, datagram.data(), datagram.size(), &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending stimulation parameters." << res;
    }
//    emit ft_write_signal(datagram);
}


void USBDAQInterface::ClearStimulationParams(uint16_t slot) {
    //CLEAR STIMULATION PATTERN DEFINITION IN ONE MEMORY SLOT-- 3 BYTES

    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);

    //Fill the packet
    msg << (quint8)command_clearStimulateParams; //0x6F
    msg << (uint8_t)slot;
    //Calculate a checksum
    uint8_t checksum = 0;
    for (int i = 0;i < datagram.length()-1; i++) {
        checksum += datagram.at(i);
    }
    msg << checksum;

//    //Send the packet
//    DWORD BytesWritten;
//    res = FT_WRITE_wrapper(ftdi, datagram.data(), datagram.size(), &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending stimulation clear command." << res;
    }
//    emit ft_write_signal(datagram);

}

void USBDAQInterface::SendStimulationStartSlot(uint16_t slot) {
    //START STIMULATION PATTERN DEFINED IN ONE MEMORY SLOT-- 3 BYTES

    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);

    //Fill the packet
    msg << (quint8)command_stimulateStart;
    msg << (uint8_t)slot;
    //Calculate a checksum
    uint8_t checksum = 0;
    for (int i = 0;i < datagram.length()-1; i++) {
        checksum += datagram.at(i);
    }
    msg << checksum;

//    //Send the packet
//    DWORD BytesWritten;
//    res = FT_WRITE_wrapper(ftdi, datagram.data(), datagram.size(), &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending stimulation start command." << res;
    } else {
        qDebug() << "Start stimulation pattern in slot" << slot;
    }
//    emit ft_write_signal(datagram);
}

void USBDAQInterface::SendStimulationStartGroup(uint16_t group) {
    //START ALL STIMULATION PATTERNS DEFINED-- 1 BYTE

    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);

    //Fill the packet
    msg << (quint8)command_stimulateStartGroup;
    msg << (uint8_t)group;
    //Calculate a checksum
    uint8_t checksum = 0;
    for (int i = 0;i < datagram.length()-1; i++) {
        checksum += datagram.at(i);
    }
    msg << checksum;

//    //Send the packet
//    DWORD BytesWritten;
//    res = FT_WRITE_wrapper(ftdi, datagram.data(), datagram.size(), &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending stimulation start all command.";
    }
//    emit ft_write_signal(datagram);
}

void USBDAQInterface::SendStimulationStopSlot(uint16_t slot) {
    //STOP RUNNING STIMULATION PATTERN DEFINED IN ONE MEMORY SLOT-- 3 BYTES

    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);

    //Fill the packet
    msg << (quint8)command_stimulateStop; //0x72
    msg << (uint8_t)slot;
    //Calculate a checksum
    uint8_t checksum = 0;
    for (int i = 0;i < datagram.length()-1; i++) {
        checksum += datagram.at(i);
    }
    msg << checksum;

//    //Send the packet
//    DWORD BytesWritten;
//    res = FT_WRITE_wrapper(ftdi, datagram.data(), datagram.size(), &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending stimulation stop command.";
    }
//    emit ft_write_signal(datagram);
}

void USBDAQInterface::SendStimulationStopGroup(uint16_t group) {
    //STOP ALL RUNNING STIMULATION PATTERNS FOR SELECTED GROUP

    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);

    //Fill the packet
    msg << (quint8)command_stimulateStopGroup;
    msg << (uint8_t)group;
    //Calculate a checksum
    uint8_t checksum = 0;
    for (int i = 0;i < datagram.length()-1; i++) {
        checksum += datagram.at(i);
    }
    msg << checksum;

//    //Send the packet
//    DWORD BytesWritten;
//    res = FT_WRITE_wrapper(ftdi, datagram.data(), datagram.size(), &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending stimulation stop all command.";
    }
//    emit ft_write_signal(datagram);
}



void USBDAQInterface::SendSettleCommand() {

    unsigned char TxBuffer[256]; // Contains data to write to device

    TxBuffer[0] = 0x66; // 0x66 = settle command
    //TxBuffer[0] = 0x65; // 0x65 = init SD card for writing

    QByteArray ar((char*)TxBuffer, 1);
//    emit ft_write_signal(ar);
    // send stop capture command
//    DWORD BytesWritten;
//    res = FT_Write(ftdi, TxBuffer, 1, &BytesWritten);
    res = FT_WRITE_wrapper(ar);
    if (res != FT_OK) {
      qDebug() << "Error sending settle command.";
    } else {
      qDebug() << "Settle command sent.";
    }

}

void USBDAQInterface::SendSettleChannel(int byteInPacket, quint8 bit, int delay, quint8 triggerState) {
    if ((state == SOURCE_STATE_RUNNING)||(state == SOURCE_STATE_INITIALIZED)) {

        QString tStateString;
        if (triggerState == 0) {
            tStateString = "Off";
        } else if (triggerState == 1) {
            tStateString = "Rising edge";
        } else if (triggerState == 2) {
            tStateString = "Falling edge";
        } else if (triggerState == 3) {
            tStateString = "Both upward and downward edge";
        }


        QByteArray datagram;
        datagram.resize(7);
        QDataStream msg(&datagram, QIODevice::ReadWrite);
        msg.setByteOrder(QDataStream::LittleEndian);
        msg << (quint8)0x6A; // 0x6A = set settle channel
        msg << (uint16_t)(byteInPacket); //the byte in the packet to use
        msg << bit; //uint8, the bit within the given byte to use (0-7)
        msg << (uint16_t)(delay); //delay for the settle to occur in number of samples
        msg << triggerState; //uint8 (0 for off, 1 for rising edge, 2 for falling edge)

//        emit ft_write_signal(datagram);
//        DWORD BytesWritten;
//        res = FT_Write(ftdi, datagram.data(), 7, &BytesWritten);
        res = FT_WRITE_wrapper(datagram);
        if (res != FT_OK) {
          qDebug() << "Error sending settle control channel.";
        } else {
          qDebug() << "Settle control channel sent."<< byteInPacket << "Byte" << bit << "Bit" << tStateString;
        }
    }

}

void USBDAQInterface::SendSDCardUnlock() {
    unsigned char TxBuffer[256]; // Contains data to write to device

    TxBuffer[0] = 0x65; // 0x65 = unlock SD card for writing
    QByteArray datagram((char*)TxBuffer, 1);
    // send stop capture command
//    DWORD BytesWritten;
//    res = FT_Write(ftdi, TxBuffer, 1, &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending unlock command.";
    } else {
      qDebug() << "SD card unlock command sent.";
    }
}

void USBDAQInterface::ConnectToSDCard() {
    unsigned char TxBuffer[256]; // Contains data to write to device

    TxBuffer[0] = 0x67; // 0x67 = ping the MCU for the card
    QByteArray datagram((char*)TxBuffer, 1);
    // send stop capture command
//    DWORD BytesWritten;
//    res = FT_Write(ftdi, TxBuffer, 1, &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending SD card Connect command.";
    } else {
      qDebug() << "SD card connect command sent.";
    }

    emit SDCardStatus(false,0,false,false);
}

void USBDAQInterface::ReconfigureSDCard(int numChannels) {

    //unsigned char TxBuffer[256]; // Contains data to write to device
    //QByteArray temp(TxBuffer, 256);
    //QDataStream msg(&temp, QIODevice::ReadWrite);


    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);
    msg << (uint8_t)0x68; // 0x68 = ping the MCU for the card
    msg << (uint16_t)numChannels;

//    DWORD BytesWritten;
//    res = FT_Write(ftdi, datagram.data(), 3, &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    //res = FT_Write(ftdi, TxBuffer, 3, &BytesWritten);
    if (res != FT_OK) {
      qDebug() << "Error sending SD card reconfigure command.";
    } else {
      qDebug() << "SD card reconfigure command sent.";
    }
}

void USBDAQInterface::CloseInterface(void) {

    restartThreadAfterShutdown = false;
    if (state == SOURCE_STATE_RUNNING) {
          StopAcquisition();
    }

    res = FT_Close(ftdi);
    if (res != FT_OK) {
      qDebug() << "Error closing";
    }

    qDebug() << "Closed FT2232H device.";
    emit stateChanged(SOURCE_STATE_NOT_CONNECTED);
    lastControllerSettings = HardwareControllerSettings();
    lastHeadstageSettings = HeadstageSettings();

}

int USBDAQInterface::MeasurePacketLength(HeadstageSettings settings){
    if(!usbDataProcessor->acquiring && state == SOURCE_STATE_INITIALIZED && settings.valid){
        //minimum length of packets
        int minlength = settings.numberOfChannels*sizeof(int16_t) + sizeof(uint32_t) + 2;//always at least numchans*2 + timetamp + 2bytes for 0x55

        //start streaming
        unsigned char buf[20] = {0};
        buf[0] = startCommandValue;
        buf[1] = 0xff;
        DWORD bytes;
        FT_STATUS res;
        res = FT_Write(ftdi, buf, 2, &bytes);

        //make sure nothing takes too long
        QElapsedTimer timer;
        timer.start();

        //get 0x55's. First for alignment, rest to measure length
        QVector<int> lengths;
        constexpr int numpackets = 99;
        for(int i = 0; i < numpackets+1; ++i){
            bool foundx55 = false;
            int numbytes = 0;
            while(!foundx55){
                res = FT_Read(ftdi, buf, 1, &bytes);
                if(res == FT_OK){
                    numbytes++;
                    if(numbytes >= minlength && buf[0] == 0x55)
                        foundx55 = true;
                    if(i == 0 && buf[0] == 0x55)
                        foundx55 = true;
                }

                if(timer.elapsed() > 500){
                    //Taking way too long, something is wrong
                    buf[0] = command_stop;
                    FT_Write(ftdi, buf, 1, &bytes);
                    return -1;
                }
            }
            if(i > 0){
                lengths.push_back(numbytes);
            }
        }

        //end streaming
        buf[0] = command_stop;
        res = FT_Write(ftdi, buf, 1, &bytes);
        FT_Purge(ftdi, FT_PURGE_RX);

        if(lengths.size() < numpackets){
            return -1;
        }

        //"vote" for what packet size is by majority (mode)
//        QVector<int> lengthsorig(lengths);
        std::sort(lengths.data(),lengths.data()+lengths.length()-1);
        int num = lengths[0];
        int mode = num;
        int count = 1;
        int countMode = 1;
        for(int i = 0; i < lengths.length(); ++i){
            if(lengths[i] == num){
                ++count;
            }
            else{
                if(count > countMode){
                    countMode = count;
                    mode = num;
                }
                count = 1;
                num = lengths[i];
            }
        }
        if(count > countMode){
            countMode = count;
            mode = num;
        }

        if(countMode > numpackets/2){
            //must still be majority
            return mode;
        }
        else{
            return -1;
        }
    }
    return -1;
}

bool USBDAQInterface::isSourceAvailable(){
    //Display all detected devices
    // int iNumDevices = 0;
    FT_DEVICE_LIST_INFO_NODE *devInfo;
    DWORD numDevs = 0;
    FT_STATUS ftStatus = FT_CreateDeviceInfoList(&numDevs);

    if((ftStatus == FT_OK) && (numDevs > 0))
    {
        //devInfo = (FT_DEVICE_LIST_INFO_NODE*)malloc(sizeof(FT_DEVICE_LIST_INFO_NODE)*numDevs);
        devInfo = new FT_DEVICE_LIST_INFO_NODE;

        ftStatus = FT_GetDeviceInfoList(devInfo,&numDevs);
        if (ftStatus == FT_OK) {
            {
                for(long unsigned int i = 0; i < numDevs; i++) {
                    if(!strcmp(devInfo[i].Description, desc)){
                        return true;
                    }
                    else if(!strcmp(devInfo[i].Description, desc_old)){
                        return true;
                    }
                    else if(!strcmp(devInfo[i].Description, desc2)){
                        return true;
                    }
                }
                //delete devInfo;
            }
        }
    }
    return false;
}
