/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef AUDIOTHREAD_H
#define AUDIOTHREAD_H


#include <QThread>
#include <QAudioOutput>
#include <QAudioSink>
#include <QMediaDevices>

#include <QElapsedTimer>
#include <QBuffer>
#include <QQueue>
#include <QTimer>
#include "iirFilter.h"
#include "stdint.h"
#include "analyses.h"
#include "configuration.h"
#include "sourceController.h"

extern eegDataBuffer rawData;


class Generator : public QIODevice
{
    Q_OBJECT
public:

    Generator(QAudioDevice dev, const QAudioFormat &format,  QObject *parent);
    ~Generator();

    //int listenChannel;
    int listenHWChannel;
    int listenRefHWChan;
    int listenRefCARGroup;
    int threshMicroVolts;
    int thresh;
    float scalingToUv;
    float masterGain;
    bool filterOn;
    bool notchFilterOn;
    bool spectralAnalysisOn;

    int inputFreqHz;
    TrodesConfigurationPointers currentConfiguration;


    quint64 soundDataRead;
    qint64 totalReadHead;
    qint64 totalWriteHead;
    int rawReadIdx;

    void start();
    void stop();
    int checkForSamples();
    void setChannel(int hwchannel);
    void setRefChannel(int hwchannel);
    void setRefGroup(int group);
    void resetBuffer();
    //ButterworthFilter filterObj;
    BesselFilter filterObj;
    NotchFilter notchFilter;
    SpectralAnalysis *specAnalysis;

    qint64 bytesWaiting();
    qint64 readData(char *data, qint64 maxlen);
    qint64 writeData(const char *data, qint64 len);
    qint64 bytesAvailable() const;

private:


    QByteArray m_buffer;
    qint64 writeHead;
    qint64 readHead;
    qint64 bufferLength;
    double interpRatio;
    int16_t currentValue;
    int16_t nextValue;




private slots:


};


class AudioController : public QObject
{
    Q_OBJECT
public:
    explicit AudioController(QObject *parent = 0);
    ~AudioController();
    //void run();
    int getGain();
    int getThresh();
    QStringList getAvailableDevices();
    QString getCurrentDevice();
    bool setupOk();
    QTimer*  m_pullTimer;
    void setConfiguration(TrodesConfigurationPointers c);

    //bool keepLooping;

signals:

    void finished();
    void stopTimer();
    void deviceChanged(QAudioSink *oldDevice);
    void newSpectrum(QList<QPointF> dataPoints);

private slots:

    void pullTimerExpired();
    void stateChanged(QAudio::State state);


private:
    bool foundSupportedAudioHardware;
    void createAudioOutput();
    QAudioDevice     m_device;
    Generator*       m_generator;
    QAudioSink*      m_audioOutput;
    QIODevice*       m_output;
    QAudioFormat    m_format;

    bool             m_pullMode;
    QByteArray       m_buffer;
    QElapsedTimer    stopWatch;
    bool             hasSupportedAudio;
    QStringList      availableDevices;
    QString          currentDevice;
    bool             deviceChangeLock;
    TrodesConfigurationPointers currentConfiguration;


public slots:
    bool setDevice(QString device);
    void setChannel(int hwchannel);
    void setRefChannel(int channel);
    void setRefGroup(int group);
    void updateAudio(void);
    void setGain(int gain);
    void setThresh(int thresh);
    void endAudio();
    void resetBuffer();
    void startAudio();
    void stopAudio();
    void toggleSpectralAnalysis(bool on);
    //void startLoop();

private slots:
    void initializeAudio();

};

#endif // AUDIOTHREAD_H
