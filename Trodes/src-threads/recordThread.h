/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef RECORDTHREAD_H
#define RECORDTHREAD_H



#include <QOpenGLWidget>
#include <QThread>
#include <QTimer>
#include <QFile>
#include "configuration.h"
#include "sharedVariables.h"
#include "hardwaresettings.h"
#include "sourceController.h"

extern eegDataBuffer rawData;

class RecordThread: public QObject
{
    Q_OBJECT
public:
    explicit RecordThread(TrodesConfiguration *workspace, bool saveDisplayedChan, int MBPerFileChunk, QObject *parent = 0);
    ~RecordThread();
    //void run();
    int openFile(QString fileName, HardwareControllerSettings hardwareSettings, HeadstageSettings headstageSettings);
    void closeFile();
    void startRecord();
    void pauseRecord();
    quint64 getBytesWritten();
    qint64  getBytesFree();
    quint64 getPacketsWritten();

//    bool rewriteStartRecordField;   //if true, record thread will overwrite the "recording started" field with the current sys timestamp
//    int64_t startRecordFieldPos;    //file position of where the sys timestamp field goes

    bool fileOpen;
    bool recording;
    bool overrideSaveDisplayedChan;
    QFile *file;

private:

    QTimer *pullTimer;
    QDataStream *outStream;
    quint64 saveMarker;
    quint64 bytesWritten;
    quint64 packetsWritten;
    QString baseName;
    int bufferLocation;
    int currentFilePart;
    bool errorSignalEmitted;

    uint32_t currentRecordTimestamp;
    uint32_t lastRecordTimestamp;
    uint32_t *tsPtr;

    short *dataArray;
    bool startNextFilePart();
    bool nextFilePartTriggered;

    bool saveDisplayedChanOnly;
    int MBPerFileChunk;
    QVector<bool> saveHWChan;
    int nChanConfigured;
    TrodesConfiguration* workspace;
signals:
    void writeError();
    void noSpaceLeftError();
    void lowDiskSpaceWarning();
    void finished();
    void triggerNextFilePart();


private slots:
    void pullTimerExpired();

public slots:
    void setUp();
    void setupSaveDisplayedChan();
    void endRecordThread();
};


#endif // RECORDTHREAD_H
