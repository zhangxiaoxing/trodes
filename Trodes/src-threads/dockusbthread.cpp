﻿/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "dockusbthread.h"
#include "globalObjects.h"
#include <string.h>
#include "CZHelp.h"
#include <time.h>

FT_STATUS dockres;
FT_HANDLE dockftdi;

DockUSBRuntime::DockUSBRuntime(bool ecu, QObject *parent) : ecuConnected(ecu){
}

void DockUSBRuntime::Run(){

    emit startHelperThread();

    unsigned char *RxBuffer;
    DWORD BytesReceived;
    //int PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + 1 + 1;

    calculateHeaderSize();
    PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + (2*headerSize); //Packet size in bytes
    //unsigned char buffer[PACKET_SIZE];
    //buffer = new unsigned char[PACKET_SIZE];
    buffer.resize(PACKET_SIZE);
//    int remainingSamples = 0;
    int leftInBuffer;
    double dTimestamp = 0.0;
    int tic = 0;
    int tempMax;
    int maxAvailable = 0;
    int numConsecErrors = 0;
    quitNow = false;
    acquiring = true;
    lastTimeStamp = 0;
    runLoopActive = true;

    packetSizeErrorThrown = false;

    PACKET_SIZE = ecuConnected ? 46 : 14;
    buffer.resize(PACKET_SIZE);

    while(quitNow != true){
        if (acquiring) {

            if (badFrameAlignment && tempSyncByteLocation > 0) {
                //We have lost packet alignment, but we have a candidate sync byte location. We dump
                //everything up to that point in the current packet.
                dockres = FT_Read(dockftdi,buffer.data(),tempSyncByteLocation,&BytesReceived);
            }

            //Read in data from FTDI chip.  This function does not return until PACKET_SIZE
            //number of bytes have been read into the buffer.

            dockres = FT_Read(dockftdi,buffer.data(),PACKET_SIZE,&BytesReceived);

            if (BytesReceived != (DWORD) PACKET_SIZE) {
                //if (aquiring) {
                qDebug() << "Error in USB acquisition";

                numConsecErrors++;
                if (!packetSizeErrorThrown && numConsecErrors > 5 && BytesReceived > 0) {

                    qDebug() << "Wrong incoming packet size";
                    packetSizeErrorThrown = true;
                    emit timeStampError(true);
                    emit failure();
                }
                //break;
                //}
            } else {
                //Process the received data packet.
                //The packet contains a 16-bit header for binary info,
                //a 32-bit time stamp, and hardwareConf->NCHAN 16-bit samples.

                leftInBuffer = PACKET_SIZE;
                RxBuffer = buffer.data();

                //The first byte is always 0x55.  Check to make sure we have
                //the correct frame alignment
                if (!checkFrameAlignment(RxBuffer)) {
                    //We don't have alignment!  So now we drop everything
                    //and try to find it.
                    numConsecErrors++;
                    if (!packetSizeErrorThrown && numConsecErrors > 5) {
                        //if we have had many bad alignments in a row,
                        //consider it a fatal error and stop

                        //quitNow = true;
                        //emit failure();
                        qDebug() << "Wrong incoming packet size";
                        packetSizeErrorThrown = true;
                        emit timeStampError(true);
                        emit failure();

                    }
                    //We dump the rest of the processing for this packet
                    continue;
                }
                numConsecErrors = 0; //Packet looks good
                checkHardwareStatus(RxBuffer+1);
                if(RxBuffer[2]==0x10){
//                    uint32_t sectors = (RxBuffer[4] << 24) | (RxBuffer[5] << 16) | (RxBuffer[6] << 8) | (RxBuffer[7]);
//                    uint16_t battery = (RxBuffer[8] << 8) | (RxBuffer[9]);
//                    printf("dock timestamp: %u, logger sd sectors written: %u, battery: %u\n", *(uint32_t*)(RxBuffer+10), sectors, battery);
//                    for(int i=4;i<10;++i)printf("%02X ", RxBuffer[i]); printf("\n");
                    QByteArray bytes((char*)(&RxBuffer[4]), 6);
                    emit payloadReceived(0, bytes);
                }
                //Read header info
                memcpy(&(rawData.digitalInfo[rawData.writeIdx*headerSize]), \
                        RxBuffer, headerSize*sizeof(uint16_t));

                RxBuffer += (2*headerSize);
                leftInBuffer -= (2*headerSize);

                //Process time stamp
                uint32_t* dataPtr = (uint32_t *)(RxBuffer);
                currentTimeStamp = *dataPtr;




                if (checkForCorrectTimeSequence() && !packetSizeErrorThrown) {
                    rawData.timestamps[rawData.writeIdx] = *dataPtr;
                    //rawData.sysTimestamps[rawData.writeIdx] = CZHelp::systemTimeMSecs();

                    rawData.sysTimestamps[rawData.writeIdx] = QTime::currentTime().msecsSinceStartOfDay();

                    RxBuffer += 4;
                    leftInBuffer -= 4;
                    dTimestamp += 1.0;
                    rawData.dTime[rawData.writeIdx] = dTimestamp;
                    // system time in nanoseconds
  //                  rawData.sysClock[rawData.writeIdx] = QDateTime::currentMSecsSinceEpoch() * 1000000;
                    rawData.sysClock[rawData.writeIdx] = AbstractSourceRuntime::getSystemTimeNanoseconds();

                    //Advance the write markers and release a semaphore
                    writeMarkerMutex.lock();
                    rawData.writeIdx = (rawData.writeIdx + 1) % EEG_BUFFER_SIZE;
                    rawDataWritten++;
                    writeMarkerMutex.unlock();

                    for (int a = 0; a < rawDataAvailable.length(); a++) {
                        rawDataAvailable[a]->release(1);
                    }
                    sourceDataAvailable->release(1);


                    if ((tempMax = rawDataAvailable[0]->available()) > maxAvailable)
                        maxAvailable = tempMax;

                    if ((++tic % 10000) == 0) {
                        maxAvailable = 0;
                    }
                }


            }
        } else {
            //Not acquiring, but connected to source
            if (packetSizeErrorThrown) {
              packetSizeErrorThrown = false;
              emit timeStampError(false);
            }
            QThread::msleep(50);
        }
    }

    runtimeHelper->quitNow = true;

    qDebug() << "USB source thread finished.";
    runLoopActive = false;
    emit timeStampError(false);
    emit finished();

}

//void DockUSBRuntime::ft_write_slot(QByteArray datagram)
//{

//}

DockUSBInterface::DockUSBInterface(QObject *parent)
{
    state = SOURCE_STATE_NOT_CONNECTED;
    usbDataProcessor = NULL;
}

DockUSBInterface::~DockUSBInterface()
{

}

quint64 DockUSBInterface::getTotalUnresponsiveHeadstagePackets() {
    quint64 rVal = 0;
    if (usbDataProcessor != NULL) {
        rVal = usbDataProcessor->totalUnresponsiveHeadstagePackets;
    }
    return rVal;
}

quint64 DockUSBInterface::getTotalDroppedPacketEvents()
{
    quint64 rVal = 0;
    if (usbDataProcessor != NULL) {
        rVal = usbDataProcessor->totalDroppedPacketEvents;
    }
    return rVal;
}

void DockUSBInterface::restartThread()
{    qDebug() << "Restarting USB source thread.";
     //start the runtime thread
     usbDataProcessor = new DockUSBRuntime(hardwareConf->ECUConnected, NULL);
     usbDataProcessor->acquiring = false;
     setUpThread(usbDataProcessor);
     state = SOURCE_STATE_INITIALIZED;
      emit stateChanged(SOURCE_STATE_INITIALIZED);
}

void DockUSBInterface::getLastPayload()
{
    if(!hasAck){
        return;
    }
    uint8_t TxBuffer[32];
    char RxBuffer[128];
    FT_STATUS ftStatus;
    DWORD BytesWritten, BytesReceived;

    TxBuffer[0] = 0x3c; //DOCK_LATEST_PAYLOAD
    ftStatus = FT_Write(dockftdi, TxBuffer, 1, &BytesWritten);
    if(ftStatus!=FT_OK || BytesWritten!=1){
        printf("Error sending DOCK_LATEST_PAYLOAD cmd\n");
    }

    ftStatus = FT_Read(dockftdi, RxBuffer, 1, &BytesReceived);
    if(ftStatus!=FT_OK || BytesReceived!=1){
        printf("Error receiving payload length %d %d\n", ftStatus, BytesReceived);
    }

    uint8_t datalen = RxBuffer[0];
    if(datalen){
        ftStatus = FT_Read(dockftdi, RxBuffer, datalen, &BytesReceived);
        if(ftStatus!=FT_OK || BytesReceived!=datalen){
            printf("Error receiving actual payload %d %d\n", ftStatus, BytesReceived);
        }

        QByteArray bytes(RxBuffer, datalen);
        emit idlePayloadForwarding(0, bytes);
    }
}

void DockUSBInterface::InitInterface()
{
    restartThreadAfterShutdown = true;
    FT_STATUS	ftStatus;
    DWORD libraryVersion = 0;

    ftStatus = FT_GetLibraryVersion(&libraryVersion);
    if (ftStatus == FT_OK){
        qDebug() << "FTDI Library version: " << (unsigned int)libraryVersion;
    }
    else{
        qDebug() << "Error reading FTDI library version.";
    }

    // Setup FTDI FT2232H interface
    connectErrorThrown = false;
#if defined (__linux) || defined (__APPLE__)
    // need to manually set VID and PID on linux
    dockres = FT_SetVIDPID(VENDOR, DEVICE);
    if (dockres != FT_OK) {
        qDebug() << "SetVIDPID failed";
        return;
    }
#endif

    char desc[] = "DockingStation A";
    dockres = FT_OpenEx(desc, FT_OPEN_BY_DESCRIPTION, &dockftdi);
    if (dockres != FT_OK) {
        qDebug() << "Open FTDI failed (See Resources/SetupHelp/).";
        connectErrorThrown = true;
        emit stateChanged(SOURCE_STATE_CONNECTERROR);
        return;
    }
    ftStatus = FT_SetBitMode(dockftdi, 0xff, 0x40);
    if (ftStatus != FT_OK) {
        qDebug() << "Can't set sync mode for FTDI device";
        connectErrorThrown = true;
        emit stateChanged(SOURCE_STATE_CONNECTERROR);
        return;
    }

    ftStatus = FT_SetUSBParameters(dockftdi, 65536, 65536);	//Set USB request transfer size
    ftStatus |= FT_SetFlowControl(dockftdi,FT_FLOW_RTS_CTS,0,0);
    ftStatus |= FT_SetChars(dockftdi, false, 0, false, 0);	 //Disable event and error characters
    ftStatus |= FT_SetTimeouts(dockftdi, 1000, 0);		//Sets the read and write timeouts in milliseconds for the FT2232H
    ftStatus |= FT_SetLatencyTimer(dockftdi, 16);		//Set the latency timer
    ftStatus |= FT_Purge(dockftdi, FT_PURGE_RX | FT_PURGE_TX); // Clear buffers
    if (dockres != FT_OK) {
        qDebug() << "Error initializing FTDI device";
        connectErrorThrown = true;
        emit stateChanged(SOURCE_STATE_CONNECTERROR);
        return;
    }
    unsigned char TxBuffer[8];
    unsigned char RxBuffer[256];
    DWORD BytesWritten, BytesReceived;

    TxBuffer[0] = 0x36; //DOCK_GETSERIAL_CMD
    ftStatus = FT_Write(dockftdi, TxBuffer, 1, &BytesWritten);
    if(ftStatus!=FT_OK || BytesWritten!=1){
        printf("Error sending DOCK_GET_SETTINGS cmd\n");
    }

    ftStatus = FT_Read(dockftdi, RxBuffer, 4, &BytesReceived);
    if(ftStatus!=FT_OK || BytesReceived!=4){
        printf("Error receiving user data\n");
    }

    dockSettings.model = *(uint16_t*)(RxBuffer);
    dockSettings.serial = *(uint16_t*)(RxBuffer+2);
//    qDebug() << "Model: " << dockSettings.model << ", Serial: " << dockSettings.serial;

    TxBuffer[0] = 0x42; //DOCK_USERDATA_LEN
    ftStatus = FT_Write(dockftdi, TxBuffer, 1, &BytesWritten);
    if(ftStatus!=FT_OK || BytesWritten!=1){
        printf("Error sending DOCK_USERDATA_LEN cmd\n");
    }

    hasAck = false;
    int datalen = 0;
    ftStatus = FT_Read(dockftdi, RxBuffer, 4, &BytesReceived);
    if(ftStatus!=FT_OK || BytesReceived!=4){
        if(BytesReceived == 0){
            hasAck = false;
        }
    }
    else{
        datalen = *(int*)RxBuffer;

        TxBuffer[0] = 0x46; //DOCK_HAS_ACK
        ftStatus = FT_Write(dockftdi, TxBuffer, 1, &BytesWritten);
        if(ftStatus!=FT_OK || BytesWritten!=1){
            printf("Error sending DOCK_HAS_ACK cmd\n");
        }

        ftStatus = FT_Read(dockftdi, RxBuffer, 1, &BytesReceived);
        if(BytesReceived && RxBuffer[0] != 0){
            hasAck = true;
        }
    }

    if(hasAck){
//        printf("User data len: %d\n", datalen);

        TxBuffer[0] = 0x43; //DOCK_GET_SETTINGS
        ftStatus = FT_Write(dockftdi, TxBuffer, 1, &BytesWritten);
        if(ftStatus!=FT_OK || BytesWritten!=1){
            printf("Error sending DOCK_GET_SETTINGS cmd\n");
        }

        ftStatus = FT_Read(dockftdi, RxBuffer, datalen, &BytesReceived);
        if(ftStatus!=FT_OK || BytesReceived!=datalen){
            printf("Error receiving user data\n");
        }

        dockSettings.rfchan= RxBuffer[0];
        dockSettings.description = (char*)(RxBuffer+1);
        dockSettings.samplingRateKhz = (*(uint8_t*)(RxBuffer+64) == 20) ? 20 : 30;

        qDebug() << "RF channel: " << dockSettings.rfchan<< "Description: " << dockSettings.description << "Sampling rate (khz): " << dockSettings.samplingRateKhz;

        //timer to check payload when idle
        connect(&payloadTimer, &QTimer::timeout, this, &DockUSBInterface::getLastPayload);
        payloadTimer.start(1000);
    }

    usbDataProcessor = new DockUSBRuntime(hardwareConf->ECUConnected, NULL);
    usbDataProcessor->acquiring = false;
    setUpThread(usbDataProcessor);
    state = SOURCE_STATE_INITIALIZED;
    emit stateChanged(SOURCE_STATE_INITIALIZED);

    connect(usbDataProcessor, &DockUSBRuntime::payloadReceived, this, &DockUSBInterface::streamPayloadForwarding);
}

void DockUSBInterface::StartAcquisition()
{

    if (!threadCreated) {
        restartThread();
    }

    payloadTimer.stop();

    static int runtimeStarted  = 0;
    unsigned char TxBuffer[256]; // Contains data to write to device
    unsigned char RxBuffer[256];
    rawData.writeIdx = 0; // location where we're currently writing

    TxBuffer[0] = startCommandValue;
    uint32_t recid = 123456789;
    TxBuffer[1] = (recid>>24);
    TxBuffer[2] = (recid>>16);
    TxBuffer[3] = (recid>>8);
    TxBuffer[4] = (recid>>0);
    uint32_t t = time(NULL);
    TxBuffer[5] = (t>>24);
    TxBuffer[6] = (t>>16);
    TxBuffer[7] = (t>>8);
    TxBuffer[8] = (t>>0);

    DWORD BytesWritten;
    int bytes;
    if(hasAck){
        bytes = 9;
    }
    else{
        bytes = 1;
    }
    dockres = FT_Write(dockftdi, TxBuffer, bytes, &BytesWritten); //need to put in rec ID and date/time
    if (dockres != FT_OK) {
      qDebug() << "Error writing" << dockres;
      return;
    }

    if(BytesWritten != bytes){
        qDebug() << "Error sending bytes over USB! Sent " << BytesWritten << "bytes";
    }

    int dockstartret = 0;
    if(hasAck){
        DWORD bytesreceived;
        dockres = FT_Read(dockftdi, RxBuffer, 4, &bytesreceived);
        if( dockres != FT_OK){
            qDebug() << "Error waiting for return code from dock";
            return;
        }

        dockstartret = *(int*)RxBuffer;
        qDebug() << "Start return code: " << dockstartret;
    }

    if (runtimeStarted == 0) {
      emit startRuntime();
      qDebug() << "Told runtime to start";
    }

    state = SOURCE_STATE_RUNNING;
    emit stateChanged(SOURCE_STATE_RUNNING);
    usbDataProcessor->resetStreamingStats();
    usbDataProcessor->acquiring = true;
    emit acquisitionStarted();

    if(hasAck && dockstartret != 0){
        QMessageBox::warning(nullptr, "Headstage did not respond", "The docking station did not receive a response from the headstage when starting streaming. Please verify the headstage is connected and breathing a green light.");
    }
}

void DockUSBInterface::StopAcquisition()
{
    if (!threadCreated) return;

    unsigned char TxBuffer[256]; // Contains data to write to device

    TxBuffer[0] = 0x62; // 0x62 = stop data capture

    // send stop capture command
    DWORD BytesWritten;
    dockres = FT_Write(dockftdi, TxBuffer, 1, &BytesWritten);
    if (dockres != FT_OK) {
      qDebug() << "Error stopping";
    } else {
      qDebug() << "Stopped acquisition.";
    }

    //The worker thread is stopped, and will restart again if restartThreadAfterShutdown is true

    usbDataProcessor->printStreamingStats();
    usbDataProcessor->quitNow = true;
    usbDataProcessor->acquiring = false;



    emit acquisitionStopped();
    state = SOURCE_STATE_INITIALIZED;
    emit stateChanged(SOURCE_STATE_INITIALIZED);



//    dockres = FT_Purge(ftdi, FT_PURGE_RX | FT_PURGE_TX);
//    if (dockres != FT_OK) {
//      qDebug() << "Error purging usb RX/TX";
//    }
}

void DockUSBInterface::CloseInterface()
{
    restartThreadAfterShutdown = false;
    if (state == SOURCE_STATE_RUNNING) {
          StopAcquisition();
    }

    dockres = FT_Close(dockftdi);
    if (dockres != FT_OK) {
      qDebug() << "Error closing";
    }

    qDebug() << "Closed FT2232H device.";
    emit stateChanged(SOURCE_STATE_NOT_CONNECTED);
    lastControllerSettings = HardwareControllerSettings();
    lastHeadstageSettings = HeadstageSettings();

    dockSettings.serial = 0;
    dockSettings.model = 0;
    dockSettings.rfchan= 0;
    dockSettings.description = "";
    dockSettings.samplingRateKhz = 0;
}

