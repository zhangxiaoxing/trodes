#ifndef USB3THREAD_H
#define USB3THREAD_H



#include <QThread>
#include <QVector>
#include "abstractTrodesSource.h"

#ifdef WIN32
    #include <windows.h>
    #include "FTD3XX.h"
#endif
#ifdef __APPLE__
    #include "ftd3xx.h"
#endif
#ifdef linux
    #include "ftd3xx.h"
#endif


#define VENDOR 0x0403
#define DEVICE 0x6010

//extern FT_STATUS res;
//extern FT_HANDLE ftdi;

class USB3Runtime : public AbstractSourceRuntime {
  Q_OBJECT
public:
  USB3Runtime(QObject *parent);
  QVector<unsigned char> buffer;
  QByteArray datagram;
  FT_HANDLE ftdi3;

public slots:
  void Run(void);

  void ft_write_slot(QByteArray datagram);
signals:
  //void ft_write_status_return(FT_STATUS, DWORD);
};

class USB3Interface : public AbstractTrodesSource {
  Q_OBJECT

public:
  USB3Interface(QObject *parent);
  ~USB3Interface(void);
  int state;
  quint64 getTotalDroppedPacketEvents();
  quint64 getTotalUnresponsiveHeadstagePackets();
  void SendImpendaceMeasureCommand(HardwareImpedanceMeasureCommand s);
  //HardwareControllerSettings lastControllerSettings;
  //HeadstageSettings          lastHeadstageSettings;

private:
  //struct libusb_transfer **transfers;
  //int n_transfers;
  char const *desc_hardware = "FTDI SuperSpeed-FIFO Bridge"; //Change later to specific name
  USB3Runtime *usbDataProcessor;
  QThread       *workerThread;

  QElapsedTimer lastSendTime;
  //A function that waits before calling FT_Write, making sure waitMSecs ms has passed since last message
  int FT_WRITE_wrapper(QByteArray &data);
  FT_STATUS retval;
  FT_STATUS res;
  FT_HANDLE ftdi3;
  DWORD byteswritten;
  QTimer writeSignalTimer;
  int waittime;
  QElapsedTimer timer;

signals:
  void ft_write_signal(QByteArray);
private slots:
  void restartThread();
  //void ft_write_slot(FT_STATUS status, DWORD bytes);

public slots:
  void InitInterface(void);
  void StartAcquisition(void);
  void StartSimulation(void);
  void StopAcquisition(void);
  void CloseInterface(void);
  void SendSettleCommand(void);
  void SendSDCardUnlock(void);
  void ConnectToSDCard(void);
  void ReconfigureSDCard(int numChannels);
  void SendHeadstageSettings(HeadstageSettings s);
  bool SendNeuroPixelsSettings(NeuroPixelsSettings s);
  void SendSaveHeadstageSettings();
  void SetStimulationParams(StimulationCommand s);
  bool SendGlobalStimulationSettings(GlobalStimulationSettings s);
  void SendGlobalStimulationAction(GlobalStimulationCommand s);
  void ClearStimulationParams(uint16_t slot);
  void SendStimulationStartSlot(uint16_t slot);
  void SendStimulationStartGroup(uint16_t group);
  void SendStimulationStopSlot(uint16_t slot);
  void SendStimulationStopGroup(uint16_t group);
  void SendControllerSettings(HardwareControllerSettings s);
  HeadstageSettings GetHeadstageSettings();
  HardwareControllerSettings GetControllerSettings();
  void SendSettleChannel(int byteInPacket, quint8 bit, int delay, quint8 triggerState);
  int MeasurePacketLength(HeadstageSettings settings);
  bool RunDiagnostic(QByteArray *data,HeadstageSettings &hsSettings, HardwareControllerSettings &cnSettings);
  bool isSourceAvailable();
};

#endif // USB3THREAD_H
