/*
Trodes is a free, open-source neuroscience data collection and experimental control toolbox

Copyright (C) 2012 Mattias Karlsson

This program is free software: you can redistribute it and/or modify
                               it under the terms of the GNU General Public License as published by
                               the Free Software Foundation, either version 3 of the License, or
                               (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "usb3thread.h"
#include "globalObjects.h"
#include "CZHelp.h"



USB3Runtime::USB3Runtime(QObject *parent) {
}


void USB3Runtime::Run() {

  int transferSize = 1024*32;
  TransferBlockRingBuffer transferBuffer(transferSize, 1000);
  runtimeHelper->setTransferBuffer(&transferBuffer);
  emit startHelperThread();
  while (!runtimeHelper->runLoopActive) {
      QThread::msleep(100);
  }


  //int PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + 1 + 1;

  calculateHeaderSize();
  PACKET_SIZE = 2*(hardwareConf->NCHAN) + 4 + (2*headerSize); //Packet size in bytes




  buffer.resize(PACKET_SIZE);

  unsigned char *RxBuffer;
  int remainingSamples = 0;
  int leftInBuffer;
  double dTimestamp = 0.0;
  int tic = 0;
  int tempMax;
  int maxAvailable = 0;
  int numConsecErrors = 0;
  quitNow = false;
  acquiring = true;
  lastTimeStamp = 0;
  runLoopActive = true;
  FT_STATUS res;

  packetSizeErrorThrown = false;
  qDebug() << "USB3 handle events loop running....";


  const int numAsynchronousReads = 10;
  //OVERLAPPED vOverlappedRead[20] = {{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0},{0}};
  OVERLAPPED vOverlappedRead[numAsynchronousReads];

  FT_STATUS readStatus[numAsynchronousReads];
  bool needReadRestart = true;
  DWORD BytesReceived[numAsynchronousReads];
  int currentReader = 0;

  for (int i=0;i<numAsynchronousReads;i++) {
      memset(&vOverlappedRead[i], 0, sizeof(OVERLAPPED));
  }





 // FT_Purge(ftdi, FT_PURGE_RX);
  while (quitNow != true) {

      //QApplication::processEvents();

      if (acquiring) {



#ifdef __APPLE__

#endif

#ifdef WIN32


          if (needReadRestart) {
              res = FT_SetStreamPipe(ftdi3,false,false,0x82,transferSize);
              for (int i=0; i < numAsynchronousReads; i++) {
                  bool bufferReservable;
                  uchar* reservePtr = transferBuffer.getNextWritePtr(&bufferReservable);
                  if (!bufferReservable) {
                      //We have a buffer overrun situation. We need to wait until the consumer has caught up
                      //Maybe put a brief sleep here? Also a debug log warning?
                      qDebug() << "Error: Memory buffer issue-- can't start USB readers.";
                      quitNow = true;
                      break;
                  }

                  res = FT_InitializeOverlapped(ftdi3,&vOverlappedRead[i]);
                  if (res != FT_OK) {
                      qDebug() << "Unable to start asynchronous reader numer" << i;
                      quitNow = true;
                      break;
                  }
                  BytesReceived[i] = 0;
                  //Read in data from FTDI chip. The actual number of bytes received may be less than that requested.
                  //readStatus[i] = FT_ReadPipe(ftdi3, 0x82, reservePtr, transferSize, &BytesReceived[i], &vOverlappedRead[i]);
                  readStatus[i] = FT_ReadPipeEx(ftdi3, 0x82, reservePtr, transferSize, &BytesReceived[i], &vOverlappedRead[i]);
                  //readStatus[i] = FT_ReadPipeEx(ftdi3, 0x82, reservePtr, transferSize, &BytesReceived[i], 1000);


              }
              currentReader = 0;
              needReadRestart = false;
          }

          if (readStatus[currentReader] == FT_IO_PENDING) {
              do {
                readStatus[currentReader] = FT_GetOverlappedResult(ftdi3, &vOverlappedRead[currentReader], &BytesReceived[currentReader], false);
                if (readStatus[currentReader] == FT_IO_INCOMPLETE) {
                    //QThread::msleep(1);
                    continue;
                } else if (FT_FAILED(readStatus[currentReader])){
                    //CloseHandle(vOverlappedRead.hEvent);
                    qDebug() << "USB3 transfer error.";
                    break;
                    //Error handling here
                } else {
                    break;
                }
              } while (acquiring);

              FT_ReleaseOverlapped(ftdi3,&vOverlappedRead[currentReader]);
              transferBuffer.setLastWriteTransferSize(BytesReceived[currentReader]);

              if (acquiring) {
                  BytesReceived[currentReader] = 0;
                  bool bufferReservable;
                  uchar* reservePtr = transferBuffer.getNextWritePtr(&bufferReservable);
                  if (!bufferReservable) {
                      //We have a buffer overrun situation. We need to wait until the consumer has caught up
                      //Maybe put a brief sleep here? Also a debug log warning?
                      qDebug() << "WARNING: USB3 processing buffer full. Waiting for processing thread to catch up.";
                      while (!bufferReservable && acquiring) {
                          reservePtr = transferBuffer.getNextWritePtr(&bufferReservable);
                      }

                  }
                  FT_InitializeOverlapped(ftdi3,&vOverlappedRead[currentReader]);
                  //Read in data from FTDI chip. The actual number of bytes received may be less than that requested.
                  //readStatus[currentReader] = FT_ReadPipe(ftdi3, 0x82, reservePtr, transferSize, &BytesReceived[currentReader], &vOverlappedRead[currentReader]);
                  readStatus[currentReader] = FT_ReadPipeEx(ftdi3, 0x82, reservePtr, transferSize, &BytesReceived[currentReader], &vOverlappedRead[currentReader]);
                  //readStatus[currentReader] = FT_ReadPipeEx(ftdi3, 0x82, reservePtr, transferSize, &BytesReceived[currentReader], 1000);
                  currentReader = (currentReader+1)%numAsynchronousReads;
              }

          } else {
              qDebug() << "Expected pending reader.";
          }


#endif

#ifdef linux

          BytesReceived[currentReader] = 0;
          bool bufferReservable;
          uchar* reservePtr = transferBuffer.getNextWritePtr(&bufferReservable);
          if (!bufferReservable) {
              //We have a buffer overrun situation. We need to wait until the consumer has caught up
              //Maybe put a brief sleep here? Also a debug log warning?
              qDebug() << "WARNING: USB3 processing buffer full. Waiting for processing thread to catch up.";
              while (!bufferReservable && acquiring) {
                  reservePtr = transferBuffer.getNextWritePtr(&bufferReservable);
              }

          }
          readStatus[currentReader] = FT_ReadPipeEx(ftdi3, 0, reservePtr, transferSize, &BytesReceived[currentReader], 1000);
          transferBuffer.setLastWriteTransferSize(BytesReceived[currentReader]);

          currentReader = (currentReader+1)%numAsynchronousReads;


 #endif



      } else {
          //Not acquiring, but connected to source
          //FT_AbortPipe(ftdi3,0x82);
          //FT_ClearStreamPipe(ftdi3,false,false,0x82);
          //for (int i=0; i<numAsynchronousReads; i++) {
          //    FT_ReleaseOverlapped(ftdi3,&vOverlappedRead[i]);
          //}

          if (packetSizeErrorThrown) {
            packetSizeErrorThrown = false;
            emit timeStampError(false);
          }
          needReadRestart = true;
          QThread::msleep(50);
      }
  }



  runtimeHelper->quitNow = true;

#ifdef WIN32
  for (int i=0; i<numAsynchronousReads; i++) {

      if (readStatus[currentReader] == FT_IO_PENDING) {
          do {
              readStatus[currentReader] = FT_GetOverlappedResult(ftdi3, &vOverlappedRead[currentReader], &BytesReceived[currentReader], false);
              if (readStatus[currentReader] == FT_IO_INCOMPLETE) {
                  //QThread::msleep(1);
                  continue;
              } else if (FT_FAILED(readStatus[currentReader])){
                  //CloseHandle(vOverlappedRead.hEvent);
                  qDebug() << "USB3 shutdown error.";
                  //FT_AbortPipe(ftdi3,0x82);
                  break;
                  //Error handling here
              } else {
                  break;
              }
          } while (1);
      }

  }
  for (int i=0; i<numAsynchronousReads; i++) {

      FT_ReleaseOverlapped(ftdi3,&vOverlappedRead[i]);

  }


  //res = FT_ClearStreamPipe(ftdi3,false,false,0x82);
  //if (res != FT_OK) {
  //    qDebug() << "Failure clearing USB3 stream pipe.";
  //}
#endif

  while (runtimeHelper->runLoopActive) {
      QThread::msleep(100);
  }

  //We need to implement a  wait here until the helper thread is finished, otherwise the transferBuffer may get deleted before
  //the helper thread is done using it.

  qDebug() << "USB3 source thread finished.";
  runLoopActive = false;

  emit timeStampError(false);
  emit finished();



}

void USB3Runtime::ft_write_slot(QByteArray datagram){
    /*DWORD BytesWritten;
    FT_STATUS r = FT_Write(ftdi, datagram.data(), datagram.size(), &BytesWritten);
    emit ft_write_status_return(r, BytesWritten);*/
}

USB3Interface::USB3Interface(QObject *) {
  state = SOURCE_STATE_NOT_CONNECTED;
  usbDataProcessor = NULL;


  //usbDataProcessor = new USB3Runtime(NULL);
  //usbDataProcessor->connect(this, SIGNAL(startRuntime()), SLOT(Run()));
  qRegisterMetaType<FT_STATUS>("FT_STATUS");
  qRegisterMetaType<DWORD>("DWORD");
  //retval = 20;//Default value if usb runtime thread never signals back
  //byteswritten = 0;
  writeSignalTimer.setSingleShot(true);
  waittime = 5;
}

USB3Interface::~USB3Interface() {

    /*
    if (usbDataProcessor != NULL) {
        delete(usbDataProcessor);
    }*/
}

bool USB3Interface::RunDiagnostic(QByteArray *data,HeadstageSettings &hsSettings, HardwareControllerSettings &cnSettings) {


    return false; //for now...


    /* bool failed = false;



    FT_STATUS	ftStatus;


    // Setup FTDI FT2232H interface

#if defined (__linux) || defined (__APPLE__)
  // need to manually set VID and PID on linux
  res = FT_SetVIDPID(VENDOR, DEVICE);
  if (res != FT_OK) {
    qDebug() << "SetVIDPID failed";
    return false;
  }
#endif




  //res = FT_Open(0, &ftdi);
  res = FT_OpenEx((void*)desc_hardware, FT_OPEN_BY_DESCRIPTION, &ftdi);
  if (res != FT_OK) {
    //try the old USB label

    return false;
  }


  res = FT_ResetDevice(ftdi);
  res |= FT_SetUSBParameters(ftdi, 65536, 65536);	//Set USB request transfer size
  res |= FT_SetFlowControl(ftdi,FT_FLOW_RTS_CTS,0,0);
  res |= FT_SetChars(ftdi, false, 0, false, 0);	 //Disable event and error characters
  res |= FT_SetBitMode(ftdi, 0xff, 0x40);
  res |= FT_Purge(ftdi, FT_PURGE_RX | FT_PURGE_TX);
  res |= FT_SetLatencyTimer(ftdi, 64);
  res |= FT_SetTimeouts(ftdi, 1000, 1000);
  if (res != FT_OK) {
    qDebug() << "Error initializing device";

    return false;
  }



    hsSettings = GetHeadstageSettings();

    cnSettings = GetControllerSettings();



    bool headstageDetected = false;
    bool ECUReported = false;

    if (cnSettings.valid) {
        if (cnSettings.ECUDetected) {
            ECUReported = true;
            setECUConnected(true);
        }
    }
    if (hsSettings.valid) {
       headstageDetected = true;
    }




    if (headstageDetected) {


        unsigned char TxBuffer[256]; // Contains data to write to device


        // Send "start data capture" command
        TxBuffer[0] = 0x62; // stop
        TxBuffer[1] = 0x63; // load channel configuration

        //TxBuffer[2] = 0x00; // only card 0 enabled = 32 channels

        TxBuffer[2] = 0x01; // only card 0 enabled = 32 channels

        //TxBuffer[3] = 0x61; // 0x61 = start data capture
        TxBuffer[3] = startCommandValue;


        // send start capture command

        QByteArray ar((char*)TxBuffer, 4);

      //  emit ft_write_signal(ar);
      //  DWORD BytesWritten;
        res = FT_WRITE_wrapper(ar);
        if (res != FT_OK) {
          qDebug() << "Error writing";
          return false;
        }


        DWORD BytesReceived;
        uint32_t numBytesRecieved = 0;
        QVector<char> buffer;

        buffer.resize(10000);



        FT_Purge(ftdi, FT_PURGE_RX);

         while (numBytesRecieved < 200000) {

                //Read in data from FTDI chip.  This function does not return until PACKET_SIZE
                //number of bytes have been read into the buffer.
                res = FT_Read(ftdi,buffer.data(),1000,&BytesReceived);
                if (BytesReceived != (DWORD) 1000) {
                    //if (aquiring) {
                    emit newDiagnosticMessage("Error: no data coming from hardware.");
                    failed = true;
                    break;
                }

                data->append(buffer.constData(),1000);
                numBytesRecieved += 1000;
         }


         TxBuffer[0] = 0x62; // 0x62 = stop data capture
         QByteArray sar((char*)TxBuffer, 1);

         res = FT_WRITE_wrapper(sar);

         if (res != FT_OK) {
           qDebug() << "Error stopping";
         }

    }

    CloseInterface();

    if (failed) {
        return false;
    } else {
        return true;
    }
    */

}

int USB3Interface::FT_WRITE_wrapper(QByteArray &data){


    DWORD BytesWritten;

#ifdef WIN32
    res = FT_WritePipe(ftdi3, 0x02, (uchar*)data.data(), data.size(), &BytesWritten, NULL);
#else
    res = FT_WritePipeEx(ftdi3,0 , (uchar*)data.data(), data.size(), &BytesWritten, 1000);
#endif
    //qDebug() << "Wrote" << BytesWritten << "to hardware";

    return res;



    /*if (usbDataProcessor == nullptr) {
        DWORD BytesWritten;
        //FT_STATUS r = FT_Write(ftdi3, data.data(), data.size(), &BytesWritten);
        //return r;
    } else {

        QEventLoop loop;
        //connect(usbDataProcessor, &USB3Runtime::ft_write_status_return, &loop, &QEventLoop::quit);

        if(waittime && timer.isValid() && timer.elapsed() < waittime){
            QThread::msleep(waittime - timer.elapsed());
        }
        timer.restart();

        emit ft_write_signal(data);
        //Loop waits for the signal ft_write_status_return from runtime thread
        loop.exec();
        //return retval;
    }*/

}

/*void USB3Interface::ft_write_slot(FT_STATUS status, DWORD bytes){
    retval = status;
    byteswritten = bytes;
}*/


void USB3Interface::InitInterface() {

   
  
    bool noDevice = true;
    bool connected = false;


    /*DWORD numDevs = 0;

    res = FT_CreateDeviceInfoList(&numDevs);
    FT_DEVICE_LIST_INFO_NODE *devInfo = (FT_DEVICE_LIST_INFO_NODE*)malloc(sizeof(FT_DEVICE_LIST_INFO_NODE)* numDevs);
    if (!FT_FAILED(res && numDevs > 0))
    {


        res = FT_GetDeviceInfoList(devInfo, &numDevs);
        if (!FT_FAILED(res)) {
            printf("List of Connected Devices:\n\n");
            for (DWORD i = 0; i < numDevs; i++)
            {
                printf("Device[%d]\n", i);
                printf("\tFlags: 0x%x %s | Type: %d | ID: 0x%08X | ftHandle=0x%x\n",
                       devInfo[i].Flags,
                       devInfo[i].Flags & FT_FLAGS_SUPERSPEED ? "[USB 3]" :
                                                                devInfo[i].Flags & FT_FLAGS_HISPEED ? "[USB 2]" :
                                                                                                      devInfo[i].Flags & FT_FLAGS_OPENED ? "[OPENED]" : "",
                       devInfo[i].Type,
                       devInfo[i].ID,
                       devInfo[i].ftHandle);
                printf("\tSerialNumber=%s\n", devInfo[i].SerialNumber);
                printf("\tDescription=%s\n", devInfo[i].Description);


            }
        }
        if (numDevs == 0) {
            noDevice = true;
        }
    } else {
        noDevice = true;
    }*/




        //res = FT_Create(devInfo[0].SerialNumber, FT_OPEN_BY_SERIAL_NUMBER, &ftdi3);

#ifdef linux
    FT_HANDLE handle = NULL;
    DWORD dwType = FT_DEVICE_UNKNOWN;

    FT_Create(0, FT_OPEN_BY_INDEX, &handle);
    if (!handle) {
        qDebug() << "Error with null handle.";
        return;
    }

    FT_GetDeviceInfoDetail(0, NULL, &dwType, NULL, NULL, NULL, NULL, NULL);

    /*union CHIP_CONFIGURATION {
        FT_60XCONFIGURATION ft600;
    } old_cfg, new_cfg;*/

    union CHIP_CONFIGURATION {
        FT_60XCONFIGURATION ft600;
    } old_cfg, new_cfg;

    if (FT_OK != FT_GetChipConfiguration(handle, &new_cfg)) {
        printf("Failed to get chip conf\r\n");
        return;
    }
    //memcpy(&new_cfg, &old_cfg, sizeof(union CHIP_CONFIGURATION));

    /*if (dwType == FT_DEVICE_600 || dwType == FT_DEVICE_601) {

        new_cfg.ft600.FIFOClock = CONFIGURATION_FIFO_CLK_100;
        new_cfg.ft600.FIFOMode = CONFIGURATION_FIFO_MODE_600;
        new_cfg.ft600.ChannelConfig = CONFIGURATION_CHANNEL_CONFIG_1;

        //set_ft600_optional_features(&new_cfg.ft600.OptionalFeatureSupport);
        //set_ft600_channels(&cfg->ChannelConfig, is_600_mode);


    } else {
        printf("Failed to set configuration-- no 600 device found.\r\n");
        return;
    }*/

    new_cfg.ft600.FIFOClock = CONFIGURATION_FIFO_CLK_100;
    new_cfg.ft600.FIFOMode = CONFIGURATION_FIFO_MODE_245;
    new_cfg.ft600.ChannelConfig = CONFIGURATION_CHANNEL_CONFIG_1;


        if (FT_OK != FT_SetChipConfiguration(handle, &new_cfg)) {
            printf("Failed to set chip conf\r\n");
            return;
        } else {
            //printf("Configuration changed\r\n");
            QThread::msleep(1000);
            //this_thread::sleep_for(chrono::seconds(1));
            //get_device_lists(6000);
        }

    FT_Close(handle);

    FT_TRANSFER_CONF conf;
    memset(&conf, 0, sizeof(FT_TRANSFER_CONF));
    conf.wStructSize = sizeof(FT_TRANSFER_CONF);
    conf.pipe[FT_PIPE_DIR_IN].fNonThreadSafeTransfer = true;
    conf.pipe[FT_PIPE_DIR_OUT].fNonThreadSafeTransfer = true;
    //conf.pipe[FT_PIPE_DIR_IN].dwURBBufferSize =

    for (DWORD i = 0; i < 4; i++)
        FT_SetTransferParams(&conf, i);
#endif

    if (isSourceAvailable()) {

        res = FT_Create((void*)desc_hardware,FT_OPEN_BY_DESCRIPTION, &ftdi3);

        //ftStatus = FT_Create(0,FT_OPEN_BY_INDEX,&ftHandle);

        //GUID DeviceGUID[2] = {0xd1e8fe6a, 0xab75, 0x4d9e, 0x97, 0xd2, 0x6, 0xfa, 0x22, 0xc7, 0x73, 0x6c};
        //ftStatus = FT_Create(&DeviceGUID[0], FT_OPEN_BY_GUID, &ftHandle);

        FT_DEVICE_DESCRIPTOR DeviceDescriptor;

        if (res != FT_OK) {
            printf("Can't open FT601 device! \n");
        } else {
            connected = true;

            /*res = FT_GetDeviceDescriptor(ftdi3, &DeviceDescriptor);
            if (FT_FAILED(res)){
                printf("Error! FT_GetDeviceDescriptor() failed, error code %d\n", (int)res);
            } else {
                printf("Opened FT601 device\n\n");
                connected = true;

            }*/
        }
    } else {

        printf("USB3: No devices found.\n\n");
    }

    if (connected) {
        //initialization went ok, so start the runtime thread
        usbDataProcessor = new USB3Runtime(NULL);
        usbDataProcessor->acquiring = false;
        usbDataProcessor->ftdi3 = ftdi3; //give access to device handle
        setUpThread(usbDataProcessor);
        //connect(this, &USB3Interface::ft_write_signal, usbDataProcessor, &USB3Runtime::ft_write_slot);
        //connect(usbDataProcessor, &USB3Runtime::ft_write_status_return, this, &USB3Interface::ft_write_slot,Qt::UniqueConnection);



        //get headstage and controller settings if this is the first init (and not a reinit)
        //We need to do this here so that the settings can be logged onto the headers of recording files.
        if (!reinitMode) {
            HeadstageSettings settingsRead = GetHeadstageSettings();
            if (settingsRead.valid) {
                emit headstageSettingsReturned(settingsRead);
            }

            HardwareControllerSettings hardwareRead = GetControllerSettings();
            if (hardwareRead.valid) {
                emit controllerSettingsReturned(hardwareRead);
            }
        }
        state = SOURCE_STATE_INITIALIZED;

        emit stateChanged(SOURCE_STATE_INITIALIZED);
    } else {
        //Connection not made
        connectErrorThrown = true;
        emit stateChanged(SOURCE_STATE_CONNECTERROR);

    }


}

void USB3Interface::StartSimulation(void) {
  static int runtimeStarted  = 0;

  qDebug() << "Sending simulation command. Aux bytes:" << (hardwareConf->headerSize*2)-2 << "Num channel banks:" << ((hardwareConf->NCHAN/32)-1);


  QByteArray datagram;
  datagram.resize(3);
  QDataStream msg(&datagram, QIODevice::ReadWrite);
  msg.setByteOrder(QDataStream::LittleEndian);
  msg << (quint8)command_startSimulation;
  msg << (quint8)((hardwareConf->headerSize*2)-2);  //Number of auxilliary bytes to add to the packet
  //msg << (quint8)(0);  //Number of auxilliary bytes to add to the packet

  msg << (quint8)((hardwareConf->NCHAN/32)-1);


  rawData.writeIdx = 0; // location where we're currently writing



  usbDataProcessor->acquiring = true;

  // send start capture command

//  emit ft_write_signal(datagram);
//  DWORD BytesWritten;
//  res = FT_Write(ftdi, datagram.data(), 3, &BytesWritten);

  //res = FT_WRITE_wrapper(datagram);
  if (res != FT_OK) {
    qDebug() << "Error writing";
    return;
  }

  if (runtimeStarted == 0) {
    emit startRuntime();
    qDebug() << "Told runtime to start";
  }

  emit acquisitionStarted();
  state = SOURCE_STATE_RUNNING;
  emit stateChanged(SOURCE_STATE_RUNNING);
}

void USB3Interface::StartAcquisition(void) {

  if (!threadCreated) {
      restartThread();
  }

  static int runtimeStarted  = 0;
  unsigned char TxBuffer[256]; // Contains data to write to device

  rawData.writeIdx = 0; // location where we're currently writing

  int channels = hardwareConf->NCHAN - 1;
  int iosize = (hardwareConf->headerSize*2)-2;

  qDebug() << "IOSize:" << iosize;

  TxBuffer[0] = 0x6b; // sim
  TxBuffer[1] = iosize; // aux count
  TxBuffer[2] = channels>>5; // chip count
  TxBuffer[3] = 0x00;




  // Send "start data capture" command
  //TxBuffer[0] = 0x01; // write to start/stop register
  //TxBuffer[1] = 0xff; // start data capture

  //TxBuffer[0] = 0x62; // stop
  //TxBuffer[1] = startCommandValue;

  usbDataProcessor->resetStreamingStats();
  usbDataProcessor->acquiring = true;

  // send start capture command

  /*QByteArray ar((char*)TxBuffer, 2);
  res = FT_WRITE_wrapper(ar);
  if (res != FT_OK) {
    qDebug() << "Error sending start acquisition command to hardware.";
    return;
  }*/

  QByteArray sim_ar((char*)TxBuffer, 4);
  res = FT_WRITE_wrapper(sim_ar);
  if (res != FT_OK) {
      qDebug() << "Error sending simulation command to hardware.";
      return;
  }



  if (runtimeStarted == 0) {
    emit startRuntime();
    qDebug() << "Told runtime to start";
  }

  emit acquisitionStarted();
  state = SOURCE_STATE_RUNNING;
  emit stateChanged(SOURCE_STATE_RUNNING);
}


void USB3Interface::StopAcquisition(void) {

  if (!threadCreated) return;

  usbDataProcessor->printStreamingStats();
  usbDataProcessor->quitNow = true;
  usbDataProcessor->acquiring = false;


  emit acquisitionStopped();


  //Need to clean this up, but we need to wait until the read thread is finished before we tell the hardware to stop sending data.
  QThread::msleep(500);
  /*while (isThreadRunning()) {
    QThread::msleep(1);
  }*/




  unsigned char TxBuffer[256]; // Contains data to write to device

  TxBuffer[0] = 0x62; // 0x62 = stop data capture
  TxBuffer[1] = 0x00;
  TxBuffer[2] = 0x00;
  TxBuffer[3] = 0x00;
  QByteArray ar((char*)TxBuffer, 4);
//  emit ft_write_signal(ar);
  // send stop capture command
//  DWORD BytesWritten;
//  res = FT_Write(ftdi, TxBuffer, 1, &BytesWritten);

  res = FT_WRITE_wrapper(ar);

  if (res != FT_OK) {
    qDebug() << "Error stopping";
  } else {
    qDebug() << "Stopped acquisition. Timestamp:" << currentTimeStamp;
  }


  //Reset connection to device
  res = FT_Close(ftdi3);
  if (res != FT_OK) {
      printf("Error closing FT601 device! \n");
  }
  res = FT_Create((void*)desc_hardware,FT_OPEN_BY_DESCRIPTION, &ftdi3);
  if (res != FT_OK) {
     printf("Error resetting FT601 device! \n");
  }


  state = SOURCE_STATE_INITIALIZED;
  emit stateChanged(SOURCE_STATE_INITIALIZED);



}

void USB3Interface::restartThread() {
    qDebug() << "Restarting USB source thread.";
    //start the runtime thread
    usbDataProcessor = new USB3Runtime(NULL);
    usbDataProcessor->acquiring = false;
    usbDataProcessor->ftdi3 = ftdi3; //give access to device handle
    setUpThread(usbDataProcessor);
    //connect(this, &USB3Interface::ft_write_signal, usbDataProcessor, &USB3Runtime::ft_write_slot);
    //connect(usbDataProcessor, &USB3Runtime::ft_write_status_return, this, &USB3Interface::ft_write_slot,Qt::UniqueConnection);
    state = SOURCE_STATE_INITIALIZED;
    emit stateChanged(SOURCE_STATE_INITIALIZED);
}

void USB3Interface::SendImpendaceMeasureCommand(HardwareImpedanceMeasureCommand s) {
    /*
    //qDebug() << "USB interface got impedance measure request.";
    if (usbDataProcessor == nullptr || !usbDataProcessor->acquiring) {
        //QVector<unsigned char> buffer;
        QByteArray buffer;

        DWORD BytesReceived;
        DWORD RxBytesAvailable;
        DWORD TxBytesAvailable;
        DWORD Event;

        //First clear anything in the receive queue
        res = FT_GetStatus(ftdi, &RxBytesAvailable, &TxBytesAvailable, &Event);
        if (res == FT_OK && RxBytesAvailable > 0) {
            buffer.resize(RxBytesAvailable);
            res = FT_Read(ftdi,buffer.data(),RxBytesAvailable,&BytesReceived);
        }

        char inputarray[1000];
        if (s.numberOfCycles > 16) {
            return; //Not allowed
        }

        QByteArray datagram;
        datagram.resize(s.WRITESETTINGSBYTES);
        s.writeSettings(datagram.data());


        res = FT_WRITE_wrapper(datagram);
        if (res != FT_OK) {
            qDebug() << "Error sending impedance measure command.";
        } else {
            //qDebug() << "Requested impedance measure.";
        }

        bool messageAvail = false;
        for (int tryNum=0; tryNum < 20;tryNum++) {
            res = FT_GetStatus(ftdi, &RxBytesAvailable, &TxBytesAvailable, &Event);
            if (res == FT_OK && RxBytesAvailable >= 3) {
                messageAvail = true;
                //qDebug() << "Got response";
                break;
            }
            QThread::msleep(100);

        }

        if (messageAvail) {
            buffer.resize(RxBytesAvailable);
            res = FT_Read(ftdi,inputarray,RxBytesAvailable,&BytesReceived);

            int expectedBytes = 3;
            uint32_t zCheck_diff = 0;
            if ((BytesReceived >= expectedBytes) && inputarray[0] == (char)received_impedanceValues) {
                zCheck_diff = (uint32_t)((reinterpret_cast<unsigned char&>(inputarray[2])))*256;
                zCheck_diff = zCheck_diff + ((reinterpret_cast<unsigned char&>(inputarray[1])));
                //zCheck_diff = (zCheck_diff<<8) + inputarray[1];
                //printf("zCheck peak-to-peak amplitude: %u\n", zCheck_diff);
            }  else {
                qDebug() << "Error reading impedance value.";

                return;
            }



            double meanPeakVoltage_nV = 0.0;
            double impedanceCalc_Ohm = 0.0;
            if (spikeConf->deviceType == "intan") {
                meanPeakVoltage_nV = (zCheck_diff*195.0)/2;
                double peakCurrent_nA;

                if (s.amplitude == 0) {
                    //0.1 pF
                    peakCurrent_nA = (((double)(s.frequency))/1000.0)*.38; //nA

                } else if (s.amplitude == 1) {
                    //1.0 pF
                    peakCurrent_nA = (((double)(s.frequency))/1000.0)*3.8; //nA

                } else if (s.amplitude == 2) {
                    //10 pF
                    peakCurrent_nA = (((double)(s.frequency))/1000.0)*38; //nA

                } else {
                    return;
                }

                impedanceCalc_Ohm = meanPeakVoltage_nV/peakCurrent_nA;
            }



            emit impedanceValueReturned(s.channel,(int)impedanceCalc_Ohm);


        } else {
            qDebug() << "No return message found after request.";
        }

    }
    */

}

quint64 USB3Interface::getTotalUnresponsiveHeadstagePackets() {
    quint64 rVal = 0;
    if (usbDataProcessor != NULL) {
        rVal = usbDataProcessor->totalUnresponsiveHeadstagePackets;
    }
    return rVal;
}

quint64 USB3Interface::getTotalDroppedPacketEvents() {

    quint64 rVal = 0;
    if (usbDataProcessor != NULL) {
        rVal = usbDataProcessor->totalDroppedPacketEvents;
    }
    return rVal;

}

void USB3Interface::SendControllerSettings(HardwareControllerSettings s) {
    //SET CONTROLLER SETTINGS (MESSAGE TO HARDWARE)-- 9 BYTES
    //<0x84><uint8 rfChannel><uint8 samplingRateKhz><uint8 s3><uint8 s4><uint8 s5><uint8 s6><uint8 s7><uint8 s8>

    /*
    QByteArray datagram;
    datagram.resize(9);
    s.writeSettings(datagram.data());

    DWORD BytesWritten;
    res = FT_Write(ftdi, datagram.data(), 9, &BytesWritten);
    if (res != FT_OK) {
      qDebug() << "Error sending controller settings.";
      QMessageBox messageBox;
      messageBox.setWindowFlags(Qt::WindowStaysOnTopHint);
      messageBox.critical(0,"Error","Settings could not be sent to hardware.");
      messageBox.setFixedSize(500,200);
      return;
    } else {
      qDebug() << "Controller settings sent.";
      //GetHeadstageSettings();
    }
    */
}

HardwareControllerSettings USB3Interface::GetControllerSettings() {
    //Ask the hardware for info about the main controller
    HardwareControllerSettings s;
    return s; //for now...

    /*
    if (usbDataProcessor == nullptr || !usbDataProcessor->acquiring) {
        //QVector<unsigned char> buffer;
        QByteArray buffer;

        DWORD BytesReceived;
        DWORD RxBytesAvailable;
        DWORD TxBytesAvailable;
        DWORD Event;

        //First clear anything in the receive queue
        res = FT_GetStatus(ftdi, &RxBytesAvailable, &TxBytesAvailable, &Event);
        if (res == FT_OK && RxBytesAvailable > 0) {
            buffer.resize(RxBytesAvailable);
            res = FT_Read(ftdi,buffer.data(),RxBytesAvailable,&BytesReceived);
        }

        QByteArray datagram;
        datagram.resize(1);
        datagram[0] = (quint8)command_getControllerSettings;

//        emit ft_write_signal(datagram);
//        DWORD BytesWritten;
//        res = FT_Write(ftdi, datagram.data(), 1, &BytesWritten);
        res = FT_WRITE_wrapper(datagram);
        if (res != FT_OK) {
            qDebug() << "Error requesting controller settings.";
        } else {
            qDebug() << "Requested controller settings.";
        }

        bool messageAvail = false;
        for (int tryNum=0; tryNum < 5;tryNum++) {
            res = FT_GetStatus(ftdi, &RxBytesAvailable, &TxBytesAvailable, &Event);
            if (res == FT_OK && RxBytesAvailable >= 17) {
                messageAvail = true;
                qDebug() << "got controller settings";
                break;
            }
            QThread::msleep(500);

        }

        if (messageAvail) {
            buffer.resize(RxBytesAvailable);
            res = FT_Read(ftdi,buffer.data(),RxBytesAvailable,&BytesReceived);
            if (buffer.at(0) == (char)received_controllerSettings) {
                qDebug() << "Controller settings returned from hardware.";
                s = AbstractTrodesSource::convertHardwareControllerSettings(buffer);
                emit controllerSettingsReturned(s);
                lastControllerSettings = s;


            } else {
                qDebug() << "Error reading controller settings message from hardware. Code: " << (quint8)buffer.at(0) << "bytes: " << RxBytesAvailable;
            }

        } else {
            qDebug() << "No return message found after request.";
        }

    }
    else if(lastControllerSettings.valid){
        //if is acquiring and previously stored hardware settings is valid
        s = lastControllerSettings;
    }

    return s;
    */
}

HeadstageSettings USB3Interface::GetHeadstageSettings() {
    //Ask the hardware for info about any connected headstage
    HeadstageSettings s;
    return s; //for now...


    /*
    if (usbDataProcessor == nullptr || !usbDataProcessor->acquiring) {
        //QVector<unsigned char> buffer;
        QByteArray buffer;

        DWORD BytesReceived;
        DWORD RxBytesAvailable;
        DWORD TxBytesAvailable;
        DWORD Event;

        //First clear anything in the receive queue
        res = FT_GetStatus(ftdi, &RxBytesAvailable, &TxBytesAvailable, &Event);
        if (res == FT_OK && RxBytesAvailable > 0) {
            buffer.resize(RxBytesAvailable);
            res = FT_Read(ftdi,buffer.data(),RxBytesAvailable,&BytesReceived);
        }



        QByteArray datagram;
        datagram.resize(1);
        datagram[0] = (quint8)command_getHeadstageSettings;

//        emit ft_write_signal(datagram);
//        DWORD BytesWritten;
//        res = FT_Write(ftdi, datagram.data(), 1, &BytesWritten);
        res = FT_WRITE_wrapper(datagram);
        if (res != FT_OK) {
            qDebug() << "Error requesting headstage settings.";
        } else {
            qDebug() << "Requested headstage settings.";
        }

        bool messageAvail = false;
        for (int tryNum=0; tryNum < 5;tryNum++) {
            res = FT_GetStatus(ftdi, &RxBytesAvailable, &TxBytesAvailable, &Event);
            if (res == FT_OK && RxBytesAvailable >= 27) {
                messageAvail = true;
                break;
            }
            QThread::msleep(500);

        }

        if (messageAvail) {
            buffer.resize(RxBytesAvailable);
            res = FT_Read(ftdi,buffer.data(),RxBytesAvailable,&BytesReceived);
            if (buffer.at(0) == (char)received_headstageSettings) {
                qDebug() << "Headstage settings returned from hardware.";
                s = AbstractTrodesSource::convertHeadstageSettings(buffer);
                emit headstageSettingsReturned(s);
                lastHeadstageSettings = s;
            } else {
                qDebug() << "Error reading headstage settings message from hardware. Code: " << (quint8)buffer.at(0);
            }

        } else {
            qDebug() << "No return message found after request.";
        }

    }
    else if(lastHeadstageSettings.valid){
        //if is acquiring and previously stored settings is valid
        s = lastHeadstageSettings;
    }

    return s;

    */
}

bool USB3Interface::SendNeuroPixelsSettings(NeuroPixelsSettings s) {

    qDebug() << "Sending np settings";
    QByteArray datagram;
    datagram.resize(s.WRITESETTINGSBYTES);

    for (int p=0;p<s.numProbes();p++) {

        s.writeSettings(datagram.data(),p);

        res = FT_WRITE_wrapper(datagram);
        if (res != FT_OK) {
            qDebug() << "Error sending headstage settings.";
            QMessageBox messageBox;
            messageBox.setWindowFlags(Qt::WindowStaysOnTopHint);
            messageBox.critical(0,"Error","Settings could not be sent to hardware.");
            messageBox.setFixedSize(500,200);
            return false;
        } else {
            qDebug() << "NeuroPixels settings sent for probe" << p;
            QThread::msleep(1500); //We need to give the headstage enough time to program the probe.
            //GetHeadstageSettings();
        }
    }

    return true;

    //SendSaveHeadstageSettings();


//    emit ft_write_signal(datagram);



    return true;
}

void USB3Interface::SendSaveHeadstageSettings() {
    //We send the command for the headstage to save settings to flash memory
    QByteArray datagram2;
    datagram2.resize(1);
    QDataStream msg2(&datagram2, QIODevice::ReadWrite);
    msg2.setByteOrder(QDataStream::LittleEndian);
    msg2 << (quint8)command_writeHSSettingsToFlash;
//    res = FT_Write(ftdi, datagram2.data(), 1, &BytesWritten);
    res = FT_WRITE_wrapper(datagram2);
    if (res != FT_OK) {
      qDebug() << "Error sending command to save settings on headstage.";
      QMessageBox messageBox;
      messageBox.setWindowFlags(Qt::WindowStaysOnTopHint);
      messageBox.critical(0,"Error","Settings could not be saved to hardware.");
      messageBox.setFixedSize(500,200);
    } else {
      qDebug() << "Headstage save settings command sent.";
    }
}

void USB3Interface::SendHeadstageSettings(HeadstageSettings s) {


    //SET HEADSTAGE SETTINGS (MESSAGE TO HARDWARE)-- 16 BYTES
    //<0x82><uint8 autoSettleOn><uint16 autosettle number of channels><uint16 autosettle thresh><uint8 smartRefOn><uint8 sensors on><uint8 s1><uint8 s2><uint8 s3><uint8 s4><uint8 s5><uint8 s6><uint8 s7><uint8 s8>
    //sensors on is an 8-bit code, least significant bit last <unused unused unused unused unused mag gyro accel>
    QByteArray datagram;
    datagram.resize(s.WRITESETTINGSBYTES);

    s.writeSettings(datagram.data());
//    emit ft_write_signal(datagram);
//    DWORD BytesWritten;
//    res = FT_Write(ftdi, datagram.data(), 16, &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending headstage settings.";
      QMessageBox messageBox;
      messageBox.setWindowFlags(Qt::WindowStaysOnTopHint);
      messageBox.critical(0,"Error","Settings could not be sent to hardware.");
      messageBox.setFixedSize(500,200);
      return;
    } else {
      qDebug() << "Headstage settings sent.";
      //GetHeadstageSettings();
    }


//    emit ft_write_signal(datagram);

    //Next we send the command for the headstage to save settings to flash memory
    QByteArray datagram2;
    datagram2.resize(1);
    QDataStream msg2(&datagram2, QIODevice::ReadWrite);
    msg2.setByteOrder(QDataStream::LittleEndian);
    msg2 << (quint8)command_writeHSSettingsToFlash;
//    res = FT_Write(ftdi, datagram2.data(), 1, &BytesWritten);
    res = FT_WRITE_wrapper(datagram2);
    if (res != FT_OK) {
      qDebug() << "Error sending command to save settings on headstage.";
      QMessageBox messageBox;
      messageBox.setWindowFlags(Qt::WindowStaysOnTopHint);
      messageBox.critical(0,"Error","Settings could not be saved to hardware.");
      messageBox.setFixedSize(500,200);
    } else {
      qDebug() << "Headstage save settings command sent.";
    }

    QThread::msleep(5000);
    //emit headstageSettingsReturned(s);
    HeadstageSettings newRead = GetHeadstageSettings();

    if (newRead == s) {
        qDebug() << "Verified new headstage settings";
    } else {


        qDebug() << "Error while verifying new headstage settings.";
        QMessageBox messageBox;
        messageBox.setWindowFlags(Qt::WindowStaysOnTopHint);
        messageBox.critical(0,"Error","Settings change could not be verified from headstage. Please make sure headstage is properly connected.");
        messageBox.setFixedSize(500,200);
    }

}

void USB3Interface::SetStimulationParams(StimulationCommand s) {
    //SEND STIMULATION PATTERN DEFINITION (MESSAGE TO HARDWARE)-- 24 BYTES

    QByteArray datagram;

    //Serialize the information in the object into a byte array containing the critical state machine information
    if(!convertToStateMachineCommand(s, &datagram)){
        qDebug() << "Stimulation parameters are not valid!";
        return;
    }

//    emit ft_write_signal(datagram);
    //Send the packet
//    DWORD BytesWritten;
//    res = FT_WRITE_wrapper(ftdi, datagram.data(), datagram.size(), &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending stimulation parameters." << res;
    }
}

bool USB3Interface::SendGlobalStimulationSettings(GlobalStimulationSettings s) {
    //SEND GLOBAL STIMULATION SETTINGS (MESSAGE TO HARDWARE)

    QByteArray datagram;

    //Serialize the information in the object into a byte array containing the critical state machine information
    if(!convertToStateMachineCommand(s, &datagram)){
        qDebug() << "Stimulation parameters are not valid!";
        return false;
    }

    //Send the packet
//    DWORD BytesWritten;
//    res = FT_WRITE_wrapper(ftdi, datagram.data(), datagram.size(), &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending stimulation parameters." << res;
    }
//    emit ft_write_signal(datagram);
    return true;
}

void USB3Interface::SendGlobalStimulationAction(GlobalStimulationCommand s) {
    //SEND GLOBAL STIMULATION ACTION (MESSAGE TO HARDWARE)

    QByteArray datagram;

    //Serialize the information in the object into a byte array containing the critical state machine information
    if(!convertToStateMachineCommand(s, &datagram)){
        qDebug() << "Stimulation parameters are not valid!";
        return;
    }

//    //Send the packet
//    DWORD BytesWritten;
//    res = FT_WRITE_wrapper(ftdi, datagram.data(), datagram.size(), &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending stimulation parameters." << res;
    }
//    emit ft_write_signal(datagram);
}


void USB3Interface::ClearStimulationParams(uint16_t slot) {
    //CLEAR STIMULATION PATTERN DEFINITION IN ONE MEMORY SLOT-- 3 BYTES

    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);

    //Fill the packet
    msg << (quint8)command_clearStimulateParams; //0x6F
    msg << (uint8_t)slot;
    //Calculate a checksum
    uint8_t checksum = 0;
    for (int i = 0;i < datagram.length()-1; i++) {
        checksum += datagram.at(i);
    }
    msg << checksum;

//    //Send the packet
//    DWORD BytesWritten;
//    res = FT_WRITE_wrapper(ftdi, datagram.data(), datagram.size(), &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending stimulation clear command." << res;
    }
//    emit ft_write_signal(datagram);

}

void USB3Interface::SendStimulationStartSlot(uint16_t slot) {
    //START STIMULATION PATTERN DEFINED IN ONE MEMORY SLOT-- 3 BYTES

    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);

    //Fill the packet
    msg << (quint8)command_stimulateStart;
    msg << (uint8_t)slot;
    //Calculate a checksum
    uint8_t checksum = 0;
    for (int i = 0;i < datagram.length()-1; i++) {
        checksum += datagram.at(i);
    }
    msg << checksum;

//    //Send the packet
//    DWORD BytesWritten;
//    res = FT_WRITE_wrapper(ftdi, datagram.data(), datagram.size(), &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending stimulation start command." << res;
    } else {
        qDebug() << "Start stimulation pattern in slot" << slot;
    }
//    emit ft_write_signal(datagram);
}

void USB3Interface::SendStimulationStartGroup(uint16_t group) {
    //START ALL STIMULATION PATTERNS DEFINED-- 1 BYTE

    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);

    //Fill the packet
    msg << (quint8)command_stimulateStartGroup;
    msg << (uint8_t)group;
    //Calculate a checksum
    uint8_t checksum = 0;
    for (int i = 0;i < datagram.length()-1; i++) {
        checksum += datagram.at(i);
    }
    msg << checksum;

//    //Send the packet
//    DWORD BytesWritten;
//    res = FT_WRITE_wrapper(ftdi, datagram.data(), datagram.size(), &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending stimulation start all command.";
    }
//    emit ft_write_signal(datagram);
}

void USB3Interface::SendStimulationStopSlot(uint16_t slot) {
    //STOP RUNNING STIMULATION PATTERN DEFINED IN ONE MEMORY SLOT-- 3 BYTES

    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);

    //Fill the packet
    msg << (quint8)command_stimulateStop; //0x72
    msg << (uint8_t)slot;
    //Calculate a checksum
    uint8_t checksum = 0;
    for (int i = 0;i < datagram.length()-1; i++) {
        checksum += datagram.at(i);
    }
    msg << checksum;

//    //Send the packet
//    DWORD BytesWritten;
//    res = FT_WRITE_wrapper(ftdi, datagram.data(), datagram.size(), &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending stimulation stop command.";
    }
//    emit ft_write_signal(datagram);
}

void USB3Interface::SendStimulationStopGroup(uint16_t group) {
    //STOP ALL RUNNING STIMULATION PATTERNS FOR SELECTED GROUP

    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);

    //Fill the packet
    msg << (quint8)command_stimulateStopGroup;
    msg << (uint8_t)group;
    //Calculate a checksum
    uint8_t checksum = 0;
    for (int i = 0;i < datagram.length()-1; i++) {
        checksum += datagram.at(i);
    }
    msg << checksum;

//    //Send the packet
//    DWORD BytesWritten;
//    res = FT_WRITE_wrapper(ftdi, datagram.data(), datagram.size(), &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending stimulation stop all command.";
    }
//    emit ft_write_signal(datagram);
}



void USB3Interface::SendSettleCommand() {

    unsigned char TxBuffer[256]; // Contains data to write to device

    TxBuffer[0] = 0x66; // 0x66 = settle command
    //TxBuffer[0] = 0x65; // 0x65 = init SD card for writing

    QByteArray ar((char*)TxBuffer, 1);
//    emit ft_write_signal(ar);
    // send stop capture command
//    DWORD BytesWritten;
//    res = FT_Write(ftdi, TxBuffer, 1, &BytesWritten);
    res = FT_WRITE_wrapper(ar);
    if (res != FT_OK) {
      qDebug() << "Error sending settle command.";
    } else {
      qDebug() << "Settle command sent.";
    }

}

void USB3Interface::SendSettleChannel(int byteInPacket, quint8 bit, int delay, quint8 triggerState) {
    if ((state == SOURCE_STATE_RUNNING)||(state == SOURCE_STATE_INITIALIZED)) {

        QString tStateString;
        if (triggerState == 0) {
            tStateString = "Off";
        } else if (triggerState == 1) {
            tStateString = "Rising edge";
        } else if (triggerState == 2) {
            tStateString = "Falling edge";
        } else if (triggerState == 3) {
            tStateString = "Both upward and downward edge";
        }


        QByteArray datagram;
        datagram.resize(7);
        QDataStream msg(&datagram, QIODevice::ReadWrite);
        msg.setByteOrder(QDataStream::LittleEndian);
        msg << (quint8)0x6A; // 0x6A = set settle channel
        msg << (uint16_t)(byteInPacket); //the byte in the packet to use
        msg << bit; //uint8, the bit within the given byte to use (0-7)
        msg << (uint16_t)(delay); //delay for the settle to occur in number of samples
        msg << triggerState; //uint8 (0 for off, 1 for rising edge, 2 for falling edge)

//        emit ft_write_signal(datagram);
//        DWORD BytesWritten;
//        res = FT_Write(ftdi, datagram.data(), 7, &BytesWritten);
        res = FT_WRITE_wrapper(datagram);
        if (res != FT_OK) {
          qDebug() << "Error sending settle control channel.";
        } else {
          qDebug() << "Settle control channel sent."<< byteInPacket << "Byte" << bit << "Bit" << tStateString;
        }
    }

}

void USB3Interface::SendSDCardUnlock() {
    unsigned char TxBuffer[256]; // Contains data to write to device

    TxBuffer[0] = 0x65; // 0x65 = unlock SD card for writing
    QByteArray datagram((char*)TxBuffer, 1);
    // send stop capture command
//    DWORD BytesWritten;
//    res = FT_Write(ftdi, TxBuffer, 1, &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending unlock command.";
    } else {
      qDebug() << "SD card unlock command sent.";
    }
}

void USB3Interface::ConnectToSDCard() {
    unsigned char TxBuffer[256]; // Contains data to write to device

    TxBuffer[0] = 0x67; // 0x67 = ping the MCU for the card
    QByteArray datagram((char*)TxBuffer, 1);
    // send stop capture command
//    DWORD BytesWritten;
//    res = FT_Write(ftdi, TxBuffer, 1, &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    if (res != FT_OK) {
      qDebug() << "Error sending SD card Connect command.";
    } else {
      qDebug() << "SD card connect command sent.";
    }

    emit SDCardStatus(false,0,false,false);
}

void USB3Interface::ReconfigureSDCard(int numChannels) {

    //unsigned char TxBuffer[256]; // Contains data to write to device
    //QByteArray temp(TxBuffer, 256);
    //QDataStream msg(&temp, QIODevice::ReadWrite);


    QByteArray datagram;
    datagram.resize(3);
    QDataStream msg(&datagram, QIODevice::ReadWrite);
    msg.setByteOrder(QDataStream::LittleEndian);
    msg << (uint8_t)0x68; // 0x68 = ping the MCU for the card
    msg << (uint16_t)numChannels;

//    DWORD BytesWritten;
//    res = FT_Write(ftdi, datagram.data(), 3, &BytesWritten);
    res = FT_WRITE_wrapper(datagram);
    //res = FT_Write(ftdi, TxBuffer, 3, &BytesWritten);
    if (res != FT_OK) {
      qDebug() << "Error sending SD card reconfigure command.";
    } else {
      qDebug() << "SD card reconfigure command sent.";
    }
}

void USB3Interface::CloseInterface(void) {

    restartThreadAfterShutdown = false;
    if (state == SOURCE_STATE_RUNNING) {
          StopAcquisition();
    }

    /*res = FT_Close(ftdi);
    if (res != FT_OK) {
      qDebug() << "Error closing";
    }*/

    res = FT_Close(ftdi3);
    if (res != FT_OK) {
        printf("Can't close FT601 device! \n");
    } else {
        // Port closed successfully
        printf("Closed FT601 device\n");
    }

    emit stateChanged(SOURCE_STATE_NOT_CONNECTED);
    lastControllerSettings = HardwareControllerSettings();
    lastHeadstageSettings = HeadstageSettings();

}

int USB3Interface::MeasurePacketLength(HeadstageSettings settings){

    return -1; //for now...

    /*
    if(!usbDataProcessor->acquiring && state == SOURCE_STATE_INITIALIZED && settings.valid){
        //minimum length of packets
        int minlength = settings.numberOfChannels*sizeof(int16_t) + sizeof(uint32_t) + 2;//always at least numchans*2 + timetamp + 2bytes for 0x55

        //start streaming
        unsigned char buf[20] = {0};
        buf[0] = startCommandValue;
        buf[1] = 0xff;
        DWORD bytes;
        FT_STATUS res;
        res = FT_Write(ftdi, buf, 2, &bytes);

        //make sure nothing takes too long
        QElapsedTimer timer;
        timer.start();

        //get 0x55's. First for alignment, rest to measure length
        QVector<int> lengths;
        constexpr int numpackets = 99;
        for(int i = 0; i < numpackets+1; ++i){
            bool foundx55 = false;
            int numbytes = 0;
            while(!foundx55){
                res = FT_Read(ftdi, buf, 1, &bytes);
                if(res == FT_OK){
                    numbytes++;
                    if(numbytes >= minlength && buf[0] == 0x55)
                        foundx55 = true;
                    if(i == 0 && buf[0] == 0x55)
                        foundx55 = true;
                }

                if(timer.elapsed() > 500){
                    //Taking way too long, something is wrong
                    buf[0] = command_stop;
                    FT_Write(ftdi, buf, 1, &bytes);
                    return -1;
                }
            }
            if(i > 0){
                lengths.push_back(numbytes);
            }
        }

        //end streaming
        buf[0] = command_stop;
        res = FT_Write(ftdi, buf, 1, &bytes);
        FT_Purge(ftdi, FT_PURGE_RX);

        if(lengths.size() < numpackets){
            return -1;
        }

        //"vote" for what packet size is by majority (mode)
//        QVector<int> lengthsorig(lengths);
        std::sort(lengths.data(),lengths.data()+lengths.length()-1);
        int num = lengths[0];
        int mode = num;
        int count = 1;
        int countMode = 1;
        for(int i = 0; i < lengths.length(); ++i){
            if(lengths[i] == num){
                ++count;
            }
            else{
                if(count > countMode){
                    countMode = count;
                    mode = num;
                }
                count = 1;
                num = lengths[i];
            }
        }
        if(count > countMode){
            countMode = count;
            mode = num;
        }

        if(countMode > numpackets/2){
            //must still be majority
            return mode;
        }
        else{
            return -1;
        }
    }
    return -1;
    */
}

bool USB3Interface::isSourceAvailable(){

    bool noDevice = false;



    DWORD numDevs = 0;

    res = FT_CreateDeviceInfoList(&numDevs);
    FT_DEVICE_LIST_INFO_NODE *devInfo = (FT_DEVICE_LIST_INFO_NODE*)malloc(sizeof(FT_DEVICE_LIST_INFO_NODE)* numDevs);
    if (!FT_FAILED(res && numDevs > 0))
    {


        res = FT_GetDeviceInfoList(devInfo, &numDevs);
        if (!FT_FAILED(res)) {
            printf("List of Connected Devices:\n\n");
            for (DWORD i = 0; i < numDevs; i++)
            {
                printf("Device[%d]\n", i);
                printf("\tFlags: 0x%x %s | Type: %d | ID: 0x%08X | ftHandle=0x%x\n",
                       devInfo[i].Flags,
                       devInfo[i].Flags & FT_FLAGS_SUPERSPEED ? "[USB 3]" :
                                                              devInfo[i].Flags & FT_FLAGS_HISPEED ? "[USB 2]" :
                                                                                                  devInfo[i].Flags & FT_FLAGS_OPENED ? "[OPENED]" : "",
                       devInfo[i].Type,
                       devInfo[i].ID,
                       devInfo[i].ftHandle);
                printf("\tSerialNumber=%s\n", devInfo[i].SerialNumber);
                printf("\tDescription=%s\n", devInfo[i].Description);

                if(!strcmp(devInfo[i].Description, desc_hardware)){
                    printf("\nTarget device found: %s\n", devInfo[i].Description);
                    return true;
                }


            }
        }

    }

    return false;
}
