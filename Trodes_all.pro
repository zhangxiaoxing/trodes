TEMPLATE = subdirs

CONFIG += c++17

#CONFIG += ordered
# Be explicit about dependencies to speed up multicore builds, per
# https://blog.rburchell.com/2013/10/every-time-you-configordered-kitten-dies.html

SUBDIRS =   Trodes \
            Modules \
            Export
#Modules.depends = Trodes
#Export.depends = Trodes
