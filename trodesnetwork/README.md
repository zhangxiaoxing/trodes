# Trodes Network

The `trodes-network` library is responsible for managing communication
endpoints within a system. It is not responsible for any Trodes business
logic and is completely agnostic to any sort of data type or GUI.

To make the system work, there should be one process running as a message
broker, which coordinates which endpoints are available on the network and
which types they are.

## Use case

This library should enable distributing Trodes-related computations
across different processes or nodes on a network. It should also enable
completely decoupling any GUI applications from business logic associated
with streaming data sources.

It should be general enough so it could be used in other cases like real-time
visualization of neuroscience simulations.

## Message Protocol

Messages are sent over the network using [ZeroMQ](https://zeromq.org/). It is
available on many platforms, so the actual network can be decoupled from a
specific language.

All messages are serialized and deserialized using
[MessagePack](https://msgpack.org/). This is an ideal choice for serialization
because it has support for many languages, and messages can be deserialized
in a client written in any language.

## Building the project

This project is build using CMake. It uses C++17. A `shell.nix` file is
provided to make dependency management easier.