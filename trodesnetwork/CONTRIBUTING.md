# Contributing

## Integrating w/ Trodes

We're currently using a somewhat awkward system right now. To make changes to
the `trodesnetwork` project, you will want to:

1. make your changes
2. compile project using CMake to generate the shared object (`.so`)
3. copy over `libtrodesnetwork.so`
4. copy over any includes to `$REPO/Libraries/Linux/trodesnetwork/include`

There might be better ways to automate this, but it's a hack until we switch
over to using CMake to exclusively build and deploy the project.
