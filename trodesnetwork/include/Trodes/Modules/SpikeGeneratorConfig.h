#pragma once

namespace trodes {

struct SpikeGeneratorConfig {
    double waveFrequency;
    double waveAmplitude;

    int waveRes;
};

}