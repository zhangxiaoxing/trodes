#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor TrodesAnalogData
struct TrodesAnalogData {
    // field impl localTimestamp
    uint32_t localTimestamp;
    // field impl analogData
    std::vector< std::vector< uint8_t > > analogData;
    // field impl systemTimestamp
    int64_t systemTimestamp;
    MSGPACK_DEFINE_MAP(localTimestamp, analogData, systemTimestamp);
};


}  // network
}  // trodes