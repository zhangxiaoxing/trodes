#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor TrodesRequest
struct TrodesRequest {
    // field impl tag
    std::string tag;
    MSGPACK_DEFINE_MAP(tag);
};


}  // network
}  // trodes