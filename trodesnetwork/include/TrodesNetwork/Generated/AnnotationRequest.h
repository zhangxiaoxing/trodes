#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor AnnotationRequest
struct AnnotationRequest {
    // field impl timestamp
    uint32_t timestamp;
    // field impl sender
    std::string sender;
    // field impl event
    std::string event;
    MSGPACK_DEFINE_MAP(timestamp, sender, event);
};


}  // network
}  // trodes