#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor FileStatus
struct FileStatus {
    // field impl message
    std::string message;
    // field impl filename
    std::string filename;
    MSGPACK_DEFINE_MAP(message, filename);
};


}  // network
}  // trodes
