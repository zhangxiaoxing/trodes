#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor AcquisitionCommand
struct AcquisitionCommand {
    // field impl command
    std::string command;
    // field impl timestamp
    uint32_t timestamp;
    MSGPACK_DEFINE_MAP(command, timestamp);
};


}  // network
}  // trodes