#pragma once

#include <string>
#include <msgpack.hpp>

#include <variant>
#include "HWStartStop.h"
#include "HWStimulationCommand.h"
#include "HWClear.h"
#include "HWGlobalStimulationSettings.h"
#include "HWGlobalStimulationCommand.h"
#include "HWTrigger.h"

namespace trodes {
namespace network {

// sum types are still a work in progress
struct HRStartStopCommand{
// hah
// field impl a
HWStartStop a;
};


struct HRSet{
// hah
// field impl a
HWStimulationCommand a;
};


struct HRClear{
// hah
// field impl a
HWClear a;
};


struct HRSettle{
// error, was expecting only one unnamed variable
};


struct HRSetGS{
// hah
// field impl a
HWGlobalStimulationSettings a;
};


struct HRSetGC{
// hah
// field impl a
HWGlobalStimulationCommand a;
};


struct HRSCTrig{
// hah
// field impl a
HWTrigger a;
};


using HardwareRequest = std::variant<HRStartStopCommand, HRSet, HRClear, HRSettle, HRSetGS, HRSetGC, HRSCTrig>;

}  // network
}  // trodes
namespace msgpack {
MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS) {
namespace adaptor {
// Place class template specialization here
template<>
struct convert<trodes::network::HardwareRequest> {
    msgpack::object const& operator()(msgpack::object const& o, trodes::network::HardwareRequest& v) const {
        if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();
        std::string tag_id = o.via.array.ptr[0].as<std::string>();
        if (tag_id != "tag") throw msgpack::type_error();
        std::string tag = o.via.array.ptr[1].as<std::string>();

// here is some code
if (tag == "HRStartStopCommand") {
// handle this case
trodes::network::HWStartStop a = o.via.array.ptr[2].as<trodes::network::HWStartStop>();
v = trodes::network::HRStartStopCommand{a};
} else if (tag == "HRSet") {
// handle this case
trodes::network::HWStimulationCommand a = o.via.array.ptr[2].as<trodes::network::HWStimulationCommand>();
v = trodes::network::HRSet{a};
} else if (tag == "HRClear") {
// handle this case
trodes::network::HWClear a = o.via.array.ptr[2].as<trodes::network::HWClear>();
v = trodes::network::HRClear{a};
} else if (tag == "HRSettle") {
// handle this case
v = trodes::network::HRSettle{};
} else if (tag == "HRSetGS") {
// handle this case
trodes::network::HWGlobalStimulationSettings a = o.via.array.ptr[2].as<trodes::network::HWGlobalStimulationSettings>();
v = trodes::network::HRSetGS{a};
} else if (tag == "HRSetGC") {
// handle this case
trodes::network::HWGlobalStimulationCommand a = o.via.array.ptr[2].as<trodes::network::HWGlobalStimulationCommand>();
v = trodes::network::HRSetGC{a};
} else if (tag == "HRSCTrig") {
// handle this case
trodes::network::HWTrigger a = o.via.array.ptr[2].as<trodes::network::HWTrigger>();
v = trodes::network::HRSCTrig{a};
} else {
    throw msgpack::type_error();
}

        return o;
    }
};
template<>
struct pack<trodes::network::HardwareRequest> {
    template <typename Stream>
    packer<Stream>& operator()(msgpack::packer<Stream>& o, trodes::network::HardwareRequest const& v) const {
        // packing member variables as an array.
auto elems = std::visit([](auto&& arg) {
    using T = std::decay_t<decltype(arg)>;
if constexpr (std::is_same_v<T, trodes::network::HRStartStopCommand>) {
    return 3;
} else if constexpr (std::is_same_v<T, trodes::network::HRSet>) {
    return 3;
} else if constexpr (std::is_same_v<T, trodes::network::HRClear>) {
    return 3;
} else if constexpr (std::is_same_v<T, trodes::network::HRSettle>) {
    return 2;
} else if constexpr (std::is_same_v<T, trodes::network::HRSetGS>) {
    return 3;
} else if constexpr (std::is_same_v<T, trodes::network::HRSetGC>) {
    return 3;
} else if constexpr (std::is_same_v<T, trodes::network::HRSCTrig>) {
    return 3;
} else {
    throw msgpack::type_error();
}
    },
    v
);
        o.pack_array(elems);
        o.pack("tag");
std::string tag = std::visit([](auto&& arg) {
    using T = std::decay_t<decltype(arg)>;
if constexpr (std::is_same_v<T, trodes::network::HRStartStopCommand>) {
    return "HRStartStopCommand";
} else if constexpr (std::is_same_v<T, trodes::network::HRSet>) {
    return "HRSet";
} else if constexpr (std::is_same_v<T, trodes::network::HRClear>) {
    return "HRClear";
} else if constexpr (std::is_same_v<T, trodes::network::HRSettle>) {
    return "HRSettle";
} else if constexpr (std::is_same_v<T, trodes::network::HRSetGS>) {
    return "HRSetGS";
} else if constexpr (std::is_same_v<T, trodes::network::HRSetGC>) {
    return "HRSetGC";
} else if constexpr (std::is_same_v<T, trodes::network::HRSCTrig>) {
    return "HRSCTrig";
} else {
    throw msgpack::type_error();
}
    },
    v
);
        o.pack(tag);
// this is sad
if (tag == "HRStartStopCommand") {
// pack
o.pack(std::get<trodes::network::HRStartStopCommand>(v).a);
} else if (tag == "HRSet") {
// pack
o.pack(std::get<trodes::network::HRSet>(v).a);
} else if (tag == "HRClear") {
// pack
o.pack(std::get<trodes::network::HRClear>(v).a);
} else if (tag == "HRSettle") {
// pack
} else if (tag == "HRSetGS") {
// pack
o.pack(std::get<trodes::network::HRSetGS>(v).a);
} else if (tag == "HRSetGC") {
// pack
o.pack(std::get<trodes::network::HRSetGC>(v).a);
} else if (tag == "HRSCTrig") {
// pack
o.pack(std::get<trodes::network::HRSCTrig>(v).a);
} else {
    throw msgpack::type_error();
}
        return o;
    }
};

} // namespace adaptor
} // MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS)
} // namespace msgpack