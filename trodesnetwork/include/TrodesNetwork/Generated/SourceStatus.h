#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor SourceStatus
struct SourceStatus {
    // field impl message
    std::string message;
    MSGPACK_DEFINE_MAP(message);
};


}  // network
}  // trodes