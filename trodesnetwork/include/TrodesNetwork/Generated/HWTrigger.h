#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor HWTrigger
struct HWTrigger {
    // field impl fn
    uint16_t fn;
    MSGPACK_DEFINE_MAP(fn);
};


}  // network
}  // trodes