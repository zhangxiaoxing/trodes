#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor TrodesNeuralData
struct TrodesNeuralData {
    // field impl localTimestamp
    uint32_t localTimestamp;
    // field impl neuralData
    std::vector< int16_t > neuralData;
    // field impl systemTimestamp
    int64_t systemTimestamp;
    MSGPACK_DEFINE_MAP(localTimestamp, neuralData, systemTimestamp);
};


}  // network
}  // trodes