#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor InfoRequest
struct InfoRequest {
    // field impl request
    std::string request;
    MSGPACK_DEFINE_MAP(request);
};


}  // network
}  // trodes