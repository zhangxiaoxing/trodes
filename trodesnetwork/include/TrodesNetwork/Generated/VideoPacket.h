#pragma once

#include <string>
#include <msgpack.hpp>

// (no additional imports)

namespace trodes {
namespace network {

// constructor VideoPacket
struct VideoPacket {
    // field impl timestamp
    uint32_t timestamp;
    // field impl lineSegment
    int32_t lineSegment;
    // field impl posOnSegment
    double posOnSegment;
    // field impl x
    int16_t x;
    // field impl y
    int16_t y;
    // field impl x2
    int16_t x2;
    // field impl y2
    int16_t y2;
    MSGPACK_DEFINE_MAP(timestamp, lineSegment, posOnSegment, x, y, x2, y2);
};


}  // network
}  // trodes