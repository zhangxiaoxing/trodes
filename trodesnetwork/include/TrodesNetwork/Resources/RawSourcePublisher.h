#pragma once

#include <TrodesNetwork/Util.h>

#include <msgpack.hpp>
#include <mutex>
#include <sstream>
#include <string>
#include <zmq.hpp>

namespace trodes {
namespace network {

template<typename T>
class RawSourcePublisher {
public:
    RawSourcePublisher(std::string endpoint)
    : ctx_(1),
      socket_(ctx_, zmq::socket_type::pub)
    {
        socket_.bind(endpoint.c_str());
    }

    ~RawSourcePublisher() {
    }

    void publish(T data) {
        // just in case this is a shared_ptr
        const std::lock_guard<std::mutex> lock(mutex_);

        zmq::message_t message(util::pack<T>(data));
        socket_.send(message, zmq::send_flags::dontwait);
    }

    std::string last_endpoint() {
        return util::sock_endpoint(&socket_);
    }

private:
    mutable std::mutex mutex_;
    zmq::context_t ctx_;
    zmq::socket_t socket_;
};


}
}