#pragma once

#include <TrodesNetwork/Connection.h>
#include <TrodesNetwork/Resources/RawSinkSubscriber.h>
#include <TrodesNetwork/Util.h>

#include <string>

namespace trodes {
namespace network {

template<typename T>
class SinkSubscriber {
public:
    SinkSubscriber(std::string address, int port, std::string name)
        : name(name),
          sink(util::get_wildcard_string(address))
    {
        Connection c(address, port);
        c.add_endpoint(name, sink.last_endpoint());
    }

    ~SinkSubscriber() {
    }

    T receive() {
        return sink.receive();
    }

private:
    std::string name;
    RawSinkSubscriber<T> sink;
};


}
}