
#include <TrodesNetwork/Connection.h>
#include <TrodesNetwork/Server.h>
#include <TrodesNetwork/Resources/SourceSubscriber.h>

#include <TrodesNetwork/Generated/TrodesAnalogData.h>
#include <TrodesNetwork/Generated/TrodesDigitalData.h>
#include <TrodesNetwork/Generated/TrodesNeuralData.h>
#include <TrodesNetwork/Generated/TrodesTimestampData.h>
#include <TrodesNetwork/Generated/TrodesLFPData.h>
#include <TrodesNetwork/Generated/TrodesSpikeWaveformData.h>

#include <Trodes/TimestampUtil.h>

#include <chrono>
#include <iostream>
#include <sstream>
#include <thread>
#include <vector>

int main(int argc, char** argv) {
    if (argc) {}
    if (argv) {}

    trodes::network::Server server;
    trodes::network::Connection c;

    std::thread listener([]() {
        trodes::network::SourceSubscriber<trodes::network::TrodesTimestampData>
            timestamps{"source.timestamps"};

        std::stringstream result;
        while (true) {
            auto recvd = timestamps.receive();
            result << "T: ";
            result << recvd.localTimestamp << " ";
            result << recvd.systemTimestamp << " ";
            result << "\n";
            std::cout << result.str();
            result.str({});
            result.clear();
        }

    });

    std::thread listener2([]() {
        trodes::network::SourceSubscriber<trodes::network::TrodesNeuralData>
            timestamps{"source.neural"};

        std::stringstream result;
        while (true) {
            auto recvd = timestamps.receive();
            result << "N: ";
            result << recvd.localTimestamp << " ";
            result << recvd.systemTimestamp << " ";
            result << "(length: " << recvd.neuralData.size() << ") ";
            result << "[";
            for (auto amplitude : recvd.neuralData) {
                result << amplitude << ",";
            }
            result << "]\n";
            std::cout << result.str();
            result.str({});
            result.clear();
        }

    });

    std::thread listener3([]() {
        trodes::network::SourceSubscriber<trodes::network::TrodesAnalogData>
            timestamps{"source.analog"};

        std::stringstream result;
        while (true) {
            auto recvd = timestamps.receive();
            result << "A: ";
            result << recvd.localTimestamp << " ";
            result << recvd.systemTimestamp << " ";
            result << "[";
            for (auto i : recvd.analogData) {
                result << "[";
                for (auto j : i) {
                    result << std::to_string(j) << ",";
                }
                result << "]";
                result << ",";
            }
            result << "]\n";
            std::cout << result.str();
            result.str({});
            result.clear();
        }

    });

    std::thread listener4([]() {
        trodes::network::SourceSubscriber<trodes::network::TrodesDigitalData>
            timestamps{"source.digital"};

        std::stringstream result;
        while (true) {
            auto recvd = timestamps.receive();
            result << "D: ";
            result << recvd.localTimestamp << " ";
            result << recvd.systemTimestamp << " ";
            result << "[";
            for (auto i : recvd.digitalData) {
                result << "[";
                for (auto j : i) {
                    result << std::to_string(j) << ",";
                }
                result << "]";
                result << ",";
            }
            result << "]\n";
            std::cout << result.str();
            result.str({});
            result.clear();
        }

    });

    std::thread listener5([]() {
        trodes::network::SourceSubscriber<trodes::network::TrodesLFPData>
            timestamps{"source.lfp"};

        std::stringstream result;
        while (true) {
            auto recvd = timestamps.receive();
            result << "L: ";
            result << recvd.localTimestamp << " ";
            result << recvd.systemTimestamp << " ";
            for (auto i : recvd.lfpData) {
                result << i << ",";
            }
            result << "\n";
            std::cout << result.str();
            result.str({});
            result.clear();
        }

    });

    std::thread listener6([]() {
        trodes::network::SourceSubscriber<trodes::network::TrodesSpikeWaveformData>
            timestamps{"source.spikes"};

        std::stringstream result;
        while (true) {
            auto recvd = timestamps.receive();
            result << "S: ";
            result << recvd.localTimestamp << " ";
            result << recvd.systemTimestamp << " ";
            result << "(" << recvd.nTrodeId << ") [";
            for (auto i : recvd.samples) {
                result << "[";
                for (auto j : i) {
                    result << j << ",";
                }
                result << "]";
                result << ",";
            }
            result << "]\n";
            std::cout << result.str();
            result.str({});
            result.clear();
        }

    });



    listener.join();
    listener2.join();
    listener3.join();
    listener4.join();
    listener5.join();
    listener6.join();

    return 0;
}
