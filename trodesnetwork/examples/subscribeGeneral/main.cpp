
#include <TrodesNetwork/Server.h>
#include <TrodesNetwork/Resources/SourceSubscriber.h>
#include <TrodesNetwork/Generated/AcquisitionCommand.h>

#include <chrono>
#include <iostream>
#include <sstream>
#include <thread>
#include <vector>

int main(int argc, char** argv) {
    if (argc) {}
    if (argv) {}

    trodes::network::Server server;

    std::thread listener([]() {
        trodes::network::SourceSubscriber<trodes::network::AcquisitionCommand>
            timestamps{"trodes.acquisition"};

        std::stringstream result;
        while (true) {
            auto recvd = timestamps.receive();
            result << "A: ";
            result << recvd.command << " ";
            result << recvd.timestamp << " ";
            result << "\n";

            std::cout << result.str();
            result.str({});
            result.clear();
        }

    });

    listener.join();

    return 0;
}
