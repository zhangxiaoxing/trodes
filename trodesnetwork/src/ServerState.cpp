#include <TrodesNetwork/ServerState.h>

#include <atomic>
#include <chrono>
#include <functional>
#include <iostream>
#include <optional>
#include <memory>
#include <thread>

namespace trodes {
namespace network {
    ServerState::ServerState()
    {
    }

    ServerState::~ServerState() {
    }

    void ServerState::add_endpoint(std::string name, std::string endpoint) {
        const std::lock_guard<std::mutex> lock(mutex_);
 
        mappy.insert_or_assign(name, endpoint);
    }

    std::optional<std::string> ServerState::get_endpoint(std::string name) {
        const std::lock_guard<std::mutex> lock(mutex_);

        auto search = mappy.find(name);
        if (search != mappy.end()) {
            return std::optional<std::string>{search->second};
        } else {
            return std::nullopt;
        }
    }

}
}
