set(TRODES_NETWORK_SRC
        ${PROJECT_SOURCE_DIR}/src/Server.cpp
        ${PROJECT_SOURCE_DIR}/src/ServerState.cpp
        ${PROJECT_SOURCE_DIR}/src/Connection.cpp
        ${PROJECT_SOURCE_DIR}/src/Util.cpp

        ${PROJECT_SOURCE_DIR}/src/TimestampUtil.cpp
)

set(TRODES_NETWORK_HDR
        ${PROJECT_SOURCE_DIR}/include/TrodesNetwork/Server.h
)

add_library(trodesnetwork SHARED ${TRODES_NETWORK_SRC} ${TRODES_NETWORK_HDR})

target_link_libraries(trodesnetwork PUBLIC pthread ${PROJECT_SOURCE_DIR}/../Libraries/Linux/libzmq/lib/libzmq.a)
# Making include directories PUBLIC means the include is propagated to dependencies.
target_include_directories(trodesnetwork PUBLIC ${PROJECT_SOURCE_DIR}/include)
target_include_directories(trodesnetwork PUBLIC ${PROJECT_SOURCE_DIR}/../extern/libzmq/include)
target_include_directories(trodesnetwork PUBLIC ${PROJECT_SOURCE_DIR}/../extern/cppzmq)
target_include_directories(trodesnetwork PUBLIC ${PROJECT_SOURCE_DIR}/../extern/msgpack-c/include)

target_compile_features(trodesnetwork PRIVATE cxx_std_17)
# this is only for GNU / Clang, so need generator expression for MSVC
target_compile_options(trodesnetwork PRIVATE -Wall -Wextra -Wconversion -Wsign-conversion)

install(TARGETS trodesnetwork
        DESTINATION ${CMAKE_BINARY_DIR}
        )
