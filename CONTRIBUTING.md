# Contributing

## Building Trodes

### Building with QMake (preferred)

The preferred way to build Trodes is using QMake, a build system provided by
the Qt ecosystem. QMake is used to build for development and deployment on
Mac, Linux, and Windows.

### Building with CMake (experimental)

The experimental way to build Trodes is using CMake. CMake can be used to build
for development, but no deployment method has been standardized with CMake.

When this project eventually migrates to Qt6, it will likely use CMake because
Qt6 projects us CMake as the preferred build system.

This is currently only used for debugging. Only `Trodes` and `cameraModule` are
part of this build system.

#### Cross-platform build commands

CMake is a meta build system, so it can build on different platforms. For 
example, it might generate Makefiles to be used by `make` on Linux but generate
Makefiles to be used by `nmake` on Windows. Using these CMake's cross-platform
commands should allow building on multiple platforms with the same commands.

`cmake -B build`
`cmake --build build --parallel 28 --config Debug`

#### Debugging

Using CMake with the `Debug` configuration should generate the necessary
debugging symbols to use a tool like `gdb`.

##### Obtaining Qt debugging binaries

To properly debug the Trodes code, it is useful to have Qt's debugging binaries
for the library. This can be obtained by using the online installer from Qt's
website.

#### A note about building TrodesNetwork with CMake

There is a `CMakeLists.txt` file in the `trodesnetwork` directory. Its sole
purpose is for building `trodesnetwork` and it does not build the entire
project.

It is not currently being used.

## Setting up development environment

### Ubuntu 20.04

1. Install Git
    - `sudo apt install git`
2. Clone repository
    - `git clone https://bitbucket.org/mkarlsso/trodes.git`
3. Install Qt
    - Offline installer tends to work better.
    - may need `sudo apt install libxkbcommon-x11-0`
4. Install more stuff
    - `sudo apt install build-essential`
    - `sudo apt install mesa-common-dev`
    - `sudo apt install chrpath`
5. Compile everything
    - `cd trodes`
    - `qmake`
    - `make -j8`
    - `make install`
6. Run your thing
    - `./bin/linux/Trodes`
7. Install ZMQ
    - `sudo apt install libzmq3-dev`
8. Install more stuff
    - `sudo apt install libboost-dev`

### Windows with Chocolatey (experimental)

Automated command-line development setups for Windows are still in the works.

1. Install Chocolatey
    - cmake
    - add cmake path because it doesn't do this automatically
    - mingw for compiler tools
2. Building something in CMake

### Windows with Visual Studio 2019 v16.8

Install the latest version of Visual Studio Community, which includes an
integrated development environment and SDKs for compiling with C++ on Windows.

![visual studio community download][doc/images/visual-studio-download.png "Visual Studio Community Download"]

