#ifndef TRODES_ETH_H
#define TRODES_ETH_H
#ifdef __cplusplus
extern "C" {
#endif

#include "trodes_settings.h"

//################################
//  Communication with MCU via ethernet
//################################

typedef struct SpkgEthConn SpkgEthConn;

SpkgEthConn* SpkgEthConn_new();
void SpkgEthConn_delete(SpkgEthConn* h);

/*
Inits and closes socket vars in handle
*/
int eth_init(SpkgEthConn* h);
void eth_close(SpkgEthConn* h);

/*
Sends start/stop acquisition commands over control port
*/
int eth_startacquisition(SpkgEthConn* h, int ecu_connected);
int eth_startsimulation(SpkgEthConn *h, int auxbytes, int intancards);
int eth_stopacquisition(SpkgEthConn* h);

/*
Receiving data over ethernet
*/
//Checks for pending packets, passing in a TIMEOUT value. Returns 1 if packet exists, 0 else.
int eth_pendingpacket(SpkgEthConn* h, unsigned int timeout);

//Reads in packet of expected size, returns bytes read in.
int eth_recvpacket(SpkgEthConn* h, void* buffer, unsigned int expectedlen);

/*
Misc. commands
*/
int eth_sendsettlecommand(SpkgEthConn* h);
int eth_sendsettlechannel(SpkgEthConn* h, uint16_t byte, uint8_t bit, uint16_t delay, uint8_t triggerstate);
int eth_getMCUSettings(SpkgEthConn* h, MCUSettings* settings);
int eth_getHSSettings(SpkgEthConn* h, HSSettings* settings);


#ifdef __cplusplus
}
#endif
#endif