#include <stdlib.h>
#include <string.h>
#include <stdio.h>
//#############################
//  Cross-platform socket definitions
//#############################

#ifdef _WIN32
    #include <winsock2.h>
    #include <winsock.h>
    #include <ws2tcpip.h>
    #include <ws2ipdef.h>
#else
    #include <sys/socket.h>
    #include <arpa/inet.h>
    #include <netdb.h>
    #include <unistd.h>
    #include <errno.h>
    #include <sys/epoll.h>
    #define SOCKET_ERROR -1
    #define INVALID_SOCKET -1
    typedef int SOCKET;
#endif

int GetErrno(){
#ifdef _WIN32
    return WSAGetLastError();
#else
    return errno;
#endif
}

int socketinit(){
#ifdef _WIN32
    WSADATA wsa_data;
    return WSAStartup(0x202, &wsa_data);
#else
    return 0;
#endif
}

int socketquit(){
#ifdef _WIN32
    return WSACleanup();
#else
    return 0;
#endif
}

int socketclose(SOCKET sock)
{
    int status = 0;
#ifdef _WIN32
    status = shutdown(sock, SD_BOTH);   
    if (status == 0) { status = closesocket(sock); }
#else
    status = shutdown(sock, SHUT_RDWR);
    if (status == 0) { status = close(sock); }
#endif
    return status;
}

#ifdef _WIN32
typedef WSAEVENT Event_fd;
#else
typedef int Event_fd;
#endif

/*Waits on a socket to get a packet
 * returns 1 if packet exists, 0 otherwise
 */ 
int socketwait(SOCKET s, Event_fd event, unsigned int timeout){
#ifdef _WIN32
    DWORD rc;
    rc = WSAWaitForMultipleEvents(1, &event, FALSE, timeout, FALSE);
    if(rc == WSA_WAIT_TIMEOUT){
        //No packet available
        return 0;
    }
    else if(rc == WSA_WAIT_FAILED){
        //Error while waiting
        return -1;
    }
    else{
        WSANETWORKEVENTS events;
        WSAEnumNetworkEvents(s, event, &events);
        if(events.lNetworkEvents & FD_READ){
            //A packet exists. No way to efficiently detect pending packet's size, so just return 
            //expected size. One alternative is to actually recv the packet and store it as an
            //intermediate buffer, but not sure if that is also slow or not.
            return 1;
        }
        else{
            return 0;
        }
    }
#else
    struct epoll_event ev;
    epoll_wait(event, &ev, 1, timeout);
    if(ev.data.fd == s && ev.events & EPOLLIN){
        return 1;
    }
    // else if(nfds == -1){
    //     return -1;
    // }
    // else if(nfds == 0){
    //     return 0;
    // }
    else{
        return 0;
    }
#endif
}

#include "trodes_eth.h"
#include "common.h"
//#############################
//  Eth
//#############################
struct SpkgEthConn{
    //Ethernet
    SOCKET ctrl_s;
    SOCKET data_s;
    int ctrlport;
    int dataport;
    // size_t packetsize;
    struct sockaddr_in broadcast_addr;
    Event_fd event_fd;
};

SpkgEthConn* SpkgEthConn_new(){
    SpkgEthConn *h = (SpkgEthConn*)malloc(sizeof(SpkgEthConn));
    memset(h, 0, sizeof(*h));
    return h;
}

void SpkgEthConn_delete(SpkgEthConn* h){
    free(h);
}

int eth_init(SpkgEthConn* h){
    if(socketinit() != 0){
        return GetErrno();
    }

// Ctrl socket
#ifdef _WIN32
    h->ctrl_s = socket(AF_INET, SOCK_DGRAM, 0);
#else
    h->ctrl_s = socket(AF_INET6, SOCK_DGRAM, 0);
#endif
    h->ctrlport = SPKG_MCU_CTRL_PORT;
    if(h->ctrl_s == INVALID_SOCKET){
        socketquit();
        free(h);
        return GetErrno();
    }
    int opt = 1;
    if(setsockopt(h->ctrl_s, SOL_SOCKET, SO_BROADCAST, (char*)&opt, sizeof(opt)) == SOCKET_ERROR){
        return GetErrno();
    }

#ifndef _WIN32
    struct sockaddr_in6 local6 = {AF_INET6, htons(0)};
    inet_pton(AF_INET6, "::", &local6.sin6_addr);
    local6.sin6_scope_id = 0;
    if (bind(h->ctrl_s, (struct sockaddr*)&local6, sizeof(local6)) == SOCKET_ERROR){
        return GetErrno();
    }
#endif
    
// Data socket
    h->data_s = socket(AF_INET, SOCK_DGRAM, 0);
    h->dataport = SPKG_MCU_DATA_PORT;
    if(h->data_s == INVALID_SOCKET){
        return GetErrno();
    }
    struct sockaddr_in local;
    local.sin_family = AF_INET;
    local.sin_addr.s_addr = INADDR_ANY;
    local.sin_port = htons(h->dataport);
    if (bind(h->data_s, (struct sockaddr*)&local, sizeof(local)) == SOCKET_ERROR){
        return GetErrno();
    }
#ifdef _WIN32
    h->event_fd = WSACreateEvent();
    WSAEventSelect(h->data_s, h->event_fd, FD_READ);
#else
    if((h->event_fd = epoll_create(1))== SOCKET_ERROR){
        return GetErrno();
    }
    struct epoll_event ev;
    ev.events = EPOLLIN;
    ev.data.fd = h->data_s;
    if(epoll_ctl(h->event_fd, EPOLL_CTL_ADD, h->data_s, &ev) == SOCKET_ERROR){
        return GetErrno();
    }
#endif

// Broadcast address object for sending commands
    memset(&h->broadcast_addr,0, sizeof(h->broadcast_addr));
    h->broadcast_addr.sin_family = AF_INET;
    h->broadcast_addr.sin_port = htons(h->ctrlport);
#ifdef _WIN32
    h->broadcast_addr.sin_addr.s_addr = INADDR_BROADCAST;
#else
    inet_pton(AF_INET, "192.168.0.255", &h->broadcast_addr.sin_addr);
#endif

// Initialization steps
    int rc = 0;
    if((rc = eth_stopacquisition(h))){
        return rc;
    }
    if((rc = eth_getMCUSettings(h, NULL))){
        return rc;
    }
    return 0;
}

void eth_close(SpkgEthConn* h){
    socketclose(h->ctrl_s);
    socketquit();
    free(h);
}

int eth_startacquisition(SpkgEthConn* h, int ecu_connected){
    char buf[20] = {0};
    if(ecu_connected)
        buf[0] = command_startWithECU;
    else
        buf[0] = command_startNoECU;
    buf[1] = 0xff;
    if (sendto(h->ctrl_s, buf, 2, 0, (struct sockaddr*)&h->broadcast_addr, sizeof(h->broadcast_addr)) == SOCKET_ERROR){
        return GetErrno();
    }

    return 0;
}

int eth_startsimulation(SpkgEthConn *h, int auxbytes, int intancards){
    char buf[20] = {0};
    buf[0] = command_startSimulation;
    buf[1] = auxbytes;
    buf[2] = intancards;
    buf[3] = 0xff;
    if (sendto(h->ctrl_s, buf, 4, 0, (struct sockaddr*)&h->broadcast_addr, sizeof(h->broadcast_addr)) == SOCKET_ERROR){
        return GetErrno();
    }

    return 0;
}

int eth_stopacquisition(SpkgEthConn* h){
    char buf[20] = {0};
    buf[0] = command_stop;
    buf[1] = 0xff;

    if (sendto(h->ctrl_s, buf, 2, 0, (struct sockaddr*)&h->broadcast_addr, sizeof(h->broadcast_addr)) == SOCKET_ERROR){
        return GetErrno();
    }

    //Expecting an ack
    if(eth_pendingpacket(h, 1000)){
        if (recv(h->ctrl_s, buf, 1, 0) == SOCKET_ERROR){
            return GetErrno();
        }
        return 0;
    }
    else{
        return -1;
    }
}

int eth_pendingpacket(SpkgEthConn* h, unsigned int timeout){
    return socketwait(h->data_s, h->event_fd, timeout);
}

int eth_recvpacket(SpkgEthConn* h, void* buffer, unsigned int expectedlen){
    int r;
    if((r =recv(h->data_s, (char*)buffer, expectedlen, 0)) == -1) return -GetErrno();
    else return r;
    // return recv(h->data_s, buffer, expectedlen, 0);
}

int eth_getMCUSettings(SpkgEthConn* h, MCUSettings* settings){
    char buffer[MCUSETTINGSSIZE] = {0};
    buffer[0] = command_getControllerSettings;
    if(sendto(h->ctrl_s, buffer, 1, 0, (struct sockaddr*)&h->broadcast_addr, sizeof(h->broadcast_addr))==SOCKET_ERROR){
      return GetErrno();
    }

    if(!eth_pendingpacket(h, 1000)){
        return -1;
    }

    int rcvlen = 0;
    buffer[0] = 0;
    if((rcvlen = recv(h->ctrl_s, buffer, MCUSETTINGSSIZE, 0)) == SOCKET_ERROR){
        return GetErrno();
    }
    if(rcvlen != MCUSETTINGSSIZE){
        return -1;
    }
    if(settings == NULL){
        return 0;
    }

    parseMCUSettings(buffer, settings);
    return 0;
}

int eth_getHSSettings(SpkgEthConn* h, HSSettings* settings){
    char buffer[HSSETTINGSSIZE] = {0};
    buffer[0] = command_getHeadstageSettings;
    sendto(h->ctrl_s, buffer, 1, 0, (struct sockaddr*)&h->broadcast_addr, sizeof(h->broadcast_addr));
    int rcvlen = 0;
    buffer[0] = 0;
    if((rcvlen = recv(h->ctrl_s, buffer, HSSETTINGSSIZE, 0)) == SOCKET_ERROR){
        return GetErrno();
    }
    if(rcvlen != HSSETTINGSSIZE){
        return -1;
    }
    if(settings == NULL){
        return 0;
    }

    
    parseHSSettings(buffer, settings);
    return 0;
}

