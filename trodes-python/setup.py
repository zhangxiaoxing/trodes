from setuptools import setup

setup(
    name="trodes",
    version="0.0.6",
    description="A library providing high-level access to TrodesNetwork",
    packages=["trodes"],
    install_requires=[
        'trodesnetwork',
        'PyQt6'
    ]
)

