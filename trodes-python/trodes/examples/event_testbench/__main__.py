import sys
import PyQt6.QtCore as QtCore
import PyQt6.QtWidgets as QtWidgets

import trodesnetwork.trodes as trodes
from trodes.examples.general_networked_application import networked_main

class EventTestbench(QtCore.QObject):
    def __init__(self, server_address, parent=None):
        super().__init__(parent=parent)

        self.server = trodes.EventServer(server_address=server_address)

        self.sender = EventSender(server_address, parent=parent)
        self.receiver = EventReceiver(server_address, parent=parent)
    
    def show(self):
        self.sender.show()
        self.receiver.show()

class EventSender(QtWidgets.QDialog):
    def __init__(self, server_address, parent=None):
        super().__init__(parent=parent)

        self.setWindowTitle("Event Sender Testbench")

        layout = QtWidgets.QVBoxLayout()
        self.setLayout(layout)

        self.button1 = QtWidgets.QPushButton("Send event 1")
        layout.addWidget(self.button1)
        self.button1.clicked.connect(self.event1)

        self.button2 = QtWidgets.QPushButton("Send event 2")
        layout.addWidget(self.button2)
        self.button2.clicked.connect(self.event2)
        
        self.sender1 = trodes.EventSender(server_address=server_address, event_name='event1')
        self.sender2 = trodes.EventSender(server_address=server_address, event_name='event2')

    def event1(self):
        print('event1 sent')
        self.sender1.publish('', '')

    def event2(self):
        print('event2 sent')
        self.sender2.publish('', '')

class EventReceiver(QtWidgets.QDialog):
    def __init__(self, server_address, parent=None):
        super().__init__(parent=parent)

        self.setWindowTitle("Event Receiver Testbench")

        layout = QtWidgets.QVBoxLayout()
        self.setLayout(layout)

        self.counter = 0

        self.label = QtWidgets.QLabel(str(self.counter))
        layout.addWidget(self.label)

        self.events_subscriber = trodes.EventSubscriber(
            server_address=server_address,
            event_name="event2",
            callback=self.receive_data)

    def receive_data(self, data):
        print('Received event.')
        self.counter += 1
        self.label.setText(str(self.counter))

if __name__ == '__main__':
    print("Event Testbench")
    networked_main(EventTestbench)