'''
Purpose of this module is to give a real-time readout of the analog
channels from trodes network.
'''

import sys
import PyQt6.QtCore as QtCore
import PyQt6.QtWidgets as QtWidgets
import pyqtgraph as pg

import trodes.examples.buffers as buffers

import json

import trodesnetwork.trodes as trodes
from trodes.examples.general_networked_application import networked_main
import trodes.examples.testbench_digital.config as config

class AnalogTestbench(QtWidgets.QDialog):
    def __init__(self, server_address, parent=None):
        super().__init__(parent=parent)
        self.text = ""

        self.setWindowTitle("Analog Testbench")

        layout = QtWidgets.QVBoxLayout()
        self.setLayout(layout)

        self.label = QtWidgets.QLabel()
        layout.addWidget(self.label)

        # graphing
        self.graphWidget = pg.PlotWidget()
        layout.addWidget(self.graphWidget)
        self.plot_data = buffers.PointerBufferedData(512)

        self.analog_subscriber = trodes.AnalogSubscriber(
            server_address=server_address,
            channel_map=config.channel_map,
            channel_name="Headstage_AccelX",
            callback=self.receive_data)

        # we have to update on a timer because updating QtWidgets too fast
        # won't work
        self.label_timer = QtCore.QTimer(parent=self)
        self.label_timer.timeout.connect(self.update_gui)
        self.label_timer.start(20)

    def receive_data(self, data):
        ts, a = data
        self.text = str(a)
        self.plot_data.add_point(a)

    def update_gui(self):
        self.label.setText(self.text)
        self.graphWidget.plot(self.plot_data.data, clear=True)

if __name__ == '__main__':
    print("Analog Testbench")
    networked_main(AnalogTestbench)