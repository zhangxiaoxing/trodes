import PyQt6.QtWidgets as QtWidgets

import trodesnetwork.trodes as trodes
from trodes.examples.general_networked_application import networked_main

class PlaybackSeeker(QtWidgets.QDialog):
    def __init__(self, network_address, parent=None):
        super().__init__(parent=parent)

        self.network_address = network_address
        self.acquisition_consumer = trodes.AcquisitionClient(self.network_address)
        self.acquisition_subscriber = trodes.AcquisitionSubscriber(self.network_address, self.update_time)

        self.setWindowTitle("Playback Seeker")

        layout = QtWidgets.QVBoxLayout()
        self.setLayout(layout)

        self.time = QtWidgets.QLabel("--:--")
        layout.addWidget(self.time)

        flo = QtWidgets.QFormLayout()
        layout.addLayout(flo)

        self.timestamp = QtWidgets.QLineEdit()
        self.timestamp.setPlaceholderText("8329281")
        flo.addRow("Timestamp", self.timestamp)

        self.seconds = QtWidgets.QLineEdit()
        self.seconds.setPlaceholderText("39238.2")
        flo.addRow("Seconds", self.seconds)

        self.seconds.textEdited.connect(lambda: self.timestamp.setText(""))

        self.playButton = QtWidgets.QPushButton("Play")
        layout.addWidget(self.playButton)
        self.playButton.clicked.connect(self.acquisition_consumer.play)

        self.pauseButton = QtWidgets.QPushButton("Pause")
        layout.addWidget(self.pauseButton)
        self.pauseButton.clicked.connect(self.acquisition_consumer.pause)

        self.seekButton = QtWidgets.QPushButton("Seek")
        layout.addWidget(self.seekButton)
        self.seekButton.clicked.connect(self.seek)

    def update_time(self, timestamp):
        self.timestamp.setText(str(timestamp))
        self.seconds.setText('{:.1f}'.format(timestamp/30000))

        raw_seconds = timestamp / 30000
        hours, minutes, seconds = int(raw_seconds // (60*60)), int(raw_seconds // 60), int(raw_seconds % 60)
        self.time.setText('{:02d}:{:02d}:{:02d}'.format(hours, minutes, seconds))

    def seek(self):
        timestampTxt = self.timestamp.text()
        secondsTxt = self.seconds.text()

        if timestampTxt != "":
            timestamp = int(timestampTxt)
            self.acquisition_consumer.seek(timestamp)
            return

        if secondsTxt != "":
            timestamp = int(float(secondsTxt)*30000)
            self.acquisition_consumer.seek(timestamp)
            return

if __name__ == "__main__":
    print("Playback seeker.")
    networked_main(PlaybackSeeker)
