# channel_map = {
#     'devices': [
#         {
#             'numBytes': 1,
#             'name': 'Controller_DIO',
#             'channels': [
#                 {
#                     'id': 'Controller_Din1'
#                 }
#             ]
#         }
#     ]
# }

channel_map = \
    {
        "HardwareConfiguration": {
            "Device": [
                {
                    "Channel": [
                    {
                        "_startByte": "0",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "0",
                        "_id": "Controller_Din1"
                    },
                    {
                        "_startByte": "0",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "1",
                        "_id": "Controller_Din2"
                    },
                    {
                        "_startByte": "0",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "2",
                        "_id": "Controller_Din3"
                    },
                    {
                        "_startByte": "0",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "3",
                        "_id": "Controller_Din4"
                    },
                    {
                        "_startByte": "0",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "4",
                        "_id": "Controller_Din5"
                    },
                    {
                        "_startByte": "0",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "5",
                        "_id": "Controller_Din6"
                    },
                    {
                        "_startByte": "0",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "6",
                        "_id": "Controller_Din7"
                    },
                    {
                        "_startByte": "0",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "7",
                        "_id": "Controller_Din8"
                    }
                    ],
                    "_name": "Controller_DIO",
                    "_numBytes": "1",
                    "_packetOrderPreference": "10",
                    "_available": "1"
                },
                {
                    "Channel": [
                    {
                        "_startByte": "0",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "0",
                        "_id": "ECU_Din1"
                    },
                    {
                        "_startByte": "0",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "1",
                        "_id": "ECU_Din2"
                    },
                    {
                        "_startByte": "0",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "2",
                        "_id": "ECU_Din3"
                    },
                    {
                        "_startByte": "0",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "3",
                        "_id": "ECU_Din4"
                    },
                    {
                        "_startByte": "0",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "4",
                        "_id": "ECU_Din5"
                    },
                    {
                        "_startByte": "0",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "5",
                        "_id": "ECU_Din6"
                    },
                    {
                        "_startByte": "0",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "6",
                        "_id": "ECU_Din7"
                    },
                    {
                        "_startByte": "0",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "7",
                        "_id": "ECU_Din8"
                    },
                    {
                        "_startByte": "1",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "0",
                        "_id": "ECU_Din9"
                    },
                    {
                        "_startByte": "1",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "1",
                        "_id": "ECU_Din10"
                    },
                    {
                        "_startByte": "1",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "2",
                        "_id": "ECU_Din11"
                    },
                    {
                        "_startByte": "1",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "3",
                        "_id": "ECU_Din12"
                    },
                    {
                        "_startByte": "1",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "4",
                        "_id": "ECU_Din13"
                    },
                    {
                        "_startByte": "1",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "5",
                        "_id": "ECU_Din14"
                    },
                    {
                        "_startByte": "1",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "6",
                        "_id": "ECU_Din15"
                    },
                    {
                        "_startByte": "1",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "7",
                        "_id": "ECU_Din16"
                    },
                    {
                        "_startByte": "2",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "0",
                        "_id": "ECU_Din17"
                    },
                    {
                        "_startByte": "2",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "1",
                        "_id": "ECU_Din18"
                    },
                    {
                        "_startByte": "2",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "2",
                        "_id": "ECU_Din19"
                    },
                    {
                        "_startByte": "2",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "3",
                        "_id": "ECU_Din20"
                    },
                    {
                        "_startByte": "2",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "4",
                        "_id": "ECU_Din21"
                    },
                    {
                        "_startByte": "2",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "5",
                        "_id": "ECU_Din22"
                    },
                    {
                        "_startByte": "2",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "6",
                        "_id": "ECU_Din23"
                    },
                    {
                        "_startByte": "2",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "7",
                        "_id": "ECU_Din24"
                    },
                    {
                        "_startByte": "3",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "0",
                        "_id": "ECU_Din25"
                    },
                    {
                        "_startByte": "3",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "1",
                        "_id": "ECU_Din26"
                    },
                    {
                        "_startByte": "3",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "2",
                        "_id": "ECU_Din27"
                    },
                    {
                        "_startByte": "3",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "3",
                        "_id": "ECU_Din28"
                    },
                    {
                        "_startByte": "3",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "4",
                        "_id": "ECU_Din29"
                    },
                    {
                        "_startByte": "3",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "5",
                        "_id": "ECU_Din30"
                    },
                    {
                        "_startByte": "3",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "6",
                        "_id": "ECU_Din31"
                    },
                    {
                        "_startByte": "3",
                        "_input": "1",
                        "_dataType": "digital",
                        "_bit": "7",
                        "_id": "ECU_Din32"
                    },
                    {
                        "_startByte": "4",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "0",
                        "_id": "ECU_Dout1"
                    },
                    {
                        "_startByte": "4",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "1",
                        "_id": "ECU_Dout2"
                    },
                    {
                        "_startByte": "4",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "2",
                        "_id": "ECU_Dout3"
                    },
                    {
                        "_startByte": "4",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "3",
                        "_id": "ECU_Dout4"
                    },
                    {
                        "_startByte": "4",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "4",
                        "_id": "ECU_Dout5"
                    },
                    {
                        "_startByte": "4",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "5",
                        "_id": "ECU_Dout6"
                    },
                    {
                        "_startByte": "4",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "6",
                        "_id": "ECU_Dout7"
                    },
                    {
                        "_startByte": "4",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "7",
                        "_id": "ECU_Dout8"
                    },
                    {
                        "_startByte": "5",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "0",
                        "_id": "ECU_Dout9"
                    },
                    {
                        "_startByte": "5",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "1",
                        "_id": "ECU_Dout10"
                    },
                    {
                        "_startByte": "5",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "2",
                        "_id": "ECU_Dout11"
                    },
                    {
                        "_startByte": "5",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "3",
                        "_id": "ECU_Dout12"
                    },
                    {
                        "_startByte": "5",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "4",
                        "_id": "ECU_Dout13"
                    },
                    {
                        "_startByte": "5",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "5",
                        "_id": "ECU_Dout14"
                    },
                    {
                        "_startByte": "5",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "6",
                        "_id": "ECU_Dout15"
                    },
                    {
                        "_startByte": "5",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "7",
                        "_id": "ECU_Dout16"
                    },
                    {
                        "_startByte": "6",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "0",
                        "_id": "ECU_Dout17"
                    },
                    {
                        "_startByte": "6",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "1",
                        "_id": "ECU_Dout18"
                    },
                    {
                        "_startByte": "6",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "2",
                        "_id": "ECU_Dout19"
                    },
                    {
                        "_startByte": "6",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "3",
                        "_id": "ECU_Dout20"
                    },
                    {
                        "_startByte": "6",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "4",
                        "_id": "ECU_Dout21"
                    },
                    {
                        "_startByte": "6",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "5",
                        "_id": "ECU_Dout22"
                    },
                    {
                        "_startByte": "6",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "6",
                        "_id": "ECU_Dout23"
                    },
                    {
                        "_startByte": "6",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "7",
                        "_id": "ECU_Dout24"
                    },
                    {
                        "_startByte": "6",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "0",
                        "_id": "ECU_Dout25"
                    },
                    {
                        "_startByte": "7",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "1",
                        "_id": "ECU_Dout26"
                    },
                    {
                        "_startByte": "7",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "2",
                        "_id": "ECU_Dout27"
                    },
                    {
                        "_startByte": "7",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "3",
                        "_id": "ECU_Dout28"
                    },
                    {
                        "_startByte": "7",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "4",
                        "_id": "ECU_Dout29"
                    },
                    {
                        "_startByte": "7",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "5",
                        "_id": "ECU_Dout30"
                    },
                    {
                        "_startByte": "7",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "6",
                        "_id": "ECU_Dout31"
                    },
                    {
                        "_startByte": "7",
                        "_input": "0",
                        "_dataType": "digital",
                        "_bit": "7",
                        "_id": "ECU_Dout32"
                    },
                    {
                        "_startByte": "8",
                        "_input": "1",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "ECU_Ain1"
                    },
                    {
                        "_startByte": "10",
                        "_input": "1",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "ECU_Ain2"
                    },
                    {
                        "_startByte": "12",
                        "_input": "1",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "ECU_Ain3"
                    },
                    {
                        "_startByte": "14",
                        "_input": "1",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "ECU_Ain4"
                    },
                    {
                        "_startByte": "16",
                        "_input": "1",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "ECU_Ain5"
                    },
                    {
                        "_startByte": "18",
                        "_input": "1",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "ECU_Ain6"
                    },
                    {
                        "_startByte": "20",
                        "_input": "1",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "ECU_Ain7"
                    },
                    {
                        "_startByte": "22",
                        "_input": "1",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "ECU_Ain8"
                    },
                    {
                        "_startByte": "24",
                        "_input": "1",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "ECU_Aout1"
                    },
                    {
                        "_startByte": "26",
                        "_input": "1",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "ECU_Aout2"
                    },
                    {
                        "_startByte": "28",
                        "_input": "1",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "ECU_Aout3"
                    },
                    {
                        "_startByte": "30",
                        "_input": "1",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "ECU_Aout4"
                    }
                    ],
                    "_name": "ECU",
                    "_numBytes": "32",
                    "_packetOrderPreference": "20",
                    "_available": "1"
                },
                {
                    "Channel": [
                    {
                        "_startByte": "2",
                        "_input": "1",
                        "_interleavedDataIDByte": "0",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "Headstage_AccelX",
                        "_interleavedDataIDBit": "3"
                    },
                    {
                        "_startByte": "4",
                        "_input": "1",
                        "_interleavedDataIDByte": "0",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "Headstage_AccelY",
                        "_interleavedDataIDBit": "3"
                    },
                    {
                        "_startByte": "6",
                        "_input": "1",
                        "_interleavedDataIDByte": "0",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "Headstage_AccelZ",
                        "_interleavedDataIDBit": "3"
                    },
                    {
                        "_startByte": "2",
                        "_input": "1",
                        "_interleavedDataIDByte": "0",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "Headstage_GyroX",
                        "_interleavedDataIDBit": "2"
                    },
                    {
                        "_startByte": "4",
                        "_input": "1",
                        "_interleavedDataIDByte": "0",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "Headstage_GyroY",
                        "_interleavedDataIDBit": "2"
                    },
                    {
                        "_startByte": "6",
                        "_input": "1",
                        "_interleavedDataIDByte": "0",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "Headstage_GyroZ",
                        "_interleavedDataIDBit": "2"
                    },
                    {
                        "_startByte": "2",
                        "_input": "1",
                        "_interleavedDataIDByte": "0",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "Headstage_MagX",
                        "_interleavedDataIDBit": "1"
                    },
                    {
                        "_startByte": "4",
                        "_input": "1",
                        "_interleavedDataIDByte": "0",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "Headstage_MagY",
                        "_interleavedDataIDBit": "1"
                    },
                    {
                        "_startByte": "6",
                        "_input": "1",
                        "_interleavedDataIDByte": "0",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "Headstage_MagZ",
                        "_interleavedDataIDBit": "1"
                    },
                    {
                        "_startByte": "2",
                        "_input": "1",
                        "_interleavedDataIDByte": "0",
                        "_dataType": "uint32",
                        "_bit": "0",
                        "_id": "RFsync",
                        "_interleavedDataIDBit": "0"
                    },
                    {
                        "_startByte": "2",
                        "_input": "1",
                        "_interleavedDataIDByte": "0",
                        "_dataType": "uint32",
                        "_bit": "0",
                        "_id": "Headstage_Ack1",
                        "_interleavedDataIDBit": "4"
                    },
                    {
                        "_startByte": "4",
                        "_input": "1",
                        "_interleavedDataIDByte": "0",
                        "_dataType": "uint32",
                        "_bit": "0",
                        "_id": "Headstage_Ack2",
                        "_interleavedDataIDBit": "4"
                    },
                    {
                        "_startByte": "6",
                        "_input": "1",
                        "_interleavedDataIDByte": "0",
                        "_dataType": "uint32",
                        "_bit": "0",
                        "_id": "Headstage_Ack3",
                        "_interleavedDataIDBit": "4"
                    },
                    {
                        "_startByte": "2",
                        "_input": "1",
                        "_interleavedDataIDByte": "0",
                        "_dataType": "analog",
                        "_bit": "0",
                        "_id": "Controller_Ain1",
                        "_interleavedDataIDBit": "5"
                    }
                    ],
                    "_name": "Multiplexed",
                    "_numBytes": "8",
                    "_packetOrderPreference": "30",
                    "_available": "1"
                }
            ],
            "_samplingRate": "30000",
            "_numChannels": "256"
        }
    }