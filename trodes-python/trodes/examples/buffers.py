import abc

import numpy as np

class BufferedDataInterface(abc.ABC):
    @abc.abstractclassmethod
    def add_point(self, x):
        pass

class RotatingBufferedData(BufferedDataInterface):
    def __init__(self, size):
        self.data = np.zeros(size)
    
    def add_point(self, x):
        self.data[:] = np.roll(self.data, -1)
        self.data[-1] = x

class PointerBufferedData(BufferedDataInterface):
    def __init__(self, size):
        self.size = size
        self.index = 0
        self.data = np.zeros(size)
    
    def add_point(self, x):
        self.data[self.index] = x
        self.index += 1
        self.index %= self.size

class PointerBufferedDataND(BufferedDataInterface):
    def __init__(self, width, size):
        self.size = size
        self.index = 0
        self.data = np.zeros((size, width))
    
    def add_point(self, x):
        self.data[self.index, :] = x
        self.index += 1
        self.index %= self.size